<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cuenta de Cobro</title>
</head>
<body>
    <!-- Estilos CSS -->
    <style>
    body{
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-family: 11px;
        margin: 10px;
    }
    .margen{
        text-align: center;
    }
    .pequeno{
        font-size: 15px;
    }

    .inicio{
        font-size: 15px;
    }

    hr{
        width: 300px;
        position: absolute;
        left: 0;
    }
    </style>
    <!-- Fin de los estilos CSS -->

  <p> <b class="inicio">{{$ciudad}},{{$fecha}}</b></p> 

  <p class="margen"><b>Cuenta de Cobro</b> </p> 

  <p class="margen">SOFTWARE QUALITY ASSURANCE S.A.</p> 

  <p class="margen">NIT: 811.042.907-7</p> <br>

  <p class="margen">DEBE A:</p> 
  
  <p class="margen">{{$datos["nombre"]}}</p> 
  <p class="margen">CC.{{$datos["cedula"]}}</p> 
  <p class="margen">Responsable del IVA-régimen simplicado</p> <br> 

   
  <p class="margen">LA SUMA DE: {{$letras}}  COP${{$datos["valor"]}}</p> <br>

  <p> <b class="margen">Por concepto de {{$motivo}}</p></b> <br>

  <p>Favor consignar estos recursos en la cuenta {{$datos["tipo_cuenta"]}} NO. {{$datos["numero_cuenta"]}} del Banco {{$datos["banco"]}}</p> <br> 
    
   <p class="margen pequeno">Solicito no practicar retención de la fuente si la misma no excede el tope mínimo de la tabla de retención del artículo 383 del estatuto tributario, y declaro no tener vinculados o contratados 2 o más trabajadores asociados a mi actividad.</p>
<br> <br> <br>
<p>_______________________________________ </p>
<p class="pequeno">Firma de {{$datos["nombre"]}}</p>
<p class="pequeno">CC. {{$datos["cedula"]}}</p>
<p class="pequeno">Email {{$datos["correo"]}}</p>
<p class="pequeno">Dirección {{$datos["direccion"]}}</p>
<p class="pequeno">Teléfono {{$datos["telefono"]}}</p>
</body>
</html>