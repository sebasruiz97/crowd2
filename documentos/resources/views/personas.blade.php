<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Correo</title>
</head>
<body>
    <!-- Estilos CSS -->
<style>

</style>
    

<p>Hola {{$msg["nombre"]}}</p>
            <p>Gracias por participar de nuestros retos de {{$mes_reto}} <br /><br /></p>
            <p><strong>{{$msg["nombre"]}} te recompensaremos COP${{number_format($msg["valor"])}}</strong></p>
            <p><br />Adjunto encontrar&aacute;s el formato de cuenta de cobro por el valor total de {{$motivo_cc}} durante los retos en los que participaste<br /><br/></p>
            <p>Debes firmarla y devolvera escaneada en este mismo correo. Los datos de tu cuenta bancaria, los tomamos de la informaci&oacute;n financiera que tienes registrada en tu perfil, si no son correctos, debes corregir tu perfil e informarnos para volver a generar tu cuenta de cobro <br/><br/></p>
            <p><strong>NOTA: El pago ser&aacute; realizado {{$fecha_pago}}, siempre y cuando nos entregues esta cuenta de cobro y tengas en tu perfil la documentaci&oacute;n obligatoria {{$fecha_max}} </strong><br>
            <br>La documentaci&oacute;n obligatoria que debe estar cargada en la secci&oacute;n de Anexos es:<br>
                 - Copia de tu documento de identificaci&oacute;n.<br>
                 - RUT (Debe tener año de expedici&oacute;n 2021, por la plataforma de la DIAN https://https://muisca.dian.gov.co/ puedes actualizarlo online y descargarlo, si tienes problemas, asegurate que tienes activada la firma digital).<br>
                 - Certificaci&oacute;n bancaria (debe corresponder a la misma cuenta que ingresaste en informaci&oacute;n financiera y estar a tu nombre).<br>
                 - Aceptaci&oacute;n de tratamiento de datos (diligenciado y firmado).  El link de descarga lo encuentras en el cuadro azul de la secci&oacute;n anexos en tu perfil.<br>
                 - Aceptaci&oacute;n de t&eacute;rminos y condiciones (diligenciado y firmado). El link de descarga lo encuentras en el cuadro azul de la secci&oacute;n anexos en tu perfil.<br>
                 - Si actualmente trabajas para SQA, debes solicitar a este correo la Adenda de trabajadores y cargarla tambi&eacute;n en tu perfil.<br><br>
            <br> El &uacute;nico documento que debes enviarnos por correo es la cuenta de cobro, los dem&aacute;s deben ser cargados directamente en la plataforma.</p>
            
            <p><strong>- TIP -</strong> No es necesario que imprimas la cuenta de cobro, si la descargas en tu computados y luego la abres en Acrobat Reader, en el men&uacute; de herramientas, la secci&oacute;n de certificados le das abrir, te da la opci&oacute;n para firmar digitalmente.</p>
            <p>Si hay alguna inconsistencia en la cuenta de cobro como por ejemplo no aparece tu apellido o no hay una cuenta bancaria, es porque así están tus datos en nuestra plataforma.  Para solucionarlo debes entrar a tu perfil y ajustarlo, 
			luego envíanos un correo a info@crowdsqa.co solicitando que volvamos a generar tu cuenta de cobro.</p>
			<p><br/><strong>CROWDSQA</strong><br>Otra forma de probar software</p>


</body>
</html>