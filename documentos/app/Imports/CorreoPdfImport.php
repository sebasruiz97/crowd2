<?php

namespace App\Imports;

use App\Models\Usuario;
use Maatwebsite\Excel\Concerns\ToModel;

use App\Mail\PdfMail;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMappedCells;

class CorreoPdfImport implements ToModel
{



    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
{

    $columnas = [
     
    "nombre" => $row[0],
    "correo" => $row[1],
    "cedula" => $row[3],
    "banco" => $row[4],
    "tipo_cuenta" => $row[5],
    "numero_cuenta" => $row[6],
    "valor" => $row[2],
    "direccion" => $row[8],
    "telefono" => $row[9]
    ];
  

  return Mail::to($row[1])->send(new PdfMail($columnas));
 }
}
