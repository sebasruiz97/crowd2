<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Luecano\NumeroALetras\NumeroALetras;


use Carbon\Carbon;

class PdfMail extends Mailable
{
    use Queueable, SerializesModels;

 


    public $subject = "Sebastian Ruiz";

    public $msg;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($msg)
    {
        $this->msg = $msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      //  return $this->view('personas');
      
      $fecha = Carbon::now()->toDateTimeString();

      $ciudad = "Bogotá D.C.";

     $mes_reto = "meses anteriores";

     $motivo_cc ="Recompensas y reembolsos RETO Homecenter";

     $motivo = "Recompensas y reembolsos durante retos de CROWDSQA en meses anteriores a septiembre ";

     $fecha_pago =" el 16 de Septiembre";
	$fecha_max = " ANTES DEL MIERCOLES 8 de Septiembre";

    $convertidor = new NumeroALetras();

    $letras = $convertidor->toString($this->msg["valor"],2,"PESOS COLOMBIANOS","");
    

    $datos = [
     
        "nombre" => $this->msg["nombre"],
        "correo" => $this->msg["correo"],
        "cedula" => $this->msg["cedula"],
        "banco" => $this->msg["banco"],
        "tipo_cuenta" => $this->msg["tipo_cuenta"],
        "numero_cuenta" => $this->msg["numero_cuenta"],
        "valor" => $this->msg["valor"],
        "direccion" => $this->msg["direccion"],
        "telefono" => $this->msg["telefono"]
        ];
       
     
    $pdf = \PDF::loadView('cuentacobro',compact('fecha_pago','datos','ciudad','fecha','letras','motivo'));



   
    /* return $this->view('personas',compact('mes_reto','motivo_cc','fecha_pago','fecha_max'))
     ->subject('CROWDSQA - 2021 '.$mes_reto.' - Cuenta de cobro de '.$motivo_cc)
      ->attach(public_path('Cuenta de Cobro.pdf'),[
          "as" => 'Cuenta de Cobro.pdf'
      ]);*/


      return $this->view('personas',compact('mes_reto','motivo_cc','fecha_pago','fecha_max'))
     ->subject('CROWDSQA - 2021 '.$mes_reto.' - Cuenta de cobro de '.$motivo_cc)
      ->attachData($pdf->output(),"Cuenta Cobro.pdf");
      
    }
}
