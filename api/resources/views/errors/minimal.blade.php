<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .code {
                font-size: 26px;
                padding: 0 15px 0 15px;
                text-align: center;
            }

            .message {
                font-size: 18px;
                text-align: center;
            }

            .btn {
                display: inline-block;
                padding: 6px 12px;
                margin-bottom: 0;
                font-size: 14px;
                font-weight: normal;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                    touch-action: manipulation;
                cursor: pointer;
                -webkit-user-select: none;
                    -moz-user-select: none;
                    -ms-user-select: none;
                        user-select: none;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;
                }
                .btn:focus,
                .btn:active:focus,
                .btn.active:focus,
                .btn.focus,
                .btn:active.focus,
                .btn.active.focus {
                outline: 5px auto -webkit-focus-ring-color;
                outline-offset: -2px;
                }
                .btn:hover,
                .btn:focus,
                .btn.focus {
                color: #333;
                text-decoration: none;
                }
                .btn:active,
                .btn.active {
                background-image: none;
                outline: 0;
                -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
                        box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
                }
                .btn.disabled,
                .btn[disabled],
                fieldset[disabled] .btn {
                cursor: not-allowed;
                filter: alpha(opacity=65);
                -webkit-box-shadow: none;
                        box-shadow: none;
                opacity: .65;
                }
                a.btn.disabled,
                fieldset[disabled] a.btn {
                pointer-events: none;
                text-decoration: none;
                }
                .btn-default {
                color: #fff;
                background-color: #304457;
                border-color: #304457;

                }
                .btn-default:focus,
                .btn-default.focus {
                color: #333;
                background-color: #8c8c8c;
                border-color: #8c8c8c;
                }
                .btn-default:hover {
                color: #333;
                background-color: #e6e6e6;
                border-color: #adadad;
                }
                .btn-default:active,
                .btn-default.active,
                .open > .dropdown-toggle.btn-default {
                color: #333;
                background-color: #e6e6e6;
                border-color: #adadad;
                }
                .btn-default:active:hover,
                .btn-default.active:hover,
                .open > .dropdown-toggle.btn-default:hover,
                .btn-default:active:focus,
                .btn-default.active:focus,
                .open > .dropdown-toggle.btn-default:focus,
                .btn-default:active.focus,
                .btn-default.active.focus,
                .open > .dropdown-toggle.btn-default.focus {
                color: #333;
                background-color: #d4d4d4;
                border-color: #8c8c8c;
                }
                .btn-default:active,
                .btn-default.active,
                .open > .dropdown-toggle.btn-default {
                background-image: none;
                }
                .btn-default.disabled:hover,
                .btn-default[disabled]:hover,
                fieldset[disabled] .btn-default:hover,
                .btn-default.disabled:focus,
                .btn-default[disabled]:focus,
                fieldset[disabled] .btn-default:focus,
                .btn-default.disabled.focus,
                .btn-default[disabled].focus,
                fieldset[disabled] .btn-default.focus {
                background-color: #fff;
                border-color: #fff;
                }
                .btn-default .badge {
                color: #fff;
                background-color: #fff;
                }

        </style>
    </head>
    <body>
            <div class="code">
                @yield('code')
            </div>

            <div class="message" style="padding: 10px;">
                @yield('message')
            </div>
    </body>
</html>
