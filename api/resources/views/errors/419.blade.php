{{-- @extends('errors::minimal')

@section('title', __('Page Expired'))
@section('code', '419')
@section('message', __('Page Expired')) --}}


@extends('errors::minimal')

@section('title', __('Página Expirada'))
@section('code')
    <div style="text-align:center;">
        <h3 style="margin-top:200px; margin-bottom:1px; font-size:90px">419</h3>
        <h3 style="margin-top: 1px"> Página expirada</h3>
    <div>

    <h1 class="centrar">
    <a class="btn btn-default btn-xs"  style="margin-top: -10px;margin-bottom: 5px; margin: 10px"
        href="{{ url('/') }}" >
        Inicio
    </a>
    </h1>
@endsection