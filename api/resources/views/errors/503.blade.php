{{-- @extends('errors::minimal')

@section('title', __('Service Unavailable'))
@section('code', '503')
@section('message', __($exception->getMessage() ?: 'Service Unavailable')) --}}


@extends('errors::minimal')

@section('title', __('Servicio no disponible'))
@section('code')
    <div style="text-align:center;">
        <h3 style="margin-top:200px; margin-bottom:1px; font-size:90px">503</h3>
        <h3 style="margin-top: 1px"> Servicio no disponible</h3>
    <div>
<h1 class="centrar">
    <a class="btn btn-default btn-xs"  style="margin-top: -10px;margin-bottom: 5px; margin: 10px"
        href="{{ url('/') }}" >
        Inicio
    </a>
    </h1>
@endsection
@section('message', __($exception->getMessage() ?: 'Servicio no disponible'))