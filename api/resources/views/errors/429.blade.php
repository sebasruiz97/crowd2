{{-- @extends('errors::minimal')

@section('title', __('Too Many Requests'))
@section('code', '429')
@section('message', __('Too Many Requests')) --}}


@extends('errors::minimal')

@section('title', __('Demasiadas peticiones'))
@section('code')
    <div style="text-align:center;">
        <h3 style="margin-top:200px; margin-bottom:1px; font-size:90px">429</h3>
        <h3 style="margin-top: 1px"> Demasiadas peticiones</h3>
    <div>

    <h1 class="centrar">
    <a class="btn btn-default btn-xs"  style="margin-top: -10px;margin-bottom: 5px; margin: 10px"
        href="{{ url('/') }}" >
        Inicio
    </a>
    </h1>
@endsection