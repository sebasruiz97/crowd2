
<a href="javascript:void(0)" data-toggle="tooltip"  data-id="{{ $id_cuentaCobro }}" data-original-title="Edit" 
    class="edit btn btn-default btn-xs ver-bug" title="Ver cuenta de Cobro"> <i class="lnr lnr-magnifier"></i></a>

<a href="{{ route('imprimir',$id_cuentaCobro) }}" data-toggle="tooltip"  id="descargar-anexo" data-id="{{ $id_cuentaCobro }}" data-original-title="Edit" 
    class="edit btn btn-default btn-xs " title="Descargar cuenta de cobro"> <i class="lnr lnr-download"></i></a>

<a href="javascript:void(0)" data-toggle='modal' id="evidencia" data-target='#myModal' data-id="{{ $id_cuentaCobro }}" data-original-title="Edit" 
    class="edit btn btn-default btn-xs evidencia" title="Anexos a la cuenta de cobro"> <i class="lnr lnr-file-add"></i></a>

<a href="javascript:void(0)" data-toggle="tooltip" id="edit-user" data-id="{{ $id_cuentaCobro }}" data-original-title="Edit" 
    class="edit btn btn-default btn-xs edit-user" title="Cambiar estado"> <i class="lnr lnr-pencil"></i></a>

@if(auth()->user()->hasRoles(['3']))
<a href="javascript:void(0);" id="delete-user" data-toggle="tooltip" data-original-title="Delete" 
    data-id="{{ $id_cuentaCobro }}" class="delete btn btn-danger btn-xs" title="Eliminar">  <i class="lnr lnr-cross"></i></a>
@endif

