<!DOCTYPE html>
 
<html lang="en">
<head>

    <style>

        table.dataTable {
            box-sizing: content-box;
            width:100% !important;
            
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);
    
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button  {
            box-sizing: border-box;
            display: inline-block;
            min-width: 1.5em;
            padding: 0.5em 1em;
            margin-left: 2px;
            text-align: center;
            text-decoration: none !important;
            cursor: pointer;
            *cursor: hand;
            color: #304457 !important;
            border: 1px solid transparent;
            border-radius: 2px;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover   {
    
            text-decoration: none !important;
            color: #ffff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);
        
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:focus  {
    
            text-decoration: none !important;
            color: #ffff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);

        }

        thead input {
        width: 100%;
        }
        
    
    </style>

    <script type="text/javascript">
        function go(miframe, url) {
            
            url2 = 'anexosBugs';
            document.getElementById(miframe).src = url2;
                    
        }
  
    </script>

    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>

    <script>
        
        $.extend( true, $.fn.dataTable.defaults, {
        "language": {
        "decimal": ",",
        "thousands": ".",
        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "infoPostFix": "",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "loadingRecords": "Cargando...",
        "lengthMenu": "Mostrar _MENU_ registros",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": ">",
            "previous": "<"
        },
        "processing": "Procesando...",
        "search": "Buscar:",
        "searchPlaceholder": "Término de búsqueda",
        "zeroRecords": "No se encontraron resultados",
        "emptyTable": "Ningún dato disponible en esta tabla",
        "aria": {
            "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "select": {
            "rows": {
                _: '%d filas seleccionadas',
                0: 'clic fila para seleccionar',
                1: 'una fila seleccionada'
            }
        }
        }           
        } );       
    </script>

</head>


<body>
 
<!-- Modal -->
    <div class="container" >       
        
         <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm " >
                <div class="modal-content">
                    
                    <div class="modal-header modal-header-info " >

                        <table width="100%" >
                            <tr>
                              <th> <h1 style=" text-align:center"><i id="iconomodal" class="cambio" ></i></h1></th>
                              <th style=" text-align:center"> <h3 id="titulomodal" class="letra" style=" text-align:center" >Anexos a la cuenta de cobro</i></h3></th>
                              <th><button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color:white">×</button></th>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-body" style="padding-left:10px; padding-right:20px">
                        
                        <iframe width="100%" height="495" src="" id="miframe" style="color:transparent; border-color:transparent"></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- fin Modal -->


<!-- Modal de Cuenta de Cobro   -->
<div class="modal" tabindex="-1" id="cuentaCobro">
    <div class="modal-dialog">
      <div class="modal-content">
        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">X</button>
        <div class="modal-header">
          <h3 class="modal-title text-center">Enviar Cuentas de Cobro</h3>
          
        </div>
        <div class="modal-body">
          <form id="subirArchivo"    enctype="multipart/form-data">
           
            @csrf

            <label for="fecha" >Fecha:  ***(RECUERDE QUE SI NO SELECCIONA LA FECHA, EL SISTEMA LE ASIGNA FECHA ACTUAL)***</label>
            <input  type="date" name="fecha" id="fecha"  class=" input">

            <label for="fechamax">Fecha Máxima:</label>
            <input class="input mb-4" required type="date" name="fechamax" id="fechamax">

            <label for="mes_reto">Mes Reto:</label>
            <input class="input mb-4" required type="text" name="mes_reto" id="mes_reto">

            <label for="concepto">Concepto:</label>
            <input class="input mb-4" required type="text" name="concepto" id="concepto">

            <label for="concepto">Descripción Cuenta de Cobro:</label>
            <input class="input mb-4" required type="text" name="descripcion" id="descripcion">

           
            <label for="archivo">Adjuntar archivo Excel o de hoja de cálculo para generar cuentas de cobro:</label>
              <input required class="btn btn-default" type="file" name="archivo" id="archivo">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" id="enviarCuentasCobro" class="btn btn-default">Enviar Cuentas de Cobro</button>
        </form>
        <div id="contenido"></div>
        </div>
      </div>
    </div>
  </div>




<!-- Fín de Modal de Cuenta de Cobro -->


<div class="container" style="width:100%">
<br>

@if(auth()->user()->hasRoles(['1','3']))
<a href="javascript:void(0)" class="btn btn-default btn-xs generar-cobro"  id="create-new-user">Generar cuentas de cobro</a>
<a href="javascript:void(0)" class="btn btn-default btn-xs "  id="crear_Cuenta_Cobro">Enviar cuentas de cobro</a>
@endif
<br><br>


@include('flash::message')

<table class="table table-bordered table-striped" id="laravel_datatable" style="width:100px; font-size:13px" data-turbolinks="false">
   <thead>
      <tr>
        <th width="50px">ID Cuenta</th>
        <th>Ciudad de la cuenta de cobro</th>
        <th width="100px">Beneficiario</th>
        <th width="100px">Tipo de Documento</th>
        <th width="100px">Número de Documento</th>
        <th width="80px">Monto</th>
        <th>Concepto de la cuenta de cobro</th>
        <th width="150px">Descripción</th>
        <th>Número de cuenta</th>
        <th>Tipo de Cuenta</th>
        <th>Entidad Bancaria</th>
        <th>Dirección del beneficiario</th>
        <th>Celular del beneficiario</th>
        <th width="50px">Estado</th>
        <th width="100px">Motivo de rechazo (si aplica)</th>
        <th>Cuenta de cobro generada por</th>
        <th width="70px">Fecha de Creación</th>
        <th width="140px">Acciones</th>
      </tr>
   </thead>
</table>
</div>
 
@include("cobros.form");
@include("cobros.formGenerar");
@include("cobros.show");
@include('flash::message')


</body>
<script>
    var SITEURL = '{{URL::to('')}}';
    var rol = '{{auth()->user()->rol_id}}';

     $(document).ready( function () {

        // Setup - add a text input to each footer cell
        $('#laravel_datatable thead tr').clone(true).appendTo( '#laravel_datatable thead' );
        $('#laravel_datatable thead tr:eq(1) th').each( function (i) {

            var title = $(this).text();
           
            $(this).html( '<input type="text" style="font-size:12px; border-color:#304457; text-color:#ffff" placeholder="Buscar" />' );
    
   
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
         
       $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      var table = $('#laravel_datatable').DataTable({

            drawCallback: function( settings ) {

            var api = this.api();
            var datos = api.rows( {page:'current'} ).data();

                $.each(datos, function(i, item) 
                {      
     
                  if(item.nombre_estado_cuentaCobro =="Rechazada por el contador" || item.nombre_estado_cuentaCobro =="Pagada" || item.nombre_estado_cuentaCobro =="Aprobada por el tester" ||item.nombre_estado_cuentaCobro =="Aprobada por el contador")
                  {
                   
                    $("a[id='delete-user'][data-id='"+item.id_cuentaCobro+"']").hide();
                  }

                  if(rol == 3)
                {                   
                    if(item.nombre_estado_cuentaCobro =="Rechazada por el contador" || item.nombre_estado_cuentaCobro =="Pagada" || item.nombre_estado_cuentaCobro =="Aprobada por el tester" ||item.nombre_estado_cuentaCobro =="Rechazada por el tester" ||item.nombre_estado_cuentaCobro =="Aprobada por el contador")
                    {   
                    
                    $("a[id='edit-user'][data-id='"+item.id_cuentaCobro+"']").hide();
                    }
                }

                if(rol == 2)
                {                   
                    if(item.nombre_estado_cuentaCobro =="Rechazada por el contador" || item.nombre_estado_cuentaCobro =="Pagada" ||item.nombre_estado_cuentaCobro =="Aprobada por el contador")
                    {   
                    
                    $("a[id='edit-user'][data-id='"+item.id_cuentaCobro+"']").hide();
                    }
                }

                if(rol == 4)
                {                   
                    if(item.nombre_estado_cuentaCobro =="Generada automáticamente" || item.nombre_estado_cuentaCobro =="Aprobado por el líder del proyecto" ||item.nombre_estado_cuentaCobro =="Rechazada por el tester" )
                    {   
                    
                    $("a[id='edit-user'][data-id='"+item.id_cuentaCobro+"']").hide();
                    }
                }

                var btns = $('.dt-button');
                btns.addClass('btn btn-info btn-xs');
                btns.removeClass('dt-button');
                
                });
         
            },
         
            orderCellsTop: true,
            fixedHeader: false,
            processing: true,
            serverSide: true,

 
            ajax: {
              url:  "cobros",
              type: 'GET',
             },
             columns: [
                      {data: 'id_cuentaCobro', name: 'id_cuentaCobro', 'visible': true},
                      {data: 'ciudad_cuentaCobro', name: 'ciudad_cuentaCobro', 'visible': false},
                      {data: 'nombre_titular', name: 'nombre_titular', 'visible': true},
                      {data: 'tipoDocumento_beneficiario', name: 'tipoDocumento_beneficiario', 'visible': true},
                      {data: 'documento_beneficiario', name: 'documento_beneficiario' },
                      {data: 'monto_cuentaCobro', name: 'monto_cuentaCobro' },
                      {data: 'concepto_cuentaCobro', name: 'concepto_cuentaCobro', 'visible': false},
                      {data: 'descripcion_cuentaCobro', name: 'descripcion_cuentaCobro',
                      
                            render: function ( data, type, row ) {

                            if ( type !== 'display' ) {
                                return type === 'export' ?
                                data.replace( /[$,]/g, '' ) :
                                data;
                            }
                            return type === 'display' && data.length > 150 ?
                            data.substr( 0, 150 ) +'…' :
                            data;
                            }
                       },
                        {data: 'cuentaBancaria_beneficiario', name: 'cuentaBancaria_beneficiario', 'visible': false },
                        {data: 'tipoCuenta_beneficiario', name: 'tipoCuenta_beneficiario', 'visible': false },
                        {data: 'banco_beneficiario', name: 'banco_beneficiario', 'visible': false },
                        {data: 'direccion_beneficiario', name: 'direccion_beneficiario', 'visible': false },
                        {data: 'celular_beneficiario', name: 'celular_beneficiario', 'visible': false },
                        {data: 'nombre_estado_cuentaCobro', name: 'nombre_estado_cuentaCobro' },
                        {data: 'nombre_rechazo_cuentaCobro', name: 'nombre_rechazo_cuentaCobro', 'visible': true },
                        {data: 'generada_por', name: 'generada_por', 'visible': false },
                        {data: 'created_at', name: 'created_at', 'visible': true },
                        {data: 'action', name: 'action', orderable: false, searchable: false,},
                    ],
            order: [[0, 'desc']],

            
            dom: 'Bfrtip',
                buttons: [
                    
                [{
                    extend: 'excel',
                    exportOptions: { orthogonal: 'export' }
                    
                }],
                
                [{
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: { orthogonal: 'export' }
                }],

                [{
                    extend: 'csv',
                    exportOptions: { orthogonal: 'export' }
                }],          

                [{
                    extend: 'print',
                    text: 'Imprimir',
                    exportOptions: { orthogonal: 'export' }
                }],

            ],
          });
        /*  Formulario para generar cuenta de cobro */

        $('body').on('click', '.generar-cobro', function () {

            $('#name-error').hide();
            $('#email-error').hide();
            $('#cobroCrudModal').html("Generar cuenta de cobro");
            $('#btn-save').val("generar-cobro");
            $('#ajax-crud-modal-cobro').modal('show');

       });

       /* Para editar y cambiar estado*/
        $('body').on('click', '.edit-user', function () {
          var id_cuentaCobro = $(this).data('id');

          $.get('cobros/' + id_cuentaCobro +'/edit', function (data) {

            $('#name-error').hide();
            $('#email-error').hide();
            $('#userCrudModal').html("Aprobar o rechazar cuenta de cobro");
            $('#btn-save').val("edit-user");
            $('#ajax-crud-modal').modal('show');
            $('#id_cuentaCobro').val(data.id_cuentaCobro);
            $('#ciudad_cuentaCobro').val(data.ciudad_cuentaCobro);

          })
       });

        /* Para llevar a anexo*/
       $('body').on('click', '.evidencia', function () {         
        
        var id_cuentaCobro = $(this).data('id');
        $('#id_cuentaCobro').val(id_cuentaCobro);

        url2 = 'anexoCobro/'+id_cuentaCobro;
        document.getElementById('miframe').src = url2;       

       });


       /* Para ver cuenta de Cobro en formato sqa*/
       $('body').on('click', '.ver-bug', function () {
          var id_cuentaCobro = $(this).data('id');


          $.get('cobro/show/' + id_cuentaCobro +'', function (data) {

            $('#VerBugModal').html("Cuenta de Cobro");                
            $('#ajax-crud-modal2').modal('show');

            var id = data[0].id_cuentaCobro;    
            $("#cuentaCobro_id").val(id);
            
            $("#tr0").html("");
            $("#tr1").html("");
            $("#tr2").html("");
            $("#tr3").html("");
            $("#tr4").html("");
            $("#tr5").html("");
            $("#tr6").html("");
            $("#tr7").html("");
            $("#tr8").html("");
            $("#tr9").html("");
            $("#tr10").html("");
            $("#tr11").html("");
            $("#tr12").html("");
            $("#tr13").html("");
            $("#tr14").html("");
            $("#tr15").html("");
            $("#tr16").html("");

            var tr0 = data[0].id_cuentaCobro;  
            var tr1 = data[0].banco_beneficiario;
            var tr2 = data[0].nombre_usuario;
            var tr3 = data[0].celular_beneficiario;
            var tr4 = data[0].ciudad_cuentaCobro;
            var tr5 = data[0].tipoDocumento_beneficiario;
            var tr6 = data[0].documento_beneficiario;
            var tr7 = data[0].monto_cuentaCobro;
            var tr8 = data[0].valor_cuentaCobro;
            var tr9 = data[0].concepto_cuentaCobro;
            var tr10 = data[0].descripcion_cuentaCobro;
            var tr11 = data[0].cuentaBancaria_beneficiario;
            var tr12 = data[0].tipoCuenta_beneficiario;
            var tr13 = data[0].direccion_beneficiario;
            var tr14 = data[0].fecha_creacion;  
            var tr15 = data[0].nombre_titular;  
            var tr16 = data[0].documento_beneficiario;        
            
            $("#tr0").append(tr0);
            $("#tr1").append(tr1);
            $("#tr2").append(tr2);
            $("#tr3").append(tr3);
            $("#tr4").append(tr4);
            $("#tr5").append(tr5);
            $("#tr6").append(tr6);
            $("#tr7").append(tr7);
            $("#tr8").append(tr8);
            $("#tr9").append(tr9);
            $("#tr10").append(tr10);
            $("#tr11").append(tr11);
            $("#tr12").append(tr12);
            $("#tr13").append(tr13);
            $("#tr14").append(tr14);
            $("#tr15").append(tr15);
            $("#tr16").append(tr16);

            $("#cuerpo").html("");
            for(var i=0; i<data.length; i++){
                var tr = `<tr>
                <td>`+data[i].cuentaCobro_cambio+`</td>
                <td>`+data[i].nota_cuentaCobro_cambio+`</td>
                <td>`+data[i].nombre_realiza_cambio+`</td>
                <td>`+data[i].fecha_cambio+`</td>
                </tr>`;
                $("#cuerpo").append(tr)
            }
        });
          
       });

       //Para eliminar
        $('body').on('click', '#delete-user', function () {
     
            var id_cuentaCobro = $(this).data("id");
            if(confirm("Está seguro que desea eliminar!")){

               
            $.ajax({
                type: "get",
                url: "cobro/delete/"+id_cuentaCobro,
                success: function (data) {

                window.location.reload();
                var oTable = $('#laravel_datatable').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
          }
        });   
       });

    
    
    // Para enviar y cambiar estado
     
        $(document).ready(function(e){
        // Submit form data via Ajax

        if ($("#userForm").length > 0) {
        $("#userForm").validate({

        submitHandler: function(form) {

        $("#userForm").on('submit', function(e){

            e.preventDefault();
            var formData = new FormData(document.getElementById("userForm"));
            formData.append('id_cuentaCobro', $('#id_cuentaCobro').prop('value'));

        var actionType = $('#btn-save').val();            
        $('#btn-save').html('Enviando..');

        $.ajax({
            data: $('#userForm').serialize(),
            type: "post",
            url:  "cobro/store",
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,

            success: function (response) {    

                window.location.reload();

                $('#userForm').trigger("reset");
                $('#ajax-crud-modal').modal('hide');                
                $('#btn-save').html('Guardar cambios');

                $('#laravel_datatable').DataTable().ajax.reload();
                var oTable = $('#laravel_datatable').dataTable();
                oTable.fnDraw(false);
            },

            error: function (data) {
                console.log('Error:', data);
                $('#btn-save').html('Guardar cambios');
            }
            
        });
        
        });

        }
        });
        }
    });


    // Para enviar y generar cuenta de cobro
     
    $(document).ready(function(e){
    // Submit form data via Ajax

    if ($("#cuentaForm").length > 0) {

    $("#cuentaForm").validate({

    submitHandler: function(form) {

    $("#cuentaForm").on('submit', function(e){

        e.preventDefault();
        var formData = new FormData(document.getElementById("cuentaForm"));

    var actionType = $('#btn-save').val();            
    $('#btn-save').html('Enviando..');

    $.ajax({
        data: $('#cuentaForm').serialize(),
        type: "post",
        url:  "cobro/generar",
        data: formData,
        dataType: "html",
        cache: false,
        contentType: false,
        processData: false,

        success: function (response) {    

            window.location.reload();

            $('#cuentaForm').trigger("reset");
            $('#ajax-crud-modal-cobro').modal('hide');                
            $('#btn-save').html('Guardar cambios');

            $('#laravel_datatable').DataTable().ajax.reload();
            var oTable = $('#laravel_datatable').dataTable();
            oTable.fnDraw(false);
        },

        error: function (data) {
            console.log('Error:', data);
            $('#btn-save').html('Guardar cambios');
        }
        
    });
    
    });

    }
    });
    }
});




    </script>


<!-- Crear cuenta de Cobro -->
<script>


// Modal
    
$(document).ready(function () {

$("#crear_Cuenta_Cobro").click(function (e) { 
  
  $('#cuentaCobro').modal('show');
});

// AJAX


$('#subirArchivo').on("submit", function (e) {
    e.preventDefault();
    var formdata = new FormData($('#subirArchivo')[0],[1],[2],[3],[4],[5])

    $.ajax({
        type: "POST",
        url: "cobro/enviar/",
        data: formdata, 
        contentType: false,
        processData: false,
        cache: false,
        beforeSend:function(){
            $('#contenido').text("Cargando.................................................")
        },
        success: function(response) {
           console.log(response.mensaje)

  
         var correos = [];
         var usuarios = [];

         $.each(response.mensaje, function (i, value) { 
              console.log(i+":"+value.email);
              
               usuarios.push(value.nombre_usuario)
               correos.push(value.email);

               $('#contenido').html("<p> se ha enviado a la cuenta de cobro a los usuarios "+usuarios +" de los correos enviados a "+correos+"</p>");
                
                
                });
         
        
          
        }, 
        error: function(error){
            console.log(error)
        },
    });

    
});




    
});
</script>
<!-- Fin de crear cuenta cobro -->

</html>

