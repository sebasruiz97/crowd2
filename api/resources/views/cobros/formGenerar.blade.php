<div class="modal fade" id="ajax-crud-modal-cobro" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto; width:600px; padding-right:40px; padding-left:20px; padding-top:20px;">
            <div class="modal-header">
                <h4 class="modal-title" id="cobroCrudModal"></h4>
            </div>
            <div class="modal-body" style="height:450px; width:600px; padding-right:40px; padding-left:40px; padding-top:20px;">

            <form id="cuentaForm" name="cuentaForm" class="form-horizontal" >

                <h5 style="color:#304457; font-weight: bold; font-size:12px; padding-bottom:5px"> Seleccione un intervalo de fechas sobre el cual desea generar las cuentas de cobro </h5>
                <br>

                <!-- Fecha de Inicio -->
                <div class="form-group col-sm-12">
                    <label for="fecha_inicio">Fecha de Inicio</label >
                    <input class="form-control" min="1980-01" name="fecha_inicio" value="{{old('fecha_inicio')}}" type="date" id="fecha_inicio" title='Es obligatorio que selecciones una fecha' required>
                    @if ($errors->has('fecha_inicio'))
                        <span class="help-block" style="color:#F3A100"; >
                            <small><i>{{ $errors->first('fecha_inicio') }}</i></small>
                        </span>
                    @endif
                </div>

                <!-- Fecha Fin -->
                <div class="form-group col-sm-12">
                    <label for="fecha_fin">Fecha Fin</label>
                    <input class="form-control" min="1980-01" name="fecha_fin" value="{{old('fecha_fin')}}" type="date" id="fecha_fin" title='Es obligatorio que selecciones una fecha' required>
                    @if ($errors->has('fecha_fin'))
                        <span class="help-block" style="color:#F3A100"; >
                            <small><i>{{ $errors->first('fecha_fin') }}</i></small>
                        </span>
                    @endif
                </div>
                        
                <div class="form-group col-sm-12" >
                    <div class="caja">
                        Se generará una cuenta de cobro por tester, para todos aquellos hallazgos que se encuentren aprobados por el cliente, cuya fecha de aprobación se encuentre dentro del rango de fechas seleccionado y sólo para proyectos CON CIERRE PROVISIONAL.
                    </div>
                </div>

                <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    <div class="centrar">
                        <button type="submit" class="btn btn-primary" id="btn-save" value="create" >Generar cuentas de cobro
                        </button>
                    </div>
                </div>

            </form>

            </div>

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<style>
    .caja {
    font-family: sans-serif;
    font-size: 13px;
    font-style: italic;
    font-weight: 400;
    color: #304457;
    background: #E0F8F7;
    margin: 0 0 25px;
    overflow: hidden;
    padding: 5px;
    }
    
</style>


<script>

$(document).ready(function(){
    function ValidarFechas()
    {
       var fechainicial = document.getElementById("fecha_inicio").value;
       var fechafinal = document.getElementById("fecha_fin").value;
    
       if(Date.parse(fechafinal) < Date.parse(fechainicial)) {
    
       alert("La fecha final debe ser mayor a la fecha inicial");
    }
    }
});

</script>


