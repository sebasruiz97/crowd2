@extends('layouts.appBugs')

@section('content')

        @include('flash::message')
       <h3 class="text-center"  style="font-size: 18px; font-weight: bold; color: #304457; text-transform: uppercase ">  Cuentas de Cobro</h3>
        @include('cobros.list')
       
@endsection
