<div class="modal fade" id="ajax-crud-modal2" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" >
        <div class="modal-content" style="height:auto; width:1000px; ">
            <div class="modal-header">
                <h4 class="modal-title" id="VerBugModal"></h4>
            </div>
            <div class="modal-body" id="modal-body"style="height:auto; width:1000px; font-size:13px; ">
                
                <form name="cuentaCobro" class="form-horizontal" method="POST" action="{{ route('usuarioCobro') }}">

                    @csrf
                    <input type="hidden" name="cuentaCobro_id" id="cuentaCobro_id" value="">
    
                
                <div style="font-size: 14px;font-weight: bold; text-align:center">
                    CUENTA DE COBRO #<h6 id="tr0" style="display:inline; text-align:center; font-weight: bold;font-size: 14px;">  </h6>
                </div>
                <div style="font-size: 14px;font-weight: bold; text-align:center">
                    SOFTWARE QUALITY ASSURANCE
                </div>
                <div style="font-size: 14px;font-weight: bold; text-align:center">
                    NIT 811.042.907-7
                </div>
                <br>
                <br>

                <div class="table-responsive">
                    <table class="table" style="" id="bugs-table">
                        <thead>
                            <tr> 
                                <th style="width: 17px; background-color: #f2f4f4; color:black">Debe a</th>  
                                <td id="tr15" style="background-color: #fff; color:black; width:83px"> </td>  
                            </tr>
                            <tr> 
                                <th id="tr5" style="background-color: #f2f4f4; color:black"></th>  
                                <td id="tr6" style="background-color: #fff; color:black"> </td>  
                            </tr>
                            <tr> 
                                <th style="background-color: #f2f4f4; color:black">La suma en pesos colombianos de</th>  
                                <td id="tr7" style="background-color: #fff; color:black"> </td>  
                            </tr>
                            <tr> 
                                <th style="background-color: #f2f4f4; color:black">Por concepto de</th>  
                                <td id="tr9" style="background-color: #fff; color:black"> </td>  
                            </tr>
                            <tr> 
                                <th style="background-color: #f2f4f4; color:black">Descripción del Servicio</th>  
                                <td id="tr10" style="background-color: #fff; color:black"> </td>  
                            </tr>  
        
                            <tr> 
                                <th colspan="2" style="background-color: #fff; color:black; padding-top:30px; width:20px; border-color:#fff">Favor consignar estos recursos en la cuenta</th>
                            </tr>  
                            <tr> 
                                <th style="width: 40px;background-color: #f2f4f4; color:black">Tipo de Cuenta</th>  
                                <td id="tr12" style="background-color: #fff; color:black; width:20px"> </td>  
                            </tr>
                            <tr> 
                                <th style="background-color: #f2f4f4; color:black">Banco</th>  
                                <td id="tr1" style="background-color: #fff; color:black"> </td>  
                            </tr>
                            <tr> 
                                <th style="background-color: #f2f4f4; color:black">Número de Cuenta</th>  
                                <td id="tr11" style="background-color: #fff; color:black"> </td>  
                            </tr>                                                    
                        </thead>   
                    </table> 

                    <h6 style="text-align: justify">"Declaro que soy Responsable del IVA - régimen simplificado y solicito no practicar retención en la fuente si la misma no excede el tope mínimo de la tabla de retención del articulo 383 del estatuto tributario, y declaro no tener vinculados o contratados 2 o más trabajadores asociados a mi actividad"</h6>
                    <br>
                    <br>
                    <br>
                    ________________________________________
                    <div id="tr2"> </div>
                    <div>
                        No Documento: <h6 id="tr16" style="display:inline;"> </h6>
                    </div>
                    <div>
                        Dirección: <h6 id="tr13" style="display:inline;"> </h6>
                    </div>
                    <div>
                        Teléfono: <h6 id="tr3" style="display:inline;"> </h6>
                    </div>
                    <br>
                    <br>
                    <div style="font-style: italic;">
                        Cuenta de cobro generada en <h6 id="tr4" style="display:inline; font-style: italic;;"> </h6> el  <h6 id="tr14" style="display:inline; font-style: italic;"> </h6>
                    </div>
                    <hr style="padding-top: 2px; padding-bottom:2px">

                    @if(auth()->user()->hasRoles(['1','3','4']))
                    <h5 style="color:#304457; font-weight: bold; padding-bottom:5px"> Descargar anexos del tester </h5>
                    <div class="form-group col-sm-3">
                        <select class="form-control xs" style="height: 27px;"  id="tipo" name="tipo" required>
                            <option value= ''> Selecciona uno  </option>
                            <option> Documento de identificación </option>
                            <option> RUT </option>
                            <option> Certificación Bancaria </option>
                            <option> Aceptación Tratamiento de Datos </option>
                            
                        </select>
                    </div>                    
                    <div class="left">
                        <button type="submit" class="btn btn-info btn-xs" value="create" style="margin-left: 5px">Descargar
                        </button>
                    </div>              
                </div>
                @endif
                </form>   
                
                <h5 style="color:#304457; font-weight: bold; padding-bottom:5px"> Historial de cambios de la cuenta de cobro </h5>
                <div class="table-responsive">
                    <table class="table" id="cambios-table">
                        <thead>
                            <tr>
                                <th>Cambio</th>
                                <th>Nota</th>
                                <th>Realizado por</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody id="cuerpo">

                        </tbody>
                    </table>            
                </div>

                <br>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>


