@extends('layouts.appBugs')

@section('content')

        @include('flash::message')
       <h3 class="text-center"  style="font-size: 18px; font-weight: bold; color: #304457; text-transform: uppercase "> Enviar Cuentas de Cobro</h3>
         <form action="{{route('cobros.enviarpdf')}}" method="post" enctype="multipart/form-data">
            @csrf
            <label for="fecha" >Fecha:  ***(RECUERDE QUE SI NO SELECCIONA LA FECHA, EL SISTEMA LE ASIGNA FECHA ACTUAL)***</label>
            <input  type="date" name="fecha" id="fecha"  class=" input">

            <label for="fechamax">Fecha Máxima:</label>
            <input class="input mb-4" required type="date" name="fechamax" id="fechamax">

            <label for="mes_reto">Mes Reto:</label>
            <input class="input mb-4" required type="text" name="mes_reto" id="mes_reto">

            <label for="concepto">Concepto:</label>
            <input class="input mb-4" required type="text" name="concepto" id="concepto">

            <label for="concepto">Descripción Cuenta de Cobro:</label>
            <input class="input mb-4" required type="text" name="descripcion" id="descripcion">

            <label for="archivo">Adjuntar documento Excel:</label>
            <input required type="file" name="archivo" id="archivo" class="btn btn-default mb-4 input">
            <button type="submit" class="btn btn-success">Importar Excel Correos</button>
          </form>
    
       
@endsection
   