<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto; width:600px; padding-right:40px; padding-left:20px; padding-top:20px;">
            <div class="modal-header">
                <h4 class="modal-title" id="userCrudModal"></h4>
            </div>
            <div class="modal-body" style="height:450px; width:600px; padding-right:40px; padding-left:40px; padding-top:20px;">

            <form id="userForm" name="bugForm" class="form-horizontal" >

                <input type="hidden" name="id_cuentaCobro" id="id_cuentaCobro" value="">

                @inject('rechazosCuentas','App\Services\rechazosCuentas')
                @inject('estadosCuentasCobro','App\Services\estadosCuentasCobro')

                <!-- Estado de la cuenta de cobro -->
                <div class="form-group col-sm-12" >
                    <label for="estado_id_cuentaCobro">Estado de la cuenta de cobro</label>
                    <select class="form-control" id="estado_id_cuentaCobro" name="estado_id_cuentaCobro" onchange="habilitar(this)"  required title="Este campo es obligatorio, selecciona un elemento de la lista">
                        @foreach($estadosCuentasCobro->get() as  $index => $estadosCuentasCobro)
                            <option value = "{{ $index }}" {{old('estado_id_cuentaCobro') == $index ? 'selected' : ''}} > {{  $estadosCuentasCobro }}</option>
                        @endforeach
                    </select>
                    <span class="error"  id="Errorestado_id_cuentaCobro" style="color:#F3A100"; >
                </div>

                @if(auth()->user()->hasRoles(['2','4']))
                <!-- Rechazo de la cuenta de cobro-->
                <div class="form-group col-sm-12">
                    <label for="motivo_rechazo_cuentaCobro">Motivo del rechazo</label>
                    <select class="form-control" id="motivo_rechazo_cuentaCobro" name="motivo_rechazo_cuentaCobro" disabled>
                        @foreach($rechazosCuentas->get() as  $index => $rechazosCuentas)
                            <option value = "{{ $index }}" {{old('motivo_rechazo_cuentaCobro') == $index ? 'selected' : ''}} > {{  $rechazosCuentas }}  </option >
                        @endforeach
                    </select>
                    <span class="error"  id="Errormotivo_rechazo_cuentaCobro" style="color:#F3A100"; >
                </div>
                @endif

                  <!-- Nota-->
                  <div class="form-group col-sm-12">
                    <label for="cobro_notaCambio">Nota</label>
                    <textarea class="form-control" name="cobro_notaCambio" value="{{old('cobro_notaCambio')}}" type="text" id="cobro_notaCambio"required  tminlength="4" maxlength="800" title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 800 caracteres"> </textarea>
                    <span class="error"  id="Errorcobro_notaCambio" style="color:#F3A100"; >
                    <span style="color:#808b96; font-size:10px" id="rchars1" ><small style="color:#808b96; font-size:10px">800</span> caracteres restantes</small>
                </div>
                
                <!-- UserID Field -->
                <div class="form-group col-sm-6">
                    <label for="rol_id" > </label>
                    <input class="form-control" id="rol_id" type="hidden" name="rol_id"  value={{auth()->user()->rol_id}} disabled >
                </div>

                <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    <div class="centrar">
                        <button type="submit" class="btn btn-primary" id="btn-save" value="create" >Guardar cambios
                        </button>
                    </div>
                </div>                
            </form>
            
            </div>

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<script>

    var maxLength1 = 800;
    $('#cobro_notaCambio').keyup(function() {
    var textlen1 = maxLength1 - $(this).val().length;
    $('#rchars1').text(textlen1);
    });

</script>

<script>
    function habilitar(select)
    {
        if(select.value==2||select.value==5)
        {
            // habilitamos
            document.getElementById("motivo_rechazo_cuentaCobro").disabled=false;
           
        }else{
            // deshabilitamos
            document.getElementById("motivo_rechazo_cuentaCobro").disabled=true;
        }
    }
</script>

<style>
    .error {
    color: #F3A100;
    font-style: italic;
    font-weight: normal;   
    }
</style>



