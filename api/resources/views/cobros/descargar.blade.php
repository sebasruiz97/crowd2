@extends('layouts.appPDF')

@section('content')

    @foreach($cuentas as $cuenta)

    <div style="font-size: 14px;font-weight: bold; text-align:center">
        CUENTA DE COBRO # {{$cuenta->id_cuentaCobro}} 
    </div>
    <div style="font-size: 14px;font-weight: bold; text-align:center">
        SOFTWARE QUALITY ASSURANCE
    </div>
    <div style="font-size: 14px;font-weight: bold; text-align:center">
        NIT 811.042.907-7
    </div>
    <br>
    <br>

     <div>
            <table class="table" style="table-layout:fixed; tablewhite-space:nowrap; overflow:hidden; font-size:12px">
                <thead>
                    <tr> 
                        <th style="width: 17px; border-color:#fff; background-color: #f2f4f4; color:black">Debe a</th>  
                        <td style="background-color: #fff; font-weight: normal; color:black; width:83px; border-color:#fff; font-family: sans-serif"> {{$cuenta->nombre_titular}}</td>  
                    </tr>
                    <tr> 
                        <th style="border-color:#fff; background-color: #f2f4f4; color:black">{{$cuenta->tipoDocumento_beneficiario}}</th>  
                        <td style="background-color: #fff; font-weight: normal; color:black; border-color:#fff"> {{$cuenta->documento_beneficiario}}</td>  
                    </tr>
                    <tr> 
                        <th style="border-color:#fff; background-color: #f2f4f4; color:black">La suma en pesos colombianos de</th>  
                        <td style="background-color: #fff; font-weight: normal; color:black; border-color:#fff">{{$cuenta->monto_cuentaCobro}} </td>  
                    </tr>
                    <tr> 
                        <th style="border-color:#fff; background-color: #f2f4f4; color:black">Por concepto de</th>  
                        <td style="background-color: #fff; font-weight: normal; color:black; border-color:#fff"> {{$cuenta->concepto_cuentaCobro}}</td>  
                    </tr>
                    <tr> 
                        <th style="border-color:#fff; background-color: #f2f4f4; color:black">Descripción del Servicio</th>  
                        <td style="background-color: #fff; font-weight: normal; color:black; border-color:#fff">{{$cuenta->descripcion_cuentaCobro}} </td>  
                    </tr>  
                    <tr> 
                        <th colspan="2" style="background-color: #fff; color:black; width:20px; border-color:#fff; padding-top:30px">Favor consignar 
                            estos recursos en la cuenta</th>
                    </tr>  
                    <tr> 
                        <th style="width: 40px; border-color:#fff; background-color: #f2f4f4; color:black">Tipo de Cuenta</th>  
                        <td style="background-color: #fff; font-weight: normal; color:black; width:20px; border-color:#fff"> {{$cuenta->tipoCuenta_beneficiario}}</td>  
                    </tr>
                    <tr> 
                        <th style="border-color:#fff; background-color: #f2f4f4; color:black">Banco</th>  
                        <td style="background-color: #fff; font-weight: normal; color:black; border-color:#fff">{{$cuenta->banco_beneficiario}} </td>  
                    </tr>
                    <tr> 
                        <th style="border-color:#fff; background-color: #f2f4f4; color:black">Número de Cuenta</th>  
                        <td id="tr11" style="background-color: #fff; font-weight: normal; color:black;  border-color:#fff"> {{$cuenta->cuentaBancaria_beneficiario}}</td>  
                    </tr>      
                    <tr> 
                        <th style="background-color: #fff; width:20px; border-color:#fff"></th>  
                        <td style="background-color: #fff; border-color:#fff"> </td>  
                    </tr>   
                    <tr> 
                        <th colspan="2" style="background-color: #fff; color:black; font-weight: normal; width:20px; border-color:#fff">"Declaro que soy Responsable del IVA - régimen simplificado y solicito no practicar retención en la fuente si la misma no excede el tope mínimo de la tabla de retención del articulo 383 del estatuto tributario, y declaro no tener vinculados o contratados 
                            2 o más trabajadores asociados a mi actividad"</th>
                    </tr>
                    <tr> 
                        <th colspan="2" style="background-color: #fff; color:black; font-weight: normal; width:20px; border-color:#fff; padding-bottom: 0px; padding-top:50px">
                        _________________________________________________
                        </th>
                    </tr>    
                    <tr> 
                        <th colspan="2" style="background-color: #fff; color:black; font-weight: bold; width:20px; border-color:#fff; padding-bottom: 0px;">
                        {{$cuenta->nombre_usuario}}
                        </th>
                    </tr>    
                    <tr> 
                        <th colspan="2" style="background-color: #fff; color:black; font-weight: normal; width:20px; border-color:#fff; padding-bottom: 0px;">
                        No Documento: {{$cuenta->documento_beneficiario}}
                        </th>
                    </tr>   
                    <tr> 
                        <th colspan="2" style="background-color: #fff; color:black; font-weight: normal; width:20px; border-color:#fff; padding-bottom: 0px;">
                        Dirección: {{$cuenta->direccion_beneficiario}}
                        </th>
                    </tr>      
                    <tr> 
                        <th colspan="2" style="background-color: #fff; color:black; font-weight: normal; width:20px; border-color:#fff; padding-bottom: 0px;">
                        Teléfono: {{$cuenta->celular_beneficiario}} 
                        </th>
                    </tr>     
                    <tr> 
                        <th colspan="2" style="background-color: #fff; color:black; font-weight: normal; width:20px; border-color:#fff; font-style: italic; padding-top:30px">
                        Cuenta de cobro generada en {{$cuenta->ciudad_cuentaCobro}} el {{$cuenta->created_at}}
                        </th>
                    </tr>     
                </thead>   
            </table> 
        </div> 
    @endforeach
@endsection
