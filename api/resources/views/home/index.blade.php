@extends('layouts.app2', ['homeTester' => true, 'homeMensajes' => true])

@section('headLinks')
<style>
    .blockTrofeos {
        display: flex;
        align-items: flex-end;
    }

    .blockTrofeos>div:hover {
        transform: scale(1.08);
    }

    .blockTrofeos img {
        width: 100%;
    }

    .blockTrofeos .datos {
        position: absolute;
        display: flex;
        justify-content: center;
        align-items: center;
        bottom: 0;
        width: 27%;
        height: 27%;
    }

    .blockTrofeos .datos label,
    .blockTrofeos .datos span {
        font-size: 14px;
        color: #448ca3;
        width: 100%;
        border: solid #448ca3;
        border-width: 0 0 1px;
        text-align: center;
    }

    .blockTrofeos .primero .datos label {
        font-size: 18px;
    }

    .blockTrofeos .datos span {
        border: none;
        font-weight: bold;
        font-size: 22px;
    }

    .blockTrofeos .primero .datos span {
        font-size: 28px;
    }

    .blockTrofeos>.primero:hover .datos {
        display: flex;
        background: url(/img/confetti.gif) no-repeat;
        background-size: cover;
        position: absolute;
        width: 100%;
        height: 36%;
    }

    .blockTrofeos .primero .datos {
        width: 36%;
        height: 36%;
    }

    .blockTrofeos .tercero .datos {
        margin-left: 2%;
    }

    .blockTrofeos>.tercero:hover .datos {
        margin-left: 7%;
    }

    .blockTrofeos>div:hover .datos {
        width: 91%;
        height: 36%;
    }

    .blockTrofeos .confetti {
        display: none;
    }

    ul.listRetos {
        padding: 0;
        list-style: none;
    }

    ul.listRetos li {
        display: flex;
    }

    ul.listRetos li>div:first-child {
        margin-right: 15px;
    }

    ul.listRetos li a {
        text-decoration: underline;
        text-underline-position: under;
    }

    .card {
        background: #e7e7e7;
        border-radius: 5px;
        padding: 15px;
        margin-bottom: 15px;
    }

    .card.right {
        text-align: right;
    }

    .border-bottom {
        border: solid;
        border-width: 0 0 1px;
    }

    .border-white {
        border-color: white;
    }

</style>
@endsection

@section('content')
<div class="row border">
    <div class="col-sm-12 col-md-5">
        <div class="blockTrofeos">
            <div class="segundo">
                <img src="/img/segundo.png" alt="" srcset="">
                <div class="datos">
                    <div>
                        <label>Camilo Perez</label>
                        <div class="centrar"><span>$80.000</span></div>
                    </div>
                </div>
            </div>
            <div class="primero">
                <img src="/img/primero.png" alt="" srcset="">
                <div class="datos">
                    <div>
                        <label>Brayan Roa</label>
                        <div class="centrar"><span>$120.000</span></div>
                    </div>
                </div>
            </div>
            <div class="tercero">
                <img src="/img/tercero.png" alt="" srcset="">
                <div class="datos">
                    <div>
                        <label>Camila Niño</label>
                        <div class="centrar"><span>$50.000</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Retos a los que me han invitado</h4>
                    <ul class="listRetos">
                        <li>
                            <div>2021-05-17</div>
                            <div><a href="#">Reto Banco</a></div>
                        </li>
                    </ul>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Mis retos (Inscritos actualmente)</h4>
                    <ul class="listRetos">
                        <li>
                            <div>2021-03-14</div>
                            <div><a href="#">Reto WOM</a></div>
                        </li>
                        <li>
                            <div>2021-03-30</div>
                            <div><a href="#">Reto homecenter</a></div>
                        </li>
                    </ul>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="card right">
            <div class="card-body">
                <div class="border-bottom border-white">
                    <label class="card-title h4" style="margin: 5px;">Recompensas totales ganadas en 2021</label>
                    <div style="margin-bottom: 5px">
                        <span class="h3">$ 389.000</span>
                    </div>
                </div>
                <div>
                    <label class="card-title h4" style="margin-bottom: 0px;">Pendiente de pago</label>
                    <span class="h4">$ 120.000</span>
                    <div>
                        <span class="badge">Próxima fecha de pago: 10-15 Mayo</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection