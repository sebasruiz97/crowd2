@extends('layouts.app2', ['homeTester' => true])

@section('headLinks')
<link href="{{ asset('css/fullcalendar.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/calendar-custom.css') }}" rel="stylesheet" type="text/css">
<script src="{{ asset('js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('js/es-fullcalendar.js') }}"></script>
@endsection

@section('content')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            headerToolbar: {
                start: '', // will normally be on the left. if RTL, will be on the right
                center: 'prev,title,next',
                end: 'today dayGridMonth,timeGridWeek,timeGridDay' // will normally be on the right. if RTL, will be on the left
            },
            initialView: 'dayGridMonth',
            locale: 'es'
        });
        calendar.render();
    });
</script>
<div id='calendar'></div>
@endsection