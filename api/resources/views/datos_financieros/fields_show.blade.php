<!-- Banco -->
<div class="form-group">
    <label for="banco_id">¿Cuál es tu entidad bancaria?</label>
    <p>{{ $datoFinanciero->tipoBanco->nombre_banco }}</p>
</div>

<!-- Cuenta Bancaria -->
<div class="form-group">
    <label for="cuentaBancaria_id">¿Que tipo de cuenta bancaria tienes?:</label>
    <p>{{ $datoFinanciero->tipoCuentaBancaria->nombre_cuentaBancaria }}</p>
</div>

<!-- Numero de cuenta -->
<div class="form-group">
    <label for="numeroCuenta">¿Cuál es el nombre del titular de la cuenta?</label>
    <p>{{ $datoFinanciero->numeroCuenta }}</p>
</div>

<!-- Nombre del titular -->
<div class="form-group">
    <label for="nombreTitular">¿Cuál es el nombre del titular de la cuenta?</label>
    <p>{{ $datoFinanciero->nombreTitular }}</p>
</div>

<!-- Sucursal -->
<div class="form-group">
    <label for="sucursal">¿Cuál es tu sucursal bancaria?</label>
    <p>{{ $datoFinanciero->sucursal }}</p>
</div>