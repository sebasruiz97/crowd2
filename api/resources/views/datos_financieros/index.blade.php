@extends('layouts.app2')

@section('content')

@if ($datosFinancieros->isEmpty())
<h1 class="pull-right">
    <a class="btn btn-default btn-xs pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px"
        href="{{ route('datosFinancieros.create') }}">Añadir información financiera</a>
</h1>
<br>
    <br>
    <br>
    <br>
    <br>
<div style="text-align:center">
    <h3 >Aún no has agregado tus datos financieros</h3>
	<h5>
		Recuerda que esta sección de tu perfil es obligatoria para quedar activo en la plataforma.
	</h5>
</div>
@else
    <h3  style="text-align:center"> Información Financiera </h3>
    @include('flash::message')
    @include('datos_financieros.table')
@endif
    @endsection