@extends('layouts.app2')

@section('content')
<div>
    <h3 class="centrar"> <strong> Editar mi información financiera </strong></h3>
</div> <br>

@foreach($datosFinancieros as $datoFinanciero)
<form action="{{ route('datosFinancieros.update', $datoFinanciero->id_datoFinanciero) }}" method="POST" id="actualizar_datosFinancieros">

    @method('PUT')
    @include('flash::message')
    @include('datos_financieros.fields_edit')

</form>
@endforeach
@endsection