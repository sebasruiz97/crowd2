@csrf

<style>

    .caja {
    font-family: sans-serif;
    font-size: 12px;
    font-weight: 400;
    color: #304457;
    background: #E0F8F7;
    margin: 0 0 25px;
    overflow: hidden;
    padding: 10px;
    }

    /* The container */
    .container1 {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 12px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    }

    /* Hide the browser's default checkbox */
    .container1 input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 20px;
    width: 20px;
    background-color:#E6E6E6;

    }

    /* On mouse-over, add a grey background color */
    .container1:hover input ~ .checkmark {
    background-color: #304457;
    }

    /* When the checkbox is checked, add a blue background */
    .container1 input:checked ~ .checkmark {
    background-color: #304457;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
    content: "";
    position: absolute;
    display: none;
    }

    /* Show the checkmark when checked */
    .container1 input:checked ~ .checkmark:after {
    display: block;
    }

    /* Style the checkmark/indicator */
    .container1 .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
    }
    
</style>

<div class="form-group col-sm-12">
    <div class="form-group col-sm-2"> </div>
    <div class="form-group col-sm-8">

        <div class="caja">
            Solicitamos tu información financiera, con el fin de tenerla disponible en el momento en que se vayan a realizar pagos por tu participación en CROWD. Por favor ten presente que tu nombre debe estar registrado, tal cómo aparece en tu documento de identificación. Las condiciones de pago por actividades realizadas en Crowd,están señaladas en nuestros Términos y condiciones, disponibles en la página de registro.  
        </div>

        
        <!-- Banco -->
        <div class="form-group col-sm-6">
            <label for="banco_id">¿Cuál es tu entidad bancaria?</label>
            <select class="form-control" id="banco_id" name="banco_id">
                <option value=''> Selecciona uno </option>
                @foreach($tipoBanco as $key => $value)
                <option value="{{ $key }}" {{ ( $key == $tipoBancoSeleccionado) ? 'selected' : '' }}>
                    {{ $value }}
                    @endforeach
            </select>
            @if ($errors->has('banco_id'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('banco_id') }}</i></small>
            </span>
            @endif

        </div>

        <!-- Tipo cuenta bancaria -->
        <div class="form-group col-sm-6">
            <label for="cuentaBancaria_id">¿Que tipo de cuenta bancaria tienes?:</label>
            <select class="form-control" id="cuentaBancaria_id" name="cuentaBancaria_id">
                <option value=''> Selecciona una </option>
                @foreach($tipoCuentaBancaria as $key => $value)
                <option value="{{ $key }}" {{ ( $key == $tipoCuentaBancariaSeleccionado) ? 'selected' : '' }}>
                    {{ $value }}
                    @endforeach
            </select>
            @if ($errors->has('cuentaBancaria_id'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('cuentaBancaria_id') }}</i></small>
            </span>
            @endif

        </div>

        <!-- Número de Cuenta-->
        <div class="form-group col-sm-6">
            <label for="numeroCuenta">¿Cuál es tu número de cuenta?</label>
            <input class="form-control" value="{{old('numeroCuenta', $datoFinanciero->numeroCuenta)}}"
                name="numeroCuenta" type="text" id="numeroCuenta" required pattern="[0-9]{6,15}" minlength="6" maxlength="15" title="Tu número de cuenta debe ser un número y contener entre 6 y 15 caracteres">
            @if ($errors->has('numeroCuenta'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('numeroCuenta') }}</i></small>
            </span>
            @endif
        </div>

        <!-- Nombre del Titular-->
        <div class="form-group col-sm-6">
            <label for="nombreTitular">¿Cuál es el nombre del titular de la cuenta?</label>
            <input class="form-control" value={{auth()->user()->name}} name="nombreTitular" type="text" id="nombreTitular" required readonly>
            @if ($errors->has('nombreTitular'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('nombreTitular') }}</i></small>
            </span>
            @endif
        </div>


        <!-- Sucursal-->
        <div class="form-group col-sm-6">
            <label for="sucursal">¿Cuál es tu sucursal bancaria?</label>
            <input class="form-control" value="{{old('sucursal', $datoFinanciero->sucursal)}}" name="sucursal"
                type="text" id="sucursal" required pattern="[a-zA-Z0-9 ]{2,50}" minlength="6" maxlength="50" title="El nombre sólo puede contener letras, números y espacios, y tener entre 2 y 50 caracteres">
            @if ($errors->has('sucursal'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('sucursal') }}</i></small>
            </span>
            @endif
        </div>

        <div  class="form-group col-sm-6">
            <label class="container1" for="no_retencion_sin_minimo" style="font-weight: normal"> Solicito no practicar retención en la fuente si la misma no excede el tope mínimo de la tabla de retención del estatuto tributario Colombiano.
                <input type="checkbox"  id="no_retencion_sin_minimo" name="no_retencion_sin_minimo" checked> 
                <span class="checkmark"></span> 
            </label>
            @if ($errors->has('no_retencion_sin_minimo'))
                <span class="help-block" style="color:#F3A100">
                    <small><i>{{ $errors->first('no_retencion_sin_minimo') }}</i></small>
                </span>
                @endif

            <label class="container1" for="no_personas_vinculadas" style="font-weight: normal"> Declaro no tener vinculados o contratados 2 o más trabajadores asociados a mi actividad
                <input type="checkbox"  id="no_personas_vinculadas" name="no_personas_vinculadas" checked> 
                <span class="checkmark"></span> 
            </label>
            @if ($errors->has('no_personas_vinculadas'))
            <span class="help-block" style="color:#F3A100" >
                <small><i>{{ $errors->first('no_personas_vinculadas') }}</i></small>
            </span>
            @endif
        </div>


        <!-- UserID Field -->
        <div class="form-group col-sm-6">
            <label for="user_id"> </label>
            <input class="form-control" name="user_id" type="hidden" value={{auth()->user()->id}} readonly="readonly">
        </div>

        <!-- Submit Field -->
        <div class="form-group col-sm-12">
        <center><label>*Todos los campos son obligatorios</label></center><br><br>
            <div class="centrar">
                <input class="btn btn-primary" type="submit" value="Guardar">
                <a href={{ route('datosFinancieros.index') }} class="btn btn-default">Cancelar</a>
            </div>
        </div>

    </div>
</div>
<div class="form-group col-sm-2"></div>
</div>

