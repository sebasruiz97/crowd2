@extends('layouts.app2')

@section('content')
<div>
    <h3 class="centrar"> <strong> Añadir información financiera </strong></h3>
</div> <br>

<form method="POST" action="{{ route('datosFinancieros.store') }}">

    @include('flash::message')
    @include('datos_financieros.fields_create')

</form>
@endsection