@extends('layouts.tester')

@section('content')
    <section class="content-header">
        <h1>
            Mi información Financiera
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    
                    @foreach($datosFinancieros as $datoFinanciero)
                    @include('datos_financieros.fields_show')
                    @endforeach
                    <a href="{{ route('datosFinancieros.index') }}" class="btn btn-default">Volver</a>
                </div>
            </div>
        </div>
    </div>
@endsection
