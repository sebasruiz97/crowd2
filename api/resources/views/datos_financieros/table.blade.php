<div class="container.fluid py-0 px-1">
    <div class="row">
        <div class="col-sm col-1 col-lg-1">
        </div>
        <div class="col-sm col-12 col-lg-12">
            <div class="table-responsive">
                <table class="table" id="datosFinancieros-table">
                    <thead>
                        <tr>
                            <th>Entidad Bancaria</th>
                            <th>Tipo de Cuenta</th>
                            <th>Número de Cuenta</th>
                            <th>Nombre del Titular</th>
                            <th>Sucursal</th>
                            <th colspan="3">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($datosFinancieros as $datoFinanciero)
                        <tr>
                            <td>{{ $datoFinanciero->tipoBanco->nombre_banco }}</td>
                            <td>{{ $datoFinanciero->tipoCuentaBancaria->nombre_cuentaBancaria}}</td>
                            <td>{{ $datoFinanciero->numeroCuenta}}</td>
                            <td>{{ $datoFinanciero->nombreTitular }}</td>
                            <td>{{ $datoFinanciero->sucursal }}</td>
                            <td>

                                <form
                                    action="{{ route('datosFinancieros.destroy',$datoFinanciero->id_datoFinanciero) }}"
                                    method="POST">
                                    @csrf
                                    @method('DELETE')

                                    <div class='pull right'>
                                        <a href="{{ route('datosFinancieros.edit',$datoFinanciero->id_datoFinanciero) }}"
                                            class="btn btn-default btn-xs" style="margin-bottom: 5px;"> <i class="lnr lnr-pencil"></i></a>
                                        <button type="submit" class="btn btn-danger btn-xs" style="margin-bottom: 5px" 
                                            onclick="return confirm('¿Estás seguro?')"> <i class="lnr lnr-cross"></i> </button>
                                    </div>
                                </form>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pull-right">
                    <small>{{ $datosFinancieros->links() }}</small>
                </div>
            
            </div>
        </div>
        <div class="col-sm col-1 col-lg-1">
        </div>
    </div>
</div>