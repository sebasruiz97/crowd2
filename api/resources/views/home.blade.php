@section('title','Home')

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{config('app.name')}}</title>
   
    <link href="inicio/estilos.css" rel="stylesheet" type="text/css">
    <link href="inicio/bootstrap.min.css" rel="stylesheet" type="text/css">

</head>

<body id="page-top">

@include('layouts.nav')


  <!-- Header -->
<!--  
<header id="home">

    <img src="img/crowd1.gif" width="100%">

  </header>
-->

    
  <!------------- SECTION About Section -->
  <section id="about" class="about-section text-center" >
  
     <div class="container-fluid" style=" margin-top: 3%; ">
      <br>

      <div class="row justify-content-center align-items-center" style="background-color:#dcdbd7;">
        <div class="col-12 col-lg-6" style="display: flex;">
          <img id="about" src="img/Banner_principal.png" style="width: 100%; align-self: flex-start;">
        </div>
        <div class="col-12 col-lg-6">
          <p id="aboutTexto" style="font-family: HelveticaNeue LtStd Cn; margin-right: 5%; text-align: right;">
            <br>Nuestra <font style="color: #208297"><b> comunidad de testers</b></font> busca mejorar la experiencia digital de tus clientes en apps y sitios web. 
   	<br><br>
   	<img src="img/crowd_texto.png" style="width:40%;">
   	<br><br>
   		Puedes elegir las características demográficas e instrumentos de prueba específicos de tu crowd, 
   		<br>aumenta la cobertura de dispositivos, 
   		<br><font style="color: #208297"><strong>escucha lo que tus clientes tienen para decir</strong></font>.
          </p>


        </div>
      </div>

    </div>
	<div class="grid_7col">
		<div class="col_1 grid_centrada">&nbsp;</div>
		<div class="col_2 grid_centrada" style="font-size:1.7vw;">
			<center><img src="/img/pruebas_exploratorias.png" width="45%"><br>
			<strong>Pruebas exploratorias</strong><br><br></center>	
		</div>
		<div class="col_3 grid_centrada" style="display: flex; align-items:center; justify-content: center;margin-top: 0"><img src="/img/mas.png" style="width: 120%;"></div>	
		<div class="col_4 grid_centrada" style="font-size:1.7vw;">
			<center><img src="/img/dispositivos_reales.png" width="45%"><br>
			<strong>Dispositivos reales</strong><br><br></center>
		</div>
		<div class="col_5 grid_centrada" style="display: flex; align-items:center; justify-content: center; margin-top: 0"><img src="/img/mas.png" style="width: 120%;"></div>
		<div class="col_6 grid_centrada" style="font-size:1.7vw;">
			<center><img src="/img/experiencia_cliente.png" width="45%"><br>
		  	<strong>Experiencia de cliente</strong><br><br></center>
		</div>
		<div class="col_7 grid_centrada"></div>
	</div>

  </section>

  <!------------- SECTION Porque CrowdSQA  -->
  <section id="porque_CrowdSQA" class="about-section text-center">
  	<div class="container-fluid" style="background-color:#e6e6e6;">
      <br><br>
     <h3 style="color: #304457;font-family:  HelveticaNeue LtStd Cn"><strong>¿ Porqué CrowdSQA ?</strong></h3>
     
     <div class="grid_2col">
     	<div class="col_1 panel-box" style="justify-self: end; cursor: pointer">
     		<div class="back" style="display: flex; align-items:center; ">
	     			Si no alcanzaste a probar todas las funcionalidades
	     			<br><br>
	     			Si sólo probaste las funcionalidades críticas
     		</div>
     		<div class="card grid_2row front" >
     		<div class="row_1"><img src="img/complemento_pruebas.png" style="width: 32%; align-self: flex-start;"></div>
     		<div class="row_2"><b>Complemento a las pruebas de software tradicionales</b></div>
     		</div>
  		
     	</div>
     	<div class="col_2 panel-box" style="justify-self: start; cursor: pointer">
     		<div class="back" style="display: flex; align-items:center;">
     			Escucha a tus clientes
     		</div>
     		<div class="card grid_2row front">
     		<div class="row_1"><img src="img/mala_experiencia.png" style="width: 32%; align-self: flex-start;"></div>
     		<div><b>Basta una mala experiencia para perder clientes</b></div>
     		</div>
     	</div>
     </div>
     
     <div class="grid_2col">
     	<div class="col_1 panel-box" style="justify-self: end;cursor: pointer">
     		<div class="back" style="display: flex; align-items:center;">
     				Sin contextualización
     				<br><br>
     				Testers disponibles con poca antelación
     				<br><br>
     				Obtén resultados entre 2 y 4 días
     		</div>
     		<div class="card grid_2row front">
     		<div class="row_1"><img src="img/no_esperes_semanas.png" style="width: 32%; align-self: flex-start;"></div>
     		<div><b>No esperes semanas para saber en qué debes mejorar, obtén rápida retroalimentación</b></div>
     		</div>
     	</div>
     	<div class="col_2 panel-box" style="justify-self: start;cursor: pointer">
     		<div class="back" style="display: flex; align-items:center;">
     			Diferentes dispositivos, navegadores, sistemas operativos, niveles de experiencia y características demográficas
     		</div>
     		<div class="card grid_2row front">
     		<div class="row_1"><img src="img/minimiza_riesgos.png" style="width: 32%; align-self: flex-start;"></div>
     		<div><b>Minimiza riesgos, aumenta la cobertura con pruebas reales en ambiente productivo</b></div>
     		</div>
     	</div>
     </div>
     
     <div class="grid_2col">
     	<div class="col_1 panel-box" style="justify-self: end;cursor: pointer">
     		<div class="back"  style="display: flex; align-items:center;">
     			Sin sorpresas, desde el principio conoces el costo total del servicio.
     		</div>
     		<div class="card grid_2row front" >
     		<div class="row_1"><img src="img/controla_presupuesto.png" style="width: 32%; align-self: flex-start;"></div>
     		<div><b>Controla tu presupuesto - planea cuánto quieres gastar</b></div>
     		</div>
     	</div>
     	<div class="col_2 panel-box" style="justify-self: start;cursor: pointer">
     		<div class="back"  style="display: flex; align-items:center;">
     			Un líder de prueba supervisa la confiabilidad de los incidentes reportados por el crowd, además de evitar incidentes duplicados.
     		</div>
     		<div class="card grid_2row front" >
     		<div class="row_1"><img src="img/confia_resultados.png" style="width: 32%; align-self: flex-start;"></div>
     		<div><b>Confía en los resultados de nuestras pruebas</b></div>
     		</div>
     	</div>
     </div>
      <div class="row justify-content-center align-items-center">
      <!--
        <div class="col-12 col-lg-6">
          <img id="comunidad" src="img/comunidad1.svg" style="widht:20%">
        </div>
        -->
        <div class="col-12 col-lg-6">
          <p>
            <br><br>
			<a href="{{ url('/') }}" style="text-decoration: none;">
			<img src="img/contactanos.png" style="width:23%; margin-top:10px; cursor: pointer" ></a><br><br>
          </p>
  	</div>
  	</div>
  	</div>
  </section>
 
  <!------------- SECTION Comunidad Section -->
  <section id="comunidad" class="about-section text-center" style="background-color: #9cecfd">
  
    <div class="container-fluid">
      <br>
     <h3 style="color: #304457;"><strong>Nuestra Comunidad</strong></h3>
     <br>
      <div class="row justify-content-center align-items-center">
        <div class="col-12 col-lg-6">
          <img id="comunidad" src="img/nuestra_comunidad.png" style="widht:17%">
        </div>
        <div class="col-12 col-lg-6">
          <p id="comunidadTexto" style="font-family: HelveticaNeue LtStd Cn; margin-right: 5%; text-align: right;">
            Nuestra comunidad de testers desafía sus habilidades y experimenta con todos sus dispositivos para encontrar problemas antes que tu reputación se vea afectada.  <br><br><strong>CrowdSQA</strong> no tiene limitaciones a los tipos de prueba que realiza, al ser pruebas exploratorias el tester tiene libertad para probar el software como desee.
          </p>

            <div>
            <a href="{{ route('register') }}" style="color:#304457"><img  src="img/quiero_ser_tester.png"  style="width:27%;cursor: pointer"></a>
            <br>
<!--           <button class="btn btn-primary" style="background-color: white; border-color:#304457;" type="button"><h5 style="color: #008fa6"><strong>Quiero ser tester</strong></h5></button>
-->
	        </div>
        </div>
      </div>

    </div>
    
    
     <div class="container" style="margin-top: 5%">
      <div class="row">
      
        <div class="col-md-4 mb-3 mb-md-4">
          <div class="card" style="width: 230px;height:230px; cursor: auto;">
            <div class="centrar">
              
              <div class="grid_3row">
                <div class="row_1 grid_centrada">
	              <p style="font-family: HelveticaNeue LtStd Cn"><b>Gana dinero extra</b></p><br><br>
              	</div>
              	<div class="row_2 grid_centrada">
              		<img src="img/dinero_extra.png" style="width:130px;vertical-align: middle;" > 
                </div>
                <div class="row_3 grid_centrada">&nbsp;</div>
              </div>

            </div>
          </div>
        </div>


        <div class="col-md-4 mb-3 mb-md-4">
          <div class="card" style="width: 230px;height:230px; cursor: auto;">
            <div class="centrar">
              
              <div class="grid_3row">
                <div class="row_1 grid_centrada">
	               <p style="font-family: HelveticaNeue LtStd Cn"><b>Prueba con tus dispositivos</b></p><br><br><br>
              	</div>
              	<div class="row_2 grid_centrada">
              		<img src="img/pruebas_sobre_dispositivos.png" style="width:130px; vertical-align: middle;" > 
                </div>
                <div class="row_3 grid_centrada">
                	
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="col-md-4 mb-3 mb-md-4">
          <div class="card" style="width: 230px;height:230px; cursor: auto;">
            <div class="centrar">
              
              <div class="grid_3row">
                <div class="row_1 grid_centrada">
	              <p style="font-family: HelveticaNeue LtStd Cn"><b>Proyectos constantes</b></p><br><br>
              	</div>
              	<div class="row_2 grid_centrada">
              		<img src="img/proyectos_constantes.png" style="width:130px;vertical-align: middle;" > 
                </div>
                <div class="row_3 grid_centrada">&nbsp;</div>
              </div>

            </div>
          </div>
        </div>

      </div>
      <br>
    </div>
   
    
    <div  style="background-color:white">
        <div class="row" >
        <img src="img/puntos.png" style="width:5%; margin-top:1%; margin-left:3%;align-content: center;align-items: center;" >
        <p><h3 style="color: #304457; margin-left:5%; margin-top:1%; align-self: center;"><center>Haz parte de nuestra comunidad</center></h3></p>
        <br><br><br><br>

        </div>

    </div> <!-- container-->

	<div class="container" style="background-color:#9cecfd;margin-top:3%">
	  <div class="row">
	  
        <div class="col-md-4 mb-3 mb-md-4">
          <div class="card" style="width: 230px;height:320px; cursor: auto;text-align: center; margin:auto;"> <!--  -->
            <div class="centrar">
            
            <div class="grid_3row">
                <div class="row_1 grid_centrada">
                	<p style="margin:7px;font-family: HelveticaNeue LtStd Cn;line-height: 20px;"><b>Regístrate</b></p>
                </div>
                <div class="row_2 grid_centrada">
                <img src="img/inscribete.png" style="width:50%; " >
                </div>
                <div class="row_3 grid_centrada">
                	<p style="margin:7px;font-family: HelveticaNeue LtStd Cn;line-height: 20px;"><br><small>Ingresa tus datos para quedar activo en nuestra plataforma, <br>te invitaremos a los proyectos que  mejor se adecúan a tu perfil.</small></p>
                </div>
            </div>

            </div>
          </div>
        </div>

        <div class="col-md-4 mb-3 mb-md-4">
          <div class="card " style="width: 230px;height:320px; cursor: auto;">
            <div class="centrar">
            
	            <div class="grid_3row">
	                <div class="row_1 grid_centrada">
	                	<p style="margin:7px;font-family: HelveticaNeue LtStd Cn;line-height: 20px;"><b>Acepta el primer reto</b></p><br><br><br>
	                </div>
	                <div class="row_2 grid_centrada">
	                <img src="img/nuevoReto.png" style="width:50%; " >
	                </div>
	                <div class="row_3 grid_centrada">
	                	<p style="margin:7px;font-family: HelveticaNeue LtStd Cn;line-height: 20px;"><small>Gana dinero extra mientras encuentras errores en los aplicativos que usas.</small></p>
	                </div>
	            </div>
            
            </div>
          </div>
        </div>
        
        <div class="col-md-4 mb-3 mb-md-4">
          <div class="card " style="width: 230px;height:320px; cursor: auto;">
            <div class="centrar">
            
            	<div class="grid_3row">
	                <div class="row_1 grid_centrada">
	                	<p style="margin:7px;font-family: HelveticaNeue LtStd Cn;line-height: 20px;"><b>Únete a nuestro crowd</b></p><br>
	                </div>
	                <div class="row_2 grid_centrada">
	                <img src="img/comunidad.png" style="width:50%; " >
	                </div>
	                <div class="row_3 grid_centrada">
	                	<p style="margin:7px;font-family: HelveticaNeue LtStd Cn;line-height: 20px;"><br><small>Gana puntos de experiencia, <br>así podrás ser elegido para participar en proyectos privados.</small></p>
	                </div>
	            </div>
            
 
            </div>
          </div>
        </div>

      </div>

	</div> <!-- container -->
 
    
  </section> <!-- FIN comunidad section -->  
  
  <!--  PIE DE PAGINA QUE ME GUSTO 
  https://testarmy.com/en/services/crowdtesting/
  -->
  
  <footer id="footer" style="text-align:center; 
  }">

   <div class="container" style="color:#ffffff;">
        <div class="grid_3col">
	      	<div class="col_1 grid_centrada redondear_esquinas_izquierda" style="background-color: #208297;font-family: HelveticaNeue LtStd Cn;">&nbsp;</div>
	      	<div class="col_2 grid_centrada" style="background-color: #208297;font-family: HelveticaNeue LtStd Cn; font-size: 1.5vw; padding: 10px">
	      		<p>Si necesitas más información no dudes en contactarnos<br></p>
	      		<div class="container">
	      			<!--<div class="grid_2col" style="margin: 0%">-->
	      				<div class="col_1 grid centrada">
	      					<p><img src="img/icono_mensaje.png" style="width:9%; " >
	      					info@crowdsqa.co</p>
	      				</div>
                <!--
	      				<div class="col_2 grid centrada"> 
	      					<p><img src="img/icono_telefono.png" style="width:9%; " >
	      					(+57) 301 460 7639</p>
	      				</div>
-->
	      			<!--</div>-->
	      		</div>
	      	</div>
	      	<div class="col_3 grid_centrada redondear_esquinas_derecha" style="background-color: #208297;font-family: HelveticaNeue LtStd Cn;">&nbsp;</div>
      </div>
   </div>


   <br>
       <!-- Terminos y Condiciones -->
    <div class="form-group col-sm-12">
	   <div class="centrar" data-toggle="modal" data-target="#tratamiento_datos" >
	        <label for="tratamientoDatos"></label>
	        <label for="tratamientoDatos" style="color: #208297;font-family: HelveticaNeue LtStd Cn;cursor: pointer"> <strong><u>Conoce nuestra política de tratamiento de datos</u></strong> </label>
	        @if ($errors->has('tratamientoDatos'))
	        <span class="help-block" style="color:#F3A100" ;>
	            <small><i>{{ $errors->first('tratamientoDatos') }}</i></small>
	        </span>
	        @endif
	    </div>
	</div>
<!--	                        
   <div align="center" style="color: #208297;font-family: HelveticaNeue LtStd Cn;"><p><strong>Conoce nuestra política de tratamiento de datos</strong></p></div>
-->
   <br>
    {{ config('app.name') }} | Copyright @ {{ date('Y') }}
  
  </footer>
    
    
    
    <!-- Bootstrap core JavaScript -->
  <script src="/js/jquery.min.js"></script>
  <script src="/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="/js/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="/js/grayscale.min.js"></script>

<!-- Modal / Ventana / Overlay en HTML -->
<div id="tratamiento_datos" class="modal fade" data-backdrop="static">
  <div class="modal-dialog" >
    @include('auth.tratamiento_datos')
  </div>
</div>

</body>
</html>