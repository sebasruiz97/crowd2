@extends('layouts.app2')

@section('content')

        @include('flash::message')
        <h3 class="text-center"  style="font-size: 18px; font-weight: bold; color: #304457; text-transform: uppercase ">  Conocimiento en Pruebas </h3>
        @include('info_pruebas.listadatospruebas',[$lista_tipo_pruebas])
@endsection
