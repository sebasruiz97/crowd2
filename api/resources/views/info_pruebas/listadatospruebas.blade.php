<!doctype html>
<html lang="en">

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.2/handlebars.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

    <script>
        $.extend(true, $.fn.dataTable.defaults, {
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "lengthMenu": "Mostrar _MENU_ registros",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": ">",
                    "previous": "<"
                },
                "processing": "Procesando...",
                "search": "Buscar:",
                "searchPlaceholder": "Término de búsqueda",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "select": {
                    "rows": {
                        _: '%d filas seleccionadas',
                        0: 'clic fila para seleccionar',
                        1: 'una fila seleccionada'
                    }
                }
            }
        });
    </script>

    <style>
        table td {
            max-width: 120px;
            text-overflow: ellipsis;
            overflow: hidden;
            word-break: break-all;
        }

        table.dataTable thead .sorting {
            background-position: center right;
            text-align: center;
            margin: auto;
            width: 100%;
        }

        .pagination>.active>a,
        .pagination>.active>a:focus,
        .pagination>.active>a:hover,
        .pagination>.active>span,
        .pagination>.active>span:focus,
        .pagination>.active>span:hover {
            z-index: 3;
            color: #fff;
            cursor: default;
            background-color: #208297;
            border-color: #208297 !important;
        }

        thead input {
            width: 100%;
        }

        .btn-default2 {
            display: inline-block;
            font-size: 12px;
            border-width: 0px;
            padding-top: 2px;
            padding-bottom: 2px;
            font: 'Helvetica';
            background-color: #208297;
            color: #fff;
            border-color: #208297;
            border-radius: 3px;
            box-shadow: none;

        }

        .btn-default2:focus,
        .btn-default2.focus {
            color: #333;
            background-color: #9cecfd;
            border-color: #9cecfd;
        }

        .btn-default2:hover {
            color: #333;
            background-color: #9cecfd;
            border-color: #9cecfd;
        }

        .btn-default2:active,
        .btn-default2.active,
        .open>.dropdown-toggle.btn-default2 {
            color: #333 !important;
            background-color: #e6e6e6 !important;
            border-color: #adadad !important;
        }

        select.input-sm {
            height: 30px;
            line-height: 17px;
            text-align: center;
        }

        .dataTables_wrapper .dataTables_info {
            clear: both;
            float: left;
            padding-top: 0.755em;
            font-size: 12px;

        }
    </style>
    <style>
        .iframe {
            color: transparent !important;
            border-color: transparent !important;
        }

        .modal-sm {
            width: 90%;
            /* New width for large modal */
            height: 400px;
            background-color: #208297;
        }

        .modal-header {
            padding: 15px;
            border-bottom: 1px solid #fff;
        }

        div.letra {
            text-transform: capitalize;

        }

        .modal-header-info {
            color: #fff;
            padding: 0px 7px;
            border-bottom: 1px solid #304457;
            background-color: #208297;
            border-color: #304457;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }
    </style>

    <script type="text/javascript">
        function go(miframe, url, titulo, clase) {
            var clave = <?php echo ($user); ?>;

            // alert(clave);
            $('#user_id').val(clave);

            document.getElementById(miframe).src = url;
            titulomodal.innerText = (titulo);
            titulomodal.style.textTransform = 'initial';
            var element = document.getElementById("iconomodal");
            element.className = element.className = "cambio";
            element.className = element.className.replace(/\bcambio\b/g, clase);

        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {

            var table = $('#tablaDatos').DataTable({
                processing: false,
                serverSide: false,
                pageLength: 6,
                ajax: {
                    url: "{{ url('/formulariosinfoPruebas') }}",
                    type: 'GET',
                },
                columns: [{
                        data: 'nombre_prueba',
                        name: 'nombre_prueba'
                    },
                    {
                        data: 'nombre_tiempoExperiencia',
                        name: 'nombre_tiempoExperiencia'
                    },
                    {
                        data: 'nombre_ejecucion',
                        name: 'nombre_ejecucion'
                    },
                    {
                        data: 'nombre_herramienta',
                        name: 'nombre_herramienta'
                    },
                    {
                        data: 'nombre_dispositivo',
                        name: 'nombre_dispositivo'
                    },
                    {
                        data: 'nombre_navegador',
                        name: 'nombre_navegador'
                    },
                    {
                        data: 'nombre_sistemaOperativo',
                        name: 'nombre_sistemaOperativo'
                    },

                    {
                        data: 'action',
                        name: 'action'
                    }
                ]
            });



        });

        //for add todo
        function addTodo() {
            CargarLista(0);
            CargarListaTiempos(0);
            CargarListaTipoEje(0);
            var clave = <?php
                        if (isset($user)) {
                            echo ($user);
                        }
                        ?>;


            $('#todo-modal').find("input,select,textarea").val('').end();
            $('#todo_id').val("");
            $('#user_id').val(clave);
            $('#todo-modal').modal('show');
            $('.divCheckbox').html('');
            $('.divCheckboxNav').html('');
        }

        function CargarLista(p) {
            var complex = <?php echo json_encode($lista_tipo_pruebas); ?>;
            // console.log(complex);
            // Limpiar  dropdown de Herramientas
            $('#prueba_id').find('option').not(':first').remove();
            var x = document.getElementById("prueba_id");
            $.each(complex, function(i, item) {
                var option = document.createElement("option");
                //  if(complex[i].userid=="")
                //   {
                option.value = complex[i].id_prueba;
                option.text = complex[i].nombre_prueba;
                x.add(option);
                //  }

                if (complex[i].id_prueba == p) {
                    // alert("entro "+p);
                    //    option.value = complex[i].id_prueba;
                    //    option.text = complex[i].nombre_prueba;
                    //  option.selected = true;
                    //  x.add(option);
                }

            })
        }

        function CargarListaTiempos(p) {
            var complex = <?php echo json_encode($lista_tiempo_experiencia); ?>;
            // console.log(complex);
            // Limpiar  dropdown de Herramientas
            document.getElementById("tiempoExperiencia_id").disabled = false;
            $('#tiempoExperiencia_id').find('option').not(':first').remove();
            var x = document.getElementById("tiempoExperiencia_id");
            $.each(complex, function(i, item) {
                var option = document.createElement("option");
                //  if(complex[i].userid=="")
                //   {
                option.value = complex[i].id_tiempoExperiencia;
                option.text = complex[i].nombre_tiempoExperiencia;
                x.add(option);
                //  }


            })
        }

        function CargarListaTipoEje(p) {
            var complex = <?php echo json_encode($lista_tipo_ejecucion); ?>;
            // console.log(complex);
            // Limpiar  dropdown de Herramientas
            document.getElementById("ejecucion_id").disabled = false;
            $('#ejecucion_id').find('option').not(':first').remove();
            var x = document.getElementById("ejecucion_id");
            $.each(complex, function(i, item) {
                var option = document.createElement("option");
                //  if(complex[i].userid=="")
                //   {
                option.value = complex[i].id_ejecucion;
                option.text = complex[i].nombre_ejecucion;
                x.add(option);
                //  }


            })
        }

        function CargarListasHeryDisp(id) {

            $(".divCheckbox").html('');

            // AJAX request 
            $.ajax({
                url: 'getherramientasxTipo/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {

                    var len = 0;
                    if (response['data'] != null) {
                        len = response['data'].length;
                    }

                    if (len > 0) {
                        // Read data and create <option >
                        for (var i = 0; i < len; i++) {
                            var id = response['data'][i].herramienta_id;
                            var name = response['data'][i].nombre_herramienta;
                            var checkbox = '<div style="padding: 0;margin-right:1px;width:33%;margin-bottom:5px;"><div style="display:flex;">';
                            checkbox += '<input class="form-check-input" id="checkbox' + i + '" name="herramientas[]" style="width:1em;height:1em;margin-right:5px" type="checkbox" value="' + id + '">';
                            checkbox += '<label class="form-check-label" for="checkbox' + i + '">' + name + '</label>';
                            checkbox += '</div></div>';

                            $(".divCheckbox").append(checkbox);
                        }
                        // ElListaHerraxTipo(id);

                    }

                }
            });

            ///fin cambio

            //disp
            document.getElementById("dispositivo_id").disabled = false;
            // Limpiar  dropdown de Herramientas
            $('#dispositivo_id').find('option').not(':first').remove();

            // AJAX request 
            $.ajax({
                url: 'getDispositivosxTipo/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {

                    var len = 0;
                    if (response['data'] != null) {
                        len = response['data'].length;
                    }

                    if (len > 0) {
                        // Read data and create <option >
                        for (var i = 0; i < len; i++) {
                            var id = response['data'][i].dispositivo_id;
                            var name = response['data'][i].nombre_dispositivo;
                            var option = "<option value='" + id + "'>" + name + "</option>";

                            $("#dispositivo_id").append(option);
                        }
                        // ElListaHerraxTipo(id);

                    }

                }
            });
            //fin disp

        }

        function CargarListasSoyNav(id) {

            $('#dispositivo_id1').val(document.getElementById("dispositivo_id").value);
            // alert(document.getElementById("dispositivo_id1").value);
            document.getElementById("sistemaOperativo_id").disabled = false;
            document.getElementById("sistemaOperativo_id").required = true;
            if (document.getElementById("navegador_id")) {
                document.getElementById("navegador_id").required = true;
            }

            // Limpiar  dropdown de Herramientas
            $('#sistemaOperativo_id').find('option').not(':first').remove();

            // AJAX request 
            $.ajax({
                url: 'getSoxDispxTipo/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {

                    var len = 0;
                    if (response['data'] != null) {
                        len = response['data'].length;
                    }

                    if (len > 0) {
                        // Read data and create <option >
                        for (var i = 0; i < len; i++) {
                            var id = response['data'][i].sistemaOperativo_id;
                            var name = response['data'][i].nombre_sistemaOperativo;
                            var option = "<option value='" + id + "'>" + name + "</option>";

                            $("#sistemaOperativo_id").append(option);
                        }
                        // ElListaHerraxTipo(id);

                    }

                }
            });

            ///fin cambio

            //disp
            if (document.getElementById("navegador_id")) {
                document.getElementById("navegador_id").disabled = false;
            }
            $('.divCheckboxNav').html('');
            // Limpiar  dropdown de Herramientas
            // $('#navegador_id').find('option').not(':first').remove();

            // AJAX request 
            $.ajax({
                url: 'getNavDispxTipo/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {

                    var len = 0;
                    if (response['data'] != null) {
                        len = response['data'].length;
                    }

                    if (len > 0) {
                        // Read data and create <option >
                        for (var i = 0; i < len; i++) {
                            var id = response['data'][i].navegador_id;
                            var name = response['data'][i].nombre_navegador;
                            var checkbox = '<div style="padding: 0;margin-right:1px;width:33%;margin-bottom:5px;"><div style="display:flex;">';
                            checkbox += '<input class="form-check-input" name="navegadores[]" style="width:1em;height:1em;margin-right:5px" type="checkbox" id="navegador_' + i + '" value="' + id + '">';
                            checkbox += '<label class="form-check-label" for="navegador_' + i + '">' + name + '</label>';
                            checkbox += '</div></div>';

                            $(".divCheckboxNav").append(checkbox);
                        }
                        //ElListaHerraxTipo(id);

                    }

                }
            });
            //fin disp

        }

        //for delete todo
        function deleteTodo(id) {

            if (confirm("Esta seguro que desea Eliminar!")) {
                $.ajax({
                    type: "get",
                    url: "{{ 'formulariosinfoPruebasdelete' }}" + '/' + id,
                    success: function(data) {
                        // alert('Fue eliminado');
                        var oTable = $('#tablaDatos').dataTable();
                        oTable.fnDraw(false);
                        location.reload(true);
                        console.log(data);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });


            }
        }

        function asiherr() {
            $('#herramienta_id1').val(document.getElementById("herramienta_id").value);
            // alert(document.getElementById("herramienta_id1").value);

        }

        function asiso() {
            $('#sistemaOperativo_id1').val(document.getElementById("sistemaOperativo_id").value);
            // alert(document.getElementById("sistemaOperativo_id").value);

        }

        function asinav() {
            $('#navegador_id1').val(document.getElementById("navegador_id").value);
            // alert(document.getElementById("navegador_id").value);

        }

        function cambiarTipoEjecucion(e) {
            const el = $(e.srcElement);
            if (el.val() == "2") {
                console.log('here');
                $('#lblHerramienta').before('<span class="requiredH text-danger">* </span>');
            } else {
                $('.requiredH').remove();
            }
        }
    </script>

</head>

<body>


    <!-- Modal -->
    <div class="container">
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm ">
                <div class="modal-content">

                    <div class="modal-header modal-header-info ">

                        <table width="100%">
                            <tr>
                                <th>
                                    <h1 style=" text-align:center"><i id="iconomodal" class="cambio"></i></h1>
                                </th>
                                <th style=" text-align:center">
                                    <h3 id="titulomodal" class="letra" style=" text-align:center">xx</i></h3>
                                </th>
                                <th><button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color:white">×</button></th>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-body" style="padding-left:10px; padding-right:20px">
                        <iframe width="100%" height="495" src="" id="miframe" style="color:transparent; border-color:transparent"></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- fin Modal -->
    <form name="frm-example" id="frm-example">


        <div class="container=fluid p-3">
            <br> <br>

            <table class="table table-row-border" id="mytable">
                <thead>
                    <th width="100px">Tipos Pruebas</th>
                    <th width="100px">Tiempo de Experiencia</th>
                    <th width="100px">Tipo Automatizacion</th>
                    <th width="100px">Herramientas Conocidas</th>
                    <th width="100px">Dispositivos Usados</th>
                    <th width="100px">Sistema Operativo Dispositivo</th>
                    <th width="100px">Navegadores Dispositivo</th>
                    <th width="100px">Acción</th>
                </thead>
                <tbody>

                    <div class="modal-header ">
                        <!--<button type="button" id="CTipo_pruebas_new" class='btn-default2 pull-right' data-toggle='modal' data-target='#myModal' title='Adicionar conocimiento en pruebas' onclick="go('miframe','formulariosinfoPruebas',title,'glyphicon glyphicon-search')">
                            <hi class="modal-title">Añadir conocimiento en pruebas</i>
                        </button>-->
                        <a href="javascript:void(0)" class="text-center btn btn-success mb-1" onclick="addTodo()">Crear</a>
                    </div>

                    @foreach($conocimientoPruebas as $datos)
                    <tr>
                        <td>{{$datos->Tipos_Pruebas}}</td>
                        <td>{{$datos->Experiencia}}</td>
                        <td>{{$datos->Tipo_Automatizacion}}</td>
                        <td>{{$datos->Herramientas_Conocidas}}</td>
                        <td>{{$datos->Dispositivos_Usados}}</td>
                        <td>{{$datos->Sistema_Operativo_Dispositivo}}</td>
                        <td>{{$datos->Navegadores_Sistema_Operativo_Dispositivo}}</td>
                        <td>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger lnr lnr-cross " onclick="deleteTodo('{{ $datos->id_info_prueba }}')"></a>
                        </td>
                    </tr>
                    @endforeach

                </tbody>

            </table>
        </div>
    </form>

    <div class="modal fade" id="todo-modal" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">



                <div class="modal-header ">

                    <h4 class="modal-title" id="todo-modal"></h4>
                </div>

                <div class="modal-body">
                    <div style="display:flex;">
                        <form id="todoForm" name="todoForm" lang="es">
                            <input type="hidden" name="todo_id" id="todo_id">
                            <input type="hidden" name="user_id" id="user_id">
                            <input type="hidden" name="prueba_id1" id="prueba_id1">
                            <input type="hidden" name="herramienta_id1" id="herramienta_id1">
                            <input type="hidden" name="dispositivo_id1" id="dispositivo_id1">
                            <input type="hidden" name="sistemaOperativo_id1" id="sistemaOperativo_id1">
                            <input type="hidden" name="navegador_id1" id="navegador_id1">


                            <div class="col-sm-12" style="justify-content: center;display: flex;">
                                <div class="form-group col-sm-11">
                                    <div style="margin-bottom: .5em" style="display: none;">
                                        <span id="lblError" class="text-danger"></span>
                                    </div>


                                    <div class="form-group col-sm-6">
                                        <label><span class="text-danger">*</span> Tipo de prueba</label>
                                        <select class="form-control" id="prueba_id" name="prueba_id" required="" onchange="CargarListasHeryDisp(this.options[this.selectedIndex].value)">
                                            <option value="" disabled selected>Selecciona Tipo de prueba</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <label><span class="text-danger">*</span> Tiempo de Experiencia</label>
                                        <select class="form-control" id="tiempoExperiencia_id" name="tiempoExperiencia_id" required="" disabled>
                                            <option value=''><small> Selecciona Tiempo de Experiencia </small> </option>
                                            @foreach($lista_tiempo_experiencia as $lista_tiempo)
                                            <option value="{{ $lista_tiempo->id_tiempoExperiencia }}">{{ $lista_tiempo->nombre_tiempoExperiencia }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <div>
                                            <label><span class="text-danger">*</span> Tipo de ejecucion</label>
                                            <select class="form-control" id="ejecucion_id" name="ejecucion_id" required="" disabled onchange="cambiarTipoEjecucion(event)">
                                                <option value=''> Tipo de ejecucion </option>
                                                @foreach($lista_tipo_ejecucion as $lista_ejecucion)
                                                <option value="{{ $lista_ejecucion->id_ejecucion }}">{{ $lista_ejecucion->nombre_ejecucion }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <label id="lblHerramienta">Herramienta</label>
                                        <div class="divCheckbox" style="display:flex;flex-wrap:wrap;"></div>
                                    </div>

                                    <div class="form-group col-sm-12" style="padding: 0;">
                                        <div class="form-group col-sm-6">
                                            <label><span class="text-danger">*</span> Dispositivo</label>
                                            <select class="form-control" required="" id="dispositivo_id" name="dispositivo_id" disabled onchange="CargarListasSoyNav(this.options[this.selectedIndex].value)">
                                                <option value=''> Selecciona Dispositivo </option>
                                            </select>
                                        </div>

                                        <div class="form-group col-sm-6">
                                            <label><span class="text-danger">*</span> Sistema Operativo</label>
                                            <select class="form-control" required="" id="sistemaOperativo_id" name="sistemaOperativo_id" disabled onclick="asiso()">
                                                <option value=''>Selecciona Sistema Operativo </option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group col-sm-12">
                                        <label>Navegador</label>
                                        <div class="divCheckboxNav" style="display:flex;flex-wrap:wrap;"></div>
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <label>
                                            *Todos los campos son obligatorios</label><br><br>
                                        <div class="centrar">

                                            <button type="submit" class="btn btn-primary" id="btn-save" value="create">Guardar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>

                </div>

            </div>

            <div class="modal-footer" style="border:none;">
            </div>
        </div>
    </div>

    </div>

    <script>
        $(document).ready(function() {
            var table = $('#mytable').DataTable({
                orderCellsTop: true,
                fixedHeader: true,
                responsive: true,
                columnDefs: [{
                    targets: -1,
                    "width": "10px",
                    className: 'dt[-head]-center'
                }],
            });

            $("#myModal").on('hide.bs.modal', function() {
                // alert('The modal is about to be hidden.');
                // table.ajax.reload();

                location.reload(true);
            });

            $(document).on('submit', '#todoForm', function(form) {

                if ($('#ejecucion_id').val() == "2") {
                    var lengthHerramientas = $('input[name="herramientas[]"]:checked').length;
                    if (lengthHerramientas == 0) {
                        $('#lblError').html('Debe seleccionar por lo menos una herramienta!');
                        $('#lblError').parent().show();
                        $('#prueba_id').focus();
                        return false;
                    }
                }

                var lengthNavegadores = $('input[name="navegadores[]"]:checked').length;
                if (lengthNavegadores == 0) {
                    $('#lblError').html('Debe seleccionar por lo menos un navegador!');
                    $('#lblError').parent().show();
                    $('#prueba_id').focus();
                    return false;
                }

                var actionType = $('#btn-save').val();
                $('#btn-save').html('Enviando..');

                $.ajax({
                    data: $('#todoForm').serialize(),
                    url: "{{ 'formulariosinfoPruebasstore' }}",
                    type: "POST",
                    dataType: 'json',
                    success: function(data) {

                        // console.log('resp',data);
                        // console.log(data.responseText)

                        $('#todoForm').trigger("reset");
                        $('#todo-modal').modal('hide');
                        $('#btn-save').html('Guardar Cambios');
                        var oTable = $('#tablaDatos').dataTable();
                        oTable.fnDraw(false);
                        location.reload(true);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btn-save').html('Guardar Cambios');
                    }
                });

                return false;

            })

        });
    </script>

    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>

</body>

</html>