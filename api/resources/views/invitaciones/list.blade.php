<!DOCTYPE html>
 
<html lang="en">
  <style>
    iframe.hidden
    {
      display:none
    }
  </style>

  <style>

    table.dataTable {
        box-sizing: content-box;
        width:100% !important;            
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #fff !important;
        border: 1px solid #304457 !important;
        background-color: #304457 !important;
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
        background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
        background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
        background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
        background: -o-linear-gradient(top, #304457 0%, #304457 100%);

    }

    .dataTables_wrapper .dataTables_paginate .paginate_button  {
        box-sizing: border-box;
        display: inline-block;
        min-width: 1.5em;
        padding: 0.5em 1em;
        margin-left: 2px;
        text-align: center;
        text-decoration: none !important;
        cursor: pointer;
        *cursor: hand;
        color: #304457 !important;
        border: 1px solid transparent;
        border-radius: 2px;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button:hover   {

        text-decoration: none !important;
        color: #ffff !important;
        border: 1px solid #304457 !important;
        background-color: #304457 !important;
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
        background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
        background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
        background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
        background: -o-linear-gradient(top, #304457 0%, #304457 100%);
    
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button:focus  {

        text-decoration: none !important;
        color: #ffff !important;
        border: 1px solid #304457 !important;
        background-color: #304457 !important;
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
        background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
        background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
        background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
        background: -o-linear-gradient(top, #304457 0%, #304457 100%);

    }

    thead input {
    width: 100%;
    }
    

  </style>

  <script>
    
    $.extend( true, $.fn.dataTable.defaults, {
    "language": {
    "decimal": ",",
    "thousands": ".",
    "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "infoPostFix": "",
    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
    "loadingRecords": "Cargando...",
    "lengthMenu": "Mostrar _MENU_ registros",
    "paginate": {
        "first": "Primero",
        "last": "Último",
        "next": ">",
        "previous": "<"
    },
    "processing": "Procesando...",
    "search": "Buscar:",
    "searchPlaceholder": "Término de búsqueda",
    "zeroRecords": "No se encontraron resultados",
    "emptyTable": "Ningún dato disponible en esta tabla",
    "aria": {
        "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sortDescending": ": Activar para ordenar la columna de manera descendente"
    },
    "select": {
        "rows": {
            _: '%d filas seleccionadas',
            0: 'clic fila para seleccionar',
            1: 'una fila seleccionada'
        }
    }
    }           
    } );       
  </script>

<body>
 
  <script type="text/javascript">
    function go(myModal, url, titulo,clase, id) {
var x=id;
document.getElementById('id_invitacion').value = x;

        document.getElementById(miframe).src = url;
        titulomodal.innerText =(titulo);
        titulomodal.style.textTransform = 'initial'; 
        var element = document.getElementById("iconomodal");
        element.className = element.className ="cambio";
        element.className = element.className.replace(/\bcambio\b/g, clase);
      
    }

</script>


@include("invitaciones.modal")
@include('flash::message')

<table class="table table-bordered table-striped " id="laravel_datatable" >
   <thead>
      <tr>
         <th>No</th>
         <th>Fecha Invitación</th>
         <th>Horas Respuesta</th>
         <th>Estado Invitacion</th>
         <th>Proyecto</th>
         <th>Descripción</th>
         <th>Fecha Inicio</th>
         <th>Fecha Fin</th>
         <th>Acción</th>
      </tr>
   </thead>
</table>
</div>

@include("invitaciones.form");



<script>
  $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>

</body>
<script>
     $(document).ready( function () 
     {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table =  $('#laravel_datatable').DataTable(
        {
           initComplete: function(settings, json) 
            {
                $.each(json.data, function(i, item) 
                {
                  // var element = document.getElementById('delete-user');
                  // var dataID = element.getAttribute('data-id');
                  // var dataID = $('myDivID').data('data-id');

                  if((item.invitacion_estado=="Aceptada") || (item.invitacion_estado=="Rechazada"))
                  {
                   
                   // var a = "$('[data-id="+item.id_invitacion+"]')";
                    $("a[id='aceptar-invitacion'][data-id='"+item.id_invitacion+"']").hide();
                    $("a[id='rechazar-invitacion'][data-id='"+item.id_invitacion+"']").hide();
                  }
                });
              

            },
            orderCellsTop: true,
            fixedHeader: true,
            processing: true,
             serverSide: true,
             ajax: {
              url:  "invitaciones",
              type: 'GET',
             },
 
            columns: 
             [
                {data: 'id_invitacion', name: 'id_invitacion', visible: true,searchable: false},
                
                { data: 'invitacion_fecha_envio', name: 'invitacion_fecha_envio', searchable: false },
                { data: 'invitacion_num_horas_cierre', name: 'invitacion_num_horas_cierre' },
                { data: 'invitacion_estado', name: 'invitacion_estado' },
                { data: 'proyecto_nombre', name: 'proyecto_nombre' },
                { data: 'proyecto_descripcion', name: 'proyecto_descripcion' },
                { data: 'proyecto_fecha_inicio', name: 'proyecto_fecha_inicio' },
                { data: 'proyecto_fecha_entrega', name: 'proyecto_fecha_entrega' },
                {data: 'action', name: 'action', orderable: false,searchable: false},
              ],
            order: [[0, 'desc']]
          });



         
///ensayo

     
       /* When click edit user */
        $('body').on('click', '.edit-user', function () 
        {
          var id_invitacion = $(this).data("id");
            if(confirm("¿Está seguro que desea ACEPTAR la invitación?"))
            {
                $.ajax(
                {
                  type: "get",
                  url: "invitaciones/edit/"+id_invitacion,
                  success: function (data) 
                  {
                    location.reload();
                    var oTable = $('#laravel_datatable').dataTable(); 
                    oTable.fnDraw(false);
                  },
                  error: function (data) 
                  {
                      console.log('Error:', data);
                  }
                });
            }
        });

        $('body').on('click', '#rechazar-invitacion', function () 
        {
            var id_invitacion = $(this).data("id");
            if(confirm("¿Está seguro que desea RECHAZAR la invitación?"))
            {
                $.ajax(
                {
                  type: "get",
                  url: "invitaciones/delete/"+id_invitacion,
                  success: function (data) 
                  {
                    
                    location.reload();
                    var oTable = $('#laravel_datatable').dataTable(); 
                    oTable.fnDraw(false);
                  },
                  error: function (data) 
                  {
                      console.log('Error:', data);
                  }
                });
            }
        });   



      });
    </script>

<script>
  function seleccionarCliente(cienteid) {
    var x = document.getElementById("invitacion_num_horas_cierre").value;

  //  document.getElementById("invitacion_num_horas_cierre2").value =  x;
    document.getElementById("invitacion_num_horas_cierre").selectedIndex = x;
  }
  </script>

</html>