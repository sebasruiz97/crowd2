

<a href="javascript:void(0);" id="aceptar-invitacion" 
    data-toggle="tooltip"  
    data-id="{{ $id_invitacion }}" 
    data-original-title="Aceptar Invitación" 
    class="edit btn btn-default btn-xs edit-user" 
    title="Aceptar Invitación"> 
    <i class="lnr lnr-pencil"></i>
</a>

<a href="javascript:void(0);" id="rechazar-invitacion" 
    data-toggle="tooltip" 
    data-original-title="Rechazar Invitación" 
    data-id="{{ $id_invitacion }}" 
    class="delete btn btn-danger btn-xs" title="Rechazar">  
    <i class="lnr lnr-cross"></i>
</a>



    {{-- @include("proyectos.menu") --}}