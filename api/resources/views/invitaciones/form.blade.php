<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto; width:600px; padding-right:40px; padding-left:20px; padding-top:10px;">
            <div class="modal-header">
                <h4 class="modal-title" id="userCrudModal"></h4>
            </div>
            <div class="modal-body" style="height:550px; width:600px; padding-right:40px; padding-left:30px; padding-top:10px;">
                <form id="userForm" name="bugForm" class="form-horizontal">

                    <input type="hidden" name="id_invitacion" id="id_invitacion" value="">
                    <input type="hidden" name="cliente_id2" id="cliente_id2" value="">
                    <input type="hidden" name="invitacion_estado2" id="invitacion_estado2" value="">
                    


                    @inject('clientes','App\Services\clientes')
                    @inject('proyectos_estados','App\Services\proyectos_estados')
                    @inject('proyecto_responsable_actual','App\Services\proyecto_responsable_actual')
                    <!-- Cliente -->
                    <div class="form-group col-sm-12" >
                        <label for="cliente_id">Cliente</label>
                        <select class="form-control" id="cliente_id" name="cliente_id" required onchange="CambioCliente()">
                            @foreach($clientes->get() as  $index => $clientes)
                                <option value = "{{ $index }}" {{old('cliente_id') == $index ? 'selected' : ''}} > {{  $clientes }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('cliente_id'))
                            <span class="help-block" style="color:#F3A100"; >
                                <small><i>{{ $errors->first('cliente_id') }}</i></small>
                            </span>
                        @endif
                        <!-- Proyecto-->
                        <label for="proyecto_nombre">Proyecto</label>
                        <input 
                            class="form-control" name="proyecto_nombre" 
                            value="{{old('proyecto_nombre')}}" type="text" 
                            id="proyecto_nombre"required pattern="[A-Za-z0-9 ]{4,15}" 
                            title="Puede contener letras, números y espacios. Y entre 4 y 15 caracteres" 
                        >
                        @if ($errors->has('proyecto_nombre'))
                            <span class="help-block" style="color:#F3A100"; >
                                <small><i>{{ $errors->first('proyecto_nombre') }}</i></small>
                            </span>
                        @endif
                        <!-- Descripción Proyecto-->
                        <label for="proyecto_descripcion">Descripción del proyecto</label>
                        <textarea 
                            class="form-control" 
                            name="proyecto_descripcion" 
                            value="{{old('proyecto_descripcion')}}" 
                            type="text" id="proyecto_descripcion"
                            required pattern="[A-Za-z0-9 ]{4,250}" 
                            title="Puede contener letras, números y espacios. Y entre 4 y 200 caracteres" > 
                        </textarea>
                        @if ($errors->has('proyecto_descripcion'))
                            <span class="help-block" style="color:#F3A100"; >
                                <small><i>{{ $errors->first('proyecto_descripcion') }}</i></small>
                            </span>
                        @endif
                        <!-- Estado Actual Proyecto -->
                        <label for="invitacion_estado">Estado del Proyecto</label>
                        <select class="form-control" id="invitacion_estado" name="invitacion_estado" required>
                            @foreach($proyectos_estados->get() as  $index => $proyectos_estados)
                                <option value = "{{ $index }}" {{old('invitacion_estado') == $index ? 'selected' : ''}} > {{  $proyectos_estados }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('invitacion_estado'))
                            <span class="help-block" style="color:#F3A100"; >
                                <small><i>{{ $errors->first('invitacion_estado') }}</i></small>
                            </span>
                        @endif
                    </div>

           
                    <!-- Fecha de Inicio -->
                    <div class="form-group col-sm-6">
                        <label for="proyecto_fecha_inicio">¿Cuando incia el proyecto?:</label>
                        <input 
                            type="date" 
                            class="form-control" 
                            min="<?php echo date("Y-m-d");?>"
                            value="<?php echo date("Y-m-d");?>"
                            name="proyecto_fecha_inicio"  
                            type="proyecto_fecha_inicio" 
                            id="proyecto_fecha_inicio" 
                            required
                            onchange='var input = document.getElementById("proyecto_fecha_entrega");
                            input.setAttribute("min", this.value);'
                        >
                        @if ($errors->has('proyecto_fecha_inicio'))
                            <span class="help-block" style="color:#F3A100"; >
                                <small><i>{{ $errors->first('proyecto_fecha_inicio') }}</i></small>
                            </span>
                        @endif
                        
                    </div>

                    <!-- Fecha Fin -->
                    <div class="form-group col-sm-6">
                    
                        

                        <label for="proyecto_fecha_entrega">¿Cuando finaliza el proyecto?:</label>
                        <input 
                            type="date" 
                            class="form-control" 
                            min="<?php echo date("Y-m-d");?>"
                            name="proyecto_fecha_entrega"  
                            value="{{old('proyecto_fecha_entrega')}}" 
                            type="proyecto_fecha_entrega" 
                            id="proyecto_fecha_entrega" 
                            
                        >
                        @if ($errors->has('proyecto_fecha_entrega'))
                            <span class="help-block" style="color:#F3A100"; >
                                <small><i>{{ $errors->first('proyecto_fecha_entrega') }}</i></small>
                            </span>
                        @endif

                        
                    </div>
                    

             
      
                                     <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <div class="centrar">
                            <button type="submit" class="btn btn-primary" id="btn-save" value="create">Guardar cambios
                            </button>
                        </div>
                    </div>

                    <!-- UserID Field -->
                    <div class="form-group col-sm-3">
                        <label for="user_id" > </label>
                        <input class="form-control" name="user_id" type="hidden" value={{auth()->user()->id}} readonly="readonly" >
                    </div>

                    <!-- Cliente ID Field -->
                    <div class="form-group col-sm-3">
                        <label for="cliente_id" > </label>
                        <input class="form-control" name="cliente_id" type="hidden" value="" readonly="readonly" >
                    </div>
                    
                    <!-- Estado ID Field -->
                    <div class="form-group col-sm-3">
                        <label for="bug_estado_id" > </label>
                        <input class="form-control" name="bug_estado_id" type="hidden" value="" readonly="readonly" >
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function CambioCliente() {
      var x = document.getElementById("cliente_id").value;
      document.getElementById("cliente_id2").value =  x;
        
    }
    $(document).ready(function() {
jQuery.extend(jQuery.validator.messages, {
  required: "Este campo es obligatorio.",
  remote: "Por favor, rellena este campo.",
  email: "Por favor, escribe una dirección de correo válida",
  url: "Por favor, escribe una URL válida.",
  date: "Por favor, escribe una fecha válida.",
  dateISO: "Por favor, escribe una fecha (ISO) válida.",
  number: "Por favor, escribe un número entero válido.",
  digits: "Por favor, escribe sólo dígitos.",
  creditcard: "Por favor, escribe un número de tarjeta válido.",
  equalTo: "Por favor, escribe el mismo valor de nuevo.",
  accept: "Por favor, escribe un valor con una extensión aceptada.",
  maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
  minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
  rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
  range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
  max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
  min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
});
});
    </script>

