@extends('layouts.appInvitacion')

@section('content')

        <h3 class="text-center"  style="font-size: 18px; font-weight: bold; color: #304457; text-transform: uppercase ">  Mis invitaciones a proyectos </h3>
        
        @include('invitaciones.list')
@endsection
