

<div class="modal fade" id="ajax-crud-modal2" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" >
        <div class="modal-content" style="height:auto; width:1000px; ">
            <div class="modal-header">
                <h4 class="modal-title" id="VerBugModal"></h4>
            </div>
            <div class="modal-body" id="modal-body"style="height:auto; width:1000px; font-size:13px; ">
                
                <div class="table-responsive">
                    <table class="table" id="bugs-table">

                        <thead>

                            <tr> 
                                <th style="width: 50px">ID del hallazgo</th>  
                                <td id="tr1" style="background-color: #fff; color:#304457; width:50px"> </td>      
                            </tr>
                            <tr> 
                                <th>Tipo de hallazgo</th> 
                                <td id="tr2" style="background-color: #fff; color:#304457"> </td>      
                            </tr>
                            <tr> 
                                <th>Reportado por</th>        
                                <td id="tr3" style="background-color: #fff; color:#304457"> </td>            
                            </tr>
                            <tr> 
                                <th>Nombre del proyecto</th>    
                                <td id="tr4" style="background-color: #fff; color:#304457"> </td>                   
                            </tr>
        
                            <tr> 
                                <th>Título del hallazgo</th>  
                                <td id="tr6" style="background-color: #fff; color:#304457"> </td> 
                            </tr>
                            <tr> 
                                <th>Descripción del hallazgo</th>  
                                <td id="tr7" style="background-color: #fff; color:#304457"> </td>   
                            </tr>
                            <tr> 
                                <th>Es reproducible</th>     
                                <td id="tr8" style="background-color: #fff; color:#304457"> </td> 
                            </tr>
                            <tr> 
                                <th>Pasos para reproducirlo</th>   
                                <td id="tr9" style="background-color: #fff; color:#304457"> </td> 
                            </tr>
                            <tr> 
                                <th>Gravedad</th>   
                                <td id="tr10" style="background-color: #fff; color:#304457"> </td> 
                            </tr>
                            <tr> 
                                <th>Dispositivo</th>   
                                <td id="tr13" style="background-color: #fff; color:#304457"> </td> 
                            </tr>
                            <tr> 
                                <th>Sistema Operativo</th>   
                                <td id="tr14" style="background-color: #fff; color:#304457"> </td> 
                            </tr>
                            <tr> 
                                <th>Navegador</th>   
                                <td id="tr15" style="background-color: #fff; color:#304457"> </td> 
                            </tr>
                            <tr> 
                                <th>Estado</th>  
                                <td id="tr11" style="background-color: #fff; color:#304457"> </td>  
                            </tr>
                            <tr> 
                                <th>Motivo de rechazo</th>  
                                <td id="tr2" style="background-color: #fff; color:#304457"> </td> 
                            </tr>

                        </thead>
                        <tbody id="cuerpo1">

                        </tbody>
                        
                    </table>            
                </div>

                <br>
                <br>
                <br>

                <h5 style="color:#304457; font-weight: bold; padding-bottom:5px"> Historial de cambios del hallazgo </h5>
                <div class="table-responsive">
                    <table class="table" id="cambios-table">
                        <thead>
                            <tr>
                                <th>Cambio</th>
                                <th>Nota</th>
                                <th>Realizado por</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody id="cuerpo">

                        </tbody>
                    </table>            
                </div>



            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>


