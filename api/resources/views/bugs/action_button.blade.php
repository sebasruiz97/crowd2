@if(auth()->user()->hasRoles(['1','2']))

<a href="javascript:void(0)" data-toggle="tooltip"  data-id="{{ $id_bug }}" data-original-title="Edit" 
    class="edit btn btn-default btn-xs ver-bug" title="Ver"> <i class="lnr lnr-magnifier"></i></a>

<a href="javascript:void(0)" data-toggle="tooltip" id="edit-user" data-id="{{ $id_bug }}" data-original-title="Edit" 
    class="edit btn btn-default btn-xs edit-user" title="Editar"> <i class="lnr lnr-pencil"></i></a>

<a href="javascript:void(0)" data-toggle='modal' id="evidencia" data-target='#myModal' data-id="{{ $id_bug }}" data-original-title="Edit" 
    class="edit btn btn-default btn-xs evidencia" title="Evidencia"> <i class="lnr lnr-file-add"></i></a>

<a href="javascript:void(0);" id="delete-user" data-toggle="tooltip" data-original-title="Delete" 
    data-id="{{ $id_bug }}" class="delete btn btn-danger btn-xs" title="Eliminar">  <i class="lnr lnr-cross"></i></a>

@endif

@if(auth()->user()->hasRoles(['3']))

<a href="javascript:void(0)" data-toggle="tooltip"  data-id="{{ $id_bug }}" data-original-title="Edit" 
    class="edit btn btn-default btn-xs ver-bug" title="Ver"> <i class="lnr lnr-magnifier"></i></a>

<a href="javascript:void(0)" data-toggle='modal'  id="evidenciaLider" data-target='#myModal' data-id="{{ $id_bug }}" data-original-title="Edit" 
class="edit btn btn-default btn-xs evidencia" title="Evidencia"> <i class="lnr lnr-file-add"></i></a>

<a href="javascript:void(0)" data-toggle="tooltip"  data-id="{{ $id_bug }}" id="edit-userLider" data-original-title="Edit" 
    class="edit btn btn-default btn-xs edit-user" title="Cambiar Estado"> <i class="lnr lnr-pencil"></i></a></a>
@endif