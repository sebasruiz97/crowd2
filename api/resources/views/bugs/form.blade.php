<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto; width:600px; padding-right:40px; padding-left:20px; padding-top:20px;">
            <div class="modal-header">
                <h4 class="modal-title" id="userCrudModal"></h4>
            </div>
            <div class="modal-body" style="height:auto; width:600px; padding-right:40px; padding-left:40px; padding-top:20px;">

                <form id="userForm" enctype="multipart/form-data" name="bugForm" class="form-horizontal" >

                    <input type="hidden" name="id_bug" id="id_bug" value="">


                    @inject('testerProyectos','App\Services\testerProyectos')
                    @inject('tiposHallazgos','App\Services\tiposHallazgos')
                    @inject('rechazosBugs','App\Services\rechazosBugs')
                    @inject('estadosBugs','App\Services\estadosBugs')
                    @inject('tipoDispositivos','App\Services\tipoDispositivos')

                    <!-- Proyecto -->
                    <div class="form-group col-sm-12">
                        <label for="proyecto_id">Proyecto</label>
                        <select class="form-control" id="proyecto_id" name="proyecto_id" required title="Este campo es obligatorio, selecciona un elemento de la lista">
                            @foreach($testerProyectos->get() as  $index => $testerProyectos)
                                <option value = "{{ $index }}" {{old('proyecto_id') == $index ? 'selected' : ''}} > {{  $testerProyectos }}</option>
                            @endforeach
                        </select>
                        <span class="error"  id="Errorproyecto_id" style="color:#F3A100"; >
                    </div>

                    <!-- Tipo de hallazgo -->
                    <div class="form-group col-sm-12">
                        <label for="bug_tipo_id">Tipo de hallazgo</label>
                        <select class="form-control" id="bug_tipo_id" name="bug_tipo_id" required title="Este campo es obligatorio, selecciona un elemento de la lista">
                            @foreach($tiposHallazgos->get() as  $index => $tiposHallazgos)
                            <option value = "{{ $index }}" {{old('bug_tipo_id') == $index ? 'selected' : ''}} > {{  $tiposHallazgos }}</option>
                        @endforeach
                        </select>
                        <span class="error"  id="Errorbug_tipo_id" style="color:#F3A100" >
                    </div>

                    <!-- Título-->
                    <div class="form-group col-sm-12">
                        <label for="bug_titulo">Título</label>
                        <input class="form-control" name="bug_titulo" value="{{old('bug_titulo')}}" type="text" id="bug_titulo" required minlength="4" maxlength="250" title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 250 caracteres" >
                        <span class="error"  id="Errorbug_titulo" style="color:#F3A100"; >                        
                    </div>

                    <!-- Descripción-->
                    <div class="form-group col-sm-12">
                        <label for="bug_descripcion">Descripción del hallazgo</label>
                        <textarea maxlength="800" class="form-control" name="bug_descripcion" value="{{old('bug_descripcion')}}" type="text" id="bug_descripcion" required  minlength="4"   title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 800 caracteres" > </textarea>
                        <span class="error"  id="Errorbug_descripcion" style="color:#F3A100"; >
                        <span style="color:#808b96; font-size:10px" id="rchars1" ><small style="color:#808b96; font-size:10px">800</span> caracteres restantes</small>
                    </div>

                    <!-- Pasos para reproducir-->
                    <div class="form-group col-sm-12">
                        <label for="bug_pasos">Pasos para reproducirlo</label>
                        <textarea class="form-control" name="bug_pasos" value="{{old('bug_pasos')}}" type="text" id="bug_pasos" required minlength="4" maxlength="1500" title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 1500 caracteres" > </textarea>
                        <span class="error"  id="Errorbug_pasos" style="color:#F3A100"; >
                        <span style="color:#808b96; font-size:10px" id="rchars2" ><small style="color:#808b96; font-size:10px">1500</span> caracteres restantes</small>
                    </div>

                    <!-- Reproducible -->
                    <div class="form-group col-sm-12">
                        <label for="bug_reproducible">¿Se puede reproducir el hallazgo?</label>
                        <select class="form-control" value="{{old('bug_reproducible')}}" id="bug_reproducible" name="bug_reproducible" required title="Este campo es obligatorio, selecciona un elemento de la lista">
                            <option value= ''> Selecciona uno  </option>
                            <option> Siempre </option>
                            <option> Aleatorio </option>
                        </select>
                        <span class="error"  id="Errorbug_reproducible" style="color:#F3A100"; >
                    </div>

                    <!-- Gravedad del hallazgo -->
                    <div class="form-group col-sm-12">
                        <label for="bug_gravedad">Gravedad del hallazgo</label>
                        <select class="form-control" value="{{old('bug_gravedad')}}" id="bug_gravedad" name="bug_gravedad" required title="Este campo es obligatorio, selecciona un elemento de la lista">
                            <option value= ''> Selecciona uno  </option>
                            <option> Menor, no afecta la regla de negocio </option>
                            <option> Mayor, no cumple con la regla de negocio </option>
                            <option> Stopper, no permite continuar con la ejecución de la prueba  </option>
                        </select>
                        <span class="error"  id="Errorbug_gravedad" style="color:#F3A100"; >
                    </div>

                    <!-- Dispositivos en que probo -->
                    <div class="form-group col-sm-12">
                        <label for="bug_dispositivo">Dispositivo en que realizó la prueba</label>
                        <select class="form-control" id="bug_dispositivo" name="bug_dispositivo" required title="Este campo es obligatorio, selecciona un elemento de la lista" >
                            @foreach($tipoDispositivos->get() as  $index => $tipoDispositivos)
                            <option value = "{{ $index }}" {{old('bug_dispositivo') == $index ? 'selected' : ''}} > {{  $tipoDispositivos }}</option>
                        @endforeach
                        </select>
                        <span class="error"  id="Errorbug_dispositivo" style="color:#F3A100" >
                    </div>

                    <!-- Sistema Operativo del Dispositivo -->
                    <div class="form-group col-sm-12">
                        <label for="bug_dispositivo_so">¿Qué sistema operativo tiene el dispositivo?</label>
                        <select class="form-control" data-old="{{ old('bug_dispositivo_so') }}"id="bug_dispositivo_so" name="bug_dispositivo_so" required title="Este campo es obligatorio, selecciona un elemento de la lista">
                            <option value= ''> </option>
                        </select>
                        @if ($errors->has('bug_dispositivo_so'))
                            <span class="help-block" style="color:#F3A100"; >
                                <small><i>{{ $errors->first('bug_dispositivo_so') }}</i></small>
                            </span>
                        @endif
                    </div>

                    <!--Navegador del dispositivo-->
                    <div class="form-group col-sm-12">
                        <label for="bug_dispositivo_navegador">¿Que navegador tiene el dispositivo?</label>
                        <select class="form-control" id="bug_dispositivo_navegador" data-old="{{ old('bug_dispositivo_navegador') }}" name="bug_dispositivo_navegador" required title="Este campo es obligatorio, selecciona un elemento de la lista">
                            <option value= ''> </option>
                        </select>
                        @if ($errors->has('bug_dispositivo_navegador'))
                            <span class="help-block" style="color:#F3A100"; >
                                <small><i>{{ $errors->first('bug_dispositivo_navegador') }}</i></small>
                            </span>
                        @endif
                    </div>
        
                    @if(auth()->user()->hasRoles(['1','3']))


                    <!-- Estado del hallazgo -->
                    <div class="form-group col-sm-12" >
                        <label for="bug_estado_id">Estado del hallazgo</label>
                        <select class="form-control" id="bug_estado_id" name="bug_estado_id" onchange="habilitar(this)"  required title="Este campo es obligatorio, selecciona un elemento de la lista">
                            @foreach($estadosBugs->get() as  $index => $estadosBugs)
                                <option value = "{{ $index }}" {{old('bug_estado_id') == $index ? 'selected' : ''}} > {{  $estadosBugs }}</option>
                            @endforeach
                        </select>
                        <span class="error"  id="Errorbug_estado_id" style="color:#F3A100"; >
                    </div>

                    <!-- Rechazo del hallazgo -->
                    <div class="form-group col-sm-12">
                        <label for="bug_rechazo_id">Motivo del rechazo</label>
                        <select class="form-control" id="bug_rechazo_id" name="bug_rechazo_id" disabled>
                            @foreach($rechazosBugs->get() as  $index => $rechazosBugs)
                                <option value = "{{ $index }}" {{old('bug_rechazo_id') == $index ? 'selected' : ''}} > {{  $rechazosBugs }}  </option >
                            @endforeach
                        </select>
                        <span class="error"  id="Errorbug_rechazo_id" style="color:#F3A100"; >
                    </div>

                      <!-- Nota-->
                      <div class="form-group col-sm-12">
                        <label for="bug_notaCambio">Nota</label>
                        <textarea class="form-control" name="bug_notaCambio" value="{{old('bug_notaCambio')}}" type="text" id="bug_notaCambio"required  tminlength="4" maxlength="800" title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 800 caracteres"> </textarea>
                        <span class="error"  id="Errorbug_notaCambio" style="color:#F3A100"; >
                        <span style="color:#808b96; font-size:10px" id="rchars3" ><small style="color:#808b96; font-size:10px">800</span> caracteres restantes</small>
                    </div>
                    
                    <!-- UserID Field -->
                    <div class="form-group col-sm-6">
                        <label for="rol_id" > </label>
                        <input class="form-control" id="rol_id" type="hidden" name="rol_id"  value={{auth()->user()->rol_id}} disabled >
                    </div>

                    @endif

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <div class="centrar">
                            <button type="submit" class="btn btn-primary" id="btn-save" value="create" >Guardar cambios
                            </button>
                        </div>
                    </div>

                    @if(auth()->user()->hasRoles(['2']))
                    <div class="form-group col-sm-12" >
                        <div class="caja">
                            Recuerda una vez reportes el hallazgo, debes agregar los anexos para que el hallazgo sea tenido en cuenta.
                        </div>
                    </div>
                    @endif
                </form>
            </div>
                <h6 style="color:#fff"> Crowd </h6>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<script>

    var maxLength1 = 800;
    $('#bug_descripcion').keyup(function() {
    var textlen1 = maxLength1 - $(this).val().length;
    $('#rchars1').text(textlen1);
    });

    var maxLength2 = 1500;
    $('#bug_pasos').keyup(function() {
    var textlen2 = maxLength2 - $(this).val().length;
    $('#rchars2').text(textlen2);
    });

    var maxLength3 = 800;
    $('#bug_notaCambio').keyup(function() {
    var textlen3 = maxLength3 - $(this).val().length;
    $('#rchars3').text(textlen3);
    });
</script>

<script>
    function habilitar(select)
    {
        if(select.value==3||select.value==5)
        {
            // habilitamos
            document.getElementById("bug_rechazo_id").disabled=false;
           
        }else{
            // deshabilitamos
            document.getElementById("bug_rechazo_id").disabled=true;
        }
    }
</script>


@if(auth()->user()->rol_id === 3 || auth()->user()->rol_id === 1 )
<script>

        var lista = document.getElementById("rol_id").value;

        // var rol = lista.options[lista.selectedIndex].value;

        $( function carg(rol) {

                if (lista === "3") {
                    $("#bug_estado_id").prop("readonly", false);
                    $("#bug_rechazo_id").prop("readonly", false);
                    $("#bug_nota_cambio").prop("readonly", false);
                    $("#bug_tipo_id").prop("disabled", true);
                    $("#proyecto_id").prop("disabled", true);
                    $("#bug_titulo").prop("readonly", true);
                    $("#bug_descripcion").prop("readonly", true);
                    $("#bug_pasos").prop("readonly", true);
                    $("#bug_reproducible").prop("disabled", true);
                    $("#bug_gravedad").prop("disabled", true);
                    $("#bug_anexo").prop("readonly", true);
                    $("#bug_dispositivo").prop("disabled", true);
                    $("#bug_dispositivo_so").prop("disabled", true);
                    $("#bug_dispositivo_navegador").prop("disabled", true);

                } else {

                    $("#bug_tipo_id").prop("disabled", false);
                    $("#proyecto_id").prop("disabled", false);
                    $("#bug_titulo").prop("disabled", false);
                    $("#bug_descripcion").prop("disabled", false);
                    $("#bug_pasos").prop("disabled", false);
                    $("#bug_reproducible").prop("disabled", false);
                    $("#bug_gravedad").prop("disabled", false);
                    $("#bug_anexo").prop("disabled", false);
                    $("#bug_estado_id").prop("disabled", true);
                    $("#bug_rechazo_id").prop("disabled", true);
                    $("#bug_nota_cambio").prop("disabled", true);
                    $("#bug_dispositivo").prop("disabled", false);
                    $("#bug_dispositivo_so").prop("disabled", false);
                    $("#bug_dispositivo_navegador").prop("disabled", false);
                }
            });


</script>
@endif


<style>
    .error {
    color: #F3A100;
    font-style: italic;
    font-weight: normal;   
    }

    .caja {
    font-family: sans-serif;
    font-size: 13px;
    font-style: italic;
    font-weight: 400;
    color: #304457;
    background: #E0F8F7;
    margin: 0 0 25px;
    overflow: hidden;
    padding: 5px;
    }
    
</style>


<!-- Script BuscarSistemaOperativo -->
<script>
    $(document).ready(function(){
        function cargarSistemaOperativo() {
            var bug_dispositivo = $('#bug_dispositivo').val();
            if ($.trim(bug_dispositivo) != '') {

                $.get('{!!URL::to('buscarVersionSistemaOperativo')!!}', {bug_dispositivo: bug_dispositivo}, function (data) {
		
                    var old = $('#bug_dispositivo_so').data('old') != '' ? $('#bug_dispositivo_so').data('old') : '';

                    $('#bug_dispositivo_so').empty();
                    $('#bug_dispositivo_so').append("<option value='' selected> Selecciona uno </option>");

                    $.each(data, function (index, value) {

                        console.log('DATA:', data);
                        $('#bug_dispositivo_so').append("<option value='" + value.sistemaOperativo_id + "'" + (old == value.sistemaOperativo_id ? 'selected' : '') + ">" + value.nombre_sistemaOperativo+"</option>");
                    })
                });
            }
        }
        cargarSistemaOperativo();
        $('#bug_dispositivo').on('change', cargarSistemaOperativo);
    });

</script>


<!-- Script buscarNavegador -->
<script>
    $(document).ready(function(){
        function cargarNavegador() {
            var bug_dispositivo = $('#bug_dispositivo').val();

            if ($.trim(bug_dispositivo) != '') {

                $.get('{!!URL::to('buscarNavegador')!!}', {bug_dispositivo: bug_dispositivo}, function (data) {

                    var old = $('#bug_dispositivo_navegador').data('old') != '' ? $('#bug_dispositivo_navegador').data('old') : '';

                    $('#bug_dispositivo_navegador').empty();
                    $('#bug_dispositivo_navegador').append("<option value='' selected> Selecciona uno </option>");

					
                    $.each(data, function (index, value) {

                        $('#bug_dispositivo_navegador').append("<option value='" + value.navegador_id + "'" + (old == value.navegador_id ? 'selected' : '') + ">" + value.nombre_navegador+"</option>");
                    })
                });
            }
        }
        cargarNavegador();
        $('#bug_dispositivo').on('change', cargarNavegador);
    });

</script>






