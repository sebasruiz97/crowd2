<!DOCTYPE html>
 
<html lang="en">
<head>

    <style>

        table.dataTable {
            box-sizing: content-box;
            width:100% !important;            
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);
    
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button  {
            box-sizing: border-box;
            display: inline-block;
            min-width: 1.5em;
            padding: 0.5em 1em;
            margin-left: 2px;
            text-align: center;
            text-decoration: none !important;
            cursor: pointer;
            *cursor: hand;
            color: #304457 !important;
            border: 1px solid transparent;
            border-radius: 2px;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover   {
    
            text-decoration: none !important;
            color: #ffff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);
        
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:focus  {
    
            text-decoration: none !important;
            color: #ffff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);

        }

        thead input {
        width: 100%;
        }
        
    
    </style>

    <script type="text/javascript">
        function go(miframe, url) {
            
            url2 = 'anexosBugs';
            document.getElementById(miframe).src = url2;
                    
        }
  
    </script>

    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>

    <script>
        
        $.extend( true, $.fn.dataTable.defaults, {
        "language": {
        "decimal": ",",
        "thousands": ".",
        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "infoPostFix": "",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "loadingRecords": "Cargando...",
        "lengthMenu": "Mostrar _MENU_ registros",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": ">",
            "previous": "<"
        },
        "processing": "Procesando...",
        "search": "Buscar:",
        "searchPlaceholder": "Término de búsqueda",
        "zeroRecords": "No se encontraron resultados",
        "emptyTable": "Ningún dato disponible en esta tabla",
        "aria": {
            "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "select": {
            "rows": {
                _: '%d filas seleccionadas',
                0: 'clic fila para seleccionar',
                1: 'una fila seleccionada'
            }
        }
        }           
        } );       
    </script>

</head>


<body>
 
    <!-- Modal -->
    <div class="container" >       
        
         <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm " >
                <div class="modal-content">
                    
                    <div class="modal-header modal-header-info " >

                        <table width="100%" >
                            <tr>
                              <th> <h1 style=" text-align:center"><i id="iconomodal" class="cambio" ></i></h1></th>
                              <th style=" text-align:center"> <h3 id="titulomodal" class="letra" style=" text-align:center" >Anexos del hallazgo</i></h3></th>
                              <th><button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color:white">×</button></th>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-body" style="padding-left:10px; padding-right:20px">
                        
                        <iframe width="100%" height="495" src="" id="miframe" style="color:transparent; border-color:transparent"></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- fin Modal -->
<div class="container" style="width:100%">
<br>

@if(auth()->user()->rol_id === 2 || auth()->user()->rol_id === 1 )
<a href="javascript:void(0)" class="btn btn-default btn-xs"  id="create-new-user">Reportar hallazgo</a>
<br><br>
@endif

<table class="table table-bordered table-striped" id="laravel_datatable" style="width:100%; font-size:13px" data-turbolinks="false">
   <thead>
      <tr>
        <th style="min-width: 50px">ID Hallazgo</th>
        <th style="min-width: 50px">Reportado por</th>
        <th style="min-width: 50px">Tipo de Hallazgo</th>
        <th style="min-width: 50px">Proyecto</th>
        <th style="min-width: 50px">Estado del proyecto</th>
        <th style="min-width: 50px">Cliente</th>
        <th style="min-width: 50px">Título</th>
        <th style="min-width: 50px">Descripción</th>
        <th style="min-width: 50px">Pasos</th>
        <th style="min-width: 50px">Reproducible</th>
        <th style="min-width: 50px">Gravedad</th>
        <th style="min-width: 50px">Dispositivo</th>
        <th style="min-width: 50px">Sistema Operativo</th>
        <th style="min-width: 50px">Navegador</th>
        <th style="min-width: 50px">Cliente</th>
        <th style="min-width: 50px">Usuario</th>
        <th style="min-width: 50px">Estado</th>
        <th style="min-width: 50px">Motivo Rechazo</th>
        <th style="min-width: 50px">Fecha de Aprobación del cliente</th>
        <th style="min-width: 50px">Fecha de rechazo del cliente</th>
        <th style="min-width: 50px">Cuenta de cobro en que la que pagó el bug</th>
        <th style="min-width: 50px">Acciones</th>
      </tr>
   </thead>
</table>
</div>
 
@include("bugs.form");
@include("bugs.show");
@include('flash::message')


</body>
<script>
    var SITEURL = '{{URL::to('')}}';
    var usuario = '{{auth()->user()->name}}';
    var rol = '{{auth()->user()->rol_id}}';

     $(document).ready( function () {

        // Setup - add a text input to each footer cell
        $('#laravel_datatable thead tr').clone(true).appendTo( '#laravel_datatable thead' );
        $('#laravel_datatable thead tr:eq(1) th').each( function (i) {

            var title = $(this).text();
           
            $(this).html( '<input type="text" style="font-size:12px; border-color:#304457; text-color:#ffff" placeholder="Buscar" />' );
    
   
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
         
       $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      var table = $('#laravel_datatable').DataTable({

        drawCallback: function( settings ) {

        var btns = $('.dt-button');
        btns.addClass('btn btn-info btn-xs');
        btns.removeClass('dt-button');

        var api = this.api();
        var datos = api.rows( {page:'current'} ).data();

            $.each(datos, function(i, item) 
            {      

                if(rol == 2){
                    $('.dt-buttons').hide();
                }

                if(item.nombre_estadoBug=="Aprobado por líder del proyecto" || item.nombre_estadoBug=="Rechazado por líder del proyecto" || item.nombre_estadoBug=="Aprobado por cliente" || item.nombre_estadoBug=="Rechazado por cliente")
                {
                
                    $("a[id='delete-user'][data-id='"+item.id_bug+"']").hide();
                    $("a[id='evidencia'][data-id='"+item.id_bug+"']").hide();
                    $("a[id='edit-user'][data-id='"+item.id_bug+"']").hide();
                }

                if(item.name!==usuario)
                {
                
                    $("a[id='delete-user'][data-id='"+item.id_bug+"']").hide();
                    $("a[id='evidencia'][data-id='"+item.id_bug+"']").hide();
                    $("a[id='edit-user'][data-id='"+item.id_bug+"']").hide();
                }

                if(item.ProyectoEstado_id!=2)
                {
                
                    $("a[id='delete-user'][data-id='"+item.id_bug+"']").hide();
                    $("a[id='evidencia'][data-id='"+item.id_bug+"']").hide();
                    $("a[id='edit-userLider'][data-id='"+item.id_bug+"']").hide();
                    $("a[id='evidenciaLider'][data-id='"+item.id_bug+"']").hide();
                }

              

            });

        },
         
            orderCellsTop: true,
            fixedHeader: false,
            processing: true,
            serverSide: true,
            scrollX: true,

 
            ajax: {
              url:  "bugs",
              type: 'GET',
             },
             columns: [
                      {data: 'id_bug', name: 'id_bug', 'visible': true},
                      {data: 'name', name: 'name', 'visible': true},
                      { data: 'nombre_tipoBug', name: 'nombre_tipoBug' },
                      { data: 'proyecto_nombre', name: 'proyecto_nombre' },
                      { data: 'estadoProyecto_Nombre', name: 'estadoProyecto_Nombre',  'visible': true },
                      { data: 'nombre_cliente', name: 'nombre_cliente','visible': false },
                      { data: 'bug_titulo', name: 'bug_titulo',
                      
                            render: function ( data, type, row ) {

                            if ( type !== 'display' ) {
                                return type === 'export' ?
                                data.replace( /[$,]/g, '' ) :
                                data;
                            }
                            return type === 'display' && data.length > 100 ?
                            data.substr( 0, 70 ) +'…' :
                            data;
                            }
                       },
                      { data: 'bug_descripcion', name: 'bug_descripcion', 'visible': false,
                            render: function ( data, type, row ) {

                                if ( type !== 'display' ) {
                                    return type === 'export' ?
                                    data.replace( /[$,]/g, '' ) :
                                    data;
                                }
                                return type === 'display' && data.length > 100 ?
                                data.substr( 0, 100 ) +'…' :
                                data;
                            }
                        },
                      { data: 'bug_pasos', name: 'bug_pasos', 'visible': false,
                            render: function ( data, type, row ) {

                                if ( type !== 'display' ) {
                                    return type === 'export' ?
                                    data.replace( /[$,]/g, '' ) :
                                    data;
                                }
                                return type === 'display' && data.length > 100 ?
                                data.substr( 0, 100 ) +'…' :
                                data;
                            }
                      },
                      { data: 'bug_reproducible', name: 'bug_reproducible' },
                      { data: 'bug_gravedad', name: 'bug_gravedad' },
                      { data: 'nombre_dispositivo', name: 'nombre_dispositivo' },
                      { data: 'nombre_sistemaOperativo', name: 'nombre_sistemaOperativo' },
                      { data: 'nombre_navegador', name: 'nombre_navegador' },
                      { data: 'nombre_cliente', name: 'nombre_cliente', 'visible': false },
                      { data: 'user_id', name: 'user_id', 'visible': false },
                      { data: 'nombre_estadoBug', name: 'nombre_estadoBug' },
                      { data: 'motivo_rechazoBug', name: 'motivo_rechazoBug' },
                      { data: 'fecha_aprobacion_cliente', name: 'fecha_aprobacion_cliente', 'visible': false },
                      { data: 'fecha_rechazo_cliente', name: 'fecha_rechazo_cliente', 'visible': false },
                      { data: 'cuentaCobro_id', name: 'cuentaCobro_id', 'visible': false },
                      {data: 'action', name: 'action', orderable: false, searchable: false,},
                   ],
            order: [[0, 'desc']],

            dom: 'Bfrtip',
                buttons: [
                    
                [{
                    extend: 'excel',
                    exportOptions: { orthogonal: 'export' }
                    
                }],
                
                [{
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: { orthogonal: 'export' }
                }],

                [{
                    extend: 'csv',
                    exportOptions: { orthogonal: 'export' }
                }],          

                [{
                    extend: 'print',
                    text: 'Imprimir',
                    exportOptions: { orthogonal: 'export' }
                }],
            ],
          });
     /*  Para crear nuevo bug */
        $('#create-new-user').click(function () {

            $('#btn-save').val("create-user");
            $('#id_bug').val('');
            $('#userForm').trigger("reset");
            $('#userCrudModal').html("Reportar nuevo hallazgo");
            $('#ajax-crud-modal').modal('show');
        });
     
       /* Para editar y cambiar estado*/
        $('body').on('click', '.edit-user', function () {
          var id_bug = $(this).data('id');
 

          $.get('bugs/' + id_bug +'/edit', function (data) {

             $('#name-error').hide();
             $('#email-error').hide();
             $('#userCrudModal').html("Editar");
              $('#btn-save').val("edit-user");
              $('#ajax-crud-modal').modal('show');
              $('#id_bug').val(data.id_bug);
              $('#bug_tipo_id').val(data.bug_tipo_id);
              $('#proyecto_id').val(data.proyecto_id);
              $('#bug_titulo').val(data.bug_titulo);
              $('#bug_descripcion').val(data.bug_descripcion);
              $('#bug_pasos').val(data.bug_pasos);
              $('#bug_reproducible').val(data.bug_reproducible);
              $('#bug_gravedad').val(data.bug_gravedad);
              $('#user_id').val(data.user_id);
              $('#cliente_id').val(data.cliente_id);
              $('#bug_estado_id').val(data.bug_estado_id);
              $('#bug_rechazo_id').val(data.bug_rechazo_id);
              $('#bug_dispositivo').val(data.bug_dispositivo);
              $('#bug_dispositivo_so').val(data.bug_dispositivo_so);

              var bug_dispositivo = $('#bug_dispositivo').val();

                if ($.trim(bug_dispositivo) != '') {

                    var old = $('#bug_dispositivo_so').data('old') != '' ? $('#bug_dispositivo_so').data('old') : '';

                    $('#bug_dispositivo_so').empty();
                    $('#bug_dispositivo_so').append("<option value='" + data.bug_dispositivo_so + "'" + (old == data.bug_dispositivo_so ? 'selected' : '') + ">" + data.nombre_sistemaOperativo+"</option>");

                }
                
                if ($.trim(bug_dispositivo) != '') {

                    var old = $('#bug_dispositivo_navegador').data('old') != '' ? $('#bug_dispositivo_navegador').data('old') : '';

                    $('#bug_dispositivo_navegador').empty();
                    $('#bug_dispositivo_navegador').append("<option value='" + data.bug_dispositivo_navegador + "'" + (old == data.bug_dispositivo_navegador ? 'selected' : '') + ">" + data.nombre_navegador+"</option>");

                }

          })
       });

        /* Para llevar a anexo*/
       $('body').on('click', '.evidencia', function () {         
        
        var id_bug = $(this).data('id');
        $('#id_bug').val(id_bug);

        url2 = 'anexoBug/'+id_bug;
        document.getElementById('miframe').src = url2;
       

       });


       /* Para ver Bug*/
       $('body').on('click', '.ver-bug', function () {
          var id_bug = $(this).data('id');

          $.get('bug/show/' + id_bug +'', function (data) {

            $('#VerBugModal').html("Reporte Detallado");                
            $('#ajax-crud-modal2').modal('show');

            $("#tr1").html("");
            $("#tr2").html("");
            $("#tr3").html("");
            $("#tr4").html("");
            $("#tr5").html("");
            $("#tr6").html("");
            $("#tr7").html("");
            $("#tr8").html("");
            $("#tr9").html("");
            $("#tr10").html("");
            $("#tr11").html("");
            $("#tr12").html("");
            $("#tr13").html("");
            $("#tr14").html("");
            $("#tr15").html("");
            
            for(var i=0; i<1; i++){
               
                var tr1 = `<tr>
                <td>`+data[0].id_bug+`</td>
                </tr>`;                
                var tr2 = `<tr>
                <td>`+data[0].nombre_tipoBug+`</td>
                </tr>`;
                var tr3 = `<tr>
                <td>`+data[0].nombre_reporta+`</td>
                </tr>`;
                var tr4 = `<tr>
                <td>`+data[0].proyecto_nombre+`</td>
                </tr>`;
                var tr5 = `<tr>
                <td>`+data[0].nombre_cliente+`</td>
                </tr>`;
                var tr6 = `<tr>
                <td>`+data[0].bug_titulo+`</td>
                </tr>`;
                var tr7 = `<tr>
                <td>`+data[0].bug_descripcion+`</td>
                </tr>`;
                var tr8 = `<tr>
                <td>`+data[0].bug_reproducible+`</td>
                </tr>`;
                var tr9 = `<tr>
                <td>`+data[0].bug_pasos+`</td>
                </tr>`;
                var tr10 = `<tr>
                <td>`+data[0].bug_gravedad+`</td>
                </tr>`;
                var tr11 = `<tr>
                <td>`+data[0].nombre_estadoBug+`</td>
                </tr>`;
                var tr12 = `<tr>
                <td>`+data[0].motivo_rechazoBug+`</td>;
                </tr>`;
                var tr13 = `<tr>
                <td>`+data[0].nombre_dispositivo+`</td>;
                </tr>`;
                var tr14 = `<tr>
                <td>`+data[0].nombre_sistemaOperativo+`</td>;
                </tr>`;
                var tr15 = `<tr>
                <td>`+data[0].nombre_navegador+`</td>;
                </tr>`;
    
                $("#tr1").append(tr1);
                $("#tr2").append(tr2);
                $("#tr3").append(tr3);
                $("#tr4").append(tr4);
                $("#tr5").append(tr5);
                $("#tr6").append(tr6);
                $("#tr7").append(tr7);
                $("#tr8").append(tr8);
                $("#tr9").append(tr9);
                $("#tr10").append(tr10);
                $("#tr11").append(tr11);
                $("#tr12").append(tr12);
                $("#tr13").append(tr13);
                $("#tr14").append(tr14);
                $("#tr15").append(tr15);
            }

            $("#cuerpo").html("");
            for(var i=0; i<data.length; i++){
                var tr = `<tr>
                <td>`+data[i].bug_cambio+`</td>
                <td>`+data[i].bug_notaCambio+`</td>
                <td>`+data[i].usuario_cambio+`</td>
                <td>`+data[i].fecha_cambio+`</td>
                </tr>`;
                $("#cuerpo").append(tr)
            }

            }) 
          
       });

       //Para eliminar
        $('body').on('click', '#delete-user', function () {
     
            var id_bug = $(this).data("id");
            if(confirm("Está seguro que desea eliminar!")){

               
            $.ajax({
                type: "get",
                url: "bug/delete/"+id_bug,
                success: function (data) {

                window.location.reload();
                var oTable = $('#laravel_datatable').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
          }
        });   
       });


  
    // Para enviar crear y actualizar
     
            $(document).ready(function(e){
            // Submit form data via Ajax

            if ($("#userForm").length > 0) {
            $("#userForm").validate({
        
            submitHandler: function(form) {

            $("#userForm").on('submit', function(e){
            
                $('input, select').prop('disabled', false);
                
                e.preventDefault();
                var formData = new FormData(document.getElementById("userForm"));
                formData.append('bug_anexo', $('#bug_anexo').prop('value'));
                

            var x = $('#id_bug').val(); 
      
            var actionType = $('#btn-save').val();            

            $('#btn-save').html('Enviando..');



            $.ajax({
                data: $('#userForm').serialize(),
                type: "post",
                url:  "bug/store",
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
	            processData: false,

                success: function (response) {    

                window.location.reload();

                $('#userForm').trigger("reset");
                $('#ajax-crud-modal').modal('hide');                
                $('#btn-save').html('Guardar cambios');

                $('#laravel_datatable').DataTable().ajax.reload();
                var oTable = $('#laravel_datatable').dataTable();
                oTable.fnDraw(false);
                },

                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Guardar cambios');
                }
              
            });
            
            });

            }
            });
            }
        });
        
    </script>
</html>

