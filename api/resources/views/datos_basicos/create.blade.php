@extends('layouts.app2')

@section('content')

<div> 
    <h3 class="centrar"> <strong>  Añadir mis datos Básicos </strong></h3>
</div> <br>
    <form method="POST" action="{{ route('datosBasicos.store') }}" enctype="multipart/form-data">
        @include('datos_basicos.fields_create')
    </form>
@endsection
