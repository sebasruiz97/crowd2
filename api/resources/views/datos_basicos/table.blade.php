<div class="container.fluid py-0 px-1">
    <div class="row">
		<div class="col-sm col-3 col-lg-3">
				<div class="profile-usertitle">
                    @foreach($datosBasicos as $datoBasico)
                    <img src="/storage/avatars/{{auth()->user()->avatar}}" class="img-circle" width="250px">
            </div>
        </div>
        <div class="col-sm col-9 col-lg-9">
            <div class="col-sm col-6 col-lg-6">
                <div class="table-responsive" >
                    <table class="table">
                        <tbody>  
                                <tr> 
                                    <th>Nombre</th>
                                    <td>{{ auth()->user()->name }}</td>
                                </tr>
                                <tr> 
                                    <th>Correo</th>
                                    <td>{{ auth()->user()->email }}</td>
                                </tr>
                                <tr> 
                                    @foreach($tiposDocumento as $documento)
                                    <th>Tipo de Documento</th>
                                    <td>{{$documento->nombre_tipoDocumento}}</td>
                                    @endforeach
                                </tr>
                                <tr> 
                                    <th>Número de Documento</th>
                                    <td>{{ auth()->user()->numeroDocumento }}</td>
                                </tr>
                                <tr> 
                                    <th>Fecha de Nacimiento</th>
                                    <td>{{ $datoBasico->fechaNacimiento }}</td>
                                </tr>
                                <tr> 
                                    <th>Genero</th>
                                    <td>{{ $datoBasico->genero }}</td>
                                </tr>
                                @endforeach
                            </tbody>  
                    </table>
                </div>
            </div>
            <div class="col-sm col-6 col-lg-6">
                <div class="table-responsive" >
                    <table class="table">
                        <tbody>  
                            @foreach($datosBasicos as $datoBasico)
                                
                                <tr> 
                                    <th>Ocupación actual</th>
                                    <td>{{ $datoBasico->ocupacion->nombre_ocupacion }}</td>
                                </tr>
                                <tr> 
                                    <th>Pais</th>
                                        <td>{{ $datoBasico->pais->nombre_pais }}</td>
                                    </tr>
                                <tr> 
                                    <th>Departamento</th>
                                    <td>{{ $datoBasico->departamento->nombre_departamento }}</td>
                                </tr>
                                <tr> 
                                    <th>Municipio</th>
                                    <td>{{ $datoBasico->municipio->nombre_municipio }}</td>
                                </tr>
                                <tr> 
                                    <th>Dirección</th>
                                    <td>{{ $datoBasico->direcion }}</td>
                                </tr>
                                <tr> 
                                    <th>Teléfono Celular</th>
                                    <td>{{ $datoBasico->telefonoCelular }}</td>
                                </tr>
                                @endforeach
                            </tbody>  
                    </table>
                </div>
            </div>
        </div>
    <div>    
<div>


