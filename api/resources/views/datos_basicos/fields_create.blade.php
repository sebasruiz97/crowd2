@csrf
@inject('paises','App\Services\paises')
@inject('ocupaciones','App\Services\ocupaciones')



<div class="form-group col-sm-12">
<div class="form-group col-sm-2"> </div>
<div class="form-group col-sm-8">

<!-- Nombre -->
<div class="form-group col-sm-6">
    <label for="name">¿Cuál es tu nombre?*:</label>
    <input class="form-control" name="name" value="{{old('name', $user->name)}}" required readonly>
    @if ($errors->has('name'))
    <span class="help-block" style="color:#F3A100"; >
        <small><i>{{ $errors->first('name') }}</i></small>
    </span>
@endif
</div>

<!-- Fecha de nacimiento -->
<div class="form-group col-sm-6">
    <label for="fechaNacimiento">¿Cuando naciste?*</label>
    <input class="form-control" name="fechaNacimiento" value="{{old('fechaNacimiento')}}" type="date" id="fechaNacimiento"  min="1930-12-31" required >
    @if ($errors->has('fechaNacimiento'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('fechaNacimiento') }}</i></small>
        </span>
    @endif
</div>

<!-- Genero -->
<div class="form-group col-sm-6">
    <label for="genero">¿Cuál es tu género?*</label>
    <select class="form-control" value="{{old('genero')}}" id="genero" name="genero" required>
        <option value= ''> Selecciona uno  </option>
        <option> Femenino </option>
        <option> Masculino </option>
        <option> Prefiero no decirlo </option>
    </select>
    @if ($errors->has('genero'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('genero') }}</i></small>
        </span>
    @endif
</div>

<!-- Pais Field -->
<div class="form-group col-sm-6">
    <label for="pais_id">¿En cuál país vives?*</label>
    <select class="form-control" id="pais_id" value="{{old('pais_id')}}" name="pais_id" required>
        @foreach($paises->get() as  $index => $paises)
            <option value = "{{ $index }}" {{old('pais_id') == $index ? 'selected' : ''}} > {{  $paises }}</option>
        @endforeach
    </select>
    @if ($errors->has('pais_id'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('pais_id') }}</i></small>
        </span>
    @endif
</div>

<!-- Departamento Field -->
<div class="form-group col-sm-6">
    <label for="departamento_id">¿En cuál departamento vives?</label>
    <select class="form-control" data-old="{{ old('departamento_id') }}" id="departamento_id" name="departamento_id" required>
        <option value= ''> </option>
    </select>
    @if ($errors->has('departamento_id'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('departamento_id') }}</i></small>
        </span>
    @endif
</div>

<!-- Municipio Field -->
<div class="form-group col-sm-6">
    <label for="municipio_id">¿En cuál municipio vives?</label>
    <select class="form-control" data-old="{{ old('municipio_id') }}" id="municipio_id" name="municipio_id" required>
        <option value= ''> </option>
    </select>
    @if ($errors->has('municipio_id'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('municipio_id') }}</i></small>
        </span>
    @endif
</div>

<!-- Dirección -->
<div class="form-group col-sm-6">
    <label for="direcion">¿Cuál es tu dirección?</label>
    <input class="form-control" name="direcion" value="{{old('direcion')}}" type="text" id="direcion" minlength="6"  maxlength="100" pattern="[A-Za-z 0-9$@$!%*#-?&]{6,100}" title="Tu dirección debe contener entre 6 y 100 caracteres">
    @if ($errors->has('direcion'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('direcion') }}</i></small>
        </span>
    @endif
</div>


<!-- Teléfono Fijo -->
<div class="form-group col-sm-6">
    <label for="telefonoFijo">¿Cuál es tu teléfono fijo?</label>
    <input class="form-control" name="telefonoFijo" value="{{old('telefonoFijo')}}" type="text" id="telefonoFijo"  minlength="6"  maxlength="10" pattern="[0-9]{6,10}" title="Tu telefono debe ser un número y contener entre 6 y 10 caracteres">
    @if ($errors->has('telefonoFijo'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('telefonoFijo') }}</i></small>
        </span>
    @endif
</div>

<!-- Teléfono Celular -->
<div class="form-group col-sm-6">
    <label for="telefonoCelular">¿Cuál es tu número de celular?*</label>
    <input class="form-control" name="telefonoCelular" value="{{old('telefonoCelular')}}" type="text" id="telefonoCelular" minlength="6"  maxlength="10" required pattern="[0-9]{6,10}" title="Tu telefono debe ser un número y contener entre 6 y 10 caracteres">
    @if ($errors->has('telefonoCelular'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('telefonoCelular') }}</i></small>
        </span>
    @endif
</div>

<!-- Ocupacion Actual -->
<div class="form-group col-sm-6">
    <label for="ocupacion_id">¿Cuál es tu ocupación actual?*</label>
    <select class="form-control" id="ocupacion_id" value="{{old('ocupacion_id')}}" name="ocupacion_id" required>
        @foreach($ocupaciones->get() as  $index => $ocupaciones)
            <option value = "{{ $index }}" {{old('ocupacion_id') == $index ? 'selected' : ''}} > {{  $ocupaciones }}</option>
        @endforeach
    </select>
    @if ($errors->has('ocupacion_id'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('ocupacion_id') }}</i></small>
        </span>
    @endif
</div>

<!-- Experiencia en Pruebas -->
<div class="form-group col-sm-6">
    <label for="experienciaPruebas">¿Tienes experiencia en pruebas de software?*</label>
    <select class="form-control" id="experienciaPruebas" value="{{old('experienciaPruebas')}}" name="experienciaPruebas" required>
        <option value= ''> Selecciona uno  </option>
        <option> Si </option>
        <option> No </option>
    </select>
    @if ($errors->has('experienciaPruebas'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('experienciaPruebas') }}</i></small>
        </span>
    @endif
</div>

<!-- Avatar -->
<div class="form-group col-sm-6">
    <label for="avatar" >¿Quieres agregar una foto?</label>
    <small><input type="file" name="avatar"></small>
</div>

<!-- UserID Field -->
<div class="form-group col-sm-6">
    <label for="user_id" > </label>
    <input class="form-control" name="user_id" type="hidden" value={{auth()->user()->id}} readonly="readonly" >
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <div class="centrar">
    <input class="btn btn-primary" type="submit" value="Guardar">
    <a href={{ route('datosBasicos.index') }} class="btn btn-default">Cancelar</a>
    <div>
</div>

</div>
</div>
<div class="form-group col-sm-2"></div>
</div>


<!-- Script BuscarDepartamento -->
<script>
    $(document).ready(function(){
        function cargarDepartamento() {
            var pais_id = $('#pais_id').val();
            if ($.trim(pais_id) != '') {
				console.log("llamare a findMunicipio con ",pais_id);
                $.get('{!!URL::to('buscarDepartamento')!!}', {id_pais: pais_id}, function (data) {
					console.log("departamentos = ",data);
                    var old = $('#departamento_id').data('old') != '' ? $('#departamento_id').data('old') : '';

                    $('#departamento_id').empty();
                    $('#departamento_id').append("<option value='' selected> Selecciona una </option>");

					
                    $.each(data, function (index, value) {

                        $('#departamento_id').append("<option value='" + value.id_departamento + "'" + (old == value.id_departamento ? 'selected' : '') + ">" + value.nombre_departamento+"</option>");
                    })
                });
            }
        }
        cargarDepartamento();
        $('#pais_id').on('change', cargarDepartamento);
    });

</script>

<!-- Script BuscarMunicipio-->
<script>
    $(document).ready(function(){
        function cargarMunicipio() {
            var departamento_id = $('#departamento_id').val();
            if ($.trim(departamento_id) != '') {
				console.log("llamare a findMunicipio con ",departamento_id);
                $.get('{!!URL::to('buscarMunicipio')!!}', {id_departamento: departamento_id}, function (data) {
					console.log("municipios = ",data);
                    var old = $('#municipio_id').data('old') != '' ? $('#municipio_id').data('old') : '';

                    $('#municipio_id').empty();
                    $('#municipio_id').append("<option value='' selected> Selecciona una </option>");

					
                    $.each(data, function (index, value) {

                        $('#municipio_id').append("<option value='" + value.id_municipio + "'" + (old == value.id_municipio ? 'selected' : '') + ">" + value.nombre_municipio+"</option>");
                    })
                });
            }
        }
        cargarMunicipio();
        $('#departamento_id').on('change', cargarMunicipio);
    });

</script>





