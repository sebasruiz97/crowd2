@extends('layouts.app2')

@section('content')


<div class="form-group col-sm-12">
    <div class="form-group col-sm-4"> </div>
    <div class="form-group col-sm-4">


<div>
    <h3 class="centrar"> <strong> Actualizar mi contraseña </strong></h3>
</div> <br>

<form method="POST" action="{{ route('actualizarContrasena', auth()->user()->id) }}" >

    @method('PUT')
    @csrf

    <div>
        <label for="current_password"> Contraseña actual*</label>
        <input type="password" class="form-control" name="current_password">
        @if ($errors->has('current_password'))
        <span class="help-block" style="color:#F3A100" ;>
            <small><i>{{ $errors->first('current_password') }}</i></small>
        </span>
        @endif
    </div>

    <div>
        <label for="password"> Nueva Contraseña*</label>
        <input type="password" class="form-control" name="password" >
        @if ($errors->has('password'))
        <span class="help-block" style="color:#208297" ;>
            <small><i>{{ $errors->first('password') }}</i></small>
        </span>
        @endif
    </div>

    <div>
        <label for="password_confirmation"> Confirma tu nueva contraseña*</label>
        <input type="password" name="password_confirmation" class="form-control">
        @if ($errors->has('password'))
        <span class="help-block" style="color:#208297" ;>
            <small><i>{{ $errors->first('password') }}</i></small>
        </span>
        @endif
    </div>

    <!-- Submit Field -->
    <br>
    <div class="form-group col-sm-12">
        <div class="centrar">
            <input class="btn btn-primary" type="submit" value="Guardar">
            <a href={{ route('datosBasicos.index') }} class="btn btn-default">Cancelar</a>
            <div>
            </div>

        </div>
    </div>
    <div class="form-group col-sm-4"></div>
    </div>

</form>
@endsection