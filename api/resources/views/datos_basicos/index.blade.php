@extends('layouts.app2')

@section('content')

    @if ($datosBasicos->isEmpty()) 
         <h1 class="pull-right">
            <a class="btn btn-default btn-xs pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px" href="{{ route('datosBasicos.create') }}">Añadir mis datos básicos</a>
        </h1>
        <br> 
        <br> 
        <br> 
        <br>
        <br>
             <div style="text-align:center">
                    <h3 >Aún no has agregado tus datos básicos</h3>
 <h5>
	Recuerda que esta información es necesaria para poder participar en los proyectos, sólo cuando hayas completado tu perfil quedarás activo en la plataforma.
</h5>
            <div>
                @else
                             
                    @foreach($datosBasicos as $datoBasico)
                        <h1 class="pull-right">
                            <form action="{{ route('datosBasicos.destroy',$datoBasico->id_datoBasico) }}" method="POST">
                            @csrf
                            @method('DELETE')
                                <a  href="{{ route('datosBasicos.edit',auth()->user()->id) }}" class="btn btn-default btn-xs" style="margin-top: -10px;">Editar</i></a>
                            </form>
                        </h1>
                    @endforeach
                    <h3  style="text-align:left"> Datos Básicos </h3> <br>
                    @include('flash::message')
                    @include('datos_basicos.table')


                    <h1 class="pull-right">
                        <form
                            action={{ route('desactivar',auth()->user()->id) }} method="GET">
                            
                            <div>
                                <a href={{ route('editarContrasena',auth()->user()->id) }} class="btn btn-info btn-xs" style="margin-bottom: 5px;"> Cambiar mi contraseña </a>
                                <button type="submit" class="btn btn-info btn-xs" style="margin-bottom: 5px" onclick="return confirm('Al desactivar tu cuenta, ya no podrás ingresar nuevamente a Crowd, ni recuperar tu perfil. ¿Estás seguro?')"> Desactivar mi cuenta</button>
                            </div>
                        </form>
                    </h1>

                

            @endif
@endsection
