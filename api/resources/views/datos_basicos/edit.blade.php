@extends('layouts.app2')

@section('content')

   <div> 
       <h3 class="centrar"> <strong>  Editar mis datos Básicos </strong></h3>
   </div> <br>
                @foreach($datosBasicos as $datoBasico)
                        <form action="{{ route('datosBasicos.update', $datoBasico->id_datoBasico) }}" method="POST" enctype="multipart/form-data" >

                            @method('PUT')
                            @include('datos_basicos.fields_edit')
                    
                        </form>
                @endforeach
@endsection

