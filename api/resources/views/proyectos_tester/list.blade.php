<!DOCTYPE html>
 
<html lang="en">
<head>
   
  

    <link rel="stylesheet" type="text/css" href="http://gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css">
    <script type="text/javascript" src="http://gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/js/dataTables.checkboxes.min.js"></script>
    
    <style>
        .dt-checkboxes
        {
            width: 14px;
            height: 14px;
        }
        th { font-size: 11px; }
        td { font-size: 10px; }
    
        input[type=text] 
        {
          transform: scale(0.85);
        } 
        div.container {
        width: 80%;
    }
    </style>

    
    <style>

        table.dataTable {
            box-sizing: content-box;
            width:100% !important;            
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);

        }

        .dataTables_wrapper .dataTables_paginate .paginate_button  {
            box-sizing: border-box;
            display: inline-block;
            min-width: 1.5em;
            padding: 0.5em 1em;
            margin-left: 2px;
            text-align: center;
            text-decoration: none !important;
            cursor: pointer;
            *cursor: hand;
            color: #304457 !important;
            border: 1px solid transparent;
            border-radius: 2px;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover   {

            text-decoration: none !important;
            color: #ffff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);
        
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:focus  {

            text-decoration: none !important;
            color: #ffff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);

        }

        thead input {
        width: 100%;
        }
      
    </style>

    <script>
        
        $.extend( true, $.fn.dataTable.defaults, {
        "language": {
        "decimal": ",",
        "thousands": ".",
        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "infoPostFix": "",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "loadingRecords": "Cargando...",
        "lengthMenu": "Mostrar _MENU_ registros",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": ">",
            "previous": "<"
        },
        "processing": "Procesando...",
        "search": "Buscar:",
        "searchPlaceholder": "Término de búsqueda",
        "zeroRecords": "No se encontraron resultados",
        "emptyTable": "Ningún dato disponible en esta tabla",
        "aria": {
            "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "select": {
            "rows": {
                _: '%d filas seleccionadas',
                0: 'clic fila para seleccionar',
                1: 'una fila seleccionada'
            }
        }
        }           
        } );       
    </script>
</head>
<body>

<th style=" text-align:center"> <h3  style=" text-align:center" >Buscar testers para el proyecto</i></h3></th> <br>

@include('flash::message')

{{-- @include("proyectos_tester.form"); --}}
      <table class="display table table-bordered table-striped  nowrap" id="laravel_datatable" width="100%" >
        <thead style="width:auto">
            <tr>
              <th></th>
              <th style="min-width: 50px;">Id</th>
              <th style="min-width: 10px">Nombre</th>
              {{-- <th style="min-width: 50px">Genero</th> --}}
              <th style="min-width: 10px">Tipos Pruebas</th>                
              <th style="min-width: 10px">Experiencia</th>
              <th style="min-width: 10px">Herramientas</th>
              <th style="min-width: 10px">Dispositivos</th>
              {{-- <th>Sistema Operativo</th> 
              <th>Navegadores</th>         --}}
              {{-- <th>Accion</th> --}}
            </tr>
        </thead>
        
      </table>
      <table>
        <tr>
          <td><a href="javascript:void(0)" class="btn btn-default btn-xs" id="asignar">Enviar Invitaciones</a></td>
          <pre id="datos_sel"></pre>
        </tr>
      </table>
      {{-- <td>   <p><b>Testers seleccionados:</b></p> --}}
      </td>

      
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>

</body>
<script>
    var id = "{{$id_proyecto ?? ''}}";

     $(document).ready( function () 
     {
          // Setup - add a text input to each footer cell
          $('#laravel_datatable thead tr').clone(true).appendTo( '#laravel_datatable thead' );
          $('#laravel_datatable thead tr:eq(1) th').each( function (i) 
          {
              if(i>0 && i<10)
              {
                    var title = $(this).text();
                    $(this).html( '<input type="text" style="font-size:12px; border-color:#304457; text-color:#ffff" placeholder="Buscar" />' );          
          
                  $( 'input', this ).on( 'keyup change', function () 
                  {
                      if ( table.column(i).search() !== this.value ) 
                      {
                          table
                              .column(i)
                              .search( this.value )
                              .draw();
                      }
                  } );
              }
              
          });
          $.ajaxSetup(
          {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          var table =$('#laravel_datatable').DataTable(
          {
              pageLength: 25,
              responsive: true,
              processing: true,
              serverSide: true,
              orderCellsTop: true,
              fixedHeader: true,
              select: true,
              ajax: 
              {
                  url:  "proyectos_tester",
                  data:{id:id},
                  type: 'GET',
              },
              columnDefs: 
              [
                  {
                      targets: 0,
                     
                      checkboxes: 
                      {
                        selectRow: true
                      },
                      
                  },

              ],

         
             columns: 
             [
                  { data: 'user_id', name: 'user_id' },
                  { data: 'user_id', name: 'user_id' },
                  { data: 'name', name: 'name' },
                //   { data: 'genero', name: 'genero' },
                  { data: 'Tipos_Pruebas', name: 'Tipos_Pruebas' },
                  { data: 'Experiencia', name: 'Experiencia' },
                  { data: 'Herramientas_Conocidas', name: 'herramientas_conocidas' },
                  { data: 'Dispositivos_Usados', name: 'dispositivos_usados' },
                //   { data: 'Sistema_Operativo_Dispositivo', name: 'sistema_operativo_dispositivo' },
                //   { data: 'Navegadores_Sistema_Operativo_Dispositivo', name: 'navegadores_sistema_operativo_dispositivo' },
                //   { data: 'action', name: 'action', orderable: false},
              ],
              order: [[1, 'desc']]
          });

          new $.fn.dataTable.FixedHeader( table );
          


          /// Caundo se hace click en enviar invitaciones
          $("#asignar").click(function(e)
          {
              var form = this;
              var rows_selected = table.column(0).checkboxes.selected();
              // Iterate over all selected checkboxes
              $.each(rows_selected, function(index, rowId)
              {
                  // Create a hidden element 
                  $(form).append
                  (
                      $('<input>')
                          .attr('type', 'hidden')
                          .attr('name', 'id[]')
                          .val(rowId)
                  );
              });
              // muestra los datos seleccionados    
              $('#datos_sel').text(rows_selected.join(","));
              var data_sel = $('#datos_sel').text();
              var idp= id;
              $('#asignar').html('Se están enviando las invitaciones, por favor espere...');
         
              if (data_sel.length>0)
              {
                  $.ajax(
                  {
                      data:{data_sel:data_sel, idp:idp},
                      url:  "proyectos_tester/asignar",
                      type: "GET",
                      success: function (data) 
                      {
                        location.reload();

      
                      },
                      error: function (data) 
                      {
                          console.log('Error:', data);
                          $('#btn-save').html('Guardar cambios');
                      }
                  });
              }
              else
              {
                  alert("Debe seleccionar al menos 1 tester para enviar invitaciones");
              }
              // remueve datos adicionados
              $('input[name="id\[\]"]', form).remove();
          });   
      });
</script>
</html>