

<a href="javascript:void(0)" 
    data-toggle="tooltip"  
    {{-- data-id="{{ $id }}"  --}}
    data-original-title="Edit" 
    class="edit btn btn-default btn-xs edit-user" 
    title="Editar"> 
    <i class="lnr lnr-pencil"></i>
</a>

<a href="javascript:void(0);" id="delete-user" 
    data-toggle="tooltip" 
    data-original-title="Delete" 
    {{-- data-id="{{ $id }}"  --}}
    class="delete btn btn-danger btn-xs" title="Eliminar">  
    <i class="lnr lnr-cross"></i>
</a>



{{-- 
    @include("proyectos_responsables.menu") --}}