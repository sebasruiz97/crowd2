<!-- Este layout es GENERAL -->

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>{{config('app.name')}}</title
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TSBNZVN');
    </script>
    <!-- End Google Tag Manager -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-186249984-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-186249984-1');
    </script>


    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Bootstrap 3.3.7 -->

    <link rel="stylesheet" href="{{ asset('/storage/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/storage/css/iconos.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- iCheck -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">

    <style>
        .profile-content {
            padding: 1px;
            margin-right: 5px;
            margin-left: 5px;
            background: #fff;
            min-height: 100px;
            margin-top: 20px !important;
        }

        .navbar-default2 {
            background-color: #ffffff !important;
            border-color: #ffffff !important;
            height: 71px;
        }

        .navbar-nav2 {
            color: #208297 !important;
            font-size: 15px;
            text-align: justify;
            list-style-type: none !important;

        }

        .navbar-nav2>li>a {
            padding-top: 10px;
            padding-bottom: 10px;
            padding-right: 30px;
            line-height: 50px;
            color: #208297 !important;
            text-decoration: none !important;

        }

        .navbar-nav2>li>a:hover,
        .navbar-nav2>li>a:focus {
            color: #9dcdd7 !important;
            background-color: transparent !important;
            border-bottom: 4px solid #208297 !important;
            padding-bottom: 25px;

        }
    </style>
    {!! htmlScriptTagJsApi() !!}
</head>


<body class="skin-blue sidebar-mini" style="overflow:auto">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TSBNZVN" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div>
        <nav class="navbar navbar-default2 01">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="/storage/images/logo_crowd_leyenda.png" width="35%" />
                    </a>
                </div>
                <div class="pull-right">
                    <ul class="navbar-nav2">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/') }}">Sobre nosotros</a>
                            <a class="nav-link" href="{{ route('contacto') }}">Contáctanos</a>
                            @auth
                            @if(auth()->user()->hasRoles(['1','2','3']))
                            <a class="nav-link" href="{{ route('datosBasicos.index') }}">Mi perfil</a>
                            @endif
                            @if(auth()->user()->hasRoles(['4']))
                            <a class="nav-link" href="{{ route('cobros.index') }}">Mi perfil</a>
                            @endif
                            @else
                            <a class="nav-link" href="{{ route('login') }}">Ingresar</a>
                            <a class="nav-link" href="{{ route('register') }}">Registrarme</a>
                            @endauth
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    @yield('content')

    <!-- jQuery 3.1.1 -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    @yield('scripts')

</body>

<footer style="font-size: 12px; text-align:center; color:#C1C4C7">
    {{ config('app.name') }} | Copyright @ {{ date('Y') }}
</footer>

</html>