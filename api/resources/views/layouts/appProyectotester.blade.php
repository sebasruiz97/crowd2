<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    
 



  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Crowd</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>  
  {{-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  
  <link  href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css" rel="stylesheet">
  <link  href="https://cdn.datatables.net/fixedheader/3.1.7/css/fixedHeader.dataTables.min.css" rel="stylesheet">
  
  <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
  <script src=" https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>


  <link rel="stylesheet" href="{{ asset('/storage/css/bootstrap.css') }}">
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" > --}}

  <link rel="stylesheet" href="{{ asset('/storage/css/iconos.css') }}">
    <style>
      thead input {
         width: 100%;
      }

    .dropdown-toggle:focus {
      outline: 0;
      color: #304457;
    }
    .dropdown-menu {
      position: absolute;
      top: 100%;
      left: 0;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 100PX;
      padding-left: 10px;
      /* padding: 5px 0;
      margin: 2px 0 0; */
      font-size: 14px;
      text-align: left;
      list-style: none;
      background-color: #304457!important;
      -webkit-background-clip: padding-box;
              background-clip: padding-box;
      border: 1px solid #304457;
      border: 1px solid rgba(0, 0, 0, .15);
      border-radius: 4px;
      -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
              box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
    }
    .dropdown-menu.pull-right {
      right: 0;
      left: auto;
    }
    .dropdown-menu .divider {
      height: 1px;
      margin: 9px 0;
      overflow: hidden;
      background-color: #304457!important;
    }
    .dropdown-menu > li > a {
      display: block;
      padding: 3px 20px;
      clear: both;
      font-weight: normal;
      line-height: 1.42857143;
      color: #ffff!important;
      white-space: nowrap;
    }
    .dropdown-menu > li > a:hover,
    .dropdown-menu > li > a:focus {
      color: #f3a100!important;
      text-decoration: none;
      background-color: transparent;
    }
    .dropdown-menu > .active > a,
    .dropdown-menu > .active > a:hover,
    .dropdown-menu > .active > a:focus {
      color: #fff!important;
      text-decoration: none;
      background-color: #f3a100!important;
      outline: 0;
    }
    .dropdown-menu > .disabled > a,
    .dropdown-menu > .disabled > a:hover,
    .dropdown-menu > .disabled > a:focus {
      color: #304457!important;
    }

    .open > .dropdown-menu {
      display: block;
      border-top: 2px solid #f3a100!important;
    }

    .dropdown-header {
      display: block;
      padding: 3px 20px;
      font-size: 12px;
      line-height: 1.42857143;
      color: #304457 !important;
      white-space: nowrap;
    }

    .navbar-default .navbar-collapse,
    .navbar-default .navbar-form {
      border-color: #304457!important;
    }
    .navbar-default .navbar-nav > .open > a,
    .navbar-default .navbar-nav > .open > a:hover,
    .navbar-default .navbar-nav > .open > a:focus {
      color: #F3A100!important;
      background-color: #304457!important;
    }

    .navbar-default .navbar-nav>li>a {
    color: #fff!important;
    }

    .dropdown-menu > li > a:hover,
    .dropdown-menu > li > a:focus {
      color: #f3a100!important;
      text-decoration: none;
      background-color: transparent!important;
    }

}

  </style>
   <style>
      .modal-sm {
         width: 80%; /* New width for large modal */
         height: 500px;
         background-color:#BBD6EC;
      }

      .navbar-default {
          background-color: #304457!important;
          border-color: #304457!important;
      }

          .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
          color: #f3A100!important;
          background-color: transparent!important;
        }

      
  </style>
  <script type="text/javascript">
      function go(miframe, url) {
          document.getElementById(miframe).src = url;
      }
  </script>
    
</head>



<div class="main">

  

    @yield('content')

    

</div>


    <!-- jQuery 3.1.1 -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    

    <script src="{{ asset("storage/vendor/jquery-steps/jquery.steps.min.js") }}"></script>
    <script src="{{ asset("storage/js/main.js") }}"></script>



  @yield('scripts')

  <script>

    var pageURL = $(location).attr("href");

    var elements = document.getElementsByClassName("title-text")
    for (var i = 0; i < elements.length; i++) {
   
      //if you want to change bg image
      //if you want to change bg color
      
      if (( elements[i].href)==pageURL)
      {

     // elements[i].style.backgroundColor="" ;
      elements[i].style.color="#304457" ;
      }
    }
  </script>
  

</body>



</html>