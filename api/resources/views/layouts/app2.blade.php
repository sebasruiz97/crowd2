<!-- Este layout es para perfil del tester -->

<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>{{config('app.name')}}</title <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('/storage/css/bootstrap.css') }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('/storage/css/iconos.css') }}">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" href="css/chats.css">

  <script>
    $(document).ready(main);

    var contador = 1;

    function main() {

      // var pageURL = $(location).attr("href");
      /*var elements = document.getElementsByClassName("title-text")
      for (var i = 0; i < elements.length; i++) {

        //if you want to change bg image
        //if you want to change bg color

        if ((elements[i].href) == pageURL) {

          // elements[i].style.backgroundColor="" ;
          elements[i].style.color = "#304457";
        }
      }*/

      $('.menu_bar').click(function() {
        // $('nav').toggle(); 

        if (contador == 1) {
          $('nav').animate({
            left: '0'
          });
          contador = 0;
        } else {
          contador = 1;
          $('nav').animate({
            left: '-100%'
          });
        }

      });

    };
  </script>

  <?php
  $route = (in_array(\Request::route()->getName(), ['user', config('chatify.path')])) ? 'user' : \Request::route()->getName();
  if (\Request::route()->getName() != config('chatify.path')) {
    $id = !isset($id) ? 0 : $route . '_' . $id;
  }
  $dark_mode = auth()->user()->dark_mode < 1 ? 'light' : 'dark';
  $messengerColor = auth()->user()->messenger_color;
  ?>
  @include('chatify.layouts.headLinks')
  @yield('headLinks')

</head>

<style>
  .border-right {
    border: 1px solid;
    border-width: 0 1px 0 0;
    border-color: #e7e7e7;
  }

  .chatPointer {
    border: 1px solid #3f8297;
    border-width: 0 0 1px;
    padding-bottom: 5px;
    margin-bottom: 5px;
  }

  .chatPointer:hover {
    background-color: #e7e7e7;
  }

  .chatPointer:last-child {
    margin-bottom: 0;
    border: none;
  }

  .chatPointer>div {
    padding: 0px 10px;
    cursor: pointer
  }

  .chatPointer.new {
    background: #99c9d3;
    color: white;
    text-shadow: grey 1px 1px;
    border: gray 1px solid;
    border-width: 1px 0;
  }

  .panel-body {
    height: 80vh;
    padding: 0;
    overflow: auto;
    max-height: 300px;
    margin-bottom: 42px !important;
  }

  .messages {
    background: #eaeaea;
  }

  .messages.typing {
    padding-bottom: 35px;
  }

  .typing-indicator {
    display: block;
    margin-top: -35px;
  }

  .listAlerts li {
    margin-bottom: 5px;
    cursor: pointer;
  }

  .listAlerts li.new {
    background: #aacdd7;
  }

  .listAlerts li>label {
    margin: 0;
  }

  .listAlerts li>p {
    font-size: 10px;
    width: 225px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  #badgeAlerta {
    position: absolute;
    margin: -10px;
    background: red;
  }

  #iconMsg {
    position: absolute;
    margin: -10px;
    background: #448ca3;
  }

  #bodyChat.new .panel-heading {
    background: #448ca3;
  }
</style>

<nav class="navbar navbar-default">
  <div class="container-fluid">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="{{ url('/') }}">
        <img src="/storage/images/logo_crowd_leyenda.png" width="35%" />
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right" style="display: flex; align-items-center;">

        {{-- Para usuario --}}

        <!--<li class="dropdown border-right">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding:20px 20px 15px;" title="Alertas">
            <i id="iconoAlerta" class="h3 icon-submenu lnr lnr-bullhorn"></i>
            @if(!alerts('contador')->isEmpty())
            <span id="badgeAlerta" class="badge">{{ count(alerts('contador')) }}</span>
            @endif
          </a>
          <ul id="listAlerts" class="dropdown-menu listAlerts">
            @if(alerts()->isEmpty())
            <li style="text-align: center;">Sin alertas</li>
            @endif
            @foreach(alerts() as $alerta)
            <li class="{{ $alerta->estado == 'enviada'? 'new' : '' }}">
              <a href="{{ $alerta->alerta->enlace }}" target="_blank" rel="noopener noreferrer">
                <label>{{ $alerta->alerta->titulo }}</label>
                <p>{{ $alerta->alerta->mensaje }}</p>
              </a>
            </li>
            @endforeach
          </ul>
        </li>-->

        <li class="dropdown border-right">
          <a id="sectIconMsg" href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding:20px 20px 15px;" title="Mensajes">
            <i class="h3 icon-submenu lnr lnr-bubble"></i>
            @if(!newMessages()->isEmpty())
            <span id="iconMsg" class="badge">{{ count(newMessages()) }}</span>
            @endif
          </a>
          <ul class="dropdown-menu" style="max-height: 215px; overflow: auto;">
            {{-- Contact --}}
            <div class="listOfContacts" style="width: 100%;height: calc(100% - 200px);"></div>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="/storage/avatars/{{auth()->user()->avatar}}" width="30px" class="img-circle" alt="Avatar"> <span>{{auth()->user()->name}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
          <ul class="dropdown-menu">
            @if(auth()->user()->hasRoles(['2']))
            <li><a href="{{ route('datosBasicos.index') }}"><i class="lnr lnr-user"></i> <span>Mi Perfil</span></a></li>
            <li><a href="{{ route('puntosTester',auth()->user()->id) }}"><i class="lnr lnr-thumbs-up"></i> <span>Mis puntos </span></a></li>
            @endif
            @if(auth()->user()->hasRoles(['3']))
            <li><a href="{{ route('datosBasicos.index') }}"><i class="lnr lnr-user"></i> <span>Mi Perfil</span></a></li>
            <li><a href="{{ route('clientes.index') }}"><i class="lnr lnr-sync"></i> <span>Clientes</span></a></li>
            <li><a href="{{ route('proyectos.index') }}"><i class="lnr lnr-cog"></i> <span>Proyectos</span></a></li>
            <li><a href="{{ route('bugs.index') }}"><i class="lnr lnr-bug"></i> <span>Hallazgos</span></a></li>
            <li><a href="{{ route('cobros.index') }}"><i class="lnr lnr-chart-bars"></i> <span>Cuentas de Cobro</span></a></li>
            @endif
            @if(auth()->user()->hasRoles(['1']))
            <li><a href="{{ route('datosBasicos.index') }}"><i class="lnr lnr-user"></i> <span>Mi Perfil</span></a></li>
            <li><a href="{{ route('usuarios.index') }}"><i class="lnr lnr-users"></i> <span>Usuarios</span></a></li>
            <li><a href="{{ route('clientes.index') }}"><i class="lnr lnr-sync"></i> <span>Clientes</span></a></li>
            <li><a href="{{ route('proyectos.index') }}"><i class="lnr lnr-cog"></i> <span>Proyectos</span></a></li>
            <li><a href="{{ route('bugs.index') }}"><i class="lnr lnr-bug"></i> <span>Hallazgos</span></a></li>
            <li><a href="{{ route('cobros.index') }}"><i class="lnr lnr-chart-bars"></i> <span>Cuentas de Cobro</span></a></li>
            <li><a href="{{ route('puntosTester',auth()->user()->id) }}"><i class="lnr lnr-thumbs-up"></i> <span>Mis puntos </span></a></li>
            @endif
            @if(auth()->user()->hasRoles(['4']))
            <li><a href="{{ route('cobros.index') }}"><i class="lnr lnr-chart-bars"></i> <span>Cuentas de Cobro</span></a></li>
            @endif
            <li> <a href="{!! url('/logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="lnr lnr-exit"></i> <span>Cerrar sesión</span></a>
              <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>
          </ul>
        </li>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="main">

  @if( count(getArrayObligatorios()) > 0 && !isset($homeMensajes))
  <div style="margin-bottom:1em;">
    <span><strong>NOTA:</strong> Aun tienes secciones obligatorias sin completar <b>(*).</b></span>
    (<a href="{{ seccionIncompleta() }}">completar aquí</a>)
    <br />
    <span>
      Recuerda que para participar en Crowd debes <strong class="badge">ser mayor de 18 años y residente fiscal colombiano</strong>.
    </span>
  </div>
  @endif

  @if(!isset($homeTester))
  <form method="POST" id="signup-form" class="signup-form" enctype="multipart/form-data">
    <h3>
      <a class="title-text {{ validarSeccion('datos basicos') }} {{ active('datosBasicos') }}" href="{{ route('datosBasicos.index') }}">
        Datos Básicos
      </a>
    </h3>
    <fieldset>
    </fieldset>

    <h3>
      <a class="title-text {{active('estudiosFormales')}}" href="{{ route('estudiosFormales.index') }}">
        Estudios Formales
      </a>
    </h3>
    <fieldset>
    </fieldset>

    <h3>
      <a class="title-text {{active('estudiosNoFormales')}}" href="{{ route('estudiosNoFormales.index') }}">
        Estudios no Formales
      </a>
    </h3>
    <fieldset>
    </fieldset>

    <h3>
      <a class="title-text {{active('datosLaborales')}}" href="{{ route('datosLaborales.index') }}">Información Laboral</a>
    </h3>
    <fieldset>
    </fieldset>

    <h3>
      <a class="title-text {{active('sinPruebas')}} {{ validarSeccion('conocimiento en pruebas') }}" href="{{ ('/sinPruebas') }}">
        Conocimiento en Pruebas
      </a>
    </h3>
    <fieldset>
    </fieldset>

    <h3>
      <a class="title-text {{active('info_dispositivos')}} {{ validarSeccion('dispositivo disponible para probar') }}" href="{{ ('/sinDispositivos') }}">Mis Dispositivos</a>
    </h3>
    <fieldset>
    </fieldset>

    <h3>
      <a class="title-text {{active('datosFinancieros')}} {{ validarSeccion('datos financieros') }}" href="{{ route('datosFinancieros.index') }}">Información Financiera</a>
    </h3>
    <fieldset>
    </fieldset>

    <h3>
      <a class="title-text {{active('anexos')}} {{ validarSeccion('Anexos') }}" href="{{ route('anexos.index') }}">Anexos</a>
    </h3>
    <fieldset>
    </fieldset>


  </form>
  @endif

  @yield('content')

</div>

<div class="row chat-window col" id="chat_window">
  <div id="bodyChat" class="col-xs-12 col" style="display: none;">
    <div class="panel">
      <div class="panel-heading top-bar">
        <div>
          <h3 class="panel-title">
            <span class="glyphicon glyphicon-comment" style="margin-right: 5px;"></span>
          </h3>
        </div>
        <div style="text-align: right;padding:0">
          <a href="#"><span id="minim_chat_window" class="glyphicon icon_minim panel-collapsed glyphicon-plus"></span></a>
          <a href="#"><span id="chat_window" class="glyphicon glyphicon-remove icon_close"></span></a>
        </div>
      </div>
      <div class="m-body app-scroll panel-body" style="display: none;">
        <div class="messages" style="min-height: 100%;"></div>
        <div class="typing-indicator" style="display: none;">
          <div class="message-card typing">
            <p>
              <span class="typing-dots">
                <span class="dot dot-1"></span>
                <span class="dot dot-2"></span>
                <span class="dot dot-3"></span>
              </span>
            </p>
          </div>
        </div>
        <div class="messenger-sendCard">
          <form id="message-form" method="POST" action="{{ route('send.message') }}" enctype="multipart/form-data">
            @csrf
            <label>
              <span class="fas fa-paperclip"></span>
              <input disabled='disabled' type="file" class="upload-attachment" name="file" accept="image/*, .txt, .rar, .zip" />
            </label>
            <textarea name="message" class="m-send app-scroll" placeholder="Escribir un mensaje.."></textarea>
            <button disabled='disabled'><span class="fas fa-paper-plane"></span></button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- jQuery 3.1.1 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>

<script src="{{ asset("storage/vendor/jquery-steps/jquery.steps.min.js") }}"></script>
<script src="{{ asset("storage/js/main.js") }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script>
  // Global float chat
  floatChat = true;
</script>
@include('chatify.layouts.footerLinks')
@yield('scripts')

<script>
  $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>

</body>

</html>