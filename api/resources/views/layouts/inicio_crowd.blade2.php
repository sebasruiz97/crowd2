<!-- Este layout es GENERAL -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Crowd SQA</title>
<!--    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
-->
    
    <!-- Bootstrap 3.3.7 -->
<!--    
    <link rel="stylesheet" href="{{ asset('/storage/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/storage/css/iconos.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>    
-->
    <!-- iCheck -->
 <!--   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">
 -->

    <style>
/*
            .profile-content {
            padding: 1px;
            margin-right: 5px;
            margin-left: 5px;
            background: #fff;
            min-height: 100px;
            margin-top: 20px !important;
            }
            .navbar-default2 {
            background-color: #304457!important;
            border-color: #304457!important;
            height:71px;
            }
            
            .navbar-nav2 {
            color:#ffff!important;
            font-size:15px;
            text-align:justify;
            list-style-type: none !important;

            }

            .navbar-nav2 > li > a {
            padding-top: 10px;
            padding-bottom: 10px;
            padding-right: 30px;
            line-height: 50px;
            color:#ffff!important;
            text-decoration: none !important;

            }

            .navbar-nav2 > li > a:hover,
            .navbar-nav2 > li > a:focus {
            color: #f3A100 !important;
            background-color: transparent !important;
            border-bottom: 4px solid #f3a100 !important;
            padding-bottom: 25px;
			}
*/
@font-face {
  font-family: 'Sonia';
  font-style: normal;
  src: url("..fonts/fonts/HelveticaNeueLTStd-LtCn");
  /* IE9 Compat Modes */
  /* Legacy iOS */ }
  
		.contenido {
		  display: grid;
		  grid-template-columns: repeat(4, 1fr);
		  /*border: 1px solid black;*/
		  grid-gap: 0px;
		  margin-left:2%;
		  margin-right:2%
		  /*grid-auto-rows: minmax(100px, auto);*/
		}
		.capa_1 {
		  grid-column: 1 / 3;
		  grid-row: 1;
		  margin-left:2%;
		  margin-top:2%;
		  font-family: Helvetica, sans-serif;
		  font-size:1.5vw;
		  /*border: 2px solid green;*/
		}
		.capa_2 {
		  grid-column: 3 / 5;
		  grid-row: 1;
		  text-align:right;
		  margin-right:5%;
		  margin-top:2%;
	/*	  border: 1px solid red;*/
		}
		.capa_3 {
		  grid-column: 1;
		  grid-row: 2;
	/*background-image:url(../../../public/images/fondo.png);*/
		  background-color:#b6ecfd;
	  	  border-radius: 100% 0% 0% 0%;
		  /*border: 1px solid blue;*/
		}
		.capa_4 {
		  grid-column: 2;
		  grid-row: 2;
		  font-family:Inter, Helvetica, sans-serif;
		  font-size:1.5vw;
		  text-align:justify;
		  text-align-last:right;
		  display: inline-block;
		  background-color:#b6ecfd;
		  border-radius: 0% 500% 800% 0%;
		}
		.capa_5 {
		  grid-column: 3 / 5;
		  grid-row: 2;
		  font-family:Inter, Helvetica, sans-serif;
		  font-size:2vw;
	      text-align:justify;
		  text-align-last:right;
		  margin-top:8%;
		  margin-left:8%;
		  margin-right:7%;
	/*	  border: 1px solid red;*/
		}
		.capa_6 {
		  grid-column: 1 / 5;
		  grid-row: 3;
		  background-color:#F3F3F3;
		  text-align:center;
		  font-family:Inter, Helvetica, sans-serif;
		  font-size:1.4vw;
		  /*border: 1px solid red;*/
		}
		.capa_7 {
		  grid-column: 1 / 5;
		  grid-row: 4;
		  margin-top:3%;
		}
		.capa_8 {
		  grid-column: 1 / 5;
		  grid-row: 5;
	  	  font-family:Inter, Helvetica, sans-serif;
		  font-size:1.5vw;
		  text-align:center;
		  background-color:#b6ecfd;
		  border-radius: 40% 0% 30% 0%;
		  margin-top:3%;
		}
		.capa_9 {
		  grid-column: 1 / 5;
		  grid-row: 6;
	/*	  border: 1px solid red;*/
		  margin-top:3%;
		}

		.beneficios {
		  display: grid;
		  grid-template-columns: repeat(3, 1fr);
		  grid-gap: 5px;
		  /*grid-auto-rows: minmax(100px, auto);*/
		}
		.divBenef_1 {
			grid-columns: 1;
			grid-rows: 1;
			text-align: center;
			font-family:Helvetica, Helvetica, sans-serif;
			font-size:1.3vw;
			display: inline-block;
	    	text-align: center;
			text-justify:auto
		}

		.divBenef_2 {
			grid-columns: 2;
			grid-rows: 1;
			text-align: center;
			font-family:Helvetica, Helvetica, sans-serif;
			font-size:1.3vw
		}	
		.divBenef_3 {
			grid-columns: 3;
			grid-rows: 1;
			text-align: center;
			font-family:Helvetica, Helvetica, sans-serif;
			font-size:1.3vw
		}	


    </style>
</head>


<body >
  
<div class="contenido">  
  <div class="capa_1"> 
    <img src="/storage/images/logo_crowd.png" style="width:40%;">
      <br>Una visión centrada en el cliente<br><br>
    </div>
    
    <div class="capa_2">
    	<a class="nav-link"  href="{{ route('login') }}">
    		<img src="/storage/images/login.png" style="width:7%;">
    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	</a>
    	<a class="nav-link"  href="{{ route('register') }}">
    		<img src="/storage/images/Registrate.png" style="width:12%;"> 
    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	</a>
        <a class="nav-link"  href="{{ route('contacto') }}">
        	<img src="/storage/images/Contactanos.png" style="width:20%;">
        </a>
	</div>
	
	<div class="capa_3">
    	<img src="/storage/images/bienvenida.png" style="width:97%;">
    </div>
    
  <div class="capa_4">
    <br><br>
    </div>

    <!--  Quienes somos -->
    <div class="capa_5">
   		<p> Somos una iniciativa 100% colombiana donde una comunidad de testers pone a prueba sus habilidades evaluando sitios web y apps, por medio del crowd testing.
        </p>
    </div>
    
  <div class="capa_6">
    <br>
      <p><b>Buscamos mejorar la experiencia digital de nuestros clientes </b></p>
        <p>&nbsp;</p>
	</div>
	
    <div class="capa_7">
    <div class="beneficios">
        	<div class="divBenef_1"><img src="/storage/images/proyectos_constantes.png" style="width:47%"></div>
            
            <div class="divBenef_2"><img src="/storage/images/ingresos_extra.png" style="width:47%"></div>
            
            <div class="divBenef_3"><img src="/storage/images/testing_software.png" style="width:47%"></div>            
        </div>
    
    </div>
    
    <div class="capa_8">
        <br>
        Celebramos la diversidad de dispositivos, navegadores, niveles de experiencia
        y percepciones<br><br>
    </div>

    <div class="capa_9">
    	
        <div class="beneficios">
        	<div class="divBenef_1">
            <img src="/storage/images/inscribete.png" style="width:25%" class="centrar"><span class="divBenef_2"></span> <br><br>
          <b>Regístrate</b><br><br>
          Diligencia los datos para quedar activo en nuestra plataforma
            </div>
            
            <div class="divBenef_2">
            	<img src="/storage/images/comunidad.png" style="width:25%"><br><br>
	        <b>Sé parte de nuestra comunidad</b> <br><br>
            Te ayudaremos a reforzar tus conocimientos en pruebas de software 
            </div>
            
            <div class="divBenef_3">
            	<img src="/storage/images/nuevoReto.png" style="width:25%"><br><br>
	        <b>Acepta el primer reto </b> <br><br>
            Gana puntos de experiencia 
            </div>
        </div>
        
    </div>

</div> <!-- div contenidos -->



    <!-- jQuery 3.1.1 -->
<!--
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

@yield('scripts')
-->
</body>


</html>

