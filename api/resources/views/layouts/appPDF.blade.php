<!-- Este layout es para plantillas de correo -->

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="{{ asset('/storage/css/bootstrap.css') }}">
</head>

<body>
@yield('content')  
</body>

</html>