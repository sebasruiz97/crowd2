{{-- <style>
  #mainNav {
      padding-top: 0;
      padding-bottom: 0;
      border-bottom: none;
      background-color: transparent!important;
      transition: background-color .3s ease-in-out;
  }
  #mainNav {
      min-height: 57px;
      background-color: #b6ecfd!important;
  }

</style> --}}

<nav class="navbar navbar-light navbar-expand-lg shadow-sm fixed-top" style="color:#304457" id="mainNav">

  <a href="{{ url('/') }}" style="text-decoration: none;">
    <img src="/img/logo_crowd_leyenda.png"  width="50%"/></a>
    <br>
  

<!--
    <button class="navbar-toggler" type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="{{ __('Toggle navigation') }}">
      <span class="navbar-toggler-icon"></span>
    </button>
-->
    <div class="container"> 
      <div class="collapse navbar-collapse border-0" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
        <!--
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" style="scrollbar-shadow-color: #f3a100" href="#about">Sobre nosotros</a>
          </li>
          -->

          @auth
            @if(auth()->user()->hasRoles(['1','2','3']))
            <a class="nav-link" href="{{ route('datosBasicos.index') }}" style="font-family: HelveticaNeueLTStd-Hv;font-size: 1.3vw">Mi perfil</a>
            @endif
            @if(auth()->user()->hasRoles(['4']))
            <a class="nav-link" href="{{ route('cobros.index') }}" style="font-family: HelveticaNeueLTStd-Hv;font-size: 1.3vw">Mi perfil</a>
            @endif
          @else
            <li class="nav-item">
              <a class="nav-link" href="{{ route('register') }}" style="font-family: HelveticaNeueLTStd-Hv;font-size: 1.3vw">Registrarme</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}" style="font-family: HelveticaNeueLTStd-Hv;font-size: 1.3vw">Ingresar</a>
            </li>
          @endauth
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{ route('contacto') }}" style="font-family: HelveticaNeueLTStd-Hv;font-size: 1.3vw">Contáctanos</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>

@yield('content')