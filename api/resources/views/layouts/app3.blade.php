<!-- Este layout es para DASHBOARD TESTER -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{config('app.name')}}</title
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    
    <!-- Bootstrap 3.3.7 -->
    
    <link rel="stylesheet" href="{{ asset('/storage/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/storage/css/iconos.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
    <a class="navbar-brand" href="{{ url('/') }}">
      <img src="/storage/images/logo_crowd_leyenda.png"  width="35%"/>
    </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">


                {{-- Para usuario --}}


                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="/storage/avatars/{{auth()->user()->avatar}}"  width="30px" class="img-circle" alt="Avatar"> <span>{{auth()->user()->name}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                  <ul class="dropdown-menu">
                    @if(auth()->user()->hasRoles(['2']))
                      <li><a href="{{ route('datosBasicos.index') }}"><i class="lnr lnr-user"></i> <span>Mi Perfil</span></a></li>

                      <li><a href="{{ route('puntosTester',auth()->user()->id) }}"><i class="lnr lnr-thumbs-up" ></i> <span>Mis puntos </span></a></li>
                      @endif
                      @if(auth()->user()->hasRoles(['3']))
                      <li><a href="{{ route('datosBasicos.index') }}"><i class="lnr lnr-user"></i> <span>Mi Perfil</span></a></li>
                      <li><a href="{{ route('clientes.index') }}"><i class="lnr lnr-sync"></i> <span>Clientes</span></a></li>
                      <li><a href="{{ route('proyectos.index') }}"><i class="lnr lnr-cog"></i> <span>Proyectos</span></a></li>
                      <li><a href="{{ route('bugs.index') }}"><i class="lnr lnr-bug"></i> <span>Hallazgos</span></a></li>
                      <li><a href="{{ route('cobros.index') }}"><i class="lnr lnr-chart-bars"></i> <span>Cuentas de Cobro</span></a></li>
                      @endif
                      @if(auth()->user()->hasRoles(['1']))
                      <li><a href="{{ route('datosBasicos.index') }}"><i class="lnr lnr-user"></i> <span>Mi Perfil</span></a></li>
                      <li><a href="{{ route('usuarios.index') }}"><i class="lnr lnr-users"></i> <span>Usuarios</span></a></li>
                      <li><a href="{{ route('clientes.index') }}"><i class="lnr lnr-sync"></i> <span>Clientes</span></a></li>
                      <li><a href="{{ route('proyectos.index') }}"><i class="lnr lnr-cog"></i> <span>Proyectos</span></a></li>
                      <li><a href="{{ route('bugs.index') }}"><i class="lnr lnr-bug"></i> <span>Hallazgos</span></a></li>
                      <li><a href="{{ route('cobros.index') }}"><i class="lnr lnr-chart-bars"></i> <span>Cuentas de Cobro</span></a></li>
                      <li><a href="{{ route('puntosTester',auth()->user()->id) }}"><i class="lnr lnr-thumbs-up" ></i> <span>Mis puntos </span></a></li>
                      @endif
                      @if(auth()->user()->hasRoles(['4']))
                      <li><a href="{{ route('cobros.index') }}"><i class="lnr lnr-chart-bars"></i> <span>Cuentas de Cobro</span></a></li>
                      @endif
                      <li> <a href="{!! url('/logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="lnr lnr-exit"></i> <span>Cerrar sesión</span></a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        </form> 
                      </li>
                  </ul>
                </li>
                
          </ul>
          </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


<div class="container.fluid py-1 px-1">
  @yield('content')
  <div class="col-sm col-12 col-lg-12">
    <div class="row">
		<div class="col-sm col-10 col-lg-12">
			<div class="col-sm col-3 col-lg-3">
				<div class="profile-content">

            <div class="profile-usertitle">
              @yield('foto')
            </div>
						<div class="profile-usertitle-name">
              @yield('nombre')
						</div>
						<div class="profile-usertitle-job">
							@yield('perfil')
						</div>
						<div class="profile-usertitle-test">
							@yield('pruebas')
            </div>
            <div class="profile-usertitle-job">
							@yield('lider')
						</div>
				</div>
			</div>

			<div class="col-sm col-9 col-lg-9">
        <div class="row">
          <div class="col-sm col-3 col-lg-3">
            <div class="profile-content">
					    <div class="profile-usertitle-job">
							  @yield('puntos_totales')
              </div>
            </div>
          </div>
       
        </div>

         
          </div>
					</div>
				</div>
			</div>
		</div>
    </div>
  </div>

    <!-- jQuery 3.1.1 -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="{{ asset("storage/vendor/jquery/jquery.min.js") }}"></script>
    <script src="{{ asset("storage/vendor/jquery-steps/jquery.steps.min.js") }}"></script>
    <script src="{{ asset("storage/js/main.js") }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

    

@yield('scripts')

<script>
  $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
  </script>


</body>
</html>


