<!-- Este layout es GENERAL -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{config('app.name')}}</title

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    
    <!-- Bootstrap 3.3.7 -->
    
    <link rel="stylesheet" href="{{ asset('/storage/css/bootstrap.css') }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/storage/css/iconos.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- iCheck -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">
	
    <link  rel="icon"   href="/storage/images/favicon_crowd.png" type="image/png" />

</head>

<body class="skin-blue sidebar-mini">
@if (!Auth::guest())
    <div class>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <a class="navbar-brand" href="{{ url('/') }}">
                  <img src="/storage/images/logo_crowd_leyenda.png"  width="35%"/>
              </div>
      
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">


                {{-- Para usuario --}}


                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="/storage/avatars/{{auth()->user()->avatar}}"  width="30px" class="img-circle" alt="Avatar"> <span>{{auth()->user()->name}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                    <ul class="dropdown-menu">
                      @if(auth()->user()->hasRoles(['2']))
                      <li><a href="{{ route('datosBasicos.index') }}"><i class="lnr lnr-user"></i> <span>Mi Perfil</span></a></li>

                      <li><a href="{{ route('puntosTester',auth()->user()->id) }}"><i class="lnr lnr-thumbs-up" ></i> <span>Mis puntos </span></a></li>
                      @endif
                      @if(auth()->user()->hasRoles(['3']))
                      <li><a href="{{ route('datosBasicos.index') }}"><i class="lnr lnr-user"></i> <span>Mi Perfil</span></a></li>
                      <li><a href="{{ route('clientes.index') }}"><i class="lnr lnr-sync"></i> <span>Clientes</span></a></li>
                      <li><a href="{{ route('proyectos.index') }}"><i class="lnr lnr-cog"></i> <span>Proyectos</span></a></li>
                      <li><a href="{{ route('bugs.index') }}"><i class="lnr lnr-bug"></i> <span>Hallazgos</span></a></li>
                      <li><a href="{{ route('cobros.index') }}"><i class="lnr lnr-chart-bars"></i> <span>Cuentas de Cobro</span></a></li>
                      @endif
                      @if(auth()->user()->hasRoles(['1']))
                      <li><a href="{{ route('datosBasicos.index') }}"><i class="lnr lnr-user"></i> <span>Mi Perfil</span></a></li>
                      <li><a href="{{ route('usuarios.index') }}"><i class="lnr lnr-users"></i> <span>Usuarios</span></a></li>
                      <li><a href="{{ route('clientes.index') }}"><i class="lnr lnr-sync"></i> <span>Clientes</span></a></li>
                      <li><a href="{{ route('proyectos.index') }}"><i class="lnr lnr-cog"></i> <span>Proyectos</span></a></li>
                      <li><a href="{{ route('bugs.index') }}"><i class="lnr lnr-bug"></i> <span>Hallazgos</span></a></li>
                      <li><a href="{{ route('cobros.index') }}"><i class="lnr lnr-chart-bars"></i> <span>Cuentas de Cobro</span></a></li>
                      <li><a href="{{ route('puntosTester',auth()->user()->id) }}"><i class="lnr lnr-thumbs-up" ></i> <span>Mis puntos </span></a></li>
                      @endif
                      @if(auth()->user()->hasRoles(['4']))
                      <li><a href="{{ route('cobros.index') }}"><i class="lnr lnr-chart-bars"></i> <span>Cuentas de Cobro</span></a></li>
                      @endif
                      <li> <a href="{!! url('/logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="lnr lnr-exit"></i> <span>Cerrar sesión</span></a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        </form> 
                      </li>
                    </ul>
                  </li>
                  
              </ul>
              </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
        </nav>
    </div>

        <div class>
            @yield('content')
        </div>

        {{-- <div class="centrar">
        <!-- Main Footer -->
        <footer class="main-footer" style="max-height: 100px;text-align: center">
            <small> <strong> <a href="#">CROWDSQA </a>.</strong>{{date('Y')}}. Todos los derechos reservados.</small>
        </footer>
      </div> --}}

    </div>
@else
    </nav>

    <div id="page-content-wrapper">
        <div class="container-fluid">
                <div class="col-lg-12">
                    @yield('content')
                </div>
        </div>
    </div>
    @endif

    <!-- jQuery 3.1.1 -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

@yield('scripts')

<script>
  $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
</body>





</html>