@csrf
@inject('anexos','App\Services\anexos')

<style>
    .caja {
        font-family: sans-serif;
        font-size: 12px;
        font-weight: 400;
        color: #304457;
        background: #E0F8F7;
        margin: 0 0 25px;
        overflow: hidden;
        padding: 10px;
    }

    /* The container */
    .container1 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 12px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default checkbox */
    .container1 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        background-color: #E6E6E6;

    }

    /* On mouse-over, add a grey background color */
    .container1:hover input~.checkmark {
        background-color: #304457;
    }

    /* When the checkbox is checked, add a blue background */
    .container1 input:checked~.checkmark {
        background-color: #304457;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .container1 input:checked~.checkmark:after {
        display: block;
    }

    /* Style the checkmark/indicator */
    .container1 .checkmark:after {
        left: 9px;
        top: 5px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>


<div class="form-group col-sm-12">
    <div class="form-group col-sm-2"> </div>
    <div class="form-group col-sm-10">

        <div class="form-group col-sm-10">
            <div class="caja">
                Para futuros pagos de cuentas de cobro generadas a tu nombre, es <strong>obligatorio</strong> que subas los anexos de "Documento de identificación", "Certificación Bancaria", "RUT", <a href="{{ route('datos') }}" style="color:blue;text-decoration: underline;" target="_blank">"Aceptación Tratamiento de Datos"</a> y <a href="{{ route('terminosCondiciones') }}" style="color:blue;text-decoration: underline;" target="_blank">"Aceptación términos y condiciones"</a>.
                Estos dos últimos formatos debes descargarlos para luego subirlos firmados a la plataforma. Tu perfil sólo estará completo cuando hayas cargado estos documentos
            </div>
        </div>

        <!-- Nombre -->
        <div class="form-group col-sm-3">
            <label for="tipoAnexo_id">¿Qué tipo de anexo vas a subir?</label>
            <select class="form-control" id="tipoAnexo_id" value="{{old('tipoAnexo_id')}}" name="tipoAnexo_id">
                @foreach($anexos->get() as $index => $anexos)
                    <option value="{{ $index }}" {{old('tipoAnexo_id') == $index ? 'selected' : ''}}> {{ $anexos }}</option>
                @endforeach
            </select>
            @if ($errors->has('tipoAnexo_id'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('tipoAnexo_id') }}</i></small>
            </span>
            @endif
        </div>

        <!-- Adjunto-->
        <div class="form-group col-sm-4">
            <label for="url">Adjunta el archivo</label>
            <small><input type="file" name="url" accept="image/png, image/jpeg, image/jpg, application/pdf"></small>
            @if ($errors->has('url'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('url') }}</i></small>
            </span>
            @endif
        </div>

        <!-- Comentarios-->
        <div class="form-group col-sm-3">
            <label for="comentarios">¿Algún comentario?</label>
            <input class="form-control" name="comentarios" value="{{old('comentarios')}}" type="text" id="comentarios">
            @if ($errors->has('comentarios'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('comentarios') }}</i></small>
            </span>
            @endif
        </div>

        <div class="form-group col-sm-10">
            <div class="centrar">
                <small><i>Sólo se admite archivos con formato pdf, png o jpeg y con un tamaño máximo de 3MB. </i></small>
            </div>


            <!-- UserID Field -->
            <div class="form-group col-sm-5">
                <label for="user_id"> </label>
                <input class="form-control" name="user_id" type="hidden" value={{auth()->user()->id}} readonly="readonly">
            </div>

            <!-- Modulo ID -->
            <div class="form-group col-sm-5">
                <label for="modulo_id"> </label>
                <input class="form-control" name="modulo_id" type="hidden" value=1 readonly="readonly">
            </div>

            <!-- Submit Field -->
            <div class="form-group col-sm-10">
                <div class="centrar">
                    <input class="btn btn-primary" type="submit" value="Guardar">
                    <a href={{ route('anexos.index') }} class="btn btn-default">Cancelar</a>
                    <div>
                    </div>

                </div>
            </div>
            <div class="form-group col-sm-1"></div>
        </div>

        <script>
            $(document).on('change', 'input[type="file"]', function() {
                // this.files[0].size recupera el tamaño del archivo
                // alert(this.files[0].size);

                var fileName = this.files[0].name;
                var fileSize = this.files[0].size;

                //aqui el tamaño del archivo se da en terabytes.
                if (fileSize > 3000000) {
                    alert('El archivo no debe superar los 3MB');
                    this.value = '';
                    this.files[0].name = '';
                } else {
                    // recuperamos la extensión del archivo
                    var ext = fileName.split('.').pop();

                    // Convertimos en minúscula porque 
                    // la extensión del archivo puede estar en mayúscula
                    ext = ext.toLowerCase();

                    // console.log(ext);
                    switch (ext) {
                        case 'jpg':
                        case 'jpeg':
                        case 'png':
                        case 'pdf':
                            break;
                        default:
                            alert('El archivo no tiene la extensión adecuada');
                            this.value = ''; // reset del valor
                            this.files[0].name = '';
                    }
                }
            });
        </script>