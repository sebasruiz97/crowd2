@extends('layouts.app2')

@section('content')

@if ($anexos->isEmpty())
<h1 class="pull-right">
    <a class="btn btn-default btn-xs pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px" href="{{ route('anexos.create') }}">Agregar anexos</a>
</h1>
<br>
<br>
<br>
<br>
<br>
<div style="text-align:center">
    <h3>Aún no has cargado ningún anexo</h3>
    <h5> Recuerda que sólo quedarás activo en la plataforma, cuando hayas completado tu perfil </h5>
</div>
<div class="form-group col-sm-10">
    <div>
        La siguiente documentación es obligatoria:
        <ul>
            @foreach ($anexosFaltantes as $anexo)
            <li>{{ $anexo->nombre_anexo }}</li>
            @endforeach
        </ul>
    </div>
</div>
@else
<h3 style="text-align:center"> Anexos cargados </h3>
@include('flash::message')
<h1 class="pull-right">
    <a class="btn btn-default btn-xs pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px" href="{{ route('anexos.create') }}">Añadir otro</a>
</h1>
@if(!$anexosFaltantes->isEmpty())
    <div class="form-group col-sm-10">
        <div>
            La siguiente documentación es obligatoria:
            <ul>
                @foreach ($anexosFaltantes as $anexo)
                <li>{{ $anexo->nombre_anexo }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
@include('anexos.table')
@endif

@endsection