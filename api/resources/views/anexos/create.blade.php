@extends('layouts.app2')

@section('content')

<div> 
    <h3 class="centrar"> <strong>  Subir anexos </strong></h3>
</div><br>

    <form method="POST" action="{{ route('anexos.store') }}" enctype="multipart/form-data">
        @include('flash::message')
        @include('anexos.fields_create')
    
    </form>
@endsection
