<div class="container.fluid py-0 px-1">
    <div class="row">
        <div class="col-sm col-1 col-lg-1">
        </div>
        <div class="col-sm col-12 col-lg-12">
            <div class="table-responsive">
                <table class="table" id="anexos-table">
                    <thead>
                        <tr>
                            <th>Tipo de Anexo</th>
                            <th>Comentarios</th>
                            <th colspan="3">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($anexos as $anexo)
                        <tr>
                            <td>{{ $anexo->nombre_anexo }}</td>
                            <td>{{ $anexo->comentarios}}</td>
                            <td>
                                <div style="display: flex;">
                                    <div style="margin-right: 5px;">
                                        <a href="{{ route('anexos.show',$anexo->url) }}" class="btn btn-default btn-xs" style="margin-rigth: 3px"><i class="lnr lnr-download"></i></a>
                                    </div>
                                    @if($anexo->approved == 0)
                                    <div>
                                        <form action="{{ route('anexos.destroy',$anexo->id_anexo) }}" method="POST">
                                            @csrf
                                            @method('DELETE')

                                            <div class='pull left'>
                                                <button type="submit" class="btn btn-danger btn-xs" style="margin-left: 3px" onclick="return confirm('¿Estás seguro?')"><i class="lnr lnr-cross"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                    @endif
                                </div>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pull-right">
                    <small><i>Sólo se admite archivos con formato pdf, png o jpeg y con un tamaño máximo de 3MB</i></small>
                </div> <br>
                <div class="pull-right">
                    <small>{{ $anexos->links() }}</small>
                </div>

            </div>
        </div>
        <div class="col-sm col-1 col-lg-1">
        </div>
    </div>
</div>