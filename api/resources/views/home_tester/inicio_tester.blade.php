@extends('layouts.app3')

@section('foto')
    <div class="centrar">
        @foreach($datosBasicos as $datoBasico)
            <img src="/storage/avatars/{{auth()->user()->avatar}}" class="img-circle" width="150px">
        @endforeach
    </div>
@endsection

@section('nombre')
    {{auth()->user()->name}}
@endsection

@section('perfil')
        {{$nivelEducacion->nombre_nivelEducacion}} en {{$areaEstudio->nombre_areaEstudio}}<br>
@endsection

@section('pruebas')
        Actualmente soy {{$ocupacion->nombre_ocupacion}}.<br> 
        Vivo en 
        @foreach ($municipios as $municipio)
        {{$municipio->nombre_municipio}},
        @endforeach
        @foreach ($departamentos as $departamento)
        {{$departamento->nombre_departamento}},
        @endforeach
        <br> <br> 

        Tengo experiencia en los sectores: <br> 
        @foreach($sectores as $sector)
        {{$sector->nombre_sector}},
        @endforeach
        
        <br> <h4 class="help-block" style="color:#304457";> </h4><hr>
  
        <h3  class="profile-usertitle-job" > Conocimiento en Pruebas:</h3>
            @foreach($pruebas as $prueba)
            {{$prueba->nombre_prueba}} - {{$prueba->nombre_ejecucion}} con {{$prueba->nombre_herramienta}}<br>
            @endforeach
        <br> <h4 class="help-block" style="color:#304457";> </h4><hr>

        <h3  class="profile-usertitle-job" > Puedo probar en:  </h3>
            @foreach($dispositivos as $dispositivo)
            {{$dispositivo->nombre_dispositivo}} con {{$dispositivo->nombre_sistemaOperativo}}<br>
            @endforeach
        <br> <h4 class="help-block" style="color:#304457";> </h4><hr>

        <div class="center">
            <a class="btn btn-primary" style="margin-top: -10px;margin-bottom: 5px; margin: 10px" href="{{ route('datosBasicos.index',auth()->user()->id) }}">Perfil Completo</a>
        </div>
    @endsection

@section('puntos_totales')
    <div class="profile-usertitle-test3" style="text-align:center" >{{$puntosAcumulados}}</div>
    <div class="profile-usertitle-test2" style="color:#304457; text-align:center" >Puntos Acumulados</div>
        <a style="text-align:center" class="profile-usertitle-test5" href="{{ route('puntosTester',auth()->user()->id) }}"> ¿Cómo puedo ganar más puntos? </a>
@endsection

@section('proyectos_participado')
    <div class="profile-usertitle-test3" >{{$proyectosParticipado}}</div>
    <div class="profile-usertitle-test2">Proyectos en los que he participado</div>
@endsection

@section('incidentes_aprobados')
    <div class="profile-usertitle-test3"  >{{$bugsAprobados}}</div>
    <div class="profile-usertitle-test2" >Incidentes aprobados</div>
    <a class="profile-usertitle-test5" style="text-decoration:none; color:#304457"> De {{$bugsReportados}} incidentes reportados</a>
@endsection

@section('invitaciones_proyectos')
    <div class="profile-usertitle-test3" >#</div>
    <div class="profile-usertitle-test2" >Invitaciones a proyectos</div>
    <a class="profile-usertitle-test5" href="#" > Ver invitaciones pendientes  </a>
@endsection

@section('proyectos')

        <h3  class="profile-usertitle-job" style="color:#F3A100"> Participación en Proyectos </h3>
        <hr>

        <div class="col-sm col-3 col-lg-3">
            <h3 class="profile-usertitle-job" style= "cursor:auto">Proyectos por Estado</h3>

            <div class="table-responsive" >
                <table class="table" style="color:#304457">
                    <tbody>  
                            <tr> 
                                <th>En ejecución</th>
                                <td>{{$proyectosAbiertos}}</td>
                                </tr>
                            <tr> 
                                <th>Cerrados</th>
                                <td>{{$proyectosCerrados}}</td>
                            </tr>
                            <tr> 
                                <th>Anulados</th>
                                <td>{{$proyectosAnulados}}</td>
                            </tr>
                            <tr> 
                                <th>Suspendidos</th>
                                <td>{{$proyectosSuspendidos}}</td>
                            </tr>   
                            <tr> 
                                <th>Cancelados</th>
                                <td>{{$proyectosCancelados}}</td>
                            </tr>   
                            <tr> 
                                <th style="color:#F3A100">Total</th>
                                <td style="color:#F3A100">{{$proyectosParticipado}}</td>
                            </tr>         
                        </tbody>  
                </table>
            </div>
        </div>

        <div style="text-align:right">
            <a class="btn btn-primary" style="margin-top: -10px;margin-bottom: 5px; margin: 10px">Mis proyectos</a>
        </div>
@endsection

@section('hallazgos')

        <h3  class="profile-usertitle-job" style="color:#F3A100;"> Hallazgos reportados </h3>
        <hr>
        
        <div class="col-sm col-3 col-lg-3">
            <h3 class="profile-usertitle-job" style= "cursor:auto">Hallazgos por tipo</h3>

            <div class="table-responsive" >
                <table class="table" style="color:#304457">
                    <tbody>  
                            <tr> 
                                <th>Bug</th>
                                <td>{{$bugs}}</td>
                                </tr>
                            <tr> 
                                <th>Nota</th>
                                <td>{{$notas}}</td>
                            </tr>
                            <tr> 
                                <th>Idea</th>
                                <td>{{$ideas}}</td>
                            </tr>
                            <tr> 
                                <th>Pregunta</th>
                                <td>{{$preguntas}}</td>
                            </tr>   
                            <tr> 
                                <th style="color:#F3A100">Total</th>
                                <td style="color:#F3A100">{{$bugsReportados}}</td>
                            </tr>         
                        </tbody>  
                </table>
            </div>
        </div>

        <div class="col-sm col-3 col-lg-3">
            <h3 class="profile-usertitle-job" style="cursor:auto">Hallazgos por estado</h3>
            <div class="table-responsive" >
                <table class="table" style="color:#304457">
                    <tbody>  
                            <tr> 
                                <th>Nuevo</th>
                                <td>{{$bugsNuevos}}</td>
                                </tr>
                            <tr> 
                                <th>Aprobado por líder</th>
                                <td>{{$bugsAprobadosLider}}</td>
                            </tr>
                            <tr> 
                                <th>Rechazado por líder</th>
                                <td>{{$bugsRechazadosLider}}</td>
                            </tr>
                            <tr> 
                                <th>Aprobado por cliente</th>
                                <td>{{$bugsAprobadosCliente}}</td>
                            </tr>                             
                            <tr> 
                                <th>Rechazado por cliente</th>
                                <td>{{$bugsRechazadosCliente}}</td>
                            </tr>  
                            <tr> 
                                <th style="color:#F3A100">Total</th>
                                <td style="color:#F3A100">{{$bugsReportados}}</td>
                            </tr>  
                        </tbody>  
                </table>
            </div>
        </div>

        <div class="col-sm col-3 col-lg-3">
            <h3 class="profile-usertitle-job" style="cursor:auto">Hallazgos por gravedad</h3>
            <div class="table-responsive" >
                <table class="table" style="color:#304457">
                    <tbody>  
                            <tr> 
                                <th>Menor</th>
                                <td>{{$bugsGravedadMenor}}</td>
                                </tr>
                            <tr> 
                                <th>Mayor</th>
                                <td>{{$bugsGravedadMayor}}</td>
                            </tr>
                            <tr> 
                                <th>Stopper</th>
                                <td>{{$bugsStopper}}</td>
                            </tr>
                            </tr>  
                            <tr> 
                                <th style="color:#F3A100">Total</th>
                                <td style="color:#F3A100">{{$bugsReportados}}</td>
                            </tr>  
                        </tbody>  
                </table>
            </div>
        </div>

        <div class="col-sm col-3 col-lg-3">
            <h3 class="profile-usertitle-job" style="cursor:auto">Hallazgos rechazados</h3>
            <div class="table-responsive" >
                <table class="table" style="color:#304457">
                    <tbody>  
                            <tr> 
                                <th>Duplicado</th>
                                <td>{{$rechazoDuplicado}}</td>
                                </tr>
                            <tr> 
                                <th>Fuera de alcance</th>
                                <td>{{$rechazoFueraAlcance}}</td>
                            </tr>
                            <tr> 
                                <th>No es un defecto</th>
                                <td>{{$rechazoNoDefecto}}</td>
                            </tr>
                            <tr> 
                                <th>No es posible reproducirlo</th>
                                <td>{{$rechazoNoReproducible}}</td>
                            </tr>
                            </tr>  
                            <tr> 
                                <th style="color:#F3A100">Total</th>
                                <td style="color:#F3A100">{{$bugsRechazados}}</td>
                            </tr>  
                        </tbody>  
                </table>
            </div>
        </div>
        
        <div style="text-align:right">
            <a class="btn btn-primary"  href="{{ route('bugs.index') }}" style="margin-top: -10px;margin-bottom: 5px; margin: 10px">Mis hallazgos</a>
        </div>

@endsection

