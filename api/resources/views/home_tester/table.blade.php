<style>

.caja {
font-family: sans-serif;
font-size: 14px;
font-weight: 400;
color: #304457;
background: #E0F8F7;
margin: 0 0 25px;
overflow: hidden;
padding: 20px;
}

</style>

<div class="container.fluid py-0 px-1">
     
    <div class="row" >
        <div class="col-sm col-3 col-lg-3">
        </div>
        <div class="col-sm col-6 col-lg-6">
            <h3  style="text-align:center"> Tienes {{$puntosTotal}} puntos acumulados </h3>
            <div class="caja">
                <small>
                    Puedes <strong> acumular puntos </strong>
                    <p>
                        <ul>
                            <li>Completando tu perfil</li>
                            <li>Participando de los proyectos de inicio a fin</li>
                            <li>Reportando incidentes que luego sean aprobados</li>
                          </ul>	
                    </p>	
                    Ten en cuenta que también pierdes puntos cuando eliminas información de tu perfil, abandonas los proyectos en los que estás participando o reportas incidentes no a lugar.
                </small>
            </div>

            <br>
            <div class="table-responsive">
                <table class="table" id="estudioFormal-table">
                    <thead>
                        <tr>
                            <th>Número de puntos</th>
                            <th>Motivo</th>
                            <th>Fecha de asignación</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($puntosTester as $puntoTester)
                        <tr>
                            <td>{{ $puntoTester->puntos}}</td>
                            <td>{{ $puntoTester->motivo}}</td>
                            <td>{{ $puntoTester->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pull-left">
                    <a class="btn btn-default btn pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px" href="{{ route('datosBasicos.index') }}">Volver </a>
                </div>
                <div class="pull-right">
                    <small>{{ $puntosTester->links() }}</small>
                </div>
            </div>
            
        </div>
        <div class="col-sm col-3 col-lg-3">
        </div>
</div>
