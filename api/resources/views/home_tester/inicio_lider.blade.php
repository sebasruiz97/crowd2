@extends('layouts.appLider')

@section('general')

<h3  class="profile-usertitle-job" style="color:#F3A100"> Resumen de proyectos asignados </h3>
<hr>

<div class="col-sm col-3 col-lg-3">
<h3 class="profile-usertitle-job" style= "cursor:auto">Proyectos por Estado</h3>
    <div class="table-responsive" >
        <table class="table" style="color:#304457; font-size:12px">
            <tbody>  
                    <tr> 
                        <th>Ingresado</th>
                        <td></td>
                        </tr>
                    <tr> 
                        <th>En ejecución</th>
                        <td></td>
                    </tr>
                    <tr> 
                        <th>Cerrado</th>
                        <td></td>
                    </tr>
                    <tr> 
                        <th style="color:#F3A100">Total</th>
                        <td style="color:#F3A100"></td>
                    </tr>         
                </tbody>  
        </table>
    </div>
</div>

<div class="col-sm col-3 col-lg-3">
    <h3 class="profile-usertitle-job" style= "cursor:auto">Clientes con más proyectos</h3>
        <div class="table-responsive" >
            <table class="table" style="color:#304457">
                <tbody>  
                        <tr> 
                            <th>Cliente 1</th>
                            <td></td>
                            </tr>
                        <tr> 
                            <th>Cliente 2</th>
                            <td></td>
                        </tr>
                        <tr> 
                            <th>Cliente 3</th>
                            <td></td>
                        </tr>
      
                    </tbody>  
            </table>
        </div>
    </div>

@endsection

@section('content')

<div class="pull-left" style="padding-right: 25px">
    <nav class="navbar navbar-light float-right">
        <form class="form-inline">
      
          <select name="tipo" class="form-control mr-sm-2" id="exampleFormControlSelect1">
            <option>Buscar por tipo</option>
            <option>Proyecto</option>
            <option>Usuario</option>
          </select>
      
           <input name="buscarpor" class="form-control mr-sm-2" type="search" placeholder="Buscar por nombre" aria-label="Search">
    
           <button class="btn btn-default btn-xs" type="submit">Buscar</button>
           <a href={{ route('inicioLider') }} class="btn btn-default btn-xs">Ver todos</a>
           
        </form>
      </nav>
    </div>

@endsection

@section('proyectos')

@if($buscar !== null)

    <h3  class="profile-usertitle-job" style="color:#F3A100;"> Hallazgos reportados </h3>
    <hr>

    <div class="col-sm col-3 col-lg-3">
        <h3 class="profile-usertitle-job" style= "cursor:auto">Hallazgos por tipo</h3>

        <div class="table-responsive" >
            <table class="table" style="color:#304457">
                <tbody>  
                        <tr> 
                            <th>Bug</th>
                            <td>{{$bugs}}</td>
                            </tr>
                        <tr> 
                            <th>Nota</th>
                            <td>{{$notas}}</td>
                        </tr>
                        <tr> 
                            <th>Idea</th>
                            <td>{{$ideas}}</td>
                        </tr>
                        <tr> 
                            <th>Pregunta</th>
                            <td>{{$preguntas}}</td>
                        </tr>   
                        <tr> 
                            <th style="color:#F3A100">Total</th>
                            <td style="color:#F3A100">{{$bugsReportados}}</td>
                        </tr>         
                    </tbody>  
            </table>
        </div>
    </div>

    <div class="col-sm col-3 col-lg-3">
        <h3 class="profile-usertitle-job" style="cursor:auto">Hallazgos por estado</h3>
        <div class="table-responsive" >
            <table class="table" style="color:#304457">
                <tbody>  
                        <tr> 
                            <th>Nuevo</th>
                            <td>{{$bugsNuevos}}</td>
                            </tr>
                        <tr> 
                            <th>Aprobado por líder</th>
                            <td>{{$bugsAprobadosLider}}</td>
                        </tr>
                        <tr> 
                            <th>Rechazado por líder</th>
                            <td>{{$bugsRechazadosLider}}</td>
                        </tr>
                        <tr> 
                            <th>Aprobado por cliente</th>
                            <td>{{$bugsAprobadosCliente}}</td>
                        </tr>                             
                        <tr> 
                            <th>Rechazado por cliente</th>
                            <td>{{$bugsRechazadosCliente}}</td>
                        </tr>  
                        <tr> 
                            <th style="color:#F3A100">Total</th>
                            <td style="color:#F3A100">{{$bugsReportados}}</td>
                        </tr>  
                    </tbody>  
            </table>
        </div>
    </div>

    <div class="col-sm col-3 col-lg-3">
        <h3 class="profile-usertitle-job" style="cursor:auto">Hallazgos por gravedad</h3>
        <div class="table-responsive" >
            <table class="table" style="color:#304457">
                <tbody>  
                        <tr> 
                            <th>Menor</th>
                            <td>{{$bugsGravedadMenor}}</td>
                            </tr>
                        <tr> 
                            <th>Mayor</th>
                            <td>{{$bugsGravedadMayor}}</td>
                        </tr>
                        <tr> 
                            <th>Stopper</th>
                            <td>{{$bugsStopper}}</td>
                        </tr>
                        </tr>  
                        <tr> 
                            <th style="color:#F3A100">Total</th>
                            <td style="color:#F3A100">{{$bugsReportados}}</td>
                        </tr>  
                    </tbody>  
            </table>
        </div>
    </div>

    <div class="col-sm col-3 col-lg-3">
        <h3 class="profile-usertitle-job" style="cursor:auto">Hallazgos rechazados</h3>
        <div class="table-responsive" >
            <table class="table" style="color:#304457">
                <tbody>  
                        <tr> 
                            <th>Duplicado</th>
                            <td>{{$rechazoDuplicado}}</td>
                            </tr>
                        <tr> 
                            <th>Fuera de alcance</th>
                            <td>{{$rechazoFueraAlcance}}</td>
                        </tr>
                        <tr> 
                            <th>No es un defecto</th>
                            <td>{{$rechazoNoDefecto}}</td>
                        </tr>
                        <tr> 
                            <th>No es posible reproducirlo</th>
                            <td>{{$rechazoNoReproducible}}</td>
                        </tr>
                        </tr>  
                        <tr> 
                            <th style="color:#F3A100">Total</th>
                            <td style="color:#F3A100">{{$bugsRechazados}}</td>
                        </tr>  
                    </tbody>  
            </table>
        </div>
    </div>

    <div style="text-align:right">
        <a class="btn btn-primary"  href="{{ route('bugs.index') }}" style="margin-top: -10px;margin-bottom: 5px; margin: 10px">Mis proyectos</a>
    </div>


@else
    No hay incidentes
@endif
@endsection

