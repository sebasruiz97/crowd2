<style>
    .listUsers {
        max-height: 300px;
        overflow: auto;
        padding-top: 10px;
        padding-left: 20px;
        list-style: none;
    }

    .listUsers li {
        cursor: pointer;
    }

    .listUsers li:hover {
        color: #3f8297;
        text-decoration: underline;
    }
</style>
<div class="modal fade" id="ajax-users-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto; padding-right:40px; padding-left:20px; padding-top:20px;">
            <div class="modal-header">
                <h4 class="modal-title" id="alertCrudModal">Asignar usuarios</h4>
            </div>
            <div class="modal-body d-flex" style="display: flex;flex-wrap: wrap;">
                <!-- Enlace -->
                <div class="row m-0" style="width: 100%;">
                    <div class="col-sm-6">
                        <label>Lista usuarios</label>
                        <div style="border:1px solid lightgray;min-height:100px">
                            <ul id="listUsers" class="listUsers">
                                @foreach($users as $user)
                                <li data-id="{{$user->id}}">
                                    {{ $user->name }} - {{ $user->email }}
                                    <input hidden type="text" name="usuarios[]" id="usuario{{$user->id}}" value="{{$user->id}}" />
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label>Usuarios asignados</label>
                        <div style="border:1px solid lightgray;min-height:100px">
                            <form method="POST" action="{{ route('alertas.usuarios') }}" id="alertUsersForm" name="bugForm" class="form-horizontal" style="width: 100%;">
                                @csrf
                                <ul id="listUsersAs" class="listUsers"></ul>
                                <div style="display: none;">
                                    <input name="alerta" id="alerta" required>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 centrar" style="margin-top: 20px;">
                    <button type="button" class="btn btn-primary" id="btn-assign" value="assign-users">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click', '#listUsers li', function(e) {
        $(this).appendTo('#listUsersAs');
    });
    $(document).on('click', '#listUsersAs li', function() {
        $(this).appendTo('#listUsers');
    });
    $('#btn-assign').on('click', function() {
        $('#alertUsersForm').submit();
    });
</script>