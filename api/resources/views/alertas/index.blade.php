@extends('layouts.appBugs')

@section('content')
<div class="container" style="width:100%">
    <br>
    <a href="javascript:void(0)" class="btn btn-default btn-xs" id="create-new-alert">Crear alerta</a>
    <br><br>
    @include('flash::message')
    <table class="table table-bordered table-striped" id="alertas_datatable" style="width:100%; font-size:13px" data-turbolinks="false">
        <thead>
            <tr>
                <th style="min-width: 60px">ID alerta</th>
                <th style="min-width: 150px">Titulo</th>
                <th style="min-width: 150px">Mensaje</th>
                <th style="min-width: 80px">Enlace</th>
                <th style="min-width: 100px">Fecha de creación</th>
                <th style="min-width: 50px">Acciones</th>
            </tr>
        </thead>
    </table>
</div>
<script>
    $(document).ready(function() {
        $('#alertas_datatable thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            $(this).html('<input type="text" style="font-size:12px; border-color:#304457; text-color:#ffff" placeholder="Buscar" />');

            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });
    });
    var table = $('#alertas_datatable').DataTable({
        drawCallback: function(settings) {
            var api = this.api();
            var datos = api.rows({
                page: 'current'
            }).data();
            $.each(datos, function(i, item) {
                var btns = $('.dt-button');
                btns.addClass('btn btn-info btn-xs');
                btns.removeClass('dt-button');
            });
        },
        orderCellsTop: true,
        fixedHeader: false,
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: "alertas",
            type: 'GET',
        },
        columns: [{
                data: 'id_alerta',
                name: 'id_alerta'
            },
            {
                data: 'titulo',
                name: 'titulo'
            },
            {
                data: 'mensaje',
                name: 'mensaje'
            },
            {
                data: 'enlace',
                name: 'enlace'
            },
            {
                data: 'created_at',
                name: 'created_at',
                type: "datetime"
            },
            {
                data: 'action',
                name: 'action'
            }
        ],
        order: [
            [0, 'desc']
        ],
        dom: 'Bfrtip',
        buttons: [
            [{
                extend: 'excel',
                exportOptions: {
                    orthogonal: 'export'
                }

            }],
            [{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    orthogonal: 'export'
                }
            }],
            [{
                extend: 'csv',
                exportOptions: {
                    orthogonal: 'export'
                }
            }],
            [{
                extend: 'print',
                text: 'Imprimir',
                exportOptions: {
                    orthogonal: 'export'
                }
            }],
        ],
    });
</script>
@include('alertas.modalCrearAlerta')
@include('alertas.modalAsignarUsuarios')
<script>
    $('#create-new-alert').click(function() {
        $('#alertForm').trigger("reset");
        $('#ajax-crud-modal').modal('show');
    });
    $(document).on('click', '.assign', function(e) {
        $('#listUsersAs li').appendTo('#listUsers');
        let obj = $(e.target).hasClass('lnr') ? $(e.target).parent() : $(e.target);
        const { id } = $(obj).data();
        $.ajax({
            type: "post",
            url: "{{ route('alertas.usuarios') }}",
            data: {
                id_alerta: id
            },
            success: function(response) {
                if (response.length > 0) {
                    response.forEach(alertaUser => {
                        $('#listUsers li[data-id="' + alertaUser.id_usuario + '"]').appendTo('#listUsersAs');
                    });
                }
                $('#alertUsersForm #alerta').val(id);
                $('#ajax-users-modal').modal('show');
            },
            error: function(data) {
                console.log('Error:', data);
                $('#btn-save').html('Guardar cambios');
            }
        });
    });
</script>
@endsection