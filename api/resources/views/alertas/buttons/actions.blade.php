<button class="assign btn btn-default btn-xs" title="Asignar usuarios" data-id="{{ $id_alerta }}">
    <i class="lnr lnr-users"></i>
</button>

<button class="reload btn btn-default btn-xs" title="Retomar alerta" data-id="{{ $id_alerta }}">
    <i class="lnr lnr-redo"></i>
</button>

<form method="post" action="{{ route('alertas.enviar') }}">
    @csrf
    <button class="send btn btn-default btn-xs" title="Enviar alerta">
        <i class="lnr lnr-rocket"></i>
        <input hidden type="text" name="id_alerta" id="id_alerta" value="{{ $id_alerta }}">
    </button>
</form>