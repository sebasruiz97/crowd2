<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto; width:600px; padding-right:40px; padding-left:20px; padding-top:20px;">
            <div class="modal-header">
                <h4 class="modal-title" id="alertCrudModal">Crear Alerta</h4>
            </div>
            <div class="modal-body d-flex" style="display: flex;flex-wrap: wrap;">
                <form action="{{ route('alertas.create') }}" id="alertForm" name="bugForm" class="form-horizontal">
                    <!-- Titulo -->
                    <div class="form-group col-sm-12" style="padding-bottom: 1px">
                        <label for="titulo">Titulo</label>
                        <input class="form-control" name="titulo" id="titulo" autocomplete="off" required>
                    </div>
                    <!-- Mensaje -->
                    <div class="form-group col-sm-12" style="padding-bottom: 1px">
                        <label for="mensaje">Mensaje</label>
                        <textarea class="form-control" name="mensaje" id="mensaje" required></textarea>
                    </div>
                    <!-- Enlace -->
                    <div class="form-group col-sm-12" style="padding-bottom: 1px">
                        <label for="enlace">Enlace</label>
                        <input class="form-control" name="enlace" id="enlace" required>
                    </div>
                    <div class="col-sm-12 centrar">
                        <button type="submit" class="btn btn-primary" id="btn-save" value="create-alert">Crear alerta</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>