<body style="background-color: #ffff">


	<hr style="background-color: #208297; border-color: #208297;  height: 35px; text-align: center; font-size:18px; margin-bottom: 10px">

	<h3 style="text-transform: uppercase; font-size:18px; text-align:center">Recibiste un nuevo mensaje</h3>
	<h4 style="font-weight: normal;  font-size:14px; text-align:center" >El mensaje fue enviado a través de la sesión de "Contáctanos" de CROWD. A continuación algunos detalles del mensaje</h4>


<div class="table" style="font-size: 13px;">
	<table style="margin: 0 auto;">
	<tbody style="text-align: center; ">  
		<tr> 
			<th style="text-align: left;  border-bottom: 0px; ">Enviado por</th>
				<td  style="text-align: justify">{{$msg['nombre']}}</td>
			</tr>
		<tr> 
			<th style="text-align: left;  border-bottom: 0px;">Correo electrónico</th>
			<td  style="text-align: justify"> - {{$msg['email']}}</td>
		</tr>
		<tr> 
			<th style="text-align: left;  border-bottom: 0px;">Asunto</th>
			<td  style="text-align: justify">{{$msg['asunto']}}</td>
		</tr>
		<tr > 
			<th style="text-align: left;  border-bottom: 0px;">Mensaje</th>
			<td  style="text-align: justify">{{$msg['mensaje']}}</td>
		</tr>				
	</tbody>  
	</table>

	<br>
	<footer style="font-size: 12px;  text-align:center; color:#C1C4C7">
		{{ config('app.name') }} | Copyright - {{ date('Y') }}
	</footer>

</div>
</body>



