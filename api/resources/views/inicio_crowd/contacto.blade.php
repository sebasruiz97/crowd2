@extends('layouts.inicio_crowd')

<style>
    .col-sm {
        overflow: hidden;
    }

    .error {
        color: #F3A100;
        font-style: italic;
        font-weight: normal;
    }

    .form-group {
        display: contents;
    }
</style>

@section('content')

<div class="row">
    <div class="col-sm col-3 col-lg-3">
    </div>
    <div class="col-sm col-6 col-lg-6">
        <!--  Imagen de  Encabezado -->
		<img src="/storage/images/contact_encabezado.png" style="align: center; width:100%; height:auto;">

        <div class="profile-content" style="width:600px; padding:40px; padding-top:20px;">
            <div class="centrar">
                <img src="/storage/images/contacto.png" style="text-align: center" width="15%">
            </div>
            <h3 style="text-align: center"> Envíanos un mensaje </h3> <br>
            @include('flash::message')
            <form method="POST" action="{{ route('mensaje') }}">
                @csrf

                @if ($errors->has('g-recaptcha-response'))
                <div style="margin-bottom: 5px;">
                    <span class="error" style="color:red">
                        <small><i>{{ $errors->first('g-recaptcha-response') }}</i></small>
                    </span>
                </div>
                @endif

                <!-- Nombre -->
                <div class="form-group col-sm-12" style="padding-bottom: 1px">
                    <label for="nombre">Nombre</label>
                    <input class="form-control" value="{{old('nombre')}}" name="nombre" type="text" id="nombre" required maxlength="50" title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 50 caracteres">
                    @if ($errors->has('nombre'))
                    <span class="help-block" style="color:#F3A100" ;>
                        <small><i>{{ $errors->first('nombre') }}</i></small>
                    </span>
                    @endif
                </div>


                <!-- Email -->
                <div class="form-group col-sm-12" style="padding-bottom: 1px">
                    <label for="email">Correo Electrónico</label>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" required minlength="4" maxlength="100" title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 100 caracteres">
                    @if ($errors->has('email'))
                    <span class="help-block" style="color:#F3A100" ;>
                        <small><i>{{ $errors->first('email') }}</i></small>
                    </span>
                    @endif
                </div>


                <!-- Asunto-->
                <div class="form-group col-sm-12" style="padding-bottom: 1px">
                    <label for="asunto">Asunto del mensaje</label>
                    <input class="form-control" value="{{old('asunto')}}" name="asunto" type="text" id="asunto" required minlength="4" maxlength="200" title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 200 caracteres">
                    @if ($errors->has('asunto'))
                    <span class="help-block" style="color:#F3A100" ;>
                        <small><i>{{ $errors->first('asunto') }}</i></small>
                    </span>
                    @endif
                </div>

                <!-- Mensaje-->
                <div class="form-group col-sm-12" style="padding-bottom: 1px">
                    <label for="mensaje">Mensaje</label>
                    <textarea class="form-control" value="{{old('mensaje')}}" name="mensaje" type="text" id="mensaje" required minlength="4" maxlength="800" title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 800 caracteres"> </textarea>
                    @if ($errors->has('mensaje'))
                    <span class="error" id="Errormensaje" style="color:#F3A100" ;>
                        <span style="color:#808b96; font-size:10px" id="rchars"><small style="color:#808b96; font-size:10px">800</span> caracteres restantes</small>
                        @endif
                </div>

                <div style="margin-top: 15px;display: inline-grid;">
                    @if($errors->has('g-recaptcha-response'))
                        <div style="border: 1px solid red;padding: 5px;display: inline-block;">
                    @else
                        <div>
                    @endif
                            {!! htmlFormSnippet() !!}
                        </div>
                    </div>

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <div class="centrar" style="margin-top: 15px;">
                            <button class="btn btn-primary" type="submit">Enviar</button>
                            <a href={{ url('/') }} class="btn btn-default">Cancelar</a>
                        </div>
                    </div>

            </form>
        </div>
        <!-- Imagen Pie de pagina -->
		<img src="/storage/images/contact_pie_pagina.png" style="align: center; width:100%; height:auto;">
    </div>
    <div class="col-sm col-3 col-lg-3">
    </div>
</div>

@endsection

<script>
    var maxLength = 800;
    window.load = function() {
        $('#mensaje').keyup(function() {
            var textlen = maxLength - $(this).val().length;
            $('#rchars').text(textlen);
        });
    }
</script>