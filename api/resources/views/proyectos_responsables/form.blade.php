<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog" id="ajax-crud-modal" >
        <div class="modal-content" style="height:auto; width:600px; padding-right:40px; padding-left:20px; padding-top:20px;">
            <div class="modal-header">
                <h4 class="modal-title" id="userCrudModal"></h4>
            </div>
            <div class="modal-body" style="height:auto; width:600px; padding-right:40px; padding-left:40px; padding-top:20px;">
                <form id="userForm" name="bugForm" class="form-horizontal">

                    <input type="hidden" name="id_proyecto" id="id_proyecto" value="">
                    <input type="hidden" name="id_proyecto_responsable" id="id_proyecto_responsable" value="">
                    <input type="hidden" name="proyectoResponsable_fecha_fin2" id="proyectoResponsable_fecha_fin2" value="">
                    <input type="hidden" name="proyectoResponsable_fecha_inicio2" id="proyectoResponsable_fecha_inicio2" value="">
                    <input type="hidden" name="proyectoResponsable_Activo_SN2" id="proyectoResponsable_Activo_SN2" value="">
                    <input type="hidden" name="id_pr" id="id_pr" value="">


                    <input type="hidden" name="proyectorole_id" id="proyectorole_id" value="">

                    <input type="hidden" name="proyectoResponsable_motivoFin2" id="proyectoResponsable_motivoFin2" value="">
                    {{-- <input type="hidden" name="proyecto_responsable_actual2" id="proyecto_responsable_actual2" value=""> --}}
                     @inject('proyectos_responsabes_tipos_novedades','App\Services\proyectos_responsabes_tipos_novedades')
                    <!-- Cliente -->
                    <div class="form-group col-sm-12" >
                        
                        <!-- Proyecto-->
                        <div class="form-group col-sm-12">
                        <label for="proyecto_nombre">Proyecto</label>
                        <input 
                            class="form-control" name="proyecto_nombre" 
                            value="{{old('proyecto_nombre')}}" type="text" 
                            id="proyecto_nombre" required 
                            disabled
                        >
                        </div>
                        <!-- Fron actual-->
                        <div class="form-group col-sm-12">
                        <label for="nombre_responsable_actual">Líder Actual</label>
                        <input 
                            class="form-control" name="nombre_responsable_actual" 
                            value="{{old('nombre_responsable_actual')}}" type="text" 
                            id="nombre_responsable_actual" required 
                            disabled
                        >
                        </div>

                        <!-- Nombre responsable-->
                        <div class="form-group col-sm-12">
                        <label for="nombre_responsable">Nombre Responsable</label>
                        <input 
                            class="form-control" name="nombre_responsable" 
                            value="{{old('nombre_responsable')}}" type="text" 
                            id="nombre_responsable" required 
                            disabled
                        >
                        </div>
                        <!-- Rol del tester-->
                        <div class="form-group col-sm-12">
                        <label for="proyectorole_Nombre">Rol en el Proyecto</label>
                        <input 
                            class="form-control" name="proyectorole_Nombre" 
                            value="{{old('proyectorole_Nombre')}}" type="text" 
                            id="proyectorole_Nombre" required 
                            disabled
                        >
                        </div>
                        <!-- Fecha de inicio-->
                        <div class="form-group col-sm-12">
                        <label for="proyectoResponsable_fecha_inicio">Fecha Inicio/Aceptación</label>
                        <input 
                            type="date" 
                            class="form-control" name="proyectoResponsable_fecha_inicio" 
                            value="{{old('proyectoResponsable_fecha_inicio')}}"  
                            id="proyectoResponsable_fecha_inicio" required 
                            disabled
                        >
                        </div>
                         <!-- Estado actual-->
                         <div class="form-group col-sm-12">
                         <label for="proyectoResponsable_Activo_SN">Estado Actual</label>
                         <input 
                            
                             class="form-control" name="proyectoResponsable_Activo_SN" 
                             min="<?php echo date("Y-m-d");?>"
                             value="{{old('proyectoResponsable_Activo_SN')}}"  
                             id="proyectoResponsable_Activo_SN" required 
                             disabled
                         >
                         </div>
                        <!-- Motivo de cambio-->
                        <div class="form-group col-sm-12">
                        <label for="proyectoResponsable_motivoFin">Motivo del cambio de Estado</label>
                        <select class="form-control" id="proyectoResponsable_motivoFin" name="proyectoResponsable_motivoFin" required onchange="CambioMotivo()">
                            @foreach($proyectos_responsabes_tipos_novedades->get() as  $index => $proyectos_responsabes_tipos_novedades)
                                <option value = "{{ $index }}" {{old('proyectoResponsable_motivoFin') == $index ? 'selected' : ''}} > {{  $proyectos_responsabes_tipos_novedades }}</option>
                            @endforeach
                        </select>
                        </div>

                        <!-- Proyecto-->
                        <div class="form-group col-sm-12">
                        <label for="proyectoResponsable_fecha_fin">Fecha Fin/DesAsignación</label>
                        <input 
                            type="date" 
                            class="form-control" 
                           
                            name="proyectoResponsable_fecha_fin" 
                            value="{{old('proyectoResponsable_fecha_fin')}}" type="text" 
                            id="proyectoResponsable_fecha_fin"  
                            disabled
                        >
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <div class="centrar">
                            <button type="submit" class="btn btn-primary" id="btn-save" value="create">Guardar cambios
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <h6 style="color:#fff"> Crowd </h6>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<script>
    function CambioMotivo() {
      var x = document.getElementById("proyectoResponsable_motivoFin").value;
      document.getElementById("proyectoResponsable_motivoFin2").value =  x;

      var x = document.getElementById("proyectoResponsable_fecha_fin").value;
      document.getElementById("proyectoResponsable_fecha_fin2").value =  x;
      var x = document.getElementById("proyectoResponsable_fecha_inicio").value;
      document.getElementById("proyectoResponsable_fecha_inicio2").value =  x;

      
      var x = document.getElementById("proyectoResponsable_motivoFin").value;
      document.getElementById("proyectoResponsable_motivoFin2").value =  x;
      
      var x = document.getElementById("proyectoResponsable_Activo_SN").value;
      document.getElementById("proyectoResponsable_Activo_SN2").value =  x;
      

    }
    $(document).ready(function() {
jQuery.extend(jQuery.validator.messages, {
  required: "Este campo es obligatorio.",
  remote: "Por favor, rellena este campo.",
  email: "Por favor, escribe una dirección de correo válida",
  url: "Por favor, escribe una URL válida.",
  date: "Por favor, escribe una fecha válida.",
  dateISO: "Por favor, escribe una fecha (ISO) válida.",
  number: "Por favor, escribe un número entero válido.",
  digits: "Por favor, escribe sólo dígitos.",
  creditcard: "Por favor, escribe un número de tarjeta válido.",
  equalTo: "Por favor, escribe el mismo valor de nuevo.",
  accept: "Por favor, escribe un valor con una extensión aceptada.",
  maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
  minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
  rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
  range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
  max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
  min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
});
});
    </script>

