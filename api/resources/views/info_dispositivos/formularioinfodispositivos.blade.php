<!DOCTYPE html>

<html lang="en">

<head>

    <title>Gestion de Formularios</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://nightly.datatables.net/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <script src="https://nightly.datatables.net/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <link rel="stylesheet" href="{{ asset('/storage/css/iconos.css') }}">

    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>

    <script>
        $.extend(true, $.fn.dataTable.defaults, {
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "lengthMenu": "Mostrar _MENU_ registros",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": ">",
                    "previous": "<"
                },
                "processing": "Procesando...",
                "search": "Buscar:",
                "searchPlaceholder": "Término de búsqueda",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "select": {
                    "rows": {
                        _: '%d filas seleccionadas',
                        0: 'clic fila para seleccionar',
                        1: 'una fila seleccionada'
                    }
                }
            }
        });
    </script>

    <style>
        label {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 5px;
            font-weight: 700;
            font-weight: bold;
            font-size: 12px;
            color: #304457;

        }

        body {
            font: "Helvetica Neue", Verdana, Arial, Helvetica, sans-serif;
            margin: 0;
            padding: 0;
            color: #333;
            background-color: #fff;
            font-size: 14px;
        }

        .modal-lg {
            max-width: 80% !important;
        }

        .error {
            color: #F3A100 !important;
            font-style: italic !important;
            font-weight: normal !important;

        }

        option:disabled {
            background: gray;
            color: #304457;
        }

        option:active {
            background: blue;
            color: #304457;
        }

        select:hover {
            background-color: #F8F7F2;
        }

        input[type=search] {
            text-align: center;
            display: inline-block;
            width: auto;
            border-color: #d5dbdb;
            border-radius: 3px;
            font-weight: normal;
            font-size: 12px;
            float: right;
        }

        .input[type=search]:hover {
            color: transparent;
            background-color: transparent;
            border-color: transparent;
        }

        .dataTables_wrapper {
            position: relative;
            font-size: 14px;
            top: 20px;
            left: 10px;
            width: 1000px
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current,
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
            border: 1px solid #208297;
            font-size: 12px;
            background-color: #f3a100 !important;
            background-origin: #f3a100 !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled,
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover,
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            cursor: default;
            color: #909090 !important;
            font-size: 12px;
            border: 1px solid transparent;
            border-color: #E3EAE9;
            box-shadow: none;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current,
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #ffff !important;
            border: 1px solid #208297;
            font-size: 12px;
            background-color: #208297 !important;
            background: -webkit-linear-gradient(top, #304457 100%, gainsboro 100%);
            background: -moz-linear-gradient(top, #304457 100%, gainsboro 100%);
            background: -ms-linear-gradient(top, #304457 100%, gainsboro 100%);
            background: -o-linear-gradient(top, #304457 100%, gainsboro 100%);
            background: linear-gradient(to bottom, #304457 100%, gainsboro 100%);

        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #ffff !important;
            border: 1px solid #304457;
            font-size: 12px;
            background-color: #208297 !important;
            background: -webkit-linear-gradient(top, #304457 100%, gainsboro 100%);
            background: -moz-linear-gradient(top, #304457 100%, gainsboro 100%);
            background: -ms-linear-gradient(top, #304457 100%, gainsboro 100%);
            background: -o-linear-gradient(top, #304457 100%, gainsboro 100%);
            background: linear-gradient(to bottom, #304457 100%, gainsboro 100%);
        }

        .dataTables_wrapper .dataTables_info {
            clear: both;
            float: left;
            padding-top: 0.755em;
            font-size: 12px;
        }

        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>thead>tr>th {
            padding: 8px;
            line-height: 1.42857143;
            vertical-align: top !important;
            border-top: transparent !important;
        }


        .modal-header {
            padding: 5px;
            border-bottom: transparent !important;
        }

        .centrar {
            text-align: center;
        }

        .btn-primary {
            color: #fff;
            background-color: #208297;
            border-color: #208297;
            width: auto;
            height: 33px;
            text-transform: none;
        }


        .btn-primary:focus,
        .btn-primary.focus {
            color: #f3a100;
            background-color: #ffff;
            border-color: #f3a100;
        }

        .btn-primary:hover {
            color: #f3a100;
            background-color: #ffff;
            border-color: #f3a100;
        }

        .btn-primary:active,
        .btn-primary.active,
        .open>.dropdown-toggle.btn-primary {
            color: #f3a100;
            background-color: #ffff;
            border-color: #f3a100;
        }

        .btn-primary:active:hover,
        .btn-primary.active:hover,
        .open>.dropdown-toggle.btn-primary:hover,
        .btn-primary:active:focus,
        .btn-primary.active:focus,
        .open>.dropdown-toggle.btn-primary:focus,
        .btn-primary:active.focus,
        .btn-primary.active.focus,
        .open>.dropdown-toggle.btn-primary.focus {
            color: #f3a100;
            background-color: #ffff;
            border-color: #f3a100;
        }

        .btn-primary:active,
        .btn-primary.active,
        .open>.dropdown-toggle.btn-primary {
            background-image: none;
        }

        .btn-primary.disabled:hover,
        .btn-primary[disabled]:hover,
        fieldset[disabled] .btn-primary:hover,
        .btn-primary.disabled:focus,
        .btn-primary[disabled]:focus,
        fieldset[disabled] .btn-primary:focus,
        .btn-primary.disabled.focus,
        .btn-primary[disabled].focus,
        fieldset[disabled] .btn-primary.focus {
            background-color: #ffff;
            border-color: #f3a100;
        }

        .btn-primary .badge {
            color: #f3a100;
            background-color: #fff;
        }

        .form-control {
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 12px;
            line-height: 1.42857143;
            color: #208297 !important;
            background-color: #fff !important;
            background-image: none;
            border: 1px solid #e5e8e8 !important;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 0px 0px #fff !important;
            box-shadow: inset 0 0px 0px #fff !important;
        }

        select:hover {
            background-color: #fff;
        }

        input,
        button,
        select,
        textarea {
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
        }

        .form-control:focus {
            border-color: #208297 !important;
            outline: 0;
            -webkit-box-shadow: inset 0 0px 0px #fff !important;
            box-shadow: inset 0 0px 0px #fff !important;
        }

        .help-block {
            display: block;
            margin-top: 5px;
            margin-bottom: 10px;
            color: #737373;
        }

        .modal-body {
            position: relative;
            padding: 15px;
            border-bottom: transparent !important;
            border-top: transparent;
            border-bottom-color: transparent;
            border-top-color: transparent;
        }

        .modal-footer {
            padding: 15px;
            text-align: right;
            border-top: 1px solid #FFFF !important;
        }

        .btn-default {
            color: #fff;
            background-color: #208297 !important;
            border-color: #208297 !important;
        }

        .btn-default:hover {
            color: #333 !important;
            background-color: #e6e6e6 !important;
            border-color: #adadad;

        }

        #marcaMenu>li>a, #modeloMenu>li>a {
            white-space: initial;
            border: 1px solid #e5e8e8;
            border-width: 0 0 1px 0;
            cursor: pointer;
        }

        .divModelo.disabled {
            cursor: not-allowed;
        }

        .divModelo.disabled>div {
            pointer-events: none;
        }
    </style>


</head>

<body>


    <div class="container" style="margin-left:70px; margin-right:20px">
        @include('flash::message')
        <!--   <h2 class="text-center mt-2 mb-3 alert alert-success">Tipos de pruebas</h2> -->
        <a href="javascript:void(0)" class="text-center btn btn-success mb-1" onclick="addTodo()">Crear</a>

        <table class="table table-row-border hover" id="tablaDatos">


            <thead style="background-color:#208297; color:#fff">
                <tr>
                    <th>Dispositivo usado</th>
                    <th width="300px">Navegador del Dispositivo</th>
                    <th width="300px">Sistema Operativo del Dispositivo</th>
                    <th width="300px">Marca del Dispositivo</th>
                    <th width="300px">Modelo del Dispositivo</th>
                    <th>Acción</th>
                </tr>
            </thead>

        </table>
    </div>

    <div class="modal fade" id="todo-modal" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-body" style="display:table">

                    <form id="todoForm" name="todoForm">

                        <table id="tablaDatos">

                            <input type="hidden" name="todo_id" id="todo_id">
                            <input type="hidden" name="user_id" id="user_id">
                            <input type="hidden" name="dispositivo_id1" id="dispositivo_id1">
                            <input type="hidden" name="sistemaOperativo_id1" id="sistemaOperativo_id1">
                            <input type="hidden" name="navegador_id1" id="navegador_id1">
                            <input type="hidden" name="modelo_id" id="modelo_id">
                            <input type="hidden" name="marca_id" id="marca_id">

                            <div class="form-group col-sm-12">
                                <div class="form-group col-sm-2"> </div>
                                <div class="form-group col-sm-8">

                                    <div class="form-group col-sm-6">
                                        <label>Tipo de Dispositivo</label>
                                        <select class="form-control" id="dispositivo_id" name="dispositivo_id" required="" onchange="CargarListasSoyNav(this.options[this.selectedIndex].value)">
                                            <option value="" disabled selected>Selecciona Tipo de Dispositivo</option>
                                        </select>
                                        @if ($errors->has('dispositivo_id'))
                                        <span class="help-block" style="color:#F3A100" ;>
                                            <small><i>{{ $errors->first('dispositivo_id') }}</i></small>
                                        </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <label>Sistema Operativo</label>
                                        <select class="form-control" id="sistemaOperativo_id" name="sistemaOperativo_id" disabled onclick="asiso()">
                                            <option value=''>Selecciona Sistema Operativo </option>
                                        </select>
                                        @if ($errors->has('sistemaOperativo_id'))
                                        <span class="help-block" style="color:#F3A100" ;>
                                            <small><i>{{ $errors->first('sistemaOperativo_id') }}</i></small>
                                        </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <label>Navegador</label>
                                        <select class="form-control" id="navegador_id" name="navegador_id" disabled onchange="asinav()">
                                            <option value=''>Selecciona Navegador </option>
                                        </select>
                                        @if ($errors->has('navegador_id'))
                                        <span class="help-block" style="color:#F3A100" ;>
                                            <small><i>{{ $errors->first('navegador_id') }}</i></small>
                                        </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-6" style="display: grid;">
                                        <label style="margin-bottom: 8px;">Marca</label>
                                        <div class="btn-group">
                                            <button class="btn btn-secondary dropdown-toggle form-control" type="button" id="dropdownMarca" data-toggle="dropdown" aria-expanded="false" style="text-align: left;">
                                                <span>Seleccionar Marca</span>
                                                <i class="icon-submenu lnr lnr-chevron-down" style="float:right;"></i>
                                            </button>
                                            <ul id="marcaMenu" class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="max-height: 150px;overflow: auto;width:100%">
                                                <li style="padding: 0 10px;">
                                                    <input autocomplete="off" class="form-control" type="search" name="searchDis" id="searchDis" style="text-align: left;float: none;width:100%" placeholder="Buscar...">
                                                </li>
                                                <li role="separator" class="divider"></li>
                                            </ul>
                                        </div>
                                        @if ($errors->has('dispositivo_id'))
                                        <span class="help-block" style="color:#F3A100" ;>
                                            <small><i>{{ $errors->first('dispositivo_id') }}</i></small>
                                        </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-6 divModelo disabled" style="display: grid;">
                                        <label>Modelo</label>
                                        <div class="btn-group">
                                            <button class="btn btn-secondary dropdown-toggle form-control" type="button" id="dropdownModelo" data-toggle="dropdown" aria-expanded="false" style="text-align: left;">
                                                <span>Seleccionar Modelo</span>
                                                <i class="icon-submenu lnr lnr-chevron-down" style="float:right;"></i>
                                            </button>
                                            <ul id="modeloMenu" class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="max-height: 150px;overflow: auto;width:100%">
                                                <li style="padding: 0 10px;">
                                                    <input autocomplete="off" class="form-control" type="search" name="searchMod" id="searchMod" style="text-align: left;float: none;width:100%" placeholder="Buscar...">
                                                </li>
                                                <li role="separator" class="divider"></li>
                                            </ul>
                                        </div>
                                        @if ($errors->has('modelo_id'))
                                        <span class="help-block" style="color:#F3A100" ;>
                                            <small><i>{{ $errors->first('modelo_id') }}</i></small>
                                        </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <div class="centrar">
                                            <button type="submit" class="btn btn-primary" id="btn-save" value="create">Guardar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group col-sm-2"></div>
                        </table>
                    </form>
                </div>

            </div>
        </div>
    </div>

    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {

            var table = $('#tablaDatos').DataTable({
                processing: false,
                serverSide: true,
                pageLength: 6,
                ajax: {
                    url: "{{ url('formulariosinfoDispositivos') }}",
                    type: 'GET',
                },
                columns: [{
                        data: 'nombre_dispositivo',
                        name: 'nombre_dispositivo'
                    },
                    {
                        data: 'nombre_navegador',
                        name: 'nombre_navegador'
                    },
                    {
                        data: 'nombre_sistemaOperativo',
                        name: 'nombre_sistemaOperativo'
                    },
                    {
                        data: 'marca_dispositivo',
                        name: 'marca_dispositivo'
                    },
                    {
                        data: 'modelo_dispositivo',
                        name: 'modelo_dispositivo'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    }
                ]
            });
        });


        //for add todo
        function addTodo() {
            CargarLista(0);
            var clave = <?php
                        if (isset($user)) {
                            echo ($user);
                        }
                        ?>;


            $('#todo-modal').find("input,select,textarea").val('').end();
            $('#todo_id').val("");
            $('#user_id').val(clave);
            $('#todo-modal').modal('show');
        }

        function CargarMarcas(term = false) {
            $.ajax({
                url: 'getMarcas',
                type: 'get',
                dataType: 'json',
                data: term ? {
                    term: term
                } : null,
                success: function(response) {
                    llenarMenu(response['data'], 'itemDispo', '#marcaMenu', 'id_dispositivo');
                }
            });
        }
        CargarMarcas();
        $(document).on('click', '#marcaMenu>li>a', function(e) {
            const {
                id
            } = e.target.dataset;
            let name = e.target.innerText;
            $('#dropdownMarca>span').html(name);
            CargarModelos(id);
            $('#marca_id').val(id);
            $('.divModelo').removeClass('disabled');
        });
        $('#marcaMenu>li>input').keyup(function(e) {
            const {
                value
            } = e.target;
            if (value === '') {
                CargarMarcas();
            } else {
                CargarMarcas(value);
            }
        });

        function CargarModelos(marca, term = false) {
            $.ajax({
                url: 'getModelos',
                type: 'get',
                dataType: 'json',
                data: {
                    idMarca: marca,
                    term
                },
                success: function(response) {
                    llenarMenu(response['data'], 'itemModelo', '#modeloMenu', 'id_modelo');
                }
            });
        }
        $(document).on('click', '#modeloMenu>li>a', function(e) {
            const {
                id
            } = e.target.dataset;
            let name = e.target.innerText;
            $('#dropdownModelo>span').html(name);
            $('#modelo_id').val(id);
        });
        $('#modeloMenu>li>input').keyup(function(e) {
            const {
                value
            } = e.target;
            const id = $('#marca_id').val();
            if (value === '') {
                CargarModelos(id);
            } else {
                CargarModelos(id, value);
            }
        });

        function llenarMenu(data, itemSelector, menuSelector, identificador) {
            $('.' + itemSelector).remove();
            var len = 0;
            if (data != null) {
                len = data.length;
            }
            if (len > 0) {
                // Read data and create <option >
                for (var i = 0; i < len; i++) {
                    var id = data[i][identificador];
                    var name = data[i].nombre;
                    var option = "<li class='" + itemSelector + "'><a data-id='" + id + "'>" + name + "</a></li>";

                    $(menuSelector).append(option);
                }
                // ElListaHerraxTipo(id);
            }
        }

        function CargarLista(p) {
            var complex = <?php echo json_encode($lista_tipo_dispositivos); ?>;
            // console.log(complex);
            // Limpiar  dropdown de Herramientas
            $('#dispositivo_id').find('option').not(':first').remove();
            var x = document.getElementById("dispositivo_id");
            $.each(complex, function(i, item) {
                var option = document.createElement("option");
                //  if(complex[i].userid=="")
                //   {
                option.value = complex[i].id_dispositivo;
                option.text = complex[i].nombre_dispositivo;
                x.add(option);
                //  }



            })
        }

        function CargarListasDisp() {


            //disp
            document.getElementById("dispositivo_id").disabled = false;
            // Limpiar  dropdown de Herramientas
            $('#dispositivo_id').find('option').not(':first').remove();

            // AJAX request 
            $.ajax({
                url: 'getDispositivos',
                type: 'get',
                dataType: 'json',
                success: function(response) {

                    var len = 0;
                    if (response['data'] != null) {
                        len = response['data'].length;
                    }

                    if (len > 0) {
                        // Read data and create <option >
                        for (var i = 0; i < len; i++) {
                            var id = response['data'][i].dispositivo_id;
                            var name = response['data'][i].nombre_dispositivo;
                            var option = "<option value='" + id + "'>" + name + "</option>";

                            $("#dispositivo_id").append(option);
                        }
                        // ElListaHerraxTipo(id);

                    }

                }
            });
            //fin disp

        }


        function CargarListasSoyNav(id) {

            $('#dispositivo_id1').val(document.getElementById("dispositivo_id").value);
            // alert(document.getElementById("dispositivo_id1").value);
            document.getElementById("sistemaOperativo_id").disabled = false;
            document.getElementById("sistemaOperativo_id").required = true;
            document.getElementById("navegador_id").required = true;

            // Limpiar  dropdown de Herramientas
            $('#sistemaOperativo_id').find('option').not(':first').remove();

            // AJAX request 
            $.ajax({
                url: 'getSoxDispxTipo/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {

                    var len = 0;
                    if (response['data'] != null) {
                        len = response['data'].length;
                    }

                    if (len > 0) {
                        // Read data and create <option >
                        for (var i = 0; i < len; i++) {
                            var id = response['data'][i].sistemaOperativo_id;
                            var name = response['data'][i].nombre_sistemaOperativo;
                            var option = "<option value='" + id + "'>" + name + "</option>";

                            $("#sistemaOperativo_id").append(option);
                        }
                        // ElListaHerraxTipo(id);

                    }

                }
            });

            ///fin cambio

            //disp
            document.getElementById("navegador_id").disabled = false;
            // Limpiar  dropdown de Herramientas
            $('#navegador_id').find('option').not(':first').remove();

            // AJAX request 
            $.ajax({
                url: 'getNavDispxTipo/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {

                    var len = 0;
                    if (response['data'] != null) {
                        len = response['data'].length;
                    }

                    if (len > 0) {
                        // Read data and create <option >
                        for (var i = 0; i < len; i++) {
                            var id = response['data'][i].navegador_id;
                            var name = response['data'][i].nombre_navegador;
                            var option = "<option value='" + id + "'>" + name + "</option>";

                            $("#navegador_id").append(option);
                        }
                        //ElListaHerraxTipo(id);

                    }

                }
            });
            //fin disp

        }
        //for delete todo
        function deleteTodo(id) {

            if (confirm("Esta seguro que desea Eliminar!")) {
                $.ajax({
                    type: "get",
                    url: "{{ 'formulariosinfoDispositivosdelete' }}" + '/' + id,
                    success: function(data) {
                        // alert('Fue eliminado');
                        var oTable = $('#tablaDatos').dataTable();
                        oTable.fnDraw(false);
                        location.reload(true);
                        console.log(data);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });


            }
        }

        function asiso() {
            $('#sistemaOperativo_id1').val(document.getElementById("sistemaOperativo_id").value);
            // alert(document.getElementById("sistemaOperativo_id").value);

        }

        function asinav() {
            $('#navegador_id1').val(document.getElementById("navegador_id").value);
            // alert(document.getElementById("navegador_id").value);

        }

        if ($("#todoForm").length > 0) {


            $("#todoForm").validate({

                submitHandler: function(form) {

                    var actionType = $('#btn-save').val();
                    $('#btn-save').html('Enviando..');

                    $.ajax({
                        data: $('#todoForm').serialize(),
                        url: "{{ 'formulariosinfoDispositivosstore' }}",
                        type: "POST",
                        dataType: 'json',
                        success: function(data) {

                            $('#todoForm').trigger("reset");
                            $('#todo-modal').modal('hide');
                            $('#btn-save').html('Guardar Cambios');
                            var oTable = $('#tablaDatos').dataTable();
                            oTable.fnDraw(false);
                            location.reload(true);



                        },
                        error: function(data) {
                            console.log('Error:', data);
                            $('#btn-save').html('Guardar Cambios');
                        }
                    });
                }
            })
        }
    </script>

    <script>
        jQuery.extend(jQuery.validator.messages, {
            required: "Este campo es requerido",
        });
    </script>

</body>

</html>