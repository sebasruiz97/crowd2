@extends('layouts.app2')

@section('content')

@include('flash::message')
    <h1 class="pull-right">
        <a class="btn btn-default btn-xs pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px"
            href="{{ url('info_dispositivos') }}">Añadir dispositivo</a>
    </h1>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div style="text-align:center">
        <h3 >Aún no has agregado con que dispositivos cuentas</h3>
			<h5>
				Recuerda que esta sección de tu perfil es obligatoria para quedar activo en la plataforma.
			</h5>
    </div>

@endsection
