<!doctype html>
<html lang="en">
  <head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.2/handlebars.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/storage/css/iconos.css') }}">
    
    <script>
        
        $.extend( true, $.fn.dataTable.defaults, {
        "language": {
        "decimal": ",",
        "thousands": ".",
        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "infoPostFix": "",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "loadingRecords": "Cargando...",
        "lengthMenu": "Mostrar _MENU_ registros",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": ">",
            "previous": "<"
        },
        "processing": "Procesando...",
        "search": "Buscar:",
        "searchPlaceholder": "Término de búsqueda",
        "zeroRecords": "No se encontraron resultados",
        "emptyTable": "Ningún dato disponible en esta tabla",
        "aria": {
            "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "select": {
            "rows": {
                _: '%d filas seleccionadas',
                0: 'clic fila para seleccionar',
                1: 'una fila seleccionada'
            }
        }
    }           
    } );       
    </script>

    <style>

        table.dataTable thead .sorting {
        background-position: left;
        text-align: left;
        margin: 10px;
        width: 200px;
        }

        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #304457;
        border-color: #304457 !important;
        }

        thead input {
           width: 100%;
        }

        .btn-default2 {
        display: inline-block;
        font-size: 12px;
        border-width: 0px;
        padding-top: 2px;
        padding-bottom: 2px;
        font: 'Helvetica';
        background-color: #208297
        color: #fff;
        border-color: #208297;
        border-radius: 3px;
        box-shadow: none;
        
        }

        .btn-default2:focus,
        .btn-default2.focus {
        color: #333;
        background-color: #8c8c8c;
        border-color: #8c8c8c;
        }
        .btn-default2:hover {
        color: #333;
        background-color: #e6e6e6;
        border-color: #adadad;
        }

        .btn-default2:active,
        .btn-default2.active,
        .open > .dropdown-toggle.btn-default2 {
        color: #333!important;
        background-color: #e6e6e6!important;
        border-color: #adadad!important;
        }

        select.input-sm {
        height: 30px;
        line-height: 17px;
        text-align:center;
        }

        .dataTables_wrapper .dataTables_info {
        clear: both;
        float: left;
        padding-top: 0.755em;
        font-size: 10px;
        }
    </style>
     <style>

        .iframe{
            color: transparent!important;
            border-color: transparent!important;
        }

        .modal-sm {
           width: 90%; /* New width for large modal */
           height: 400px;
           background-color:#208297;
        }

        .modal-header {
            padding: 15px;
            border-bottom: 1px solid #fff;
        }
        div.letra{
             text-transform: capitalize;
         
            }
        .modal-header-info {
            color:#fff;
            padding:0px 7px;
            border-bottom:1px solid #208297;
            background-color: #208297;
            border-color: #208297;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }


    </style>
    
    <script type="text/javascript">
        function go(miframe, url, titulo,clase) {
            var clave = <?php echo ($user); ?>;
     
          // alert(clave);
           $('#user_id').val(clave);
          
            document.getElementById(miframe).src = url;
            titulomodal.innerText =(titulo);
            titulomodal.style.textTransform = 'initial'; 
            var element = document.getElementById("iconomodal");
            element.className = element.className ="cambio";
            element.className = element.className.replace(/\bcambio\b/g, clase);
          
        }
  
    </script>

  </head>
  <body>

   
    <!-- Modal -->
    <div class="container" >       
        
         <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm " >
                <div class="modal-content">
                    
                    <div class="modal-header modal-header-info " >

                        <table width="100%" >
                            <tr>
                              <th> <h1 style=" text-align:center"><i id="iconomodal" class="cambio" ></i></h1></th>
                              <th style=" text-align:center"> <h3 id="titulomodal" class="letra" style=" text-align:center" >xx</i></h3></th>
                              <th><button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color:white">×</button></th>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-body" style="padding-left:10px; padding-right:20px">
                        
                        <iframe width="100%" height="495" src="" id="miframe" style="color:transparent; border-color:transparent"></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- fin Modal -->
    <form name="frm-example" id="frm-example" >

        
        <div class="container=fluid p-3" >
            <br> <br>


            <div class="modal-header ">

                            <button 
                                type="button" 
                                id="CTipo_pruebas_new" 
                                class='btn-default2 pull-right' data-toggle='modal' 
                                data-target='#myModal' 
                                style="background-color:#208297; color:white"
                                title='Adicionar dispositivos disponibles para probar' 
                                onclick="go('miframe','formulariosinfoDispositivos',title,'lnr lnr-laptop-phone')">
                                <hi class="modal-title">Añadir dispositivos</i>
                            </button> 
            </div>


        
            <table class="table table-row-border" id="mytable"  >
                <thead >
                    <th>Dispositivos Usados</th>
                    <th>Sistema Operativo Dispositivo</th>
                    <th >Navegadores Dispositivo</th>
                </thead>
                <tbody  >
                 
                  
                        @foreach($dispositivos as $datos)
                        <tr>
                        
                        <td >{{$datos->Dispositivos_Usados}}
                                                
                        </td>
                        <td >{{$datos->Sistema_Operativo_Dispositivo}}
                            
                        </td>
                        <td >{{$datos->Navegadores_Sistema_Operativo_Dispositivo}}
                                                
                        </td>
                </tr>
                @endforeach
                   
                </tbody>

            </table>
        </div>
    </form>

    <script>
         $(document).ready(function() {
            var table = $('#mytable').DataTable( {
                orderCellsTop: true,
                fixedHeader: true,
                responsive: true,
                columnDefs: [
                        {   targets: -1,  
                            "width": "10px", 
                            className: 'dt[-head]-center'
                        }
                        ],
            });
        
            $("#myModal").on('hide.bs.modal', function(){
               // alert('The modal is about to be hidden.');
              // table.ajax.reload();

              location.reload(true);
                });
        
        });
    </script>
    
    <script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>

  </body>
</html>