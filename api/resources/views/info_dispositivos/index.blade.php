@extends('layouts.app2')

@section('content')

        <h3 class="text-center"  style="font-size: 18px; font-weight: bold; color: #298207; text-transform: uppercase ">  Dispositivos disponibles para probar </h3>
        
        @include('info_dispositivos.listadatosdispositivos')
@endsection
