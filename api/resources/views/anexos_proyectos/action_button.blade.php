@if(auth()->user()->hasRoles(['1','3']))

<a href="{{ route('anexosProyectos.show',$id_anexoProyecto) }}" data-toggle="tooltip"  id="descargar-anexo" data-id="{{ $id_anexoProyecto }}" data-original-title="Edit" 
    class="edit btn btn-default btn-xs " title="Descargar"> <i class="lnr lnr-download"></i></a>

<a href="javascript:void(0);" id="delete-user" data-toggle="tooltip" data-original-title="Delete" 
    data-id="{{ $id_anexoProyecto }}" class="delete btn btn-danger btn-xs" title="Eliminar">  <i class="lnr lnr-cross"></i></a>

@endif


@if(auth()->user()->hasRoles(['2']))

<a href="{{ route('anexosProyectos.show',$id_anexoProyecto) }}" data-toggle="tooltip"  id="descargar-anexo" data-id="{{ $id_anexoProyecto }}" data-original-title="Edit" 
    class="edit btn btn-default btn-xs " title="Descargar"> <i class="lnr lnr-download"></i></a>

@endif