
<!DOCTYPE html>
 
<html lang="en">
<head>

    <style>

        .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);
      
        }
    
        .dataTables_wrapper .dataTables_paginate .paginate_button  {
            box-sizing: border-box;
            display: inline-block;
            min-width: 1.5em;
            padding: 0.5em 1em;
            margin-left: 2px;
            text-align: center;
            text-decoration: none !important;
            cursor: pointer;
            *cursor: hand;
            color: #304457 !important;
            border: 1px solid transparent;
            border-radius: 2px;
        }
    
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover   {
      
            text-decoration: none !important;
            color: #ffff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);
        
        }
    
        .dataTables_wrapper .dataTables_paginate .paginate_button:focus  {
      
            text-decoration: none !important;
            color: #ffff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);
    
        }
     
    </style>
    
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>

    <script>
        
        $.extend( true, $.fn.dataTable.defaults, {
        "language": {
        "decimal": ",",
        "thousands": ".",
        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "infoPostFix": "",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "loadingRecords": "Cargando...",
        "lengthMenu": "Mostrar _MENU_ registros",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": ">",
            "previous": "<"
        },
        "processing": "Procesando...",
        "search": "Buscar:",
        "searchPlaceholder": "Término de búsqueda",
        "zeroRecords": "No se encontraron resultados",
        "emptyTable": "Ningún dato disponible en esta tabla",
        "aria": {
            "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "select": {
            "rows": {
                _: '%d filas seleccionadas',
                0: 'clic fila para seleccionar',
                1: 'una fila seleccionada'
            }
        }
        }           
        } );       
    </script>

</head>
<body>
 
<div class="container">
<br>

@if(auth()->user()->hasRoles(['1','3']))
<th style=" text-align:center"> <h3  style=" text-align:center" >Anexos del proyecto</i></h3></th> <br>
@endif

@if(auth()->user()->rol_id === 3 || auth()->user()->rol_id === 1 )
<a href="javascript:void(0)" class="btn btn-default btn-xs"  id="agregar_anexos_proyectos">Agregar Anexo</a>
<br><br>
@endif

@include('flash::message')
 
<table class="table table-bordered table-striped" id="anexo_datatable" style="width:100%; font-size:13px">
   <thead>
      <tr>
        <th width="100px">ID Proyecto</th>
        <th>ID Anexo</th>
        <th width="200px">Nombre del anexo</th>
        <th>Usuario</th>
        <th width="500px">Nota</th>
        <th width="120px">Acciones</th>
      </tr>
   </thead>
</table>
<img id="output" width="200" />	
<img id="image_preview_container" src=""
</div>
 
@include("anexos_proyectos.form");


<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>

</body>
<script>

    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        $(document).ready( function () {

         
            var table = $('#anexo_datatable').DataTable({
                
            processing: false,
            serverSide: false,
            pageLength: 6,
            ajax: {
                url: "{{$id}}",
                type: 'GET',
            },
            columns: [
                      {data: 'proyecto_id', name: 'proyecto_id', 'visible': true},
                      {data: 'id_anexoProyecto', name: 'id_anexoProyecto', 'visible': false},
                      {data: 'url_anexoProyecto', name: 'url_anexoProyecto', 'visible': true},      
                      {data: 'user_id', name: 'user_id', 'visible': false},    
                      {data: 'nota_anexoProyecto', name: 'nota_anexoProyecto', 'visible': true},                
                      {data: 'action', name: 'action', orderable: false},
                   ],
            order: [[0, 'desc']]
        });

    

     /*  Para crear nuevo anexo */
        $('#agregar_anexos_proyectos').click(function () {
        
            $('#btn-save').val("create-user");
            $('#id_anexoProyecto').val('');
            $('#userForm').trigger("reset");
            $('#anexoCrudModal').html("Agregar anexo");
            $('#ajax-crud-modal').modal('show');
        });
           

       //Para eliminar
        $('body').on('click', '#delete-user', function () {
     
            var id_anexoProyecto = $(this).data("id");
            if(confirm("Estás seguro que deseas eliminar!")){

               
            $.ajax({
                type: "get",
                url: "delete/"+id_anexoProyecto,
                success: function (data) {

                    window.location.reload();
                $('#anexo_datatable').DataTable().ajax.reload();
                var oTable = $('#anexo_datatable').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
          }
        });   
  
       
       });

  
    // Para enviar crear y actualizar
     
    $(document).ready(function(e){
            // Submit form data via Ajax

            if ($("#userForm").length > 0) {
            $("#userForm").validate({
        
            submitHandler: function(form) {

            $("#userForm").on('submit', function(e){
                e.preventDefault();
                
                var archivos= document.getElementById('url_anexoProyecto')
                var archivo = archivos.files;

                var formData = new FormData();

                for(i=0; i<archivo.length; i++){
                formData.append('url_anexoProyecto[]',archivo[i]); 
                }    

				formData.append('nota_anexoProyecto', $('#nota_anexoProyecto').prop('value'));

                var proyecto_id = {{$id}}
                formData.append("proyecto_id", proyecto_id);

  
            var actionType = $('#btn-save').val();            

            $('#btn-save').html('Enviando..');
            

            $.ajax({
                
                data: $('#userForm').serialize(),
                type: "post",
                url:  "/anexoProyectostore",
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
	            processData: false,

                success: function (response) {          

                window.location.reload();
                $('#anexo_datatable').DataTable().ajax.reload();
                $('#userForm').trigger("reset");
                $('#ajax-crud-modal').modal('hide');                
                $('#btn-save').html('Guardar cambios');
                
                var oTable = $('#anexo_datatable').dataTable();
                oTable.fnDraw(false);
                
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Guardar cambios');
                }
              
            });
            
        });

        }
        });
        }
    });

        
    </script>
</html>

