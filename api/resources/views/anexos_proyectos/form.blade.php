<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto; width:600px; padding-right:40px; padding-left:20px; padding-top:20px;">
            <div class="modal-header">
                <h4 class="modal-title" id="anexoCrudModal"></h4>
            </div>
            <div class="modal-body" style="height:400px; width:600px; padding-right:40px; padding-left:40px; padding-top:20px;">
                <form method="POST" enctype="multipart/form-data" id="userForm" name="bugForm" class="form-horizontal">

                    <input type="hidden" name="id_anexoProyecto" id="id_anexoProyecto" value="">

                    
                    <!-- Adjuntos-->
                    <div class="form-group col-sm-12">
                        <label for="url_anexoProyecto" >Agregar anexos al proyecto</label>
                        <small><input type="file" id="url_anexoProyecto" name="url_anexoProyecto[]" accept="image/png, image/jpeg, image/jpg, application/pdf" required title="Este campo es obligatorio, debes adjuntar uno o más archivos" multiple= "multiple"></small>
                        <span class="error"  id="Errorurl_anexoProyecto" style="color:#F3A100" >
                    </div>
                  

                      <!-- Nota-->
                    <div class="form-group col-sm-12">
                        <label for="nota_anexoProyecto">Nota</label>
                        <textarea class="form-control" name="nota_anexoProyecto" value="{{old('nota_anexoProyecto')}}" type="text" id="nota_anexoProyecto" minlength="4" maxlength="100"  title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 100 caracteres" > </textarea>
                        <span class="error"  id="Errornota_anexoProyecto" style="color:#F3A100" >
                        <span style="color:#808b96; font-size:10px" id="rchars1" ><small style="color:#808b96; font-size:10px">100</span> caracteres restantes</small>
                    </div>
                  

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <div class="centrar">
                            <button type="submit" class="btn btn-primary" id="btn-save" value="create">Guardar cambios
                            </button>
                        </div>
                    </div>

                    <br>
                    <div class="pull-left">
                        <small><i>Sólo se admite archivos con formato pdf, png o jpeg y con un tamaño máximo de 4MB. </i></small>
                    </div> 

                    <!-- ROL ID Field -->
                    <div class="form-group col-sm-6">
                        <label for="rol_id" > </label>
                        <input class="form-control" id="rol_id" type="hidden" name="rol_id"  value={{auth()->user()->rol_id}} readonly >
                    </div>


                    <!-- Proyecto ID Field -->
                    <div class="form-group col-sm-6">
                        <label for="proyecto_id" > </label>
                        <input class="form-control" id="proyecto_id" type="hidden" name="proyecto_id" value="" readonly >
                    </div>


                </form>
            </div>

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<script>

    var maxLength = 100;
    $('#nota_anexoProyecto').keyup(function() {
    var textlen = maxLength - $(this).val().length;
    $('#rchars1').text(textlen);
    });

</script>

<style>
    .error {
    color: #F3A100;
    font-style: italic;
    font-weight: normal;
    
    }
</style>

<script>


    $(document).on('change','input[type="file"]',function(){
        // this.files[0].size recupera el tamaño del archivo
        // alert(this.files[0].size);
        
        var fileName = this.files[0].name;
        var fileSize = this.files[0].size;

        //aqui el tamaño del archivo se da en terabytes.
        if(fileSize > 4000000){
            alert('El archivo no debe superar los 4MB');
            this.value = '';
            this.files[0].name = '';
        }else{
            // recuperamos la extensión del archivo
            var ext = fileName.split('.').pop();
            
            // Convertimos en minúscula porque 
            // la extensión del archivo puede estar en mayúscula
            ext = ext.toLowerCase();
        
            // console.log(ext);
            switch (ext) {
                case 'jpg':
                case 'jpeg':
                case 'png':
                case 'pdf': break;
                default:
                    alert('El archivo no tiene la extensión adecuada');
                    this.value = ''; // reset del valor
                    this.files[0].name = '';
            }
        }
    });

</script>

<script>

    $(document).ready(function() {
    jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
    });
    });
</script>
