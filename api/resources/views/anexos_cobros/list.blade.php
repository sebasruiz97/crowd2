
<!DOCTYPE html>
 
<html lang="en">
<head>

    <style>

        .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);
      
        }
    
        .dataTables_wrapper .dataTables_paginate .paginate_button  {
            box-sizing: border-box;
            display: inline-block;
            min-width: 1.5em;
            padding: 0.5em 1em;
            margin-left: 2px;
            text-align: center;
            text-decoration: none !important;
            cursor: pointer;
            *cursor: hand;
            color: #304457 !important;
            border: 1px solid transparent;
            border-radius: 2px;
        }
    
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover   {
      
            text-decoration: none !important;
            color: #ffff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);
        
        }
    
        .dataTables_wrapper .dataTables_paginate .paginate_button:focus  {
      
            text-decoration: none !important;
            color: #ffff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);
    
        }
     
    </style>
    
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>

    <script>
        
        $.extend( true, $.fn.dataTable.defaults, {
        "language": {
        "decimal": ",",
        "thousands": ".",
        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "infoPostFix": "",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "loadingRecords": "Cargando...",
        "lengthMenu": "Mostrar _MENU_ registros",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": ">",
            "previous": "<"
        },
        "processing": "Procesando...",
        "search": "Buscar:",
        "searchPlaceholder": "Término de búsqueda",
        "zeroRecords": "No se encontraron resultados",
        "emptyTable": "Ningún dato disponible en esta tabla",
        "aria": {
            "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "select": {
            "rows": {
                _: '%d filas seleccionadas',
                0: 'clic fila para seleccionar',
                1: 'una fila seleccionada'
            }
        }
        }           
        } );       
    </script>

</head>
<body>
 
<div class="container">
<br>

@include('flash::message')


@if(auth()->user()->hasRoles(['2','4']))
<a href="javascript:void(0)" class="btn btn-default btn-xs"  id="agregar_anexos_bugs" data-id="{{ $id }}">Agregar Anexo</a>
<br><br>
@endif

<table class="table table-bordered table-striped" id="anexo_datatable" style="width:100%; font-size:13px">
   <thead>
      <tr>
        <th width="100px">ID Anexo</th>
        <th width="100px">ID Cuenta de Cobro</th>
        <th width="300px">Tipo de Anexo</th>       
        <th>Url</th>
        <th width="200px">Comentarios</th>
        <th width="200px">Subido por</th>
        <th width="200px">Fecha </th>
        <th width="120px">Acciones</th>
      </tr>
   </thead>
</table>
</div>
 

@include("anexos_cobros.form");


<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>

</body>
<script>

    var usuario = '{{auth()->user()->name}}';
    var rol = '{{auth()->user()->rol_id}}';
    var mensaje = '{{$mensaje}}';

    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        

        $(document).ready( function () {

            if(rol == 2){
                alert(mensaje);
            }

            var table = $('#anexo_datatable').DataTable({

            initComplete: function(settings, json) {

            $.each(json.data, function(i, item) 
            {


            if(item.subido_por!==usuario)
            {
                $("a[id='delete-user'][data-id='"+item.id_anexo_cuentaCobro+"']").hide();
            }

            });
            },
                
            processing: false,
            serverSide: false,
            pageLength: 6,
            ajax: {
                url: "{{$id}}",
                type: 'GET',

             
            },

           
            columns: [
                      {data: 'id_anexo_cuentaCobro', name: 'id_anexo_cuentaCobro', 'visible': false},
                      {data: 'cuentaCobro_id', name: 'cuentaCobro_id', 'visible': false},
                      {data: 'nombre_anexo', name: 'nombre_anexo', 'visible': true},
                      {data: 'url_anexo_cuentaCobro', name: 'url_anexo_cuentaCobro', 'visible': false},
                      {data: 'nota_anexo_cuentaCobro', name: 'nota_anexo_cuentaCobro', 'visible': true},  
                      {data: 'subido_por', name: 'subido_por', 'visible': true},      
                      {data: 'fecha_subida', name: 'fecha_subida', 'visible': true},        
                      {data: 'action', name: 'action', orderable: false},
                   ],
            order: [[0, 'desc']]
        });

        

    

     /*  Para crear nuevo anexo */
        $('#agregar_anexos_bugs').click(function () {
        
            $('#btn-save').val("create-user");
            $('#id_anexo_cuentaCobro').val('');
            $('#cobroForm').trigger("reset");
            $('#anexoCrudModal').html("Agregar anexo");
            $('#ajax-crud-modal').modal('show');
        });
           

       //Para eliminar
        $('body').on('click', '#delete-user', function () {
     
            var id_anexo_cuentaCobro = $(this).data("id");
            if(confirm("Estás seguro que deseas eliminar!")){

               
            $.ajax({
                type: "get",
                url: "delete/"+id_anexo_cuentaCobro,
                success: function (data) {

                window.location.reload();
                $('#anexo_datatable').DataTable().ajax.reload();
                var oTable = $('#anexo_datatable').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
          }
        });   

        //Para visualizar
        $('body').on('click', '#ver-anexo', function () {
     
            var id_anexoBug = $(this).data("id");    
               
            $.ajax({
                type: "get",
                url: "show/"+id_anexoBug,
                success: function (data) {

                    var win = window.open(data);
                
                }
            });

        });   
       
       });

  
    // Para enviar crear y actualizar
     
    $(document).ready(function(e){
            // Submit form data via Ajax

            if ($("#cobroForm").length > 0) {
            $("#cobroForm").validate({
        
            submitHandler: function(form) {

            $("#cobroForm").on('submit', function(e){
                e.preventDefault();
                
                var archivos= document.getElementById('url_anexo_cuentaCobro')
                var archivo = archivos.files;

                var formData = new FormData();

                for(i=0; i<archivo.length; i++){
                formData.append('url_anexo_cuentaCobro[]',archivo[i]); 
                }    

				formData.append('tipoAnexo_id', $('#tipoAnexo_id').prop('value'));
                formData.append('nota_anexo_cuentaCobro', $('#nota_anexo_cuentaCobro').prop('value'));

                var id_cuentaCobro = {{$id}}
                formData.append("id_cuentaCobro", id_cuentaCobro);

  
            var actionType = $('#btn-save').val();            

            $('#btn-save').html('Enviando..');
            

            $.ajax({
                
                data: $('#cobroForm').serialize(),
                type: "post",
                url:  "/anexoCobrostore",
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
	            processData: false,

                success: function (response) {          

                window.location.reload();
                $('#anexo_datatable').DataTable().ajax.reload();
                $('#cobroForm').trigger("reset");
                $('#ajax-crud-modal').modal('hide');                
                $('#btn-save').html('Guardar cambios');
                
                var oTable = $('#anexo_datatable').dataTable();
                oTable.fnDraw(false);



                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Guardar cambios');
                }
              
            });
            
        });

        }
        });
        }
    });
        
    </script>
</html>

