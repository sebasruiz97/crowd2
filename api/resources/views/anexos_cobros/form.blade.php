<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto; width:600px; padding-right:40px; padding-left:20px; padding-top:20px;">
            <div class="modal-header">
                <h4 class="modal-title" id="anexoCrudModal"></h4>
            </div>
            <div class="modal-body" style="height:400px; width:600px; padding-right:40px; padding-left:40px; padding-top:20px;">
                <form method="POST" enctype="multipart/form-data" id="cobroForm" name="cobroForm" class="form-horizontal">
                      
                    @inject('anexos','App\Services\anexosCobro')

                    <input type="hidden" name="id_cuentaCobro" id="id_cuentaCobro" value="">

                    <!-- Tipo de Anexo -->
                    <div class="form-group col-sm-12">
                        <label for="tipoAnexo_id">¿Qué tipo de anexo vas a subir?</label>
                        <select class="form-control" id="tipoAnexo_id" value="{{old('tipoAnexo_id')}}" name="tipoAnexo_id">
                            @foreach($anexos->get() as  $index => $anexos)
                                <option value = "{{ $index }}" {{old('tipoAnexo_id') == $index ? 'selected' : ''}} > {{  $anexos }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('tipoAnexo_id'))
                            <span class="help-block" style="color:#F3A100"; >
                                <small><i>{{ $errors->first('tipoAnexo_id') }}</i></small>
                            </span>
                        @endif
                    </div>
                    
                    <!-- Adjuntos-->
                    <div class="form-group col-sm-12">
                        <label for="url_anexo_cuentaCobro" >Adjuntar archivo</label>
                        <small><input type="file" id="url_anexo_cuentaCobro" name="url_anexo_cuentaCobro" accept="image/png, image/jpeg, image/jpg, application/pdf" required title="Este campo es obligatorio, debes adjuntar uno o más archivos" ></small>
                        <span class="error"  id="Errorurl_url_anexo_cuentaCobro" style="color:#F3A100" >
                    </div>
                  

                      <!-- Nota-->
                    <div class="form-group col-sm-12">
                        <label for="nota_anexo_cuentaCobro">Nota</label>
                        <textarea class="form-control" name="nota_anexo_cuentaCobro" value="{{old('nota_anexo_cuentaCobro')}}" type="text" id="nota_anexo_cuentaCobro" minlength="4" maxlength="100"  title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 100 caracteres" > </textarea>
                        <span class="error"  id="Errornota_anexo_cuentaCobro" style="color:#F3A100" >
                        <span style="color:#808b96; font-size:10px" id="rchars1" ><small style="color:#808b96; font-size:10px">100</span> caracteres restantes</small>
                    </div>
                  

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <div class="centrar">
                            <button type="submit" class="btn btn-primary" id="btn-save" value="create">Guardar cambios
                            </button>
                        </div>
                    </div>

                    <br>
                    <div class="pull-left">
                        <small><i>Sólo se admite archivos con formato pdf, png o jpeg y con un tamaño máximo de 3MB. </i></small>
                    </div> 

                    <!-- ROL ID Field -->
                    <div class="form-group col-sm-6">
                        <label for="rol_id" > </label>
                        <input class="form-control" id="rol_id" type="hidden" name="rol_id"  value={{auth()->user()->rol_id}} readonly >
                    </div>


                    <!-- user_id Field -->
                    <div class="form-group col-sm-6">
                        <label for="bug_id" > </label>
                        <input class="form-control" id="user_id" type="hidden" name="user_id" value="" readonly >
                    </div>


                </form>

            </div>

            

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<script>

    var maxLength = 100;
    $('#nota_anexo_cuentaCobro').keyup(function() {
    var textlen = maxLength - $(this).val().length;
    $('#rchars1').text(textlen);
    });

</script>


<style>
    .error {
    color: #F3A100;
    font-style: italic;
    font-weight: normal;
    
    }
</style>


<script>


    $(document).on('change','input[type="file"]',function(){
        // this.files[0].size recupera el tamaño del archivo
        // alert(this.files[0].size);
        
        var fileName = this.files[0].name;
        var fileSize = this.files[0].size;

        //aqui el tamaño del archivo se da en terabytes.
        if(fileSize > 3000000){
            alert('El archivo no debe superar los 3MB');
            this.value = '';
            this.files[0].name = '';
        }else{
            // recuperamos la extensión del archivo
            var ext = fileName.split('.').pop();
            
            // Convertimos en minúscula porque 
            // la extensión del archivo puede estar en mayúscula
            ext = ext.toLowerCase();
        
            // console.log(ext);
            switch (ext) {
                case 'jpg':
                case 'jpeg':
                case 'png':
                case 'pdf': break;
                default:
                    alert('El archivo no tiene la extensión adecuada');
                    this.value = ''; // reset del valor
                    this.files[0].name = '';
            }
        }
    });

</script>

<script>

    $(document).ready(function() {
    jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
    });
    });
</script>
