<head>
</head>

<body style="background-color: #ffff">


	<hr style="background-color: #304457; border-color: #304457;  height: 35px; text-align: center; font-size:18px; margin-bottom: 10px">

	<h3 style="text-transform: uppercase; font-size:18px; text-align:center">Pago de una cuenta de cobro</h3>
	<h4 style="font-weight: normal;  font-size:14px; text-align:justify" >Te informamos que se ha generado el pago para la cuenta de cobro #<strong> <i>{{$cuentaCobro_id}}</i></strong>, a nombre de  <strong> <i>{{$nombre}}</i></strong> el día  <strong> <i>{{$fecha}}</i></strong> 
	<h4 style="font-weight: normal;  font-size:14px;  text-align:justify; " >
		Para consultar más detalles de la cuenta de cobro generada, ingresa a 
		<a href="{{ route('cobros.index') }}" class="btn btn-default; text-align:center; ;">ver cuenta de cobro</a>
	</h4>
	<footer style="font-size: 12px;  text-align:center; color:#C1C4C7">
		{{ config('app.name') }} | Copyright - {{ date('Y') }}
	</footer>

</div>
</body>

