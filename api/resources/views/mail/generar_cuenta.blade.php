<head>
</head>

<body style="background-color: #ffff">


	<hr style="background-color: #304457; border-color: #304457;  height: 35px; text-align: center; font-size:18px; margin-bottom: 10px">

	<h3 style="text-transform: uppercase; font-size:18px; text-align:center">Generación de nueva cuenta de cobro</h3>
	<h4 style="font-weight: normal;  font-size:14px; text-align:justify" >Te informamos que se ha generado la cuenta de cobro #<strong> <i>{{$id_cuentaCobro}}</i></strong>, a nombre de  <strong> <i>{{$nombre}}</i></strong> el día  <strong> <i>{{$fecha}}</i></strong> 
	<h4 style="font-weight: normal;  font-size:14px;  text-align:justify; " >
		Para realizar el pago de la cuenta de cobro, es necesario que sigas los siguientes pasos. 
		<ul>
			<li>Ingresar a Crowd y revisar la cuenta de cobro generada. Si está de acuerdo debe marcarla como "Aprobada" a través del botón "Cambiar Estado". Si no está de acuerdo, puede marcarla como "Rechazada", indicando las razones. En caso de que marque cómo rechazada la cuenta de cobro, el líder del proyecto le contactará para resolver sus inquietudes.</li>
			<li>Descargar a través del boton "Descargar" la cuenta de cobro.</li>
			<li> A través del botón "Agregar Anexos", debe adjuntar la cuenta de cobro firmada (puede ser firma digital), así como la seguridad social (en caso de que se indique que es necesaria). </li>
			<li>Esperar el pago de la cuenta de cobro que será realizado por parte de SQA, de acuerdo a los tiempos establecidos en los términos y condiciones </li>
		</ul>
	</h4>
	<h4 style="font-weight: normal;  font-size:14px;  text-align:justify; " >
		Tenga presente que para realizar el pago de la cuenta de cobro, es OBLIGATORIO que tenga cargados en la plataforma (desde su perfil), los siguientes anexos:
		<ul>
			<li>Documento de Identificación</li>
			<li>RUT</li>
			<li>Certificación Bancaria</li>
			<li>Aceptación de tratamiento de datos personales firmada</li>
		</ul>
	<h4 style="font-weight: normal;  font-size:14px;  text-align:justify; " >
		Tenga en cuenta que las cuentas de cobro se generan con una fecha de corte, por lo que si no contienen todos los hallazgos que ya tiene aprobados por el cliente, esto serán cancelados en una próxima cuenta de cobro. 
	
	</h4>
	<h4 style="font-weight: normal;  font-size:14px;  text-align:justify; " >
		Para consultar más detalles de la cuenta de cobro generada, ingresa a 
		<a href="{{ route('cobros.index') }}" class="btn btn-default; text-align:center; ;">ver cuenta de cobro</a>
	</h4>
	<footer style="font-size: 12px;  text-align:center; color:#C1C4C7">
		{{ config('app.name') }} | Copyright - {{ date('Y') }}
	</footer>

</div>
</body>

