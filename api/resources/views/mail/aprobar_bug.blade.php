<head>
</head>

<body style="background-color: #ffff">


	<hr style="background-color: #304457; border-color: #304457;  height: 35px; text-align: center; font-size:18px; margin-bottom: 10px">

	<h3 style="text-transform: uppercase; font-size:18px; text-align:center">Aprobación de un hallazgo en Crowd</h3>
	<h4 style="font-weight: normal;  font-size:14px; text-align:center" >Te informamos que el hallazgo # <strong> <i>{{$id}}</i></strong>, ha sido marcado cómo <strong> <i>{{$estadoBug}}</i></strong>. 
	<h4 style="font-weight: normal;  font-size:14px; text-align:center" >A continuación algunos detalles del hallazgo: </h4>


<div class="table" style="font-size: 13px;">
	<table style="margin: 0 auto;">
	<tbody style="text-align: center; ">  
		<tr> 
			<th style="text-align: left;  border-bottom: 0px; ">Fecha de Reporte</th>
				<td  style="text-align: justify">{{$fechaCreacion}}</td>
			</tr>
		<tr> 
			<th style="text-align: left;  border-bottom: 0px;">Proyecto</th>
			<td  style="text-align: justify">{{$proyecto}}</td>
		</tr>
		<tr> 
			<th style="text-align: left;  border-bottom: 0px;">Tipo de Hallazgo</th>
			<td  style="text-align: justify">{{$tipoBug}}</td>
		</tr>
		<tr > 
			<th style="text-align: left;  border-bottom: 0px;">Título del hallazgo</th>
			<td  style="text-align: justify">{{$tituloBug}}</td>
		</tr>				
	</tbody>  
	</table>

	<h4 style="font-weight: normal;  font-size:14px; text-align:center; " >Se generará la cuenta de cobro de acuerdo a las políticas de pago de Crowd</h4>
	<h4 style="font-weight: normal;  font-size:14px;  text-align:center; " >
		Para consultar más detalles del hallazgo, ingresa a 
		<a href="{{ route('bugs.index') }}" class="btn btn-default; text-align:center; ;">ver hallazgo</a>
	</h4>
	<footer style="font-size: 12px;  text-align:center; color:#C1C4C7">
		{{ config('app.name') }} | Copyright - {{ date('Y') }}
	</footer>

</div>
</body>

