<head>
</head>

<body style="background-color: #ffff">


	<hr style="background-color: #304457; border-color: #304457;  height: 35px; text-align: center; font-size:18px; margin-bottom: 10px">

	<h3 style="text-transform: uppercase; font-size:18px; text-align:center">Te invitamos a participar de un nuevo proyecto</h3>
	<h4 style="font-weight: normal;  font-size:14px; text-align:center" >Hola {{$name}}, hay un nuevo proyecto para probar en el cual nos gustaría que participarás,
	<h4 style="font-weight: normal;  font-size:14px; text-align:center" >A continuación algunos detalles del proyecto: </h4>


<div class="table" style="font-size: 13px;">
	<table style="margin: 0 auto;">
	<tbody style="text-align: center; ">  
		<tr> 
			<th style="text-align: left;  border-bottom: 0px; ">Nombre del proyecto</th>
				<td  style="text-align: justify">{{$proyectoNombre}}</td>
			</tr>
		<tr> 
			<th style="text-align: left;  border-bottom: 0px;">Descripción del proyecto</th>
			<td  style="text-align: justify">{{$proyectoDescripcion}}</td>
		</tr>
		<tr> 
			<th style="text-align: left;  border-bottom: 0px;">Fecha de inicio del proyecto</th>
			<td  style="text-align: justify">{{$proyectoInicio}}</td>
		</tr>			
	</tbody>  
	</table>

	<h4 style="font-weight: normal;  font-size:14px;  text-align:center; " >
		Para consultar más detalles del proyecto, ingresa a 
		<a href="{{ route('invitaciones.index') }}" class="btn btn-default; text-align:center; ;">Mis invitaciones</a>
	</h4>
	<footer style="font-size: 12px;  text-align:center; color:#C1C4C7">
		{{ config('app.name') }} | Copyright - {{ date('Y') }}
	</footer>

</div>
</body>

