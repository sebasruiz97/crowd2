<head>
</head>

<body style="background-color: #ffff">


	<hr style="background-color: #304457; border-color: #304457;  height: 35px; text-align: center; font-size:18px; margin-bottom: 10px">

	<h3 style="text-transform: uppercase; font-size:18px; text-align:center">Rechazo de una cuenta de cobro</h3>
	<h4 style="font-weight: normal;  font-size:14px; text-align:justify" >Te informamos que el <strong>{{$quienRechaza}}</strong>ha rechazado la cuenta de cobro #<strong> <i>{{$id_cuentaCobro}}</i></strong>, generada el día  <strong> <i>{{$fecha}}</i></strong> por <strong> <i>{{$motivoRechazo}}</i></strong>. 
		Para consultar más detalles de la cuenta de cobro rechazada, ingresa a 
		<a href="{{ route('cobros.index') }}" class="btn btn-default; text-align:center; ;">ver cuenta de cobro</a>. 
		{{$mensaje}} 
	</h4>
	<footer style="font-size: 12px;  text-align:center; color:#C1C4C7">
		{{ config('app.name') }} | Copyright - {{ date('Y') }}
	</footer>

</div>
</body>

