<head>
</head>

<body style="background-color: #ffff">


	<hr style="background-color: #304457; border-color: #304457;  height: 35px; text-align: center; font-size:18px; margin-bottom: 10px">

	<h3 style="text-transform: uppercase; font-size:18px; text-align:center">Se requiere más información de un hallazgo en Crowd</h3>
	<h4 style="font-weight: normal;  font-size:14px; text-align:center" >El hallazgo corresponde a:</h4>


<div class="table" style="font-size: 13px;">
	<table style="margin: 0 auto;">
	<tbody style="text-align: center; ">  
		<tr> 
			<th style="text-align: left;  border-bottom: 0px; ">ID del hallazgo</th>
				<td  style="text-align: justify">{{$id}}</td>
			</tr>
		<tr> 
			<th style="text-align: left;  border-bottom: 0px;">Proyecto</th>
			<td  style="text-align: justify">{{$proyecto}}</td>
		</tr>
		<tr> 
			<th style="text-align: left;  border-bottom: 0px;">Tipo de Hallazgo</th>
			<td  style="text-align: justify">{{$tipoBug}}</td>
		</tr>
		<tr > 
			<th style="text-align: left;  border-bottom: 0px;">Título del hallazgo</th>
			<td  style="text-align: justify">{{$tituloBug}}</td>
		</tr>
		<tr> 
			<th style="text-align: left;  border-bottom: 0px;">Reportado por</th>
			<td  style="text-align: justify">{{$usuario}}</td>
		</tr>
		<tr> 
			<th style="text-align: left;  border-bottom: 0px;">Estado del hallazgo</th>
			<td  style="text-align: justify">{{$estadoBug}}</td>
		</tr>
		<tr> 
			<th style="text-align: left;  border-bottom: 0px;">Notas</th>
			<td  style="text-align: justify">{{$cambio}}</td>
		</tr>
				
		
	</tbody>  
	</table>

	<h4 style="font-weight: normal;  font-size:14px;  text-align:center" >
		Para consultar más detalles del hallazgo, ingresa a 
		<a href="{{ route('bugs.index') }}" class="btn btn-default; text-align:center">ver hallazgo</a>
	</h4>
	<footer style="font-size: 12px;  text-align:center; color:#C1C4C7">
		{{ config('app.name') }} | Copyright - {{ date('Y') }}
	</footer>

</div>
</body>

