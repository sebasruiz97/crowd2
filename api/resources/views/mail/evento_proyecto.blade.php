<head>
</head>

<body style="background-color: #ffff">


	<hr style="background-color: #304457; border-color: #304457;  height: 35px; text-align: center; font-size:18px; margin-bottom: 10px">

	<h3 style="text-transform: uppercase; font-size:18px; text-align:center">{{$titulo}}</h3>
	<h4 style="font-weight: normal;  font-size:14px; text-align:justify" >
		Hola {{$name}}, te informamos que a partir de este momento, damos <strong>{{$evento}}</strong>
		al proyecto <strong>{{$proyectoNombre}}</strong>. Por lo que, {{$indicacion}}.
	<h4 style="font-weight: normal;  font-size:14px;  text-align:justify; " >
		Para consultar más detalles del proyecto, ingresa a 
		<a href="{{ route('misproyectos.index') }}" class="btn btn-default; text-align:justify; ;">Mis proyectos</a>
	</h4>
	<footer style="font-size: 12px;  text-align:center; color:#C1C4C7">
		{{ config('app.name') }} | Copyright - {{ date('Y') }}
	</footer>

</div>
</body>

