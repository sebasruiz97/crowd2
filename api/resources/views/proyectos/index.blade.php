@extends('layouts.appProyecto')

@section('content')

        <h3 class="text-center"  style="font-size: 18px; font-weight: bold; color: #304457; text-transform: uppercase ">  Proyectos </h3>
        
        @include('proyectos.list')
@endsection
