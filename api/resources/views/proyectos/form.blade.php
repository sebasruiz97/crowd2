<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto; width:600px; padding-right:40px; padding-left:20px; padding-top:20px;">
            <div class="modal-header">
                <h4 class="modal-title" id="userCrudModal"></h4>
            </div>
            <div class="modal-body" style="height:auto; width:600px; padding-right:40px; padding-left:40px; padding-top:20px;">
                <form id="userForm" name="bugForm" class="form-horizontal">

                    <input type="hidden" name="id_proyecto" id="id_proyecto" value="">
                    <input type="hidden" name="cliente_id2" id="cliente_id2" value="">
                    <input type="hidden" name="ProyectoEstado_id2" id="ProyectoEstado_id2" value="">
                    <input type="hidden" name="proyecto_responsable_actual2" id="proyecto_responsable_actual2" value="">


                    @inject('clientes','App\Services\clientes')
                    @inject('proyectos_estados','App\Services\proyectos_estados')
                    @inject('proyecto_responsable_actual','App\Services\proyecto_responsable_actual')

                    <!-- Cliente -->
                    <div class="form-group col-sm-12" >
                        <label for="cliente_id">Cliente</label>
                        <select class="form-control" id="cliente_id" name="cliente_id" required onchange="CambioCliente()">
                            @foreach($clientes->get() as  $index => $clientes)
                                <option value = "{{ $index }}" {{old('cliente_id') == $index ? 'selected' : ''}} > {{  $clientes }}</option>
                            @endforeach
                        </select>
                        <span class="error"  id="cliente_id" style="color:#F3A100"; >
                    </div>

                    <!-- Proyecto-->
                    <div class="form-group col-sm-12" >
                        <label for="proyecto_nombre">Proyecto</label>
                        <input 
                            class="form-control" name="proyecto_nombre" 
                            value="{{old('proyecto_nombre')}}" type="text" 
                            id="proyecto_nombre"required pattern="[A-Za-z0-9 ]{4,15}" 
                            title="Este campo es obligatorio y puede contener letras, números y espacios. Y entre 4 y 50 caracteres" 
                            maxlength="50"
                            minlength="4"
                        >
                        <span class="error"  id="proyecto_nombre" style="color:#F3A100"; >
                    </div>

                    <!-- Descripción-->
                    <div class="form-group col-sm-12">
                        <label for="proyecto_descripcion">Descripción del proyecto</label>
                        <textarea maxlength="1200" class="form-control" name="proyecto_descripcion" value="{{old('proyecto_descripcion')}}" type="text" id="proyecto_descripcion" required  minlength="4"   title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 1200 caracteres" > </textarea>
                        <span class="error"  id="Errorproyecto_descripcion" style="color:#F3A100"; >
                        <span style="color:#808b96; font-size:10px" id="rchars1" ><small style="color:#808b96; font-size:10px">1200</span> caracteres restantes</small>
                    </div>

                    <!-- Estado Actual Proyecto -->
                    <div class="form-group col-sm-12" >
                        <label for="ProyectoEstado_id">Estado del Proyecto</label>
                        <select class="form-control" id="ProyectoEstado_id" name="ProyectoEstado_id" required onchange="CambioEstado()">
                            @foreach($proyectos_estados->get() as  $index => $proyectos_estados)
                                <option value = "{{ $index }}" {{old('ProyectoEstado_id') == $index ? 'selected' : ''}} > {{  $proyectos_estados }}</option>
                            @endforeach
                        </select>
                        <span class="error"  id="ProyectoEstado_id" style="color:#F3A100"; >
                    </div>

           
                    <!-- Fecha de Inicio -->
                    <div class="form-group col-sm-6" style="padding-right: 50px">
                        <label for="proyecto_fecha_inicio">¿Cuando inicia el proyecto?:</label>
                        <input 
                            type="date" 
                            class="form-control" 
                            {{-- min="<?php echo date("Y-m-d");?>" --}}
                            value="<?php echo date("Y-m-d");?>"
                            name="proyecto_fecha_inicio"  
                            type="proyecto_fecha_inicio" 
                            id="proyecto_fecha_inicio" 
                            required
                            onchange='var input = document.getElementById("proyecto_fecha_entrega");
                            input.setAttribute("min", this.value);'
                        >
                        <span class="error"  id="proyecto_fecha_inicio" style="color:#F3A100"; >                    
                    </div>

                    <!-- Fecha Fin -->
                    <div class="form-group col-sm-6" style="padding-left: 50px">
                        <label for="proyecto_fecha_entrega">¿Cuando finaliza el proyecto?:</label>
                        <input 
                            type="date" 
                            class="form-control" 
                            {{-- min="<?php echo date("Y-m-d");?>" --}}
                            name="proyecto_fecha_entrega"  
                            value="{{old('proyecto_fecha_entrega')}}" 
                            type="proyecto_fecha_entrega" 
                            id="proyecto_fecha_entrega" 
                            sytle="margin-left: 200px;"
                            required
                            
                        >
                        <span class="error"  id="proyecto_fecha_entrega" style="color:#F3A100"; >  
                    </div>
                    

                    <!-- Presupuesto Proyecto-->
               
                    <div class="form-group col-sm-12" >
                        <label for="proyecto_presupuesto">Presupuesto</label>
                        <input 
                            class="money" 
                            name="proyecto_presupuesto" 
                            value="{{old('proyecto_presupuesto')}}" 
                            type="text" 
                            data-type="currency"
                            id="proyecto_presupuesto"
                            pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" 
                            {{-- required pattern="[0-9]{6,12}"  --}}
                            title="Puede contener solo  números. Y entre 4 y 20 digitos" 
                            >
                        <span class="error"  id="proyecto_presupuesto" style="color:#F3A100"; >  
                    </div>
              
                    <!-- Responsable Actual Proyecto -->
                    <div class="form-group col-sm-12">
                        <label for="proyecto_responsable_actual">Responsable del Proyecto (SQA)</label>
                        <select class="form-control" id="proyecto_responsable_actual" name="proyecto_responsable_actual" required onchange="CambioEstado2()">
                            @foreach($proyecto_responsable_actual->get() as  $index => $proyecto_responsable_actual)
                                <option value = "{{ $index }}" {{old('proyecto_responsable_actual') == $index ? 'selected' : ''}} > {{  $proyecto_responsable_actual }}</option>
                            @endforeach
                        </select>
                        <span class="error"  id="proyecto_responsable_actual" style="color:#F3A100"; >  
                    </div>             
                                     
                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <div class="centrar">
                            <button type="submit" class="btn btn-primary" id="btn-save" value="create">Guardar cambios
                            </button>
                        </div>
                    </div>

                    <!-- UserID Field -->
                    <div class="form-group col-sm-3">
                        <label for="user_id" > </label>
                        <input class="form-control" name="user_id" type="hidden" value={{auth()->user()->id}} readonly="readonly" >
                    </div>

                    <!-- Cliente ID Field -->
                    <div class="form-group col-sm-3">
                        <label for="cliente_id" > </label>
                        <input class="form-control" name="cliente_id" type="hidden" value="" readonly="readonly" >
                    </div>
                    
                    <!-- Estado ID Field -->
                    <div class="form-group col-sm-3">
                        <label for="bug_estado_id" > </label>
                        <input class="form-control" name="bug_estado_id" type="hidden" value="" readonly="readonly" >
                    </div>

                </form>
            </div>
            <h5 style="color:#fff;">Crowd</h5>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<script>

    var maxLength1 = 1200;
    $('#proyecto_descripcion').keyup(function() {
    var textlen1 = maxLength1 - $(this).val().length;
    $('#rchars1').text(textlen1);
    });

</script>


<script>
    function CambioCliente() {
      var x = document.getElementById("cliente_id").value;
      document.getElementById("cliente_id2").value =  x;
    
      
        
    }
    function CambioEstado() {
      var x = document.getElementById("id_proyecto").value;
      var e =  document.getElementById("ProyectoEstado_id").value;
      document.getElementById("ProyectoEstado_id2").value=e

    }
    function CambioEstado2() {
    
    var r =  document.getElementById("proyecto_responsable_actual").value;
    document.getElementById("proyecto_responsable_actual2").value=r
    
    }
    $(document).ready(function() {
        
        $("input[data-type='currency']").on({
            keyup: function() {
            formatCurrency($(this));
            },
            blur: function() { 
            formatCurrency($(this), "blur");
            }
        });


        function formatNumber(n) {
        // format number 1000000 to 1,234,567
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }


        function formatCurrency(input, blur) {
        // appends $ to value, validates decimal side
        // and puts cursor back in right position.
        
        // get input value
        var input_val = input.val();
        
        // don't validate empty input
        if (input_val === "") { return; }
        
        // original length
        var original_len = input_val.length;

        // initial caret position 
        var caret_pos = input.prop("selectionStart");
            
        // check for decimal
        if (input_val.indexOf(".") >= 0) {

            // get position of first decimal
            // this prevents multiple decimals from
            // being entered
            var decimal_pos = input_val.indexOf(".");

            // split number by decimal point
            var left_side = input_val.substring(0, decimal_pos);
            var right_side = input_val.substring(decimal_pos);

            // add commas to left side of number
            left_side = formatNumber(left_side);

            // validate right side
            right_side = formatNumber(right_side);
            
            // On blur make sure 2 numbers after decimal
            if (blur === "blur") {
            right_side += "00";
            }
            
            // Limit decimal to only 2 digits
            right_side = right_side.substring(0, 2);

            // join number by .
            input_val = "$" + left_side + "." + right_side;

        } else {
            // no decimal entered
            // add commas to number
            // remove all non-digits
            input_val = formatNumber(input_val);
            input_val = "$" + input_val;
            
            // final formatting
            if (blur === "blur") {
            input_val += ".00";
            }
        }
        
        // send updated string to input
        input.val(input_val);

        // put caret back in the right position
        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
        }     

        jQuery.extend(jQuery.validator.messages, {
        required: "Este campo es obligatorio.",
        remote: "Por favor, rellena este campo.",
        email: "Por favor, escribe una dirección de correo válida",
        url: "Por favor, escribe una URL válida.",
        date: "Por favor, escribe una fecha válida.",
        dateISO: "Por favor, escribe una fecha (ISO) válida.",
        number: "Por favor, escribe un número entero válido.",
        digits: "Por favor, escribe sólo dígitos.",
        creditcard: "Por favor, escribe un número de tarjeta válido.",
        equalTo: "Por favor, escribe el mismo valor de nuevo.",
        accept: "Por favor, escribe un valor con una extensión aceptada.",
        maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
        minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
        rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
        range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
        max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
        min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
        });
});
    </script>

<style>
    .error {
    color: #F3A100;
    font-style: italic;
    font-weight: normal;   
    }
</style>


