<!DOCTYPE html>
 
<html lang="en">
  <style>
    iframe.hidden
    {
      display:none
    }
  </style>

  <style>

    table.dataTable {
        box-sizing: content-box;
        width:100% !important;            
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #fff !important;
        border: 1px solid #304457 !important;
        background-color: #304457 !important;
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
        background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
        background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
        background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
        background: -o-linear-gradient(top, #304457 0%, #304457 100%);

    }

    .dataTables_wrapper .dataTables_paginate .paginate_button  {
        box-sizing: border-box;
        display: inline-block;
        min-width: 1.5em;
        padding: 0.5em 1em;
        margin-left: 2px;
        text-align: center;
        text-decoration: none !important;
        cursor: pointer;
        *cursor: hand;
        color: #304457 !important;
        border: 1px solid transparent;
        border-radius: 2px;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button:hover   {

        text-decoration: none !important;
        color: #ffff !important;
        border: 1px solid #304457 !important;
        background-color: #304457 !important;
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
        background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
        background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
        background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
        background: -o-linear-gradient(top, #304457 0%, #304457 100%);
    
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button:focus  {

        text-decoration: none !important;
        color: #ffff !important;
        border: 1px solid #304457 !important;
        background-color: #304457 !important;
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
        background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
        background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
        background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
        background: -o-linear-gradient(top, #304457 0%, #304457 100%);

    }

    thead input {
    width: 100%;
    }
      
  </style>


  <script>
    
    $.extend( true, $.fn.dataTable.defaults, {
    "language": {
    "decimal": ",",
    "thousands": ".",
    "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "infoPostFix": "",
    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
    "loadingRecords": "Cargando...",
    "lengthMenu": "Mostrar _MENU_ registros",
    "paginate": {
        "first": "Primero",
        "last": "Último",
        "next": ">",
        "previous": "<"
    },
    "processing": "Procesando...",
    "search": "Buscar:",
    "searchPlaceholder": "Término de búsqueda",
    "zeroRecords": "No se encontraron resultados",
    "emptyTable": "Ningún dato disponible en esta tabla",
    "aria": {
        "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sortDescending": ": Activar para ordenar la columna de manera descendente"
    },
    "select": {
        "rows": {
            _: '%d filas seleccionadas',
            0: 'clic fila para seleccionar',
            1: 'una fila seleccionada'
        }
    }
    }           
    } );       
  </script>

<body>
 
  <script type="text/javascript">
    function go(myModal, url, titulo,clase, id) {
var x=id;
document.getElementById('id_proyecto').value = x;

        document.getElementById(miframe).src = url;
       // titulomodal.innerText =(titulo);
        //titulomodal.style.textTransform = 'initial'; 
        var element = document.getElementById("iconomodal");
        element.className = element.className ="cambio";
        element.className = element.className.replace(/\bcambio\b/g, clase);
      
    }

</script>

<br><br>
@if(auth()->user()->rol_id === 1 )
<a href="javascript:void(0)" class="btn btn-default btn-xs" id="create-new-user">Crear nuevo proyecto</a>
@endif

@include("proyectos.modal")
@include('flash::message')


<table class="table table-bordered table-striped " id="laravel_datatable" >
   <thead>
      <tr>

         <th>#</th>
         <th>Cliente</th>
         <th>Proyecto</th>
         <th>Descripción</th>
         <th>Estado</th>
         <th>Responsable Actual</th>
         <th>Fecha Inicio</th>
         <th>Fecha Fin</th>
         <th>Presupuesto</th>
         <th>Accion</th>
      </tr>
   </thead>
</table>
</div>

@include("proyectos.form");

<script>
  $("input[data-type='currency']").on({
      keyup: function() {
        formatCurrency($(this));
      },
      blur: function() { 
        formatCurrency($(this), "blur");
      }
  });
  
  
  function formatNumber(n) {
    // format number 1000000 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  }
  
  
  function formatCurrency(input, blur) {
    // appends $ to value, validates decimal side
    // and puts cursor back in right position.
    
    // get input value
    var input_val = input.val();
    
    // don't validate empty input
    if (input_val === "") { return; }
    
    // original length
    var original_len = input_val.length;
  
    // initial caret position 
    var caret_pos = input.prop("selectionStart");
      
    // check for decimal
    if (input_val.indexOf(".") >= 0) {
  
      // get position of first decimal
      // this prevents multiple decimals from
      // being entered
      var decimal_pos = input_val.indexOf(".");
  
      // split number by decimal point
      var left_side = input_val.substring(0, decimal_pos);
      var right_side = input_val.substring(decimal_pos);
  
      // add commas to left side of number
      left_side = formatNumber(left_side);
  
      // validate right side
      right_side = formatNumber(right_side);
      
      // On blur make sure 2 numbers after decimal
      if (blur === "blur") {
        right_side += "00";
      }
      
      // Limit decimal to only 2 digits
      right_side = right_side.substring(0, 2);
  
      // join number by .
      input_val = "$" + left_side + "." + right_side;
  
    } else {
      // no decimal entered
      // add commas to number
      // remove all non-digits
      input_val = formatNumber(input_val);
      input_val = "$" + input_val;
      
      // final formatting
      if (blur === "blur") {
       // input_val += ".00";
      }
    }
    
    // send updated string to input
    input.val(input_val);
  
    // put caret back in the right position
    var updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
  }
  
  
  </script>

  <script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
  </script>


</body>
<script>
   var SITEURL = '{{URL::to('')}}';
   var rol = '{{auth()->user()->rol_id}}';

     $(document).ready( function () 
     {
      // Setup - add a text input to each footer cell
      $('#laravel_datatable thead tr').clone(true).appendTo( '#laravel_datatable thead' );
          $('#laravel_datatable thead tr:eq(1) th').each( function (i) 
          {
              if(i>0 && i<10)
              {
                    var title = $(this).text();
                    $(this).html( '<input type="text" style="font-size:12px; border-color:#304457; text-color:#ffff" placeholder="Buscar" />' );          
                  $( 'input', this ).on( 'keyup change', function () 
                  {
                      if ( table.column(i).search() !== this.value ) 
                      {
                          table
                              .column(i)
                              .search( this.value )
                              .draw();
                      }
                  } );
              }
              
          });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table =  $('#laravel_datatable').DataTable(
        {

          drawCallback: function( settings ) {

          var btns = $('.dt-button');
          btns.addClass('btn btn-info btn-xs');
          btns.removeClass('dt-button');

          var api = this.api();
          var datos = api.rows( {page:'current'} ).data();

          $.each(datos, function(i, item) 
            {
              if(rol == 3){
                $("a[id='delete-user'][data-id='"+item.id_proyecto+"']").hide();
              }

            });

          },
          
          
            orderCellsTop: true,
            fixedHeader: true,
            dom: 'Bfrtip',
                buttons: [
                    
                [{
                    extend: 'excel',
                    exportOptions: { orthogonal: 'export' }
                    
                }],
                
                [{
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: { orthogonal: 'export' }
                }],

                [{
                    extend: 'csv',
                    exportOptions: { orthogonal: 'export' }
                }],          

                [{
                    extend: 'print',
                    text: 'Imprimir',
                    exportOptions: { orthogonal: 'export' }
                }],
            ],
             processing: true,
             serverSide: true,
             ajax: {
              url:  "proyectos",
              type: 'GET',
             },
 
            columns: 
             [
                {data: 'id_proyecto', name: 'id_proyecto', visible: false},
               
                { data: 'nombre_cliente', name: 'nombre_cliente' },
                { data: 'proyecto_nombre', name: 'proyecto_nombre' },
                { data: 'proyecto_descripcion', name: 'proyecto_descripcion' },
                { data: 'estadoProyecto_Nombre', name: 'estadoProyecto_Nombre' },
                { data: 'nombre_responsable_actual', name: 'nombre_responsable_actual' },
                { data: 'proyecto_fecha_inicio', name: 'proyecto_fecha_inicio' },
                { data: 'proyecto_fecha_entrega', name: 'proyecto_fecha_entrega' },
                { data: 'proyecto_presupuesto',render: $.fn.dataTable.render.number( ',', '.', 2, '$' ), name: 'proyecto_presupuesto' },

                {data: 'action', name: 'action', orderable: false,searchable: false},
              ],
            order: [[0, 'desc']]
          });



         
///ensayo

     /*  When user click add user button */
        $('#create-new-user').click(function () 
        {
            $('#btn-save').val("create-user");
            $('#id_proyecto').val('');
            $('#userForm').trigger("reset");
            $('#userCrudModal').html("Crear un nuevo proyecto");
            document.getElementById('proyecto_responsable_actual').disabled = false;
            document.getElementById('ProyectoEstado_id').disabled = false;
            $('#ajax-crud-modal').modal('show');
        });
     
       /* When click edit user */
        $('body').on('click', '.edit-user', function () 
        {
            var id_proyecto = $(this).data('id');
            $.get('proyectos/' + id_proyecto +'/edit', function (data) 
            {
             // console.log(data[0].proyectoEstado_id);
                $('#name-error').hide();
                $('#email-error').hide();
                $('#userCrudModal').html("Editar información del proyecto");
                $('#btn-save').val("edit-user");
               
                ///campos formulario
                $('#id_proyecto').val(data[0].id_proyecto);
                $('#cliente_id2').val(data[0].cliente_id);
                $('#ProyectoEstado_id2').val(data[0].ProyectoEstado_id);

                $('#proyecto_nombre').val(data[0].proyecto_nombre);
                $('#proyecto_descripcion').val(data[0].proyecto_descripcion);
                $('#proyecto_fecha_inicio').val(data[0].proyecto_fecha_inicio);
                $('#proyecto_fecha_entrega').val(data[0].proyecto_fecha_entrega);
                $('#proyecto_presupuesto').val(data[0].proyecto_presupuesto);
                $('#proyecto_responsable_actual2').val(data[0].proyecto_responsable_actual);
                campo = document.getElementById('cliente_id2');
                document.getElementById('cliente_id').value = campo.value;
                campo2 = data[0].proyectoEstado_id;
                document.getElementById('ProyectoEstado_id').value = campo2;
                document.getElementById('ProyectoEstado_id2').value = campo2;
                campo3 = data[0].proyecto_responsable_actual;
                document.getElementById('proyecto_responsable_actual').value = campo3;
                document.getElementById('proyecto_responsable_actual2').value = campo3;
                campo3 = data[0].proyecto_fecha_inicio;
                document.getElementById('proyecto_fecha_inicio').value = campo3;
                @if(auth()->user()->rol_id!= 1  )
                  document.getElementById('proyecto_responsable_actual').disabled = true;
                @endif
            
                $.ajax(
                  {
                      type: "get",
                      url: SITEURL+"/bugsxproyecto/"+id_proyecto,
                      success: function (data) 
                      {
                        total = data.length;
                        if (total>0)
                        {
                          alert("No puede cambiar el estado de este proyecto, dado que existen hallazgos pendientes de aprobación o rechazo. Para cambiar el estado, primero debe dar cierre a los hallazgos.")
						              document.getElementById('ProyectoEstado_id').disabled = true;
                          
                        }
                         //console.log('ok:', data);  
                      },
                      error: function (data) 
                      {
                        total=0;
                          //console.log('Error:', data);
                      }
                  });
                $('#ajax-crud-modal').modal('show');

            })
        });

        $('body').on('click', '#delete-user', function () 
        {
            var id_proyecto = $(this).data("id");
            if(confirm("¿Está seguro que desea ELIMINAR el proyecto?"))
            {
                $.ajax(
                {
                  type: "get",
                  url: "proyecto/delete/"+id_proyecto,
                  success: function (data) 
                  {
                    window.location.reload();
                    var oTable = $('#laravel_datatable').dataTable(); 
                    oTable.fnDraw(false);
                  },
                  error: function (data) 
                  {
                  
          
                      
                  }
                });
            }
        });   

        //**//
        $('body').on('click', '#responsable-proyecto', function () 
        {
        
          var id_proyecto = $(this).data("id");
    
          // go('myModal','proyectos_responsables','title','glyphicon glyphicon-search',id_proyecto);
          document.getElementById('miframe').src = 'proyectos_responsables/ver/'+id_proyecto;
         // titulomodal.innerText =('Gestion de Testers');
         // titulomodal.style.textTransform = 'initial'; 

          $('#myModal').modal('show');
          // var element = document.getElementById("myModal");
          // element.className = element.className ="cambio";
          // element.className = element.className.replace(/\bcambio\b/g, '');
        });
   
        $('body').on('click', '#anexo-proyecto', function () 
        {
          var id_proyecto = $(this).data("id");
          document.getElementById('miframe').src = 'anexoProyecto/'+id_proyecto;
          $('#myModal').modal('show');
        });
     
        if ($("#userForm").length > 0) 
        {
              $("#userForm").validate(
              {
                submitHandler: function(form) 
                {
                    var x = $('#id_proyecto').val();
                    var y = $('#proyecto_presupuesto').val();
                    var TotalPresupuesto = y;
                    TotalPresupuesto = TotalPresupuesto.replace(/,/g, "");
                    TotalPresupuesto = TotalPresupuesto.replace('$', "");
                    TotalPresupuesto = TotalPresupuesto.replace(',', "");
                    TotalPresupuesto = TotalPresupuesto.replace('.00', "");
                   
                    $('#proyecto_presupuesto').val(TotalPresupuesto);
                    var actionType = $('#btn-save').val();
                    $('#btn-save').html('Enviando..');
                    $.ajax(
                    {
                        data: $('#userForm').serialize(),
                        url:  "proyecto/store",
                        type: "POST",
                        dataType: 'json',
                        success: function (data) 
                        { 
                          
                          // console.log("epa",data);
                            window.location.reload();
                            $('#userForm').trigger("reset");
                            $('#ajax-crud-modal').modal('hide');
                            $('#btn-save').html('Guardar cambios');
                            var oTable = $('#laravel_datatable').dataTable();
                            oTable.fnDraw(false);
                        },
                        error: function (data) 
                        {
                         // console.log(data.responseText);
                            console.log('Errorxxx:', data);
                            $('#btn-save').html('Guardar cambios');
                        }
                    });
                }
              })
          }
      });
    </script>

<script>
  function seleccionarCliente(cienteid) {
    var x = document.getElementById("cliente_id").value;

  //  document.getElementById("cliente_id2").value =  x;
    document.getElementById("cliente_id").selectedIndex = x;
  }
  </script>

</html>