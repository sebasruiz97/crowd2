<div class="modal fade" id="ajax-usuario-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto; width:600px; padding-right:40px; padding-left:20px; padding-top:20px;">
            <div class="modal-header">
                <h4 class="modal-title" id="editarCrudModal"></h4>
            </div>
            <div class="modal-body" style="height:auto; width:600px; padding-right:40px; padding-left:40px; padding-top:20px;">

                @inject('tiposDocumentos','App\Services\tiposDocumentos')
                @inject('roles','App\Services\roles')

                <form id="userEditForm" name="userEditForm" class="form-horizontal" >

                    <input type="hidden" name="id_usuario" id="id_usuario" value="">

                     <!-- Rol del usuario -->
                     <div class="form-group col-sm-12"  style="padding-bottom: 1px">
                        <label for="rol_id">Rol para el usuario</label>
                        <select class="form-control" id="rol_usuario" value="{{old('rol_id')}}" name="rol_usuario" required title="Este campo es requerido, seleccione un elemento de la lista">
                            @foreach($roles->get() as $index => $roles)
                            <option value="{{ $index }}" {{old('rol_id') == $index ? 'selected' : ''}}>
                                {{ $roles }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('rol_id'))
                        <span class="help-block" style="color:#F3A100" ;>
                            <small><i>{{ $errors->first('rol_usuario') }}</i></small>
                        </span>
                        @endif
                    </div>

                    <!-- Nombre -->
                     <div class="form-group col-sm-12"  style="padding-bottom: 1px">
                        <label for="name">Nombre</label>
                        <input type="text" class="form-control" name="name_usuario" id="name_usuario" value="{{ old('name') }}" required title="Este campo es requerido">
                        @if ($errors->has('name'))
                        <span class="help-block" style="color:#F3A100" ;>
                            <small><i>{{ $errors->first('name_usuario') }}</i></small>
                        </span>
                        @endif
                    </div>

                     <!-- Email -->
                    <div class="form-group col-sm-12"  style="padding-bottom: 1px">
                        <label for="email">Correo Electrónico</label>
                        <input type="email" class="form-control" name="email_usuario" id="email_usuario" value="{{ old('email') }}" required title="Este campo es requerido y debe ingresar un formato válido de correo electrónico">
                        @if ($errors->has('email'))
                        <span class="help-block" style="color:#F3A100" ;>
                            <small><i>{{ $errors->first('email_usuario') }}</i></small>
                        </span>
                        @endif
                    </div>

                    <!-- Tipo de Documento -->
                    <div class="form-group col-sm-12"  style="padding-bottom: 1px">
                        <label for="tipoDocumento_usuario">Tipo de Documento</label>
                        <select class="form-control" id="tipoDocumento_usuario" value="{{old('tipoDocumento_usuario')}}" name="tipoDocumento_usuario" required title="Este campo es requerido, seleccione un elemento de la lista">
                            @foreach($tiposDocumentos->get() as $index => $tiposDocumentos)
                            <option value="{{ $index }}" {{old('tipoDocumento_usuario') == $index ? 'selected' : ''}}>
                                {{ $tiposDocumentos }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('tipoDocumento_usuario'))
                        <span class="help-block" style="color:#F3A100" ;>
                            <small><i>{{ $errors->first('tipoDocumento_usuario') }}</i></small>
                        </span>
                        @endif
                    </div>

                    <!-- Numero de Documento -->
                    <div class="form-group col-sm-12"  style="padding-bottom: 1px" >
                        <label for="numeroDocumento_usuario">Número de Documento</label>
                        <input class="form-control" value="{{old('numeroDocumento_usuario')}}" name="numeroDocumento_usuario" type="text" id="numeroDocumento_usuario" required title="Este campo es requerido">
                        @if ($errors->has('numeroDocumento_usuario'))
                        <span class="help-block" style="color:#F3A100" ;>
                            <small><i>{{ $errors->first('numeroDocumento_usuario') }}</i></small>
                        </span>
                        @endif
                    </div>

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <div class="centrar">
                            <button type="submit" class="btn btn-primary" id="btn-edit" value="create" >Guardar cambios
                            </button>
                        </div>
                    </div>

                </form>
            </div>
                <h6 style="color:#fff"> Crowd </h6>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>


<style>
    .error {
    color: #F3A100;
    font-style: italic;
    font-weight: normal;   
    }
    
</style>


<script>

    $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Por favor verifica el valor ingresado."
    );

    $('#btn-edit').click(function() { 

        $("#name_usuario").rules("add", {
            required: true,
            regex: "[a-zA-Z ]",
            minlength: 2,
            maxlength: 50,                
            messages: {
                required: "Este campo es requerido",
                minlength: "Debe tener cómo mínimo 2 caracteres",
                maxlength: "Debe tener cómo máximo 50 caracteres",
                regex: "Sólo puede contener letras y espacios"
            }
        });    

          $("#numeroDocumento_usuario").rules("add", {
            required: true,
            regex: "[0-9]",
            minlength: 5,
            maxlength: 15,                
            messages: {
                required: "Este campo es requerido",
                minlength: "Debe tener cómo mínimo 6 caracteres",
                maxlength: "Debe tener cómo máximo 15 caracteres",
                regex: "Sólo puede contener números"
            }
        });             
  
    }); 
</script>

