<div class="modal fade" id="clave-crud-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto; width:600px; padding-right:40px; padding-left:20px; padding-top:20px;">
            <div class="modal-header">
                <h4 class="modal-title" id="claveCrudModal"></h4>
            </div>
            <div class="modal-body" style="height:auto; width:600px; padding-right:40px; padding-left:40px; padding-top:20px;">

                @inject('tiposDocumentos','App\Services\tiposDocumentos')
                @inject('roles','App\Services\roles')

                <form id="claveForm" name="claveForm" class="form-horizontal"  onsubmit="return checkForm(this)">

                    <!-- ID del usuario -->
                    <div class="form-group col-sm-12" style="padding-bottom: 1px"> 
                        <label for="usuario_id">ID del usuario</label>
                        <input class="form-control" name="usuario_id" id="usuario_id" value="" readonly>
                    </div>

                    <!-- Contraseña -->
                    <div class="form-group col-sm-12" style="padding-bottom: 1px">
                        <label for="password"  style="text-align: left"> <SPAN title="La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&" >Nueva contraseña</SPAN></label>
                        <input type="password" class="form-control"  name="cambio_password" id="cambio_password" >
                    </div>

                    <!-- Confirmación de Contraseña -->
                    <div class="form-group col-sm-12"style="padding-bottom: 1px" >
                        <label for="password_confirmation">Confirmación de contraseña</label>
                        <input type="password" name="cambio_password_confirmation" id="cambio_password_confirmation" class="form-control">
                    </div> <br>
       
                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <div class="centrar">
                            <button type="submit" class="btn btn-primary" id="btn-clave" value="create" >Guardar cambios
                            </button>
                        </div>
                    </div>

                </form>
            </div>
                <h6 style="color:#fff"> Crowd </h6>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>


<style>
    .error {
    color: #F3A100;
    font-style: italic;
    font-weight: normal;   
    }
    
</style>


{{-- <script>

    $(document).ready(function(){

        $.validator.addMethod(
                "regex",
                function(value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "Por favor revisa lo que ingresaste"
        );

        $('#btn-clave').click(function() {       

            $('#cambio_password').rules("add", {
                
                required: true,
                regex: "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]$",
                minlength: 6,
                maxlength: 15,                
                messages: {
                    required: "Este campo es requerido",
                    minlength: "Debe tener cómo mínimo 6 caracteres",
                    maxlength: "Debe tener cómo máximo 15 caracteres",
                    regex: "La contraseña debe contener al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&"
                }
            });      

            
            $('#cambio_password_confirmation').rules("add", {
                required: true,
                regex: "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]$",
                minlength: 6,
                maxlength: 15,                
                messages: {
                    required: "Este campo es requerido",
                    minlength: "Debe tener cómo mínimo 6 caracteres",
                    maxlength: "Debe tener cómo máximo 15 caracteres",
                    regex: "La contraseña debe contener al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&"
                }
            });         

    
        }); 
    });
</script> --}}
<script type="text/javascript">


    function checkForm(form)
    {

        re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]$/;
        if(!re.test(form.cambio_password.value)) {
            form.cambio_password.focus();
            return false;
        }
            return true;
    }


</script>