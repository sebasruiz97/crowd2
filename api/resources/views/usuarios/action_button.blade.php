

<a href="{{ route('usuarios.show',"$id") }}" data-toggle="tooltip" data-id="{{ $id }}" data-original-title="Ver usuario" 
    class="edit btn btn-default btn-xs " title="Ver perfil"> <i class="lnr lnr-magnifier"></i></a>

<a href="javascript:void(0)" data-toggle="tooltip" id="edit-user" data-id="{{ $id }}" data-original-title="Edit" 
    class="edit btn btn-default btn-xs edit-user" title="Editar usuario"> <i class="lnr lnr-pencil"></i></a>

<a href="javascript:void(0)" data-toggle="tooltip" id="clave-user" data-id="{{ $id }}" data-original-title="Edit" 
    class="edit btn btn-default btn-xs clave-user" title="Cambiar contraseña"> <i class="lnr lnr-lock"></i></a>

<form action="{{ route('usuarios.destroy',"$id") }}" method="POST">
    @csrf
    @method('DELETE')

    <div class='btn-group'>
        <button type="submit" class="btn btn-danger btn-xs" title="Desactivar usuario" onclick="return confirm('Al desactivar su cuenta, ya no podrá ingresar nuevamente a Crowd, ni recuperar su perfil. ¿Estás seguro?')"><i class="lnr lnr-cross"></i></button>
    </div>
</form>


