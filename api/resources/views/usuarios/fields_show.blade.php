<!--DATOS BASICOS -->

@foreach($users as $user)

@include('flash::message')
<h3 style="text-align:center"> {{$user->name}} </h3> <br>

<div class="container-fluid">

    <div class="row">

        <div class="col-sm col-12 col-lg-12">
            <div class="col-sm col-1 col-lg-1">
            </div>
            <div class="col-sm col-10 col-lg-10">
                <div class="col-sm col-3 col-lg-3">

                    <div class="profile-usertitle">
                        <img src="/storage/avatars/{{$user->avatar}}" class="img-circle" width="200px">
                    </div>

                    <div class="centrar" style="color:#f3a100; font-size:15px">
                        @if($puntosPerfil>7)
                        <i> <small> Tiene el perfil completo </small> </i>
                        @else
                        <i> <small> No tiene el perfil completo </small> </i>
                        @endif
                    </div>

                </div>
                <div class="col-sm col-9 col-lg-9">
                    <div class="col-sm col-6 col-lg-6">
                        <br> <br>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Nombre</th>
                                        <td>{{$user->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Correo</th>
                                        <td>{{$user->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tipo de Documento</th>
                                        <td>{{$user->nombre_tipoDocumento}}</td>
                                    </tr>
                                    <tr>
                                        <th>Número de Documento</th>
                                        <td>{{$user->numeroDocumento }}</td>
                                    </tr>
                                    <tr>
                                        <th>Fecha de Nacimiento</th>
                                        <td>{{ $user->fechaNacimiento }}</td>
                                    </tr>
                                    <tr>
                                        <th>Genero</th>
                                        <td>{{ $user->genero }}</td>
                                    </tr>
                                    <tr>
                                        <th>Ocupación actual</th>
                                        <td>{{ $user->nombre_ocupacion }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="col-sm col-6 col-lg-6">
                        <br> <br>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    @foreach($users as $user)

                                    <tr>
                                        <th>Pais</th>
                                        <td>{{ $user->nombre_pais }}</td>
                                    </tr>
                                    <tr>
                                        <th>Departamento</th>
                                        <td>{{ $user->nombre_departamento }}</td>
                                    </tr>

                                    <tr>
                                        <th>Municipio</th>
                                        <td>{{ $user->nombre_municipio }}</td>
                                    </tr>

                                    <tr>
                                        <th>Dirección</th>
                                        <td>{{ $user->direcion }}</td>
                                    </tr>
                                    <tr>
                                        <th>Teléfono Fijo</th>
                                        <td>{{ $user->telefonoFijo }}</td>
                                    </tr>
                                    <tr>
                                        <th>Teléfono Celular</th>
                                        <td>{{ $user->telefonoCelular }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm col-1 col-lg-1">
                </div>
                <div>
                    <div>
                    </div>
                </div>
                <div class="col-sm col-1 col-lg-1">
                </div>
            </div>
        </div>


        <!--ESTUDIOS FORMALES -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm col-12 col-lg-12">
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                    <div class="col-sm col-10 col-lg-10">
                        <br>
                        <h3 style="text-align:left"> Estudios Formales </h3>
                        <div class="table-responsive">
                            <table class="table" id="usuario-table">
                                <thead>
                                    <tr>
                                        <th>Nivel de Educación</th>
                                        <th>Area de Estudio</th>
                                        <th>Estado del Estudio</th>
                                        <th>Fecha de Inicio</th>
                                        <th>Fecha Fin</th>
                                        <th>Universidad o institución</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($estudiosFormales as $estudioFormal)
                                    <tr>
                                        <td>{{ $estudioFormal->nombre_nivelEducacion }}</td>
                                        <td>{{ $estudioFormal->nombre_areaEstudio }}</td>
                                        <td>{{ $estudioFormal->nombre_estadoEstudio }}</td>
                                        <td>{{ $estudioFormal->fechaInicio_estudioFormal }}</td>
                                        <td>{{ $estudioFormal->fechaFin_estudioFormal }}</td>
                                        <td>{{ $estudioFormal->nombre_universidad }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pull-right">
                                {{ $estudiosFormales->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                </div>
            </div>
        </div>



        <!--ESTUDIOS NO FORMALES -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm col-12 col-lg-12">
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                    <div class="col-sm col-10 col-lg-10">
                        <br>
                        <h3 style="text-align:left"> Estudios no Formales </h3>
                        <div class="table-responsive">
                            <table class="table" id="usuario-table">
                                <thead>
                                    <tr>
                                        <th>Tipo de Estudio</th>
                                        <th>Nombre de Estudio</th>
                                        <th>Fecha de Finalización</th>
                                        <th>Duración en meses</th>
                                        <th>Lugar del estudio</th>
                                        <th>Universidad, institución o plataforma</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($estudiosNoFormales as $estudioNoFormal)
                                    <tr>
                                        <td>{{ $estudioNoFormal->nombre_tipoEstudio }}</td>
                                        <td>{{ $estudioNoFormal->nombre_estudioNoFormal }}</td>
                                        <td>{{ $estudioNoFormal->fechaFin_estudioNoFormal }}</td>
                                        <td>{{ $estudioNoFormal->duracion }}</td>
                                        <td>{{ $estudioNoFormal->lugar_estudio }}</td>
                                        <td>
                                            {{ $estudioNoFormal->nombre_plataforma }}
                                            {{ $estudioNoFormal->nombre_universidad }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pull-right">
                                {{ $estudiosNoFormales->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                </div>
            </div>
        </div>

        <!--DATOS LABORALES -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm col-12 col-lg-12">
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                    <div class="col-sm col-10 col-lg-10">
                        <br>
                        <h3 style="text-align:left"> Información Laboral </h3>
                        <div class="table-responsive">
                            <table class="table" id="usuario-table">
                                <thead>
                                    <tr>
                                        <th>Empresa</th>
                                        <th>Sector</th>
                                        <th>Cargo</th>
                                        <th>Fecha de Inicio</th>
                                        <th>Fecha Fin</th>
                                        <th>Responsabilidades</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($datosLaborales as $datoLaboral)
                                    <tr>
                                        <td>{{ $datoLaboral->empresa }}</td>
                                        <td>{{ $datoLaboral->nombre_sector }}</td>
                                        <td>{{ $datoLaboral->nombre_cargo }}</td>
                                        <td>{{ $datoLaboral->fechaInicio_datoLaboral }}</td>
                                        <td>
                                            @if(($datoLaboral->fechaFin_datoLaboral) === null)
                                            Trabajo actual
                                            @else
                                            {{ $datoLaboral->fechaFin_datoLaboral }}
                                            @endif
                                        </td>
                                        <td>{{ $datoLaboral->responsabilidades }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pull-right">
                                {{ $datosLaborales->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                </div>
            </div>
        </div>

        <!--CONOCIMIENTO EN PRUEBAS -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm col-12 col-lg-12">
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                    <div class="col-sm col-10 col-lg-10">
                        <br>
                        <h3 style="text-align:left"> Conocimiento en pruebas</h3>
                        <div class="table-responsive">
                            <table class="table" id="usuario-table">
                                <thead>
                                    <tr>
                                        <th>Tipo prueba</th>
                                        <th>Tiempo experiencia</th>
                                        <th>Tipo ejecucion</th>
                                        <th>Herramienta usada</th>
                                        <th>Dispositivo usado</th>
                                        <th>Navegador</th>
                                        <th>Sistema Operativo</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($conocimientoPruebas as $conocimientoPrueba)
                                    <tr>
                                        <td>{{ $conocimientoPrueba->nombre_prueba}}</td>
                                        <td>{{ $conocimientoPrueba->nombre_tiempoExperiencia }}</td>
                                        <td>{{ $conocimientoPrueba->nombre_ejecucion }}</td>
                                        <td>{{ $conocimientoPrueba->nombre_herramienta }}</td>
                                        <td>{{ $conocimientoPrueba->nombre_dispositivo }}</td>
                                        <td>{{ $conocimientoPrueba->nombre_navegador }}</td>
                                        <td>{{ $conocimientoPrueba->nombre_sistemaOperativo }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pull-right">
                                {{ $conocimientoPruebas->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                </div>
            </div>
        </div>


        <!--DISPOSITIVOS -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm col-12 col-lg-12">
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                    <div class="col-sm col-10 col-lg-10">
                        <br>
                        <h3 style="text-align:left"> Dispositivos disponibles</h3>
                        <div class="table-responsive">
                            <table class="table" id="usuario-table">
                                <thead>
                                    <tr>
                                        <th>Dispositivo usado</th>
                                        <th>Navegador del dispositivo</th>
                                        <th>Sistema Operativo del dispositivo</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($dispositivos as $dispositivo)
                                    <tr>
                                        <td>{{ $dispositivo->nombre_dispositivo }}</td>
                                        <td>{{ $dispositivo->nombre_navegador }}</td>
                                        <td>{{ $dispositivo->nombre_sistemaOperativo }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pull-right">
                                {{ $dispositivos->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                </div>
            </div>
        </div>


        <!--DATOS FINANCIEROS -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm col-12 col-lg-12">
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                    <div class="col-sm col-10 col-lg-10">
                        <br>
                        <h3 style="text-align:left"> Información Financiera </h3>
                        <div class="table-responsive">
                            <table class="table" id="usuario-table">
                                <thead>
                                    <tr>
                                        <th>Entidad Bancaria</th>
                                        <th>Tipo de Cuenta</th>
                                        <th>Número de Cuenta</th>
                                        <th>Nombre del Titular</th>
                                        <th>Sucursal</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($datosFinancieros as $datoFinanciero)
                                    <tr>
                                        <td>{{ $datoFinanciero->nombre_banco }}</td>
                                        <td>{{ $datoFinanciero->nombre_cuentaBancaria }}</td>
                                        <td>{{ $datoFinanciero->numeroCuenta }}</td>
                                        <td>{{ $datoFinanciero->nombreTitular }}</td>
                                        <td>{{ $datoFinanciero->sucursal }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pull-right">
                                {{ $datosFinancieros->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                </div>
            </div>
        </div>



        <!--ANEXOS -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm col-12 col-lg-12">
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                    <div class="col-sm col-10 col-lg-10">
                        <br>
                        <h3 style="text-align:left"> Anexos cargados </h3>

                        @if ( count($anexos) == 0 )
                        El tester no ha agregado ningún anexo
                        @else

                        <div class="table-responsive">
                            <table class="table" id="usuario-table">
                                <thead>
                                    <tr>
                                        <th>Tipo de Anexo</th>
                                        <th colspan="3">Descargar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($anexos as $anexo)
                                    <tr>
                                        <td>{{ $anexo->nombre_anexo }}</td>
                                        <td>
                                            <div style="display:flex">
                                                <div style="margin-right: 5px;">
                                                    <form action="{{ route('anexos.destroy',$anexo->id_anexo) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')

                                                        <div class='pull left'>
                                                            <a href="{{ route('anexos.show',$anexo->url) }}" class="btn btn-default btn-xs" style="margin-rigth: 3px"><i class="lnr lnr-download"></i></a>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div>
                                                    <form action="{{ route('anexos.approve', $anexo->id_anexo) }}" method="POST">
                                                        @csrf
                                                        <div class="pull left">
                                                            <button class="btn btn-xs {{ $anexo->approved == 1? 'btn-success' : 'btn-default' }}" style="margin-rigth: 3px">
                                                                {{ $anexo->approved == 1 ? 'Aprobado' : 'Aprobar' }}
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pull-right">
                                <small>{{ $anexos->links() }}</small>
                            </div>
                        </div>

                        @endif
                    </div>
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                </div>
            </div>
        </div>