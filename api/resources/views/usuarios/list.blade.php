<!DOCTYPE html>
 
<html lang="en">
<head>

    <style>

        table.dataTable {
            box-sizing: content-box;
            width:100% !important;            
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);
    
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button  {
            box-sizing: border-box;
            display: inline-block;
            min-width: 1.5em;
            padding: 0.5em 1em;
            margin-left: 2px;
            text-align: center;
            text-decoration: none !important;
            cursor: pointer;
            *cursor: hand;
            color: #304457 !important;
            border: 1px solid transparent;
            border-radius: 2px;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover   {
    
            text-decoration: none !important;
            color: #ffff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);
        
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:focus  {
    
            text-decoration: none !important;
            color: #ffff !important;
            border: 1px solid #304457 !important;
            background-color: #304457 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #304457), color-stop(100%, #304457));
            background: -webkit-linear-gradient(top, #304457 0%, #304457 100%);
            background: -moz-linear-gradient(top, #304457 0%, #304457 100%);
            background: -ms-linear-gradient(top, #304457 0%, #304457 100%);
            background: -o-linear-gradient(top, #304457 0%, #304457 100%);

        }

        thead input {
        width: 100%;
        }
        
    
    </style>

    <script type="text/javascript">
        function go(miframe, url) {
            
            url2 = 'anexosBugs';
            document.getElementById(miframe).src = url2;
                    
        }
  
    </script>

    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>

    <script>
        
        $.extend( true, $.fn.dataTable.defaults, {
        "language": {
        "decimal": ",",
        "thousands": ".",
        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "infoPostFix": "",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "loadingRecords": "Cargando...",
        "lengthMenu": "Mostrar _MENU_ registros",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": ">",
            "previous": "<"
        },
        "processing": "Procesando...",
        "search": "Buscar:",
        "searchPlaceholder": "Término de búsqueda",
        "zeroRecords": "No se encontraron resultados",
        "emptyTable": "Ningún dato disponible en esta tabla",
        "aria": {
            "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "select": {
            "rows": {
                _: '%d filas seleccionadas',
                0: 'clic fila para seleccionar',
                1: 'una fila seleccionada'
            }
        }
        }           
        } );       
    </script>

</head>


<body>
 
    <!-- Modal -->
    <div class="container" >       
        
         <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm " >
                <div class="modal-content">
                    
                    <div class="modal-header modal-header-info " >

                        <table width="100%" >
                            <tr>
                              <th> <h1 style=" text-align:center"><i id="iconomodal" class="cambio" ></i></h1></th>
                              <th style=" text-align:center"> <h3 id="titulomodal" class="letra" style=" text-align:center" >Anexos del hallazgo</i></h3></th>
                              <th><button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color:white">×</button></th>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-body" style="padding-left:10px; padding-right:20px">
                        
                        <iframe width="100%" height="495" src="" id="miframe" style="color:transparent; border-color:transparent"></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- fin Modal -->
<div class="container" style="width:100%">
<br>

<a href="javascript:void(0)" class="btn btn-default btn-xs"  id="create-new-user">Crear usuario</a>
<br><br>

<table class="table table-bordered table-striped" id="laravel_datatable" style="width:100%; font-size:13px" data-turbolinks="false">
   <thead>
      <tr>
        <th style="min-width: 20px">ID del usuario</th>
        <th style="min-width: 50px">Rol del usuario</th>
        <th style="min-width: 100px">Nombre</th>
        <th style="min-width: 80px">Correo</th>
        <th style="min-width: 50px">Experiencia en pruebas</th>
        <th style="min-width: 80px">Tipo de documento</th>
        <th style="min-width: 50px">Número de Documento</th>
        <th style="min-width: 50px">Departamento</th>
        <th style="min-width: 50px">Municipio</th>
        <th style="min-width: 50px">Telefono Celular</th>
        <th style="min-width: 50px">Ocupación actual</th>
        <th style="min-width: 50px">Puntos Acumulados</th>
        <th style="min-width: 50px">Estado</th>
        <th style="min-width: 100px">Acciones</th>
      </tr>
   </thead>
</table>
</div>
 
@include("usuarios.form");
@include("usuarios.formEditar");
@include("usuarios.formClave");
@include('flash::message')


</body>
<script>
    var SITEURL = '{{URL::to('')}}';
    var usuario = '{{auth()->user()->name}}';
    var rol = '{{auth()->user()->rol_id}}';

     $(document).ready( function () {

        // Setup - add a text input to each footer cell
        $('#laravel_datatable thead tr').clone(true).appendTo( '#laravel_datatable thead' );
        $('#laravel_datatable thead tr:eq(1) th').each( function (i) {

            var title = $(this).text();
           
            $(this).html( '<input type="text" style="font-size:12px; border-color:#304457; text-color:#ffff" placeholder="Buscar" />' );
    
   
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
         
       $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      var table = $('#laravel_datatable').DataTable({

        drawCallback: function( settings ) {

        var api = this.api();
        var datos = api.rows( {page:'current'} ).data();

            $.each(datos, function(i, item) 
            {      

                var btns = $('.dt-button');
                btns.addClass('btn btn-info btn-xs');
                btns.removeClass('dt-button');

            });

        },
         
            orderCellsTop: true,
            fixedHeader: false,
            processing: true,
            serverSide: true,
            scrollX: true,

 
            ajax: {
              url:  "usuarios",
              type: 'GET',
             },
             columns: [
                      {data: 'id', name: 'id', 'visible': true},
                      {data: 'descripcion', name: 'descripcion', 'visible': true},
                      { data: 'name', name: 'name' },
                      { data: 'email', name: 'email' },
                      { data: 'experienciaPruebas', name: 'experienciaPruebas',  'visible': false },
                      { data: 'nombre_tipoDocumento', name: 'nombre_tipoDocumento','visible': true },
                      { data: 'numeroDocumento', name: 'numeroDocumento' },
                      { data: 'nombre_departamento', name: 'nombre_departamento',  'visible': true },
                      { data: 'nombre_municipio', name: 'nombre_municipio',  'visible': false },
                      { data: 'telefonoCelular', name: 'telefonoCelular',  'visible': false },
                      { data: 'nombre_ocupacion', name: 'nombre_ocupacion' },
                      { data: 'puntosAcumulados', name: 'puntosAcumulados', 'visible': false },
                      { data: 'nombre_estado', name: 'nombre_estado', 'visible': true },
                      {data: 'action', name: 'action', orderable: false, searchable: false,},
                   ],
            order: [[0, 'desc']],

            dom: 'Bfrtip',
                buttons: [
                    
                [{
                    extend: 'excel',
                    exportOptions: { orthogonal: 'export' }
                    
                }],
                
                [{
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: { orthogonal: 'export' }
                }],

                [{
                    extend: 'csv',
                    exportOptions: { orthogonal: 'export' }
                }],          

                [{
                    extend: 'print',
                    text: 'Imprimir',
                    exportOptions: { orthogonal: 'export' }
                }],
            ],
          });
     /*  Para crear nuevo usuario */
        $('#create-new-user').click(function () {

            $('#btn-save').val("create-user");
            $('#id').val('');
            $('#userForm').trigger("reset");
            $('#userCrudModal').html("Añadir un nuevo usuario");
            $('#ajax-crud-modal').modal('show');
        });
     
       /* Para editar usuario */
        $('body').on('click', '.edit-user', function () {
          var id = $(this).data('id');
 

          $.get('usuarios/' + id +'/edit', function (data) {

            $('#editarCrudModal').html("Editar usuario");
            $('#btn-edit').val("edit-user");
            $('#ajax-usuario-modal').modal('show');
            $('#id_usuario').val(data.id);
            $('#rol_usuario').val(data.rol_id);
            $('#name_usuario').val(data.name);
            $('#email_usuario').val(data.email);
            $('#tipoDocumento_usuario').val(data.tipoDocumento_id);
            $('#numeroDocumento_usuario').val(data.numeroDocumento);

          });
       });


       /* Para editar clave */
       $('body').on('click', '.clave-user', function () {
          var id = $(this).data('id');
 
          $.get('usuarios/' + id +'/edit', function (data) {

            $('#claveCrudModal').html("Cambiar contraseña de acceso");
            $('#btn-clave').val("edit-clave");
            $('#clave-crud-modal').modal('show');
            $('#usuario_id').val(data.id);
          });
       });

       //Para desactivar usuario
        $('body').on('click', '#delete-user', function () {
     
            var id = $(this).data("id");
            if(confirm("Está seguro que desea eliminar!")){

               
            $.ajax({
                type: "get",
                url: "usuario/delete/"+id,
                success: function (data) {

                window.location.reload();
                var oTable = $('#laravel_datatable').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
          }
        });   
       });


  
    // Para enviar a crear usuario
     
        $(document).ready(function(e){
        // Submit form data via Ajax

        if ($("#userForm").length > 0) {
        $("#userForm").validate({

            rules: {
                password: {
                    required:true,
                    minlength:8,
                    maxlength:15,

                },
                password_confirmation: {
                    required:true,
                    minlength:8,
                    maxlength:15,
                    equalTo:"#password",

                }
            },

            messages: {
                password: {
                    required: "Este campo es requerido y contener al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&",
                    minlength: "Debe tener mínimo 8 caracteres",
                    maxlength: "Debe tener máximo 15 caracteres",

                },
                password_confirmation: {
                    required: "Este campo es requerido y contener al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&",
                    minlength: "Debe tener minimo 8 caracteres",
                    maxlength: "Debe tener máximo 15 caracteres",
                    equalTo: "Las contraseñas no coinciden",

                }
            },
      
    
    
        submitHandler: function(form) {

        $("#userForm").on('submit', function(e){
                        
            e.preventDefault();
            var formData = new FormData(document.getElementById("userForm"));            

        var x = $('#id').val(); 
    
        var actionType = $('#btn-save').val();            

        $('#btn-save').html('Enviando..');


        $.ajax({
            data: $('#userForm').serialize(),
            type: "post",
            url:  "usuario/store",
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,

            success: function (response) {    

            window.location.reload();

            $('#userForm').trigger("reset");
            $('#ajax-crud-modal').modal('hide');                
            $('#btn-save').html('Guardar cambios');

            $('#laravel_datatable').DataTable().ajax.reload();
            var oTable = $('#laravel_datatable').dataTable();
            oTable.fnDraw(false);
            },

            error: function (data) {
                console.log('Error:', data);
                $('#btn-save').html('Guardar cambios');
            }
            
        });
        
        });

        }
        });
        }
    });


// Para enviar a editar usuario
     
    $(document).ready(function(e){
            // Submit form data via Ajax

            if ($("#userEditForm").length > 0) {
            $("#userEditForm").validate({
      
            submitHandler: function(form) {

            $("#userEditForm").on('submit', function(e){
                           
                e.preventDefault();
                var formData = new FormData(document.getElementById("userEditForm"));            

            var x = $('#id_usuario').val(); 
      
            var actionType = $('#btn-save').val();            

            $('#btn-save').html('Enviando..');


            $.ajax({
                data: $('#userEditForm').serialize(),
                type: "post",
                url:  "usuario/update",
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
	            processData: false,

                success: function (response) {    

                window.location.reload();

                $('#userEditForm').trigger("reset");
                $('#ajax-usuario-modal').modal('hide');                
                $('#btn-save').html('Guardar cambios');

                $('#laravel_datatable').DataTable().ajax.reload();
                var oTable = $('#laravel_datatable').dataTable();
                oTable.fnDraw(false);
                },

                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Guardar cambios');
                }
              
            });
            
            });

            }
            });
            }
        });

        
        // Para enviar a cambiar contraseña
     
    $(document).ready(function(e){
            // Submit form data via Ajax

            if ($("#claveForm").length > 0) {


            $("#claveForm").validate({

            rules: {
                cambio_password: {
                    required:true,
                    minlength:8,
                    maxlength:15,

                },
                cambio_password_confirmation: {
                    required:true,
                    minlength:8,
                    maxlength:15,
                    equalTo:"#cambio_password",

                }
            },

            messages: {
                cambio_password: {
                    required: "Este campo es requerido y contener al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&",
                    minlength: "Debe tener mínimo 8 caracteres",
                    maxlength: "Debe tener máximo 15 caracteres",

                },
                cambio_password_confirmation: {
                    required: "Este campo es requerido y contener al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&",
                    minlength: "Debe tener minimo 8 caracteres",
                    maxlength: "Debe tener máximo 15 caracteres",
                    equalTo: "Las contraseñas no coinciden",

                }
            },
      
            submitHandler: function(form) {

            $("#claveForm").on('submit', function(e){
                           
                e.preventDefault();
                var formData = new FormData(document.getElementById("claveForm"));            

            var x = $('#id_usuario').val(); 
      
            var actionType = $('#btn-save').val();            

            $('#btn-clave').html('Enviando..');


            $.ajax({
                data: $('#claveForm').serialize(),
                type: "post",
                url:  "usuario/cambiarClave",
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
	            processData: false,

                success: function (response) {    

                window.location.reload();

                $('#claveForm').trigger("reset");
                $('#clave-crud-modal').modal('hide');                
                $('#btn-clave').html('Guardar cambios');

                $('#laravel_datatable').DataTable().ajax.reload();
                var oTable = $('#laravel_datatable').dataTable();
                oTable.fnDraw(false);
                },

                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-clave').html('Guardar cambios');
                }
              
            });
            
            });

            }
            });
            }
        });


        
    </script>
</html>

