<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto; width:600px; padding-right:40px; padding-left:20px; padding-top:20px;">
            <div class="modal-header">
                <h4 class="modal-title" id="userCrudModal"></h4>
            </div>
            <div class="modal-body" style="height:auto; width:600px; padding-right:40px; padding-left:40px; padding-top:20px;">

                @inject('tiposDocumentos','App\Services\tiposDocumentos')
                @inject('roles','App\Services\roles')

                <form id="userForm" name="bugForm" class="form-horizontal" onsubmit="return checkFormDos(this)">

                     <!-- Rol del usuario -->
                     <div class="form-group col-sm-12"  style="padding-bottom: 1px">
                        <label for="rol_id">Rol para el usuario</label>
                        <select class="form-control" id="rol_id" value="{{old('rol_id')}}" name="rol_id" required title="Este campo es requerido, seleccione un elemento de la lista">
                            @foreach($roles->get() as $index => $roles)
                            <option value="{{ $index }}" {{old('rol_id') == $index ? 'selected' : ''}}>
                                {{ $roles }}</option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Nombre -->
                     <div class="form-group col-sm-12"  style="padding-bottom: 1px">
                        <label for="name">Nombre</label>
                        <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" >
                    </div>

                     <!-- Email -->
                    <div class="form-group col-sm-12"  style="padding-bottom: 1px">
                        <label for="email">Correo Electrónico</label>
                        <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}" required title="Este campo es requerido y debe ingresar un formato válido de correo electrónico">
                    </div>

                    <!-- Tipo de Documento -->
                    <div class="form-group col-sm-12"  style="padding-bottom: 1px">
                        <label for="tipoDocumento_id">Tipo de Documento</label>
                        <select class="form-control" id="tipoDocumento_id" value="{{old('tipoDocumento_id')}}" name="tipoDocumento_id" required title="Este campo es requerido, seleccione un elemento de la lista">
                            @foreach($tiposDocumentos->get() as $index => $tiposDocumentos)
                            <option value="{{ $index }}" {{old('tipoDocumento_id') == $index ? 'selected' : ''}}>
                                {{ $tiposDocumentos }}</option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Numero de Documento -->
                    <div class="form-group col-sm-12"  style="padding-bottom: 1px" >
                        <label for="numeroDocumento">Número de Documento</label>
                        <input class="form-control" value="{{old('numeroDocumento')}}" name="numeroDocumento" type="text" id="numeroDocumento" required >
                     </div>

                    <!-- Contraseña -->
                    <div class="form-group col-sm-12" style="padding-bottom: 1px">
                        <label for="password"  style="text-align: left"> <SPAN title="La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&" >Contraseña</SPAN></label>
                        <input type="password" class="form-control" id="password" name="password" >
                    </div>

                    <!-- Confirmación de Contraseña -->
                    <div class="form-group col-sm-12"style="padding-bottom: 1px" >
                        <label for="password_confirmation">Confirmación de contraseña</label>
                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                    </div> <br>
       
                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <div class="centrar">
                            <button type="submit" class="btn btn-primary" id="btn-save" value="create" >Guardar cambios
                            </button>
                        </div>
                    </div>

                </form>
            </div>
                <h6 style="color:#fff"> Crowd </h6>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function(){

        $.validator.addMethod(
                "regex",
                function(value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "Por favor revisa lo que ingresaste"
        );

        $('#btn-save').click(function() { 

            $("#name").rules("add", {
                required: true,
                regex: "[a-zA-Z ]",
                minlength: 2,
                maxlength: 50,                
                messages: {
                    required: "Este campo es requerido",
                    minlength: "Debe tener cómo mínimo 2 caracteres",
                    maxlength: "Debe tener cómo máximo 50 caracteres",
                    regex: "Sólo puede contener letras y espacios"
                }
            });    

            $("#numeroDocumento").rules("add", {
                required: true,
                regex: "[0-9]",
                minlength: 5,
                maxlength: 15,                
                messages: {
                    required: "Este campo es requerido",
                    minlength: "Debe tener cómo mínimo 6 caracteres",
                    maxlength: "Debe tener cómo máximo 15 caracteres",
                    regex: "Sólo puede contener números"
                }
            });             
    
        }); 
    });
</script>

<script type="text/javascript">


    function checkFormDos(form)
    {

        re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]$/;
        if(!re.test(form.password.value)) {
            form.password.focus();
            return false;
        }
            return true;
    }


</script>

