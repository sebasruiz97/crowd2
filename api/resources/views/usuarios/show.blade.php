@extends('layouts.app')

@section('content')

<h1 class="pull-right">
    <a class="btn btn-default btn-xs pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px" href="{{ route('usuarios.index') }}">Volver </a>
</h1>


    <div class="row">

            @include('usuarios.fields_show')
    </div>
@endsection
