
@extends('layouts.app2')

@section('content')

    @if ($estudiosFormales->isEmpty()) 
    <h1 class="pull-right">
        <a class="btn btn-default btn-xs pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px" href="{{ route('estudiosFormales.create') }}">Añadir estudios formales</a>
    </h1>
<br>
    <br>
    <br>
    <br>
    <br>
    <div style="text-align:center">
        <h3 >Aún no has agregado tus estudios formales</h3>
        <h5> Recuerda que esta información es necesaria para poder participar en los proyectos, sólo cuando hayas completado tu perfil quedarás activo en la plataforma. </h5>
    </div>
    @else
        <h3  style="text-align:center"> Estudios Formales </h3>
        <h1 class="pull-right">
            <a class="btn btn-default btn-xs pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px" href="{{ route('estudiosFormales.create') }}">Añadir otro</a>
        </h1>
        @include('flash::message')
        @include('estudios_formales.table')
    @endif
@endsection



