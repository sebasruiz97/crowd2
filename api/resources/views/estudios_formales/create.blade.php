@extends('layouts.app2')

@section('content')
<div> 
    <h3 class="centrar"> <strong>  Añadir estudios formales </strong></h3>
</div> <br>

                    <form method="POST" action="{{ route('estudiosFormales.store') }}">

                        @include('estudios_formales.fields_create')
		
                    </form>
@endsection
