@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Estudios Formales
        </h1>
	
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    
                    @foreach($estudiosFormales as $estudioFormal)
                    @include('estudios_formales.fields_show')
                    @endforeach
                    <a href="{{ route('estudiosFormales.index') }}" class="btn btn-default">Volver</a>
                </div>
            </div>
        </div>
    </div>
@endsection
