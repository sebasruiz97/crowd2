@csrf

<script type="text/javascript">
    function showDiv(select){
          if(select.value==361){
           document.getElementById('otra_universidad').style.display = "block";
           document.getElementById('otra_universidad').disabled = false;
          } else{
            document.getElementById('otra_universidad').style.display = "none";
            document.getElementById('otra_universidad').disabled = true;
          }
    } 
</script>


<div class="form-group col-sm-12">
    <div class="form-group col-sm-2"> </div>
    <div class="form-group col-sm-8">

<!-- Nivel de Educación -->
<div class="form-group col-sm-6">
    <label for="nivelEducacion_id">¿Cuál es el nivel de educación?*:</label>
    <select class="form-control" id="nivelEducacion_id" name="nivelEducacion_id" required>
        @foreach($nivelEducacion as $key => $value)
             <option value="{{ $key }}" {{ ( $key == $nivelSeleccionado) ? 'selected' : '' }}> 
                {{ $value }} 
        @endforeach
    </select>
    @if ($errors->has('nivelEducacion_id'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('nivelEducacion_id') }}</i></small>
        </span>
    @endif
</div>

<!-- Area de Estudio -->
<div class="form-group col-sm-6">
    <label for="areaEstudio_id">¿En que área es el estudio?*:</label>
    <select class="form-control" id="areaEstudio_id" name="areaEstudio_id" required >
        @foreach($areaEstudio as $key => $value)
             <option value="{{ $key }}" {{ ( $key == $areaSeleccionada) ? 'selected' : '' }}> 
                {{ $value }} 
        @endforeach
    </select>
    @if ($errors->has('areaEstudio_id'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('areaEstudio_id') }}</i></small>
        </span>
    @endif
</div>


<!-- Estado Estudio -->
<div class="form-group col-sm-6">
    <label for="estadoEstudio_id">¿En que estado tienes el estudio?*:</label>
    <select class="form-control" id="estadoEstudio_id" name="estadoEstudio_id" required >
        @foreach($estadoEstudio as $key => $value)
             <option value="{{ $key }}" {{ ( $key == $estadoSeleccionado) ? 'selected' : '' }}> 
                {{ $value }} 
        @endforeach
    </select>
    @if ($errors->has('estadoEstudio_id'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('estadoEstudio_id') }}</i></small>
        </span>
    @endif
</div>

<!-- Fecha de Inicio -->
<div class="form-group col-sm-6">
    <label for="fechaInicio_estudioFormal">¿Cuando iniciaste el estudio?*:</label>
    <input type="month" class="form-control" min="1980-01"value="{{old('fechaInicio_estudioFormal', $estudioFormal->fechaInicio_estudioFormal)}}" name="fechaInicio_estudioFormal"  type="fechaInicio_estudioFormal" id="fechaInicio_estudioFormal" required>
    @if ($errors->has('fechaInicio_estudioFormal'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('fechaInicio_estudioFormal') }}</i></small>
        </span>
    @endif
</div>

<!-- Fecha Fin -->
<div class="form-group col-sm-6" 
style="display:{{ ($estadoSeleccionado == 2 || $estadoSeleccionado == 3) && !$errors->has('fechaFin_estudioFormal')? 'none' : 'block' }}">
    <label for="fechaFin_estudioFormal">¿Cuando finalizaste el estudio?:</label>
    <input type="month" class="form-control" min="1980-01" name="fechaFin_estudioFormal"  value="{{old('fechaFin_estudioFormal', $estudioFormal->fechaFin_estudioFormal)}}" type="fechaFin_estudioFormal" id="fechaFin_estudioFormal" >
    @if ($errors->has('fechaFin_estudioFormal'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('fechaFin_estudioFormal') }}</i></small>
        </span>
    @endif
</div>

<!-- DEpartamento Field -->
<div class="form-group col-sm-6">
    <label for="departamento_id">¿A que departamento pertenece la institución donde estudiaste?*</label>
    <select class="form-control" id="departamento_id" name="departamento_id">
        <option value= ''> Selecciona uno </option>
        @foreach($departamentos as $key => $value)
            <option value="{{ $key }}" {{ ( $key == $departamentoSeleccionado) ? 'selected' : '' }}> 
            {{ $value }} 
        @endforeach
    </select>
    @if ($errors->has('departamento_id'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('departamento_id') }}</i></small>
        </span>
    @endif
</div>

<!-- Universidad Field -->
<div class="form-group col-sm-6">
    <label for="universidad_id">¿En cuál universidad o instituto realizaste el estudio?*</label>
    <select class="form-control" data-old= {{ $universidadSeleccionada}} id="universidad_id" name="universidad_id" onchange="showDiv(this)">
        <option value= ''> </option>
    </select>
    @if ($errors->has('universidad_id'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('universidad_id') }}</i></small>
        </span>
    @endif
</div>

<!-- Otra universidad -->
<div class="form-group col-sm-6" id="otra_universidad" style="display:none;" >
    <label for="otra_universidad">¿Cuál es el nombre de tu universidad?*:</label>
    <input class="form-control" name="otra_universidad" data-old= {{ $otraUniversidadSeleccionada}} type="text" id="otra_universidad" pattern="[A-Za-z0-9 ]{4,15}" title="Puede contener letras, números y espacios. Y entre 4 y 15 caracteres" >
    @if ($errors->has('otra_universidad'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('otra_universidad') }}</i></small>
        </span>
    @endif
</div>

<!-- UserID Field -->
<div class="form-group col-sm-6">
    <label for="user_id" > </label>
    <input class="form-control" name="user_id" type="hidden" value={{auth()->user()->id}} readonly="readonly" >
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <div class="centrar">
    <input class="btn btn-primary" type="submit" value="Guardar">
    <a href={{ route('estudiosFormales.index') }} class="btn btn-default">Cancelar</a>
    </div>
</div>

</div>
</div>
<div class="form-group col-sm-2"></div>
</div>




<!-- Script BuscarUniversidad -->
<script>
    $(document).ready(function(){
        function cargarUniversidad() {
            var departamento_id = $('#departamento_id').val();
            if ($.trim(departamento_id) != '') {
				console.log("llamare a finduniversidad con ",departamento_id);
                $.get('{!!URL::to('buscarUniversidad')!!}', {id_departamento: departamento_id}, function (data) {
					console.log("universidades = ",data);
                    var old = $('#universidad_id').data('old') != '' ? $('#universidad_id').data('old') : '';

                    $('#universidad_id').empty();
                    $('#universidad_id').append("<option value='' selected> Selecciona una </option>");

					
                    $.each(data, function (index, value) {

                        $('#universidad_id').append("<option value='" + value.id_universidad + "'" + (old == value.id_universidad ? 'selected' : '') + ">" + value.nombre_universidad+"</option>");
                    })
                });
            }
        }
        cargarUniversidad();
        $('#departamento_id').on('change', cargarUniversidad);
        $('#departamento_id').on('change', cargarUniversidad);
        $(document).on('change', '#estadoEstudio_id', function(){
            const value = $('#estadoEstudio_id').val();
            if(value == 2 || value == 3){
                $('#fechaFin_estudioFormal').parent().hide();
            } else {
                $('#fechaFin_estudioFormal').parent().show();
            }
        });
    });

</script>


