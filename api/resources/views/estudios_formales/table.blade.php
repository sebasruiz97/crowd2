
<div class="container.fluid py-0 px-1">

    <div class="row" >
        <div class="col-sm col-1 col-lg-1">
        </div>
        <div class="col-sm col-12 col-lg-12">
            <div class="table-responsive">
                <table class="table" id="estudioFormal-table">
                    <thead>
                        <tr>
                            <th>Nivel de Educación</th>
                            <th>Área de Estudio</th>
                            <th>Estado del Estudio</th>
                            <th>Fecha de Inicio</th>
                            <th>Fecha Fin</th>
                            <th>Universidad o institución</th>
                            <th colspan="3">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($estudiosFormales as $estudioFormal)
                        <tr>
                            <td>{{ $estudioFormal->nivelEducacion->nombre_nivelEducacion }}</td>
                            <td>{{ $estudioFormal->areaEstudio->nombre_areaEstudio }}</td>
                            <td>{{ $estudioFormal->estadoEstudio->nombre_estadoEstudio }}</td>
                            <td>{{ $estudioFormal->fechaInicio_estudioFormal }}</td>
                            <td>{{ $estudioFormal->fechaFin_estudioFormal }}</td>
                            <td>
                                @if($estudioFormal->universidad_id == 361)
                                    {{ $estudioFormal->otra_universidad }}</td>
                                @else
                                    {{ $estudioFormal->universidad->nombre_universidad }}</td>
                                @endif
                            <td>

                                <form action="{{ route('estudiosFormales.destroy',$estudioFormal->id_estudioFormal) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                
                                    <div class='pull-center'>
                                        <a  href="{{ route('estudiosFormales.edit',$estudioFormal->id_estudioFormal) }}" class="btn btn-default btn-xs"><i class="lnr lnr-pencil"></i></a>
                                        <button type="submit" class="btn btn-danger btn-xs" onclick = "return confirm('¿Estás seguro?')"><i class="lnr lnr-cross"></i></button>
                                    </div>
                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pull-right">
                    <small>{{ $estudiosFormales->links() }}</small>
                </div>
            </div>
        <div class="col-sm col-1 col-lg-1">
        </div>
        </div>
    </div>
</div>





