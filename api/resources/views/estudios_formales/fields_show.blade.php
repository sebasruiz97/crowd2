<!-- Nivel de Educación -->
<div class="form-group">
    <label for="nivelEducacion_id">Nivel de Educación:</label>
    <p>{{ $estudioFormal->nivelEducacion->nombre_nivelEducacion }}</p>
</div>

<!-- Area de Estudios -->
<div class="form-group">
    <label for="areaEstudio_id">Área de Estudios:</label>
    <p>{{ $estudioFormal->areaEstudio->nombre_areaEstudio }}</p>
</div>

<!-- Estado de Estudio -->
<div class="form-group">
    <label for="estadoEstudio_id">Estado del Estudio:</label>
    <p>{{ $estudioFormal->estadoEstudio->nombre_estadoEstudio }}</p>
</div>

<!-- Fecha de Inicio -->
<div class="form-group">
    <label for="fechaInicio_estudioFormal">Fecha de Inicio:</label>
    <p>{{ $estudioFormal->fechaInicio_estudioFormal }}</p>
</div>

<!-- Fecha Fin -->
<div class="form-group">
    <label for="fechaFin_estudioFormal">Fecha Fin:</label>
    <p>{{ $estudioFormal->fechaFin_estudioFormal }}</p>
</div>

<!-- Universidad-->
<div class="form-group">
    <label for="universidad">Universidad o instituto:</label>
    <p>{{ $estudioFormal->universidad }}</p>
</div>