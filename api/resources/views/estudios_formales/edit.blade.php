@extends('layouts.app2')

@section('content')

<div> 
    <h3 class="centrar"> <strong>  Editar mi estudio formal </strong></h3>
</div> <br>

                @foreach($estudiosFormales as $estudioFormal)
                        <form action="{{ route('estudiosFormales.update', $estudioFormal->id_estudioFormal) }}" method="POST" id="actualizar_estudioFormal">

                            @method('PUT')
                            @include('estudios_formales.fields_edit')
                    
                        </form>
                @endforeach
@endsection

