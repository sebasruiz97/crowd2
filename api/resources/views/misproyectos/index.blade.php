@extends('layouts.appMisproyectos')

@section('content')

        <h3 class="text-center"  style="font-size: 18px; font-weight: bold; color: #304457; text-transform: uppercase ">  Mis Proyectos </h3>
        
        @include('misproyectos.list')
@endsection
