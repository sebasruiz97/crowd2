<div class="modal-content" data-backdrop="static">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 style="color: #304457; text-align:center"> <strong> TÉRMINOS Y CONDICIONES CROWDSQA </strong></h4>            </div>
  <div class="modal-body" data-backdrop="static">
      <div class="row">
          <div class="col-3">
          </div>
          <div class="col-1">
          </div>

          <div class="col-8">
            <div class="form-group" style="color:#304457;padding:50px;margin-right:5px;margin-top:0px">
            <div class="row">
            El presente documento regula el acceso, condiciones y uso que Usted realice del sitio web https://crowdsqa.co (en adelante “EL APLICATIVO”), de propiedad de SOFTWARE QUALITY ASSURANCE S.A.- SQA S.A. (En adelante “CROWDSQA” o “EL OPERADOR”), sociedad anónima constituida conforme a las leyes colombianas e identificada con NIT 811042907 – 7.
            Por favor lea detenidamente estas condiciones antes de acceder y/o hacer uso del APLICATIVO, en el evento de estar de acuerdo con lo aquí establecido y con el correspondiente uso de sus datos personales, solicitamos manifieste expresamente su aceptación por medio del “Checkbox” destinado para esto. El uso del APLICATIVO supone la aceptación y vinculación al contenido aquí incluido y una autorización de uso de sus datos personales, para las finalidades que se describen más adelante.
            <br>
            </div>
              <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">1. DEFINICIONES</h6>
              </div>
              <br>
              <div class="row">
                <ul>
                  <li>
                    <p>
                    APLICATIVO: Sitio web de propiedad de SQA, de acceso gratuito, que permite la recepción y el envío de información. EL INTERESADO podrá utilizar el APLICATIVO sin que esto implique para ningún efecto el otorgamiento de una licencia de la tecnología o licencia permanente de uso del aplicativo y de su contenido de ningún tipo. El uso del aplicativo podrá en cualquier momento ser limitado o restringido a cualquiera de los INTERESADOS por determinación del OPERADOR.
                    </p>
                  </li>
                  <li>
                    <p>
                    OPERADOR DEL APLICATIVO: Encargado de administrar operativa y funcionalmente el APLICATIVO; para los efectos de los presentes términos será SQA, o la persona natural o jurídica que ésta designe.
                    </p>
                  </li>
                  <li>
                    <p>
                    INTERESADO: Persona natural, mayor de edad, residente fiscal Colombiano, que, previa aceptación de los presentes términos y condiciones, voluntariamente se vincula al APLICATIVO para recibir información, herramientas o contenido por parte del OPERADOR que faciliten o permita encontrar BUGS presentados en soluciones y sistemas tecnológicos, con la finalidad de que el interesado determine su interés en reportar el error del sistema.
                    </p>
                  </li>
                  <li>
                  	<p>
                  	BUG O ERROR: Se refiere a un problema considerado un “error” en un programa de computador, solución tecnológica o sistema de software que desencadena un resultado indeseado en el desarrollo normal del sistema y que requiere una corrección para su óptimo funcionamiento.
                  	</p>
                  </li>
                  <li>
                  	<p>
                  	RETO: Se refiere a cualquier problema, información o contenido que pone el OPERADOR en favor de los INTERESADOS, con el objetivo de que suministren, como parte del servicio aceptado en estos términos y condiciones, información y reportes respecto a la existencia y contenido de BUGS, ERRORES o incidentes presentados, en los términos contenidos en la descripción e información suministrada con cada RETO.
                  	</p>
                  </li>
                  <li>
                  	<p>
                  	INTERACCIÓN EN EL APLICATIVO: Facultad de acceso por parte de cada INTERESADO para recibir la información enviada por el OPERADOR, con el fin de que el INTERESADO detecte y reporte BUGS, los cuales deberán en todo caso ser aceptados como tales por el OPERADOR. Una vez los reportes en cuestión sean aprobados, el INTERESADO recibirá el pago por los honorarios de sus servicios.
                  	</p>
                  </li>
                  <li>
                  	<p>
                  	REPORTE DEL BUG: Información enviada por el INTERESADO al OPERADOR por medio del APLICATIVO donde se indica la existencia y prueba del BUG, junto con toda la información anexa o complementaria requerida.
                  	</p>
                  </li>
                  <li>
                  	<p>
                  	MENSAJES DE DATOS: Información generada, enviada, recibida, almacenada o comunicada por medios electrónicos, ópticos o similares, tales como: (i) Intercambio Electrónico de Datos (EDI); (ii) Internet; (iii) correo electrónico, entre otros. (Ley 527 de 1999 art. 2 lit. a).
                  	</p>
                  </li>
                  <li>
                  	<p>
                  	COOKIES: Ficheros enviados por el APLICATIVO que se descargan en su computador o dispositivo móvil, y permiten almacenar y recuperar información sobre los hábitos de navegación de un INTERESADO o un equipo.
                  	</p>
                  </li>
                  <li>
                  	<p>
                    DATOS PERSONALES: Información vinculada o que pueda vincularse a una o varias personas naturales determinadas o determinables que puede ser recopilada, almacenada, compartida, utilizada y tratada por el OPERADOR de conformidad con lo dispuesto en la Política de Protección de Datos de CROWDSQA, disponible en la página web https://crowdsqa.co
                  	</p>
                  </li>
                  <li>
                    <p>
                    TIEMPO DE DISPONIBILIDAD: Tiempo durante el cual un RETO estará disponible para los INTERESADOS en el APLICATIVO para registrar el REPORTE DEL BUG. Este tiempo será el indicado en las condiciones de cada reto.
                    </p>
                  </li>
                </ul>
              </div>
              <div class="row">
                <h6 style="font-weight:bold; font-size:14px">2. OBJETO</h6>
              </div>
              <div class="row">
                <p>
                Estos Términos y Condiciones regulan el acceso que concede el OPERADOR a cada INTERESADO, para que, a través del APLICATIVO, reciba por medio de mensajes de datos información respecto a RETOS o potenciales errores o bugs en sistemas tecnológicos, plataformas, soluciones de software, software y/o códigos, y la devuelva al OPERADOR indicando el REPORTE DEL BUG respectivo; a cambio de una remuneración por cada BUG reportado correctamente y aprobado en los términos previstos.
                <br><br>
                La remuneración por el servicio prestado por el INTERESADO será variable para cada REPORTE DE BUG aprobado, dependiendo del grado de complejidad del mismo y las condiciones del servicio o RETO, la suma a remunerar será indicada en el APLICATIVO junto con la información de cada potencial BUG, con la finalidad de que el INTERESADO conozca previamente las condiciones técnicas y económicas requeridas para sus servicios y determine si desea aceptarlos.
                <br><br>
                Por medio de la aceptación de estos términos y condiciones se establece una relación comercial entre el OPERADOR y el INTERESADO, quien debe encontrarse ubicado en territorio colombiano, o quien, estando en territorio extranjero tiene la condición de residente fiscal colombiano (en los términos del artículo 10 del Estatuto Tributario colombiano). El INTERESADO accede al APLICATIVO y al finalizar y aprobarse el reporte de cada BUG que éste reporte apropiadamente e ingresar el REPORTE en el APLICATIVO, así como los ajustes solicitados por el líder del reto, recibe la contraprestación descrita en una cuenta bancaria colombiana.
                </p>
              </div>
            
              <div class="row">
                <h6 style="font-weight:bold; font-size:14px">3. MODIFICACIÓN</h6>
              </div>
              <div class="row">
                <p>
                El OPERADOR podrá modificar autónomamente, de manera unilateral y en cualquier momento estos Términos y Condiciones de uso del APLICATIVO; EL OPERADOR comunicará las modificaciones en los Términos y Condiciones a los INTERESADO antes o a más tardar al momento de implementar las modificaciones a través de cualquier medio que considere idóneo, tales como correo electrónico, envío físico, notificación o pop-up en el APLICATIVO, etc. EL INTERESADO Podrá determinar si acepta o no las modificaciones realizadas y en caso de aceptación deberá expresarlo mediante la selección de dicha opción en el Checkbox del APLICATIVO.
                <br><br>
                En el evento de no estar de acuerdo con las modificaciones, EL INTERESADO no podrá continuar con el uso del aplicativo y se desactivará la información a la que este tenga acceso.
                <br><br>
                La última versión publicada será la que regulará las relaciones comerciales que se generen al momento de realizarse cada transacción.
                </p>
              </div>
            
              <div class="row">
                <h6 style="font-weight:bold; font-size:14px">4. CONDICIONES DE LOS RETOS</h6>
              </div>
              <div class="row">
                <p>
                EL INTERESADO accederá al APLICATIVO, donde encontrará el listado de RETOS disponibles para descifrar o a través de comunicados enviados por EL OPERADOR, cada RETO indicará la siguiente información mínima:
                  <br>
                  <ul>
                  	<li>Descripción del Proyecto en específico.</li>
					<li>Capacidades o conocimientos requeridos del INTERESADO (si aplica)</li>
					<li>Condiciones de aceptación del reporte o información que deberá proporcionar el interesado para aprobación por parte del OPERADOR.</li>
					<li>Tiempo Máximo de entrega de la solución del BUG</li>
					<li>Valor o remuneración por la totalidad del servicio prestado </li>
					<li>Tiempos máximos de respuesta por parte del OPERADOR.</li>
					<li>Valor o remuneración por la totalidad del servicio prestado</li>
                  </ul>
                  <br><br>
                  EL INTERESADO seleccionara en EL APLICATIVO el RETO que esté interesado en resolver. En el momento en que se realice la selección, EL INTERESADO accederá a la información o contenido a revisar, junto con toda la información pertinente para el servicio.
                  <br><br>
                  La información respecto al PROYECTO seleccionado estará disponible para el interesado y habilitada durante el TIEMPO DE DISPONIBILIDAD del BUG., Al momento en que se cumpla la fecha indicada en las condiciones para la entrega del reporte del BUG  sin que se haya registrado en el APLICATIVO la correspondiente respuesta, reporte o identificación, se desactivará para el INTERESADO  la información que aparece en el sistema respecto a dicho BUG.
                  <br><br>
                  EL INTERESADO podrá por medio de EL APLICATIVO indicar si requiere información adicional a la proporcionada, evento en el cual se podrá, a criterio exclusivo del OPERADOR, proporcionar para éste dicha información dentro de los (dos) 2 días siguientes a su solicitud.
                  <br><br>
                  La información respecto al RETO seleccionado estará disponible para el interesado y habilitada durante el TIEMPO DE DISPONIBILIDAD del RETO, al momento en que se cumpla la fecha indicada en las condiciones para el reporte del BUG sin que se haya registrado en el APLICATIVO el correspondiente reporte, corrección o ajuste al mismo, se desactivará para el INTERESADO la información que aparece en el sistema respecto a dicho RETO.
                  <br><br>
                  Así mismo, un mismo RETO o BUG podrá ser investigado o revisado por múltiples INTERESADOS. Por lo tanto, de igual modo, tan pronto sea aprobado por EL OPERADOR un reporte específico de uno de los INTERESADOS, cesará la posibilidad para los demás INTERESADOS de recibir remuneración alguna, teniendo en cuenta que se priorizará el primer REPORTE enviado. Los INTERESADOS aceptan que en aquellos casos en los que el REPORTE comunicado no sea aceptable en criterio del OPERADOR, u otro INTERESADO transfiera un reporte aceptable antes en el tiempo, no habrá lugar a remuneración, indemnización, compensación y sin que exista algún perjuicio a favor de los INTERESADOS que no lograron registrar el reporte satisfactorio en el APLICATIVO.
                </p>
              </div>
              <div class="row">
                <h6 style="font-weight:bold; font-size:14px">5. DETALLES DE LA INTERACCIÓN EN EL APLICATIVO</h6>
              </div>
              <div class="row">
                <p>
                Toda persona natural y mayor de edad que haga uso del APLICATIVO, previa aceptación de estos Términos y Condiciones y una vez se autentique como INTERESADO, recibirá por parte del OPERADOR, acceso al listado de RETOS que el OPERADOR requiere descifrar; el INTERESADO podrá conocer los detalles de cada RETO, el tiempo máximo en que debe tenerse registrar los reportes de BUGs, el valor del servicio como condición económica y las características técnicas del servicio. EL INTERESADO seleccionará el RETO que esté interesado en desarrollar. 
								<br><br>
                Una vez EL INTERESADO haya resuelto el RETO respectivo, deberá ingresar el REPORTE del BUG al mismo en el APLICATIVO, donde el OPERADOR validará el registro de dicha respuesta y lo marcará como aceptado o rechazado, según corresponda, indicando en el aplicativo las razones correspondientes cuando el registro del REPORTE sea incorrecto o no logre el objetivo deseado.
                <br><br>
                En el evento en que el INTERESADO no registre en el APLICATIVO los reportes de BUGS asociados al RETO durante el TIEMPO DE DISPONIBILIDAD del mismo, la información relacionada con el RETO podrá desaparecer del APLICATIVO, por lo que el INTERESADO no podrá continuar trabajando sobre el mismo RETO y no recibirá remuneración alguna por los servicios prestados y por el tiempo en que estuvo dedicado participando del RETO.
                <br><br>
                Si durante el TIEMPO DE DISPONIBILIDAD del RETO, el INTERESADO no realiza los ajustes solicitados a su reporte del BUG, no recibirá remuneración alguna por los servicios prestados y el tiempo en que estuvo dedicado participando del RETO.
                <br><br>
				        Así mismo, entendiendo que los INTERESADOS tienen acceso al listado de RETOS y pueden determinar libremente cuales desean descifrar; varios INTERESADOS pueden desarrollar de manera simultánea un RETO respectivo, sin que exista exclusividad o reserva de PROYECTOS RETOS para algún INTERESADO. No obstante, la remuneración únicamente se pagará a quien ingrese primero el REPORTE satisfactorio al aplicativo de manera correcta.

                </p>
              </div>
              <div class="row">
                <h6 style="font-weight:bold; font-size:14px">6. FORMA Y CONDICIONES DE PAGO DEL SERVICIO</h6>
              </div>
              <div class="row">
                <p>
                Los INTERESADOS declaran y aceptan que no hay lugar a reclamación, indemnización, declaración de incumplimiento o penalidad en los eventos en que no exista remuneración por el tiempo dedicado a la participación de cada RETO o en la detección de incidentes o BUGS. La Remuneración se entrega únicamente al primer INTERESADO en ingresar el REPORTE al APLICATIVO de manera correcta, los demás servicios prestados no serán remunerados.
                <br><br>
                Si en el lapso de un (1) día el INTERESADO no ha realizado los ajustes solicitados al bug reportado, y si este bug fue reportado también por otro INTERESADO, el OPERADOR podrá asignarlo al segundo INTERESADO que lo reportó, sin que el primero reciba remuneración alguna.
                <br><br>
                El OPERADOR mantendrá el récord de los registros aprobados que realice el INTERESADO para la solución de cada RETO y una vez al mes, dentro de los primero quince (15) días de cada mes, realizará el pago consolidado de los registros exitosos de BUGS del mes anterior, previa deducción de los valores a que haya lugar por concepto de retención en la fuente y otras deducciones que establezca la ley colombiana para la prestación de servicios, según aplique. El OPERADOR realizará los pagos a través de transferencia electrónica a la cuenta bancaria que el INTERESADO haya reportado al momento de su inscripción en el APLICATIVO.
                <br><br>
                Como requisito para el pago por los servicios prestados el INTERESADO deberá ingresar al aplicativo el comprobante del pago de la seguridad social que corresponda a los ingresos recibidos por el servicio contratado por el OPERADOR, mes vencido. Mientras este requisito no se haya cumplido el OPERADOR tendrá la facultad de retener el pago al INTERESADO hasta tanto se haya verificado el cumplimiento de dicha condición, lo que este último declara aceptar.
                <br><br>
                Al aceptar estos Términos y Condiciones y acceder al APLICATIVO, el INTERESADO declara que cuenta con plena capacidad legal para ser sujeto de derechos y obligaciones, de conformidad con lo establecido en el Código Civil Colombiano y así mismo declara que su residencia fiscal es Colombia.
                <br><br>
                Para ser INTERESADO y obtener un usuario y contraseña válidos para acceder al APLICATIVO cada persona debe acreditar que es mayor de edad y hacer entrega al OPERADOR de los siguientes documentos: (i) Cédula de Ciudadanía o identificación; (ii) RUT; (iii) Certificación de Cuenta Bancaria Colombiana Activa.
                </p>
              </div>
             
              <div class="row">
                <h6 style="font-weight:bold; font-size:14px">7. TRATAMIENTO DE DATOS PERSONALES</h6>
              </div>
             <div class="row">
               <p>
               El registro en el APLICATIVO exige que el INTERESADO comparta determinados datos personales con el OPERADOR (SQA S.A.), sociedad domiciliada en Medellín y ubicada en la dirección carrera 30#7AA-207 Torre Scaglia, El poblado, teléfono (57) 3153989513, e-mail: info@crowdsqa.co; por lo que, al aceptar estos Términos y Condiciones, autoriza expresamente al OPERADOR para tratar, recolectar, almacenar, transmitir y compartir sus datos personales de conformidad con lo dispuesto en la Política de Tratamiento de Datos Personales de SQA, disponible en la página web: https://crowdsqa.co y se compromete a mantener la información completa y actualizada, de lo contrario su acceso al APLICATIVO no será autorizado.
               <br><br>
               En cumplimiento de la Ley 1581 de 2012, sus Decretos Reglamentarios y nuestra Política de Protección de Datos Personales, se le informa que los datos personales que usted suministre, serán tratados mediante el uso y mantenimiento de medidas de seguridad técnicas, físicas y administrativas a fin de impedir que terceros no autorizados accedan a los mismos.
               <br><br>
               El OPERADOR es responsable del tratamiento de los datos personales recolectados a través del APLICATIVO, responsabilidad que podrá delegar en un tercero, en calidad de responsable o encargado de la información, asegurando contractualmente el adecuado tratamiento de los mismos y garantizando las medidas de seguridad necesarias para la protección de los datos personales.
               <br><br>
               Los datos personales recolectados por el OPERADOR podrán ser objeto de análisis para fines de efectuar mejoras en el APLICATIVO, así mismo dichos datos se tratan con la finalidad de ser usados para actividades de operación y registro del aplicativo, atender o formalizar cualquier tipo de trámite, registros contables o financieros, realizar encuestas y comunicados relacionados con el servicio prestado, cumplir con obligaciones contractuales o legales adquiridas y desarrollar el objeto social principal y los complementarios del OPERADOR.
               <br><br>
               Los datos personales que se recopilan a través de la presente autorización son aquellos aportados voluntariamente por el INTERESADO, bien sea mediante el APLICATIVO, mediante el diligenciamiento de formularios o aquellos que voluntariamente aporte el INTERESADO para la ejecución de cualquier relación comercial o negocial que establezca (o evaluación de ofertas y propuestas comerciales) con el OPERADOR. Los cuales comprenden datos de identificación, direcciones comerciales o personales, teléfono comercial o personal, estudio y/o conocimientos intelectuales, actividad comercial y/o laboral, pagos e información de seguridad social, retenciones en la fuente, número y datos de cuenta bancaria, además de información sobre transacciones y toda aquella información que sea necesaria para el cumplimiento de cualquier obligación adquirida por cualquiera de las partes, de la cual el INTERESADO sea titular o cuente con la respectiva autorización expresa para compartirla en los términos definidos en el presente documento. A éstos se les dará el tratamiento establecido de conformidad con la normatividad vigente y la política de tratamiento de datos del OPERADOR.
               <br><br>
               Adicionalmente, los datos se recolectan con la finalidad de verificar la identidad y mayoría de edad de la persona, su carácter de residente fiscal colombiano, la verificación del pago de aportes y afiliación a seguridad social, conocer su cuenta bancaria para la realización de los pagos, información tributaria para realizar retenciones en la fuente y reportes de información a las autoridades y entidades que legalmente lo exijan; así mismo se tiene acceso a estos datos para reportar ante las diferentes autoridades los pagos realizados, para fines de auditoria y conservación de la información contable y fiscal del OPERADOR y para enviar informes y reportes al INTERESADO sobre las actividades realizadas dentro del aplicativo.
               <br><br>
               Cada INTERESADO podrá ejercer sus derechos legales de conocer, actualizar, modificar y suprimir los datos personales existentes en las bases de datos asociadas al APLICATIVO. Para ello deberá realizar la solicitud de consulta, reclamo o supresión en los mecanismos de enlace o contacto establecidos en la Política de Tratamiento de Datos de CROWDSQA.
               <br><br>
               Cada INTERESADO es responsable de toda la actividad que ocurre en la Cuenta que se habilite para él en el APLICATIVO y se compromete a mantener en todo momento de forma segura y secreta el nombre de usuario, la contraseña de acceso así como toda la información a la que tenga acceso por medio del APLICATIVO.
               </p>
             </div>
             
   
              <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">8. FUNCIONAMIENTO DEL APLICATIVO</h6>
              </div>
              <div class="row">
                  <p>
                  El OPERADOR administra directamente o por medio de terceras personas el APLICATIVO, y en ningún caso responderá por daños directos o indirectos que sufra el INTERESADO por la utilización o incapacidad de utilización del mismo.
                  <br><br>
                  El APLICATIVO está concebido para que los INTERESADO puedan tener acceso las 24 horas del día. El OPERADOR realiza los mejores esfuerzos para mantener en operación el APLICATIVO, pero en ningún caso garantiza disponibilidad y continuidad permanente del mismo, así como tampoco brinda garantías de proporcionar cierto número de BUGS o servicios para ser prestados o realizados por el INTERESADO.
                  <br><br>
                  El OPERADOR se reserva el derecho de prohibir el acceso al APLICATIVO al INTERESADO que realice conductas que vayan en contra de estos Términos y Condiciones.
                  <br><br>
                  </p>
              </div>
              <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">9. EXCLUSIÓN DE RESPONSABILIDAD Y GARANTÍA</h6>
              </div>
              <div class="row"
              	<p>
                EL INTERESADO empleará el sitio bajo su entera responsabilidad y acepta que el OPERADOR se exima de las siguientes responsabilidades y garantías, dándoles el mayor alcance que la ley le permita:
                <br><br>
                EL OPERADOR no garantiza la disponibilidad y continuidad del funcionamiento del APLICATIVO y de los Servicios. Cuando ello sea razonablemente posible, EL OPERADOR advertirá previamente las interrupciones en el funcionamiento del APLICATIVO.
                <br><br>
                EL OPERADOR se exime de cualquier responsabilidad por los daños y perjuicios de toda naturaleza que puedan deberse a la falta de disponibilidad o de continuidad del funcionamiento del APLICATIVO, a la defraudación de la utilidad que los INTERESADO hubieren podido atribuir al sitio y a los servicios, y en particular, aunque no de modo exclusivo, a las fallas que EL APLICATIVO pueda presentar.
                <br><br>
                En cualquier caso, se deja constancia expresa que cualquier obligación asumida por EL OPERADOR será una obligación de medio de acuerdo con la Ley colombiana aplicable al respecto y no da garantías expresas respecto de resultados esperados por los INTERESADO.
                <br><br>
                En ningún caso, EL OPERADOR podrá considerarse responsables de los daños directos o indirectos y, en especial perjuicio material, pérdida de datos o del programa, perjuicio financiero, que resulte del acceso o de la utilización de este APLICATIVO.
                <br><br>
                EL OPERADOR no asume la responsabilidad por daños a, o virus que puedan infectar su computador u otras propiedades dado su acceso a usar, o navegar el Sitio o su descarga de cualquier material.
              	</p>
              </div>
              <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">10. DEBERES DEL INTERESADO</h6>
              </div>
              <div class="row">
              	<p>
              		Son deberes del INTERESADO: 
              		<br><br>
                  (i)Suministrar información veraz, completa y fidedigna al momento de vincularse al APLICATIVO y mantener dicha información actualizada; (ii) Abstenerse de transferir o compartir con terceros los datos de ingreso al APLICATIVO (nombre de usuario y contraseña); (iii) Abstenerse de hacer un uso indebido del APLICATIVO y/o realizar actos en contra del OPERADOR o de terceros; (iv) Informar inmediatamente al OPERADOR en caso de olvido o pérdida de los datos de validación; (vi) Abstenerse de realizar conductas que atenten contra el correcto funcionamiento del APLICATIVO; (vii) Abstenerse de descifrar, descargar, descompilar o desensamblar cualquier elemento del APLICATIVO diferente a los expresamente autorizados; (viii) Abstenerse de divulgar o compartir con terceras personas los datos e información obtenida durante el uso del APLICATIVO, teniendo en cuenta que se trata de información CONFIDENCIAL de uso exclusivo del OPERADOR. (iv) Realizar oportunamente la afiliación y pago al sistema de seguridad social con la finalidad de recibir la remuneración pactada. (vi) tener la condición de residente fiscal colombiano. (vii) Abstenerse de utilizar, en cualquier manera, en beneficio propio o de terceros no autorizados la información o contenidos a los que tenga acceso en uso del APLICATIVO Y/O DEL RETO.
              	</p>
              </div>
              <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">11. DEBERES DEL OPERADOR</h6>
              </div>
              <div class="row">
              	<p>
              		Son deberes de OPERADOR: 
                  <br><br>
              		(i) Suministrar información cierta, fidedigna, suficiente, clara y actualizada respecto del APLICATIVO, sus condiciones de uso y su funcionamiento; (ii) Poner a disposición de los INTERESADO los Términos y Condiciones de uso del APLICATIVO y notificarlos de las actualizaciones que realice de los mismos; (iii) Informar a los INTERESADO cuando se realicen registros de BUGS incorrectos; (iv) Mantener el récord mensual de los registros de BUGS correctos que realice cada INTERESADO; (v) Enviar al correo electrónico suministrado por el INTERESADO el récord mensual de los registros correctos realizados; (vi) Efectuar, en la fecha establecida en estos Términos y Condiciones, los pagos correspondientes a cada INTERESADO, previo cumplimiento de los requisitos aquí indicados para el pago; (vii) Utilizar los datos personales del INTERESADO únicamente para los fines establecidos en los presentes Términos y Condiciones y en la Política de Protección de Datos Personales de CROWSQA.
              	</p>
              </div>
              
              <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">12. CONFIDENCIALIDAD</h6>
              </div>           
              <div class="row">
              	<p>
                EL INTERESADO acepta expresamente mantener bajo reserva, sin revelación a terceras personas y de manera estrictamente confidencial, evitando por ende en que se haga de dominio público o se revele a terceros, toda la información de propiedad de EL OPERADOR o de terceras personas a las que pueda tener acceso el INTERESADO o se le comunique a través del APLICATIVO. Así las cosas, EL OPERADOR sólo podrá acceder y hacer uso de la información a la que acceda, para la ejecución del objeto descrito en el numeral segundo del presente documento, debiendo proteger dicha información confidencial, con el fin de que no sea revelada y conocida por terceros, custodiándola con la diligencia y cuidado apropiados a las circunstancias; no obstante lo cual, podrá revelarla a las autoridades competentes para los fines y en los casos previstos expresamente en la ley, cuando ello sea necesario en cumplimiento de obligaciones y deberes legales y siempre y cuando medie orden judicial o de autoridad competente, pero en tal caso, de manera previa a la revelación, le comunicará de manera previa al OPERADOR tal hecho, indicando qué información será revelada, la autoridad a la cual será revelada la información confidencial, así como los motivos que justifican su proceder.
                <br><br>
                Toda la información que el INTERESADO reciba o le sea revelada por medio del APLICATIVO o por parte del OPERADOR en cualquier medio y de cualquier tipo o clase será considerada confidencial, será sometida a reserva y tendrá la connotación de secreto comercial. EL INTERESADO no podrá compartir, revelar, copiar, descargar, conservar, guardar o utilizar la información para fines diferentes a los establecidos en el objeto de los presentes Términos y Condiciones.
                <br><br>
                Una vez sea desactivado para el INTERESADO cada BUG este deberá destruir y eliminar inmediatamente de sus servidores, equipos o herramientas o de sus archivos cualquier información que le haya sido proporcionada para el desarrollo del BUG respectivo.
                <br><br>
                <strong>“Información Confidencial”</strong> Para efectos del presente documento y relación el término Información Confidencial tendrá el siguiente alcance:
                <br><br>
                -Para el objeto del presente contrato toda la información revelada por EL OPERADOR a un INTERESADO será de carácter <strong>confidencial o privilegiado</strong>, así como sus copias, incluyendo todo tipo de programas, aplicativos, configuraciones, software, códigos, códigos fuentes, interfaces, soluciones, errores, bugs, notas, datos, análisis, gráficos, diseños, desarrollos, conceptos, resúmenes, estudios o registros, entre otros, proporcionados por el OPERADOR, y que haya sido proporcionada en forma escrita u oral, por medios electrónicos o digitales, o bien, en virtud de la revisión de libros, expedientes y documentos.
                <br><br>
                -Que no haya sido dada a conocer al público por cualquier medio, previamente a la entrega.
                <br><br>
                La obligación de confidencialidad a cargo del INTERESADO, estará vigente desde su registro en el APLICATIVO, mientras exista cualquier tipo de relación contractual o negocial entre las partes y por un plazo de (5) años desde el último pago en remuneración de sus servicios recibido por EL INTERESADO o desde la última habilitación de un BUG realizado mediante el APLICATIVO, lo último que ocurra. Pese a la vigencia pactada, el plazo de confidencialidad no aplicará respecto de aquella información que sea considerada secreto empresarial o comercial en los términos de la legislación vigente, frente a la cual la obligación de confidencialidad se mantendrá de manera indefinida.
                <br><br>
                A la terminación o extinción por cualquier causa de las tareas detalladas, nacerá a cargo del INTERESADO la obligación de devolver al INTERESADO la integridad del material tangible o digital que contenga la información confidencial puesta en sus manos y/o destruir dicha información o sus reproducciones, según requerimiento de EL OPERADOR. Es prohibido en todo momento al INTERESADO generar copia o reproducción no autorizada de la información o elementos a los que tenga acceso.
                <br><br>
                El INTERESADO reconoce y acepta que la utilización o revelación NO autorizada por el OPERADOR de la información confidencial causa un grave perjuicio al OPERADOR por lo que responderá por las obligaciones de custodia, seguridad y confidencialidad de la información hasta por culpa levísima.
                <br><br>
                La información y contenido del APLICATIVO es proporcionado “AS IS”, y por lo tanto, no existen garantías de ninguna clase respecto de la idoneidad de la información para un propósito particular; que el APLICATIVO o la información cumplirá con ciertos estándares o requerimientos o que siempre estará disponible, de manera ininterrumpida. Ningún consejo o información, bien sea obtenido de forma oral o escrita, que obtenga el INTERESADO creará de manera alguna garantía no expresada en estos términos y condiciones. El uso que el INTERESADO haga uso del APLICATIVO y su contenido será bajo su propio riesgo y bajo ninguna circunstancia recibirá compensación o indemnización alguna respecto de dicho uso.
              	</p>
              </div>
              <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">13. PROPIEDAD INTELECTUAL</h6>
              </div>
              <div class="row">
              	<p>
                EL INTERESADO reconoce y manifiesta expresamente que EL OPERADOR será propietario de los Derechos Patrimoniales de Propiedad Intelectual que pudieren existir sobre las creaciones que produzca EL INTERESADO en cumplimiento de las obligaciones derivadas de la ejecución del objeto descrito en el presente documento, como quiera que sobre estas obras y creaciones se ha pactado una remuneración y se consideran una obra por encargo. Así mismo, EL OPERADOR podrá disponer de dichos derechos de cualquier manera que estime, podrá cederlos, licenciarlos, a título gratuito u oneroso y explotarlos con fines económicos. Así mismo los derechos que EL OPERADOR adquiere por virtud de este contrato se extenderán de manera perpetua e ilimitada, siendo su única restricción el respetar los derechos morales que pueda tener EL OPERADOR u otro autor sobre las obras.
                <br><br>
                EL INTERESADO se obliga por este medio a suscribir todos los documentos que requiera el OPERADOR para formalizar cualquier transferencia de derechos de autor relacionadas con creaciones realizadas por EL INTERESADO respecto a los servicios prestados por este medio del APLICATIVO.
                <br><br>
                En caso de que las Partes infrinjan derechos de propiedad intelectual de terceros en la ejecución del presente contrato, responderán por los perjuicios que lleguen a causarle a la otra parte, en virtud de las reclamaciones que aquellos terceros puedan formular.
                <br><br>
                LAS PARTES declaran que cuenta con la autorización, licenciamiento y/o titularidad de todos los derechos en el uso de las herramientas tecnológicas, procesos, plataformas, diseños, infraestructuras, software, soluciones, códigos, programas y cualquier otro proceso que pueda ser propiedad de terceras personas y que sean necesarias para la prestación de los servicios.
                <br><br>
                El diseño del APLICATIVO y de todos sus elementos, que incluyen, de manera enunciativa, software o código HTML, scripts, texto, material gráfico, fotografías, imágenes, diseños, video, audio, escritos y otros elementos que aparezcan como parte de este portal web (en conjunto, "Contenido"), son de propiedad exclusiva de CROWDSQA , están protegidos por las leyes de derechos de autor en Colombia y otras leyes y tratados internacionales ratificados por Colombia. Todo el contenido es suministrado por CROWDSQA con una licencia limitada como un servicio para quien visita el APLICATIVO y solo se podrá usar para fines personales. Esta licencia limitada está sujeta a las siguientes restricciones: EL INTERESADO no podrá copiar, reproducir, publicar, transmitir, distribuir, ejecutar, exhibir, hacer público, modificar, crear trabajos derivados, vender, conceder una licencia o explotar de alguna otra manera este APLICATIVO, alguno de sus contenidos o software relacionado; CROWDSQA puede revocar esta licencia limitada en cualquier momento por cualquier motivo o sin motivo aparente. Se prohíbe el uso no autorizado del contenido, ya sea en forma de marcos, enlaces en línea o cualquier otra asociación con el contenido o la información que se origine en el Portal web de CROWDSQA.
                <br><br>
                El uso reiterado del APLICATIVO no le otorga al INTERESADO ningún derecho de propiedad u otros sobre el mismo, pues su uso se restringe estrictamente al autorizado de acuerdo con la licencia limitada de uso descrita anteriormente.
                <br><br>
                EL INTERESADO acepta, reconoce y declara que por medio del APLICATIVO y en virtud de los servicios que este prestará al OPERADOR NO se realiza bajo ningún efecto y en ningún evento una transferencia de propiedad intelectual al INTERESADO respecto a la información que este reciba del OPERADOR o de Terceros para la ejecución de los servicios contratados. EL INTERESADO se obliga a respetar la propiedad intelectual del OPERADOR y de terceros y a no utilizar dicha información en beneficio propio o de cualquier tercero y para fines distintos de los contenidos en el presente documento y la descripción de cada BUG.
                <br><br>
                Al usar el APLICATIVO, el INTERESADO acepta mantener indemne, de manera total, al OPERADOR y a sus clientes por cualquier reclamo hecho por una tercera parte por una supuesta infracción del INTERESADO de los derechos de propiedad intelectual de dichas terceras partes.
                <br><br>
                EL INTERESADO tampoco podrá compartir publicidad, información promocional o material comercial de ninguna clase a través del APLICATIVO, utilizar material prohibido o material sujeto a derechos de autor, sin el consentimiento escrito del legítimo titular de dicho trabajo.

              	</p>
              </div>
              <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">14. CONDICIONES DE SEGURIDAD DEL INTERESADO</h6>
              </div>
              <div class="row">
              	<p>
                EL INTERESADO, garantiza que para la prestación del servicio, contará como mínimo con los siguientes elementos de seguridad:
              		<ul>
              			<li>
              				Tener implementada una versión actualizada de antivirus.
              			</li>
              			<li>
                    Tener implementados controles de acceso que involucren usuario y clave, y en los casos que se requiera, restricciones para la escritura en dispositivos de almacenamiento extraíble (usb, celulares, discos duros externos, entre otros).
              			</li>
              		</ul>
              	</p>
              </div>
              <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">15. DOMICILIO Y LEGISLACIÓN APLICABLE</h6>
              </div>
              <div class="row">
              	<p>
                  Estos Términos y Condiciones y en general la relación que surge entre el OPERADOR y el INTERESADO tras su aceptación, se rigen por las normas colombianas. 
              		<br><br>
              		Los INTERESADOS aceptan que no existe ninguna relación de colaboración empresarial, asociación, vínculo laboral o de mediación con el OPERADOR como resultado del uso de su portal y de los demás servicios asociados.
              	</p>
              </div>
			  <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">16. ATAQUES INFORMÁTICOS</h6>
              </div>
              <div class="row">
              	<p>
                  EL interesado no debe realizar un uso indebido de este APLICATIVO ni de los aplicativos objeto de prueba del cliente mediante la introducción intencionada de virus, troyanos, gusanos, bombas lógicas o cualquier otro programa o material tecnológicamente perjudicial o dañino. EL INTERESADO no tratará de tener acceso no autorizado al APLICATIVO ni a los aplicativos del cliente, al servidor en que este se encuentra alojado o a cualquier servidor, ordenador o información relacionada con el mismo, sea ésta perteneciente al OPERADOR, a uno de sus clientes o a cualquier otra tercera parte.
              		<br><br>
              		El incumplimiento de lo aquí dispuesto podría significar la comisión de infracciones tipificadas por la normativa aplicable lo que faculta al OPERADOR a iniciar las acciones legales correspondientes.
              	</p>
              </div>
              <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">17. RELACIÓN ENTRE LAS PARTES</h6>
              </div>
              <div class="row">
              	<p>
                La relación que se establece entre las partes es meramente comercial y no crea una asociación, asociación de riesgo compartido ("joint venture"), sociedad o agencia, ni impone obligación o responsabilidad de índole societaria a ninguna de LAS PARTES.
                <br><br>
                La relación que se crea por medio del presente documento es una relación de tipo comercial y por lo tanto el INTERESADO no podrá hacer ninguna reclamación de tipo laboral con base en lo aquí establecido y en los servicios prestados.
                <br><br>
                EL INTERESADO presta los servicios con plena autonomía técnica, administrativa y financiera y determina voluntariamente la selección de los servicios que desea efectuar, así mismo puede voluntariamente y en cualquier momento abandonar los servicios iniciados sin que exista una contraprestación por los servicios realizados y sin que se genere ningún tipo de penalidad por abandonar la labor diferente a la desactivación de la información del BUG o RETO seleccionado.
                <br><br>
                EL INTERESADO podrá ejecutar los servicios en el lugar, tiempo y condiciones que desee siempre que la entrega de los productos solicitados cumpla con los requerimientos del producto.
                <br><br>
                EL INTERESADO, sus empleados, contratistas, subcontratistas, agentes y representantes no son empleados o agentes del OPERADOR. Por lo tanto el INTERESADO asumirá la responsabilidad como único empleador de su personal (en caso de tenerlo), siendo a su cargo los salarios, prestaciones sociales, afiliación al sistema de seguridad social, indemnizaciones, honorarios, pago de servicios y demás obligaciones a que hubiere lugar.
                <br><br>
                LAS PARTES se mantendrán indemnes una frente a la otra, por todos los daños y/o perjuicio que para estas se deriven de reclamaciones judiciales o extrajudiciales que en su contra interpongan los funcionarios de la otra parte o de sus subcontratistas, con causa o con ocasión de la ejecución de lo aquí contratado.
              	</p>
              </div>
              <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">18. ACEPTACIÓN TOTAL DE LOS TÉRMINOS Y CONDICIONES</h6>
              </div>
              <div class="row">
              	<p>
                El INTERESADO manifiesta expresamente que cuenta con la capacidad legal para usar el APLICATIVO y que ha suministrado información completa, real, veraz y fidedigna. De igual manera declara de forma expresa e inequívoca que ha leído, que entiende y que acepta la totalidad de las situaciones reguladas en estos Términos y Condiciones y se compromete al cumplimiento total de los deberes aquí contenidos. La aceptación aquí descrita se entiende absolutamente válida de conformidad con lo dispuesto por el artículo 15 y siguientes de la Ley 527 de 1999, respecto de la manifestación de voluntad a través de medios electrónicos. Para cualquier inquietud respecto al uso del portal escríbanos por correo electrónico a info@crowdsqa.co
              	</p>
              </div>
            
              <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">19. VIGENCIA</h6>
              </div>
              <div class="row">
                  <p>
                      La presente política rige a partir del 1 de septiembre del año 2020 y estará sujeto a actualizaciones
                      en la medida en que se modifiquen o se dicten nuevas disposiciones legales sobre la materia.<br>
                      Cuando se cumplan estas condiciones, CROWDSQA informará a los titulares de los datos
                      personales, sus causahabientes o representantes, las nuevas medidas dictadas sobre la
                      materia, antes de implementar las nuevas políticas. Además, deberá obtener del titular una
                      nueva autorización cuando el cambio se refiera a la finalidad del tratamiento.<br><br>
            
                  </p>
                  <p> Fecha de emisión: Septiembre de 2020 </p>
              </div>			
            </div>
          </div>
        </div>
  </div>
  <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
  </div>
</div>