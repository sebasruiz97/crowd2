@extends('layouts.inicio_crowd')
@section('content')

@inject('tiposDocumentos','App\Services\tiposDocumentos')

<div class="container-inicio " style="margin-top:20px">
    <div class="row">

        <div class="col-sm col-3 col-sm-3">
        </div>
        <div class="col-sm col-9 col-sm-6">
            <!--  Imagen de  Encabezado -->
			<img src="/storage/images/contact_encabezado.png" style="align: center; width:100%; height:auto;">
            
            <div class="profile-content" style="display:inline-block; width:auto; padding-top:20px; padding-left:40px; padding-right:40px; padding-bottom:20px;">
                <div class="centrar">
                    <img src="/storage/images/registro2.png" style="text-align: center" width="15%">
                </div>
                <h3 style="text-align: center"> Registrarme </h3> <br>
                <form method="post" action="{{ url('/register') }}">
                    {!! csrf_field() !!}

                    @if ($errors->has('g-recaptcha-response'))
                    <div style="margin-bottom: 5px;">
                        <span class="error" style="color:red">
                            <small><i>{{ $errors->first('g-recaptcha-response') }}</i></small>
                        </span>
                    </div>
                    <br>
                    @endif

                    <!-- Nombre -->
                    <div class="form-group col-sm-12" style="padding-bottom: 1px">
                        <label for="name">Nombre(s)</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required pattern="[a-zA-Z ]{2,50}" title="Tu nombre sólo puede contener letras y espacios">
                        @if ($errors->has('name'))
                        <span class="help-block" style="color:#F3A100" ;>
                            <small><i>{{ $errors->first('name') }}</i></small>
                        </span>
                        @endif
                    </div>

                    <!-- APELLIDOS -->
                    <div class="form-group col-sm-12" style="padding-bottom: 1px">
                        <label for="lastname">Apellido(s)</label>
                        <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" required pattern="[a-zA-Z ]{2,50}" title="Tu apellido sólo puede contener letras y espacios">
                        @if ($errors->has('lastname'))
                        <span class="help-block" style="color:#F3A100" ;>
                            <small><i>{{ $errors->first('lastname') }}</i></small>
                        </span>
                        @endif
                    </div>

                    <!-- Email -->
                    <div class="form-group col-sm-12" style="padding-bottom: 1px">
                        <label for="email">Correo Electrónico</label>
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                        <span class="help-block" style="color:#F3A100" ;>
                            <small><i>{{ $errors->first('email') }}</i></small>
                        </span>
                        @endif
                    </div>

                    <!-- Tipo de Documento -->
                    <div class="form-group col-sm-12" style="padding-bottom: 1px">
                        <label for="tipoDocumento_id">Tipo de Documento</label>
                        <select class="form-control" id="tipoDocumento_id" value="{{old('tipoDocumento_id')}}" name="tipoDocumento_id" required>
                            @foreach($tiposDocumentos->get() as $index => $tiposDocumentos)
                            <option value="{{ $index }}" {{old('tipoDocumento_id') == $index ? 'selected' : ''}}>
                                {{ $tiposDocumentos }}
                            </option>
                            @endforeach
                        </select>
                        @if ($errors->has('tipoDocumento_id'))
                        <span class="help-block" style="color:#F3A100" ;>
                            <small><i>{{ $errors->first('tipoDocumento_id') }}</i></small>
                        </span>
                        @endif
                    </div>

                    <!-- Numero de Documento -->
                    <div class="form-group col-sm-12" style="padding-bottom: 1px">
                        <label for="numeroDocumento">Número de Documento</label>
                        <input class="form-control" value="{{old('numeroDocumento')}}" name="numeroDocumento" type="text" id="numeroDocumento" required pattern="[0-9]{6,15}" title="Tu número de documento debe ser un número y contener mínimo 6 caracteres">
                        @if ($errors->has('numeroDocumento'))
                        <span class="help-block" style="color:#F3A100" ;>
                            <small><i>{{ $errors->first('numeroDocumento') }}</i></small>
                        </span>
                        @endif
                    </div>

                    <!-- Contraseña -->
                    <div class="form-group col-sm-12" style="padding-bottom: 1px">
                        <label for="password" style="text-align: left"> <SPAN title="La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&">Contraseña</SPAN></label>
                        <input type="password" class="form-control" id="password" name="password" required pattern="[A-Za-z0-9$@$!%*?&]{8,15}" onkeyup="validPass()" title="  La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&">
                        <span id="errorPass" class="help-block" style="color:#F3A100;display:none" ;>
                            <small><i></i></small>
                        </span>
                        @if ($errors->has('password'))
                        <span class="help-block" style="color:#F3A100" ;>
                            <small><i>{{ $errors->first('password') }}</i></small>
                        </span>
                        @endif
                    </div>

                    <!-- Confirmación de Contraseña -->
                    <div class="form-group col-sm-12" style="padding-bottom: 1px">
                        <label for="password_confirmation">Confirmación de contraseña</label>
                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" required pattern="[A-Za-z0-9$@$!%*?&]{8,15}" title="La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&">
                        @if ($errors->has('password'))
                        <span class="help-block" style="color:#F3A100" ;>
                            <small><i>{{ $errors->first('password') }}</i></small>
                        </span>
                        @endif
                    </div> <br>

                    <!-- Terminos y Condiciones -->
                    <div class="form-group col-sm-12">

                        <div class="centrar" data-toggle="modal" data-target="#terminos_condiciones">
                            <label for="terminosCondiciones"></label>
                            <input type="checkbox" name="terminosCondiciones" accepted required>
                            <label for="terminosCondiciones" style="color: #304457"> <u>Lee y acepta los términos y condiciones</u> </label>
                            @if ($errors->has('terminosCondiciones'))
                            <span class="help-block" style="color:#F3A100" ;>
                                <small><i>{{ $errors->first('terminosCondiciones') }}</i></small>
                            </span>
                            @endif
                        </div>

                        <br>
                        <div style="text-align:justify">
                            <i> <small>
                                    <h6 class="help-block" style="color:#304457">Tu <strong>
                                            <font style="color: #208297">nombre, número de documento y correo electrónico</font>
                                        </strong> serán usados para tu identificación en la plataforma. Por lo que no podrás cambiarlos, una vez te hayas registrado.
                                        <br><br> Ten en cuenta que te pediremos que nos confirmes tu dirección de correo.
                                    </h6>
                                </small> </i>
                        </div>

                        <br>

                        <!-- Botones -->

                        @if($errors->has('g-recaptcha-response'))
                        <center style="border: 1px solid red;padding: 5px;display: inline-block;">
                            {!! htmlFormSnippet() !!}
                        </center>
                        <br>
                        @else
                        <center>
                            {!! htmlFormSnippet() !!}
                        </center>
                        @endif

                        <br>

                        <div class="form-group col-sm-12" style="line-height:8px">
                            <div class="centrar">
                                <button type="submit" class="btn btn-primary" onclick="return Validate()">Enviar</button>
                                <a href={{ url('/') }} class="btn btn-primary">Cancelar</a>
                            </div>
                        </div>

                </form>
                <div class="form-group col-sm-12" style="line-height:8px">
                    <div class="centrar">
                        <a href="{{ url('/login') }}" class="text-center">Ya tengo una cuenta</a>
                    </div>
                </div>
            </div>
        </div>
		<!-- Imagen Pie de pagina -->
		<img src="/storage/images/contact_pie_pagina.png" style="align: center; width:100%; height:auto;">
    </div>

    <div class="col-sm col-3 col-sm-3">
    </div>
</div>

{{-- Para que salga recaptcha --}}
{{-- @captcha --}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>

<script>
    $(function() {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

<script>
    function Validate() {
        var password = document.getElementById("password").value;
        var confirmPassword = document.getElementById("password_confirmation").value;
        if (password != confirmPassword) {
            alert("Las contraseñas no coinciden. ");
            return false;
        }
        return true;
    }

    function validPass() {
        var password = document.getElementById("password").value;
        const regxs = {
            "lower": /[a-z]/g,
            "upper": /[A-Z]/g,
            "number": /[0-9]/g,
            "caracters": /[$@$!%*?&]/g
        }
        let msg = "La contraseña debe contener al menos ";
        let err = false;
        if (!regxs.lower.test(password)) {
            err = true;
            msg += "una letra minúscula, ";
        }
        if (!regxs.upper.test(password)) {
            err = true;
            msg += "una letra mayúscula, ";
        }
        if (!regxs.number.test(password)) {
            err = true;
            msg += "un dígito (0-9) ";
        }
        if (!regxs.caracters.test(password)) {
            if (err)
                msg += "y almenos ";
            err = true;
            msg += "uno de los siguientes caracteres especiales: $@$!%*?&";
        }
        if (err) {
            erroPass(msg)
        } else {
            $('#errorPass').hide();
        }
    }

    function erroPass(msg) {
        $('#errorPass small i').html(msg);
        $('#errorPass').show();
    }
</script>

<!-- Modal / Ventana / Overlay en HTML -->
<div id="terminos_condiciones" class="modal fade" data-backdrop="static">
    <div class="modal-dialog">
        @include('auth.terminos_condiciones')
    </div>
</div>

</body>

@endsection

</html>