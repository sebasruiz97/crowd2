@extends('layouts.app')

<div class="container-inicio ">
    <div class="row" >
        <div class="col-sm col-4 col-lg-4">
        </div>
        <div class="col-sm col-4 col-lg-4">
            <center>
            <div>
                <img src="/storage/images/logo_crowd_leyenda.png"  width="80%"/>
            </div>
            </center>
            <br>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form method="post" action="{{ url('/password/email') }}">
            {!! csrf_field() !!}

            <div>
                <label for="email"></label>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Digita tu correo electrónico">
                    @if ($errors->has('email'))
                        <span class="help-block" style="color:#F3A100" ;>
                            <small><i>{{ $errors->first('email') }}</i></small>
                        </span>
                    @endif
                </div>

                <br>

                <div class="centrar">
                    <button type="submit" class="btn btn-primary ">Enviar link para recuperar contraseña</button>
                </div>
            </div>

        </form>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
