@extends('layouts.app')

<div class="container-inicio ">
    <div class="row" >
        <div class="col-sm col-4 col-lg-4">
        </div>
        <div class="col-sm col-4 col-lg-4">
            <center>
            <div>
                <img src="/storage/images/logo_crowd_leyenda.png"  width="80%"/>
            </div>
            </center>
            <br>

            <div class="centrar">
                <h3>Resetear Contraseña </h3>
            </div>

                <form method="post" action="{{ url('/password/reset') }}">
                    {!! csrf_field() !!}

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div>
                        <label for="email"></label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Digita tu correo electrónico" required>
                            @if ($errors->has('email'))
                                <span class="help-block" style="color:#F3A100" ;>
                                    <small><i>{{ $errors->first('email') }}</i></small>
                                </span>
                            @endif
                        </div>
    
                        <div>
                            <label for="password"></label> <SPAN title="La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&"></SPAN>
                            <input type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="Digita tu nueva contraseña nueva" required pattern="[A-Za-z0-9$@$!%*?&]{8,15}" title="La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&">
                            @if ($errors->has('password'))
                                <span class="help-block" style="color:#208297" ;>
                                    <small><i>{{ $errors->first('password') }}</i></small>
                                </span>
                            @endif
                        </div>
    
                        <div>
                            <label for="password_confirmation"></label> 
                                <input type="password" name="password_confirmation" class="form-control" placeholder="Confirma la nueva contraseña" required pattern="[A-Za-z0-9$@$!%*?&]{8,15}" title="La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&">
                                @if ($errors->has('password'))
                                    <span class="help-block" style="color:#208297" ;>
                                        <small><i>{{ $errors->first('password') }}</i></small>
                                    </span>
                                @endif
                            </div>
        
                            <br>

                        <div class="centrar">
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                </form>
</div>
<div class="col-sm col-4 col-lg-4">
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

<script>
    function Validate() {
        var password = document.getElementById("password").value;
        var confirmPassword = document.getElementById("password_confirmation").value;
        if (password != confirmPassword) {
            alert("Las contraseñas no coinciden. ");
            return false;
        }
        return true;
    }
</script>

</body>
</html>
