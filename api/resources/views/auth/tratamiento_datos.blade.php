<div class="modal-content" data-backdrop="static">
  <div class="modal-header" >
  	  <h4 style="color: #304457; text-align:center"> <center><strong> POLÍTICA DE TRATAMIENTO DE DATOS CROWDSQA</strong></center></h4>
	  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  </div>
  <div class="modal-body" data-backdrop="static" >
      <div class="row">
          <div class="col-1" style="width: 5px">
          </div>
          <br>
          <div class="col-10">
            <div class="form-group" style="color:#304457;padding:0px;margin-right:5px;margin-top:0px;">
              <div class="row" >
                  <h6 style="font-weight:bold; font-size:14px">1. IDENTIFICACIÓN DEL RESPONSABLE DEL TRATAMIENTO</h6>
              </div>
              <br>
              <div class="row" >
            <p style="font-size:14px">Razón Social e Identificación.: Software Quality Assurance SQA S.A, que en adelante se denominará SQA S.A, sociedad comercial identificada con NIT. 811.042.907-7 y creada por escritura pública No. 16 el día 08 de Enero del año 2004, registrada en la Cámara de Comercio de Medellín el 16 de Enero del año 2004.
<br><br>
Domicilio y dirección. SQA S.A tiene su domicilio en la ciudad de Medellín, y su sede principal se encuentra ubicada en la carrera 30 No. 7 AA 207 Torre Scaglia barrio Poblado. Medellín.
<br><br>
Correo Electrónico. info@sqasa.co
<br><br>
Teléfonos: (+57) 315 398 9513
                    </p>

              </div>
              <div class="row">
                <h6 style="font-weight:bold; font-size:14px">2. DEFINICIONES</h6>
              </div>
              <div class="row">
                <p  style="font-size:14px">
                  Para efectos de la presente política y de acuerdo a lo contemplado en la Ley Estatutaria 1581 de 2012, se entiende por:
					<ul>
						<li>
							Autorización: Consentimiento previo, expreso e informado del Titular para llevar a cabo el Tratamiento de datos personales;
						</li>
						<li>
							Base de Datos: Conjunto organizado de datos personales que sea objeto de Tratamiento;
						</li>
						<li>
							Dato personal: Cualquier información vinculada o que pueda asociarse a una o varias personas naturales determinadas o determinables;
						</li>
						<li>
							Encargado del Tratamiento: Persona natural o jurídica, pública o privada, que por sí misma o en asocio con otros, realice el Tratamiento de datos personales por cuenta del Responsable del Tratamiento;
						</li>
						<li>
							Responsable del Tratamiento: Persona natural o jurídica, pública o privada, que por sí misma o en asocio con otros, decida sobre la base de datos y/o el Tratamiento de los datos;
						</li>
						<li>
							Titular: Persona natural cuyos datos personales sean objeto de Tratamiento;
						</li>
						<li>
							Tratamiento: Cualquier operación o conjunto de operaciones sobre datos personales, tales como la recolección, almacenamiento, uso, circulación o supresión.
						</li>
					</ul>
                </p>
              </div>
            
              <div class="row">
                <h6 style="font-weight:bold; font-size:14px">3.  TRATAMIENTO AL CUAL SERÁN SOMETIDOS LOS DATOS Y FINALIDAD DEL MISMO</h6>
              </div>
              <div class="row">
                <p style="font-size:14px">
                  El tratamiento de los datos personales de la persona con la cual SQA S.A tuviere establecida o estableciera una relación, permanente u ocasional, lo realizará en el marco legal que regula la materia. En todo caso, los datos personales podrán ser recolectados y tratados para:
					<ul>
						<li>
							Desarrollar el objeto social de SQA S.A conforme a sus estatutos legales.
						</li>
						<li>
							Cumplir las normas aplicables tributaria y comercial.
						</li>
						<li>
							Cumplir lo dispuesto por el ordenamiento jurídico colombiano en materia laboral y de seguridad social (Salud, Pensión, Riesgos Laborales y Parafiscales), entre otras, aplicables a ex empleados, empleados actuales y candidatos a futuro empleo
						</li>
						<li>
							Remitir información comercial de SQA S.A y realizar encuestas relacionadas con los servicios o bienes que presta.
						</li>
						<li>
							Desarrollar programas conforme a sus estatutos
						</li>
						<li>
							Cumplir todos sus compromisos contractuales contraídos con clientes, proveedores y empleados.
						</li>
						<li>
							Ponerlo a disposición de sus clientes finales a fin de determinar idoneidad y capacidad técnica para el desarrollo de las actividades para las cuales contraten a SQA S.A
						</li>
						<li>
							Administración de sistemas de información, gestión de claves, administración de usuarios requerida para la prestación del servicio.
						</li>
					</ul>
					<br>
					<b>Datos sensibles</b>
					<br><br>
					Los datos sensibles son aquellos datos que afectan la intimidad del titular o cuyo uso indebido puede generar su discriminación. Para el caso de datos personales sensibles, SQA S.A podrá hacer uso y tratamiento de ellos cuando:
					<ul>
						<li>
							El titular haya dado su autorización explícita, salvo en los casos que por ley no sea requerido el otorgamiento de dicha autorización.
						</li>
						<li>
							El tratamiento sea necesario para salvaguardar el interés vital del Titular y este se encuentre física o jurídicamente incapacitado. En estos eventos, los representantes legales deberán otorgar su autorización.
						</li>
						<li>
							El tratamiento sea efectuado en el curso de las actividades legítimas y con las debidas garantías por parte de una fundación, ONG, asociación o cualquier otro organismo sin ánimo de lucro, cuya finalidad sea política, filosófica, religiosa o sindical, siempre que se refieran exclusivamente a sus miembros o a las personas que mantengan contactos regulares por razón de su finalidad. En estos eventos, los datos no se podrán suministrar a terceros sin la autorización del titular.
						</li>
						<li>
							El Tratamiento se refiera a datos que sean necesarios para el reconocimiento, ejercicio o defensa de un derecho en un proceso judicial.
						</li>
						<li>
							El Tratamiento tenga una finalidad histórica, estadística o científica. En este evento deberán adoptarse las medidas conducentes a la supresión de identidad de los titulares.
						</li>
					</ul>
					<br>
					SQA S.A restringirá el tratamiento de datos personales sensibles a lo estrictamente indispensable y solicitará al titular el consentimiento previo, expreso e informado sobre la finalidad de su tratamiento. Esta autorización deberá obtenida por cualquier medio que pueda ser objeto de consulta y verificación posterior.
                </p>
              </div>
            
              <div class="row">
                <h6 style="font-weight:bold; font-size:14px">4. DERECHOS DEL TITULAR DE LA INFORMACIÓN</h6>
              </div>
              <div class="row">
                <p style="font-size:14px">
                  De acuerdo con lo prescrito por la normatividad vigente aplicable en materia de protección de datos, los titulares de los datos personales tienen derecho a:
                  <br>
				 <ul>
					 <li>
					 	Acceder, conocer, actualizar y rectificar sus datos personales frente a la SQA S.A en su condición de responsable del tratamiento. Este derecho se podrá ejercer, entre otros, frente a datos parciales, inexactos, incompletos, fraccionados, que induzcan a error, o aquellos cuyo tratamiento esté expresamente prohibido o no haya sido autorizado.
					 </li>
				 	 <li>
				 	 	Solicitar prueba de la autorización otorgada a la SQA S.A para el tratamiento de datos, mediante cualquier medio válido, salvo en los casos en que no sea necesaria la autorización.
				 	 </li>
				 	 <li>
				 	 	Ser informado por SQA S.A, previa solicitud, respecto del uso que le ha dado a sus datos personales.
				 	 </li>
				 	 <li>
				 	 	Presentar ante la Superintendencia de Industria y Comercio, quejas por infracciones a lo dispuesto en la ley 1581 de 2012 y las demás normas que la modifiquen, adicionen o complementen, previo trámite de consulta o requerimiento ante SQA S.A
				 	 </li>
				 	 <li>
				 	 	Revocar la autorización y/o solicitar la supresión del dato.
				 	 </li>
				 	 <li>
				 	 	Acceder en forma gratuita a sus datos personales que hayan sido objeto de tratamiento, al menos una vez cada mes calendario, y cada vez que existan modificaciones sustanciales de la presente política que motiven nuevas consultas.
				 	 </li>
				 </ul>
				 <br>
				 <b>Estos derechos podrán ser ejercidos por:</b>
				 <br>
				 <ul>
				 	<li>
				 		El titular, quien deberá acreditar su identidad en forma suficiente por los distintos medios que le ponga a disposición SQA S.A
				 	</li>
				 	<li>
				 		Los causahabientes del titular, quienes deberán acreditar tal calidad.
				 	</li>
				 	<li>
				 		El representante y/o apoderado del titular, previa acreditación de la representación o apoderamiento.
				 	</li>
				 	<li>
				 		Otro a favor o para el cual el titular hubiere estipulado.
				 	</li>
				 </ul>
				 <br>
				 <b>Nota</b>
				 Para los datos personales correspondientes a Empleados de SQA S. A. La información del EMPLEADO se tiene en virtud del vínculo contractual que lo liga con la Empresa, razón por la cual aplicará la excepción de que trata el artículo 9 inciso segundo y artículo 11 parte final del inciso primero del Decreto 1377 de 2013, que indica: la revocatoria de la autorización no procederá cuando la información se requiera para el cumplimiento de una obligación contractual, que es la que precisamente existe entre SQA S.A., y EL EMPLEADO.
                </p>
              </div>
              <div class="row">
                <h6 style="font-weight:bold; font-size:14px">5. RESPONSABLE Y ENCARGADO DEL TRATAMIENTO DE DATOS PERSONALES</h6>
              </div>
              <div class="row">
                <p style="font-size:14px">
                  SQA S.A será la responsable del tratamiento de los datos personales. El proceso Administrativo y financiero será el encargado del tratamiento de los datos personales. Cualquier comunicación sobre el asunto deberá efectuarse a través los siguientes canales de servicio escrito
                  <ul>
                  	<li>
                  		Carrera 30 No. 7 AA 207 Torre Scaglia Barrio Poblado. Medellín.
                  	</li>
                  	<li>
                  		Carrera 8 # 64 - 42. Oficina 501. Bogotá
                  	</li>
                  	<li>
                  		Correo electrónico: pqrsf@sqasa.com
                  	</li>
                  </ul>
                </p>
              </div>
              <div class="row">
                <h6 style="font-weight:bold; font-size:14px">6. PROCEDIMIENTO PARA LA ATENCIÓN DE CONSULTAS, RECLAMOS, PETICIONES DE RECTIFICACIÓN, ACTUALIZACIÓN Y SUPRESIÓN DE DATOS</h6>
              </div>
              <div class="row">
                <p style="font-size:14px">
                Los titulares o sus causahabientes podrán consultar la información personal del titular que repose en SQA S.A, quien suministrará toda la información contenida en el registro individual o que esté vinculada con la identificación del Titular. Igualmente, se proporciona por parte de SQA S.A el mecanismo a través del cual el titular puede elevar reclamos para efectos de actualizar, rectificar, suprimir el dato o revocar la autorización de forma definitiva.

Los procedimientos para atender las consultas, reclamos o peticiones del titular, se definen con base en la Ley Estatutaria 1581 de 2012 y son los siguientes:.
<br><br>
				<b>CONSULTAS</b>
				<br><br>
				Los titulares o sus causahabientes podrán consultar la información personal del Titular que repose en cualquier base de datos, sea esta del sector público o privado. El Responsable del Tratamiento o Encargado del Tratamiento deberán suministrar a estos toda la información contenida en el registro individual o que esté vinculada con la identificación del Titular.
				<br><br>
				La consulta se formulará por el medio habilitado por el Responsable del Tratamiento o Encargado del Tratamiento, siempre y cuando se pueda mantener prueba de esta.
				<br><br>
				La consulta será atendida en un término máximo de diez (10) días hábiles contados a partir de la fecha de recibo de la misma. Cuando no fuere posible atender la consulta dentro de dicho término, se informará al interesado, expresando los motivos de la demora y señalando la fecha en que se atenderá su consulta, la cual en ningún caso podrá superar los cinco (5) días hábiles siguientes al vencimiento del primer término.
				<br><br>
				Parágrafo. Las disposiciones contenidas en leyes especiales o los reglamentos expedidos por el Gobierno Nacional podrán establecer términos inferiores, atendiendo a la naturaleza del dato personal.
				<br><br>
				<b>RECLAMOS</b>
				<br><br>
				El titular o sus causahabientes que consideren que la información contenida en una base de datos debe ser objeto de corrección, actualización o supresión, o cuando adviertan el presunto incumplimiento de cualquiera de los deberes contenidos en esta ley, podrán presentar un reclamo ante el Responsable del Tratamiento o el Encargado del Tratamiento el cual será tramitado bajo las siguientes reglas:
				<br><br>
				1. El reclamo se formulará mediante solicitud dirigida al Responsable del Tratamiento o al Encargado del Tratamiento, con la identificación del Titular, la descripción de los hechos que dan lugar al reclamo, la dirección, y acompañando los documentos que se quiera hacer valer. Si el reclamo resulta incompleto, se requerirá al interesado dentro de los cinco (5) días siguientes a la recepción del reclamo para que subsane las fallas. Transcurridos dos (2) meses desde la fecha del requerimiento, sin que el solicitante presente la información requerida, se entenderá que ha desistido del reclamo.
				<br><br>
				En caso de que quien reciba el reclamo no sea competente para resolverlo, dará traslado a quien corresponda en un término máximo de dos (2) días hábiles e informará de la situación al interesado.
				<br><br>
				2. Una vez recibido el reclamo completo, se incluirá en la base de datos una leyenda que diga "reclamo en trámite" y el motivo del mismo, en un término no mayor a dos (2) días hábiles. Dicha leyenda deberá mantenerse hasta que el reclamo sea decidido.
				<br><br>
				3. El término máximo para atender el reclamo será de quince (15) días hábiles contados a partir del día siguiente a la fecha de su recibo. Cuando no fuere posible atender el reclamo dentro de dicho término, se informará al interesado los motivos de la demora y la fecha en que se atenderá su reclamo, la cual en ningún caso podrá superar los ocho (8) días hábiles siguientes al vencimiento del primer término.
				<br><br>
				Requisito de procedibilidad. El Titular o causahabiente sólo podrá elevar queja ante la Superintendencia de Industria y Comercio una vez haya agotado el trámite de consulta o reclamo ante el Responsable del Tratamiento o Encargado del Tratamiento.
                </p>
              </div>
             
              <div class="row">
                <h6 style="font-weight:bold; font-size:14px">7. MEDIDAS DE SEGURIDAD DE LA INFORMACIÓN</h6>
              </div>
             <div class="row">
             Dando cumplimiento al principio de seguridad establecido en la normatividad vigente, SQA S.A adoptará las medidas técnicas, humanas y administrativas que sean necesarias para otorgar seguridad a los registros evitando su adulteración, pérdida, consulta, uso o acceso no autorizado o fraudulento.
             </div>
             <br>
   
              <div class="row">
                  <h6 style="font-weight:bold; font-size:14px">8. VIGENCIA</h6>
              </div>
              <div class="row">
                  <p style="font-size:14px">
                      La presente política rige a partir del 17 de enero del año 2017 y estará sujeto a actualizaciones en la medida en que se modifiquen o se dicten nuevas disposiciones legales sobre la materia.
Cuando se cumplan estas condiciones, SQA S.A informará a los titulares de los datos personales, sus causahabientes o representantes, las nuevas medidas dictadas sobre la materia, antes de implementar las nuevas políticas. Además, deberá obtener del titular una nueva autorización cuando el cambio se refiera a la finalidad del tratamiento.
                  </p>
              </div>
			
            </div>
          </div>

        </div>
  </div>
  <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
  </div>
</div>