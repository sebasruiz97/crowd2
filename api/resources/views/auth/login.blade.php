@extends('layouts.inicio_crowd')
@section('content')

<style>
    .col-sm{
    overflow-x: hidden;
    }

    body{overflow-y: hidden;
     overflow-x: hidden;
     }
</style>

<div class="container-inicio2 ">
    <div class="row" >
        <div class="col-sm col-3 col-sm-3">
        </div>
        <div class="col-sm col-6 col-sm-6">
            <!--  Imagen de  Encabezado -->
			<img src="/storage/images/contact_encabezado.png" style="align: center; width:100%; height:auto;">
			
            <div class="profile-content" style="height:auto; width:auto; padding-top:20px; padding-left:40px; padding-right:40px; padding-bottom:20px; overflow: hidden;">
                <div class="centrar">
                    <img src="/storage/images/login.png" style="text-align: center" width="15%">
                </div>
                <h3 style="text-align: center"> Iniciar sesión </h3> <br> 
    
                <form method="post" action="{{ url('/login') }}">
                    {!! csrf_field() !!}

                    <div class="form-group col-sm-12" style="line-height:8px";>
                        <label for="numeroDocumento">Número de Documento</label>
                        <input type="text" class="form-control" name= "numeroDocumento" value="{{ old('numeroDocumento') }}" required pattern="[0-9]{6,15}" title="Tu número de documento debe ser un número y contener mínimo 6 caracteres">
                        @if ($errors->has('numeroDocumento'))
                            <span class="help-block" style="color:#F3A100" ;>
                                <small><i>{{ $errors->first('numeroDocumento') }}</i></small>
                            </span>
                        @endif
                    </div>
                    
                    <div class="form-group col-sm-12"  style="line-height:8px">
                        <label for="password">Contraseña</label>
                        <input type="password" class="form-control" name="password" value="{{ old('password') }}"  required pattern="[A-Za-z0-9$@$!%*?&]{8,15}" title="La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&">
                        @if ($errors->has('password'))
                            <span class="help-block" style="color:#F3A100" ;>
                                <small><i>{{ $errors->first('password') }}</i></small>
                            </span>
                        @endif
                    </div>
                    
                    <div class="form-group col-sm-12" style="line-height:8px">
                        <div class="centrar">
                            <div>
                                <button class="btn btn-primary" type="submit">Iniciar</button>
                                <a href={{ url('/') }} class="btn btn-primary">Cancelar</a>
                            </div>
                        </div>
                    </div>

                </form>

                <div class="centrar">
                    <a href="{{ url('/password/reset') }}" class="text-center">Olvide mi contraseña</a><br>
                    <a href="{{ url('/register') }}" class="text-center">Registrarme</a>
                </div>

            </div>
				<!-- Imagen Pie de pagina -->
				<img src="/storage/images/contact_pie_pagina.png" style="align: center; width:100%; height:auto;">
            </div>
            <div class="col-sm col-3 col-sm-3">
            </div>
    
        </div>

    </div>
<div>

        
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>

<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

</body>

@endsection
</html>

