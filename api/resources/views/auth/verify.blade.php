<style>
    a:link,
    a:visited,
    a:active {
        text-decoration: none;
    }

    img {
        border: 0;
    }

    .textEmail {
        border: 5px solid;
        padding: 5px 15px;
        color: #0c213d;
        font-family: -webkit-pictograph;
        letter-spacing: 1px;
        font-weight: bold;
        border-radius: 1em;
        font-size: 18px;
    }

    .textEmail b {
        text-decoration: underline;
    }

    .alertContact {
        background-color: red;
        color: white;
        padding: 5px 15px;
        font-size: 18px;
        border-radius: 1em;
    }

    .alertContact a {
        color: white;
        font-weight: bold;
    }

    center {
        display: flex;
        justify-content: center;
        margin-top: 10px;
    }
</style>

@if (!session()->has('expired'))
    <center><img src="/storage/images/confirmacion_correo2.png" width="70%" /></center>
    <center>
        <span class="alertContact">
            Si tienes problemas para acceder al correo o quieres modificarlo contactanos
            <a href="{{ route('contacto') }}">aqui</a>.
        </span>
    </center>
    <center>
        <span class="textEmail">El enlace fue enviado al correo: <b>{{ Auth()->user()->email }}</b></span>
    </center>
    <form method="POST" action="{{ route('verification.resend') }}">
        @csrf
        <center style="margin: 0">
            <button type="submit" style="background-color: transparent;border: 0;">
                <a href="{{ route('verification.resend') }}"><img src="/storage/images/envio_correo2.png" style="width:100%; border:0; vertical-align: middle;" /></a>
            </button>
        </center>
    </form>
@else
    <style>
        .divExpired {
            font-family: -webkit-pictograph;
            border: 2px solid red;
            padding: 5px 20px;
            border-radius: .5em;
            font-size: 18px;
        }

        .divExpired label {
            font-weight: bold;
            color: red;
        }
    </style>
    <center style="margin: 2em;">
        <a href="{{ url('/') }}">
            <img src="/storage/images/logo_crowd_leyenda.png" width="35%" />
        </a>
    </center>
    <center>
        <div class="divExpired">
            <label>Error al verificar correo</label>
            <p style="margin:5px;margin-bottom: 0;">
                Parece que el enlace de verificación del correo no es válido o ha vencido.
            </p>
            <p style="margin:0">
                Haga clic en el siguiente botón para restablecer la contraseña de nuevo.
            </p>
            <div style="margin-top: 2.3em;">
                <span class="textEmail">El enlace será enviado al correo: <b>{{ Auth()->user()->email }}</b></span>
                <form method="POST" action="{{ route('verification.resend') }}" style="margin-top: 1em;">
                    @csrf
                    <center style="margin: 0">
                        <button type="submit" style="background-color: transparent;border: 0;">
                            <a href="{{ route('verification.resend') }}"><img src="/storage/images/envio_correo2.png" style="width:100%; border:0; vertical-align: middle;" /></a>
                        </button>
                    </center>
                </form>
            </div>
        </div>
    </center>
@endif


<a href="{!! url('/logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    <h6 style="font-family: sans-serif; text-align:center; color:#304457">Regresar</h6>
</a>
<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">{{ csrf_field() }}
</form>