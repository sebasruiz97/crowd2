@csrf

@inject('sectores','App\Services\sectores')
@inject('tiposMetodologias', 'App\Services\tiposMetodologias')
@inject('modeloServicios','App\Services\modeloServicios')
@inject('paises','App\Services\paises')

<div class="form-group col-sm-12">
    <div class="form-group col-sm-2"> </div>
    <div class="form-group col-sm-8">

        <div>
            <br> <h4 class="help-block" style="color:#F3A100";><strong > Información Básica </strong></h4> <hr>

            <!-- NIT-->
            <div class="form-group col-sm-6">
                <label for="nit_cliente">NIT :</label>
                <input class="form-control" name="nit_cliente" value="{{old('nit_cliente')}}" type="text" id="nit_cliente" required>
                @if ($errors->has('nit_cliente'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('nit_cliente') }}</i></small>
                    </span>
                @endif
            </div>

            <!-- Nombre del cliente -->
            <div class="form-group col-sm-6">
                <label for="nombre_cliente">Nombre del Cliente :</label>
                <input class="form-control" name="nombre_cliente" value="{{old('nombre_cliente')}}" type="text" id="nombre_cliente" required>
                @if ($errors->has('nombre_cliente'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('nombre_cliente') }}</i></small>
                    </span>
                @endif
            </div>

            <!-- Descripción del cliente -->
            <div class="form-group col-sm-12">
                <label for="descripcion">Breve descripción del cliente :</label>
                <textarea class="form-control" name="descripcion" value="{{old('descripcion')}}" type="text" id="descripcion" minlength="4" pattern="[A-Za-z0-9 ]{4,250}" title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 250 caracteres" required></textarea>
                @if ($errors->has('descripcion'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('descripcion') }}</i></small>
                    </span>
                @endif
            </div>

            <!-- Sector -->
            <div class="form-group col-sm-6">
                <label for="sector_id">Sector:</label>
                <select class="form-control" id="sector_id" name="sector_id">
                    @foreach($sectores->get() as  $index => $sectores)
                        <option value = "{{ $index }}" {{old('sector_id') == $index ? 'selected' : ''}} > {{  $sectores }}</option>
                    @endforeach
                </select>
                    @if ($errors->has('sector_id'))
                        <span class="help-block" style="color:#F3A100"; >
                            <small><i>{{ $errors->first('sector_id') }}</i></small>
                        </span>
                    @endif
            </div>

            <!-- Pais Field -->
            <div class="form-group col-sm-6">
                <label for="pais_id">País</label>
                <select class="form-control" id="pais_id" value="{{old('pais_id')}}" name="pais_id" required>
                    @foreach($paises->get() as  $index => $paises)
                        <option value = "{{ $index }}" {{old('pais_id') == $index ? 'selected' : ''}} > {{  $paises }}</option>
                    @endforeach
                </select>
                @if ($errors->has('pais_id'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('pais_id') }}</i></small>
                    </span>
                @endif
            </div>

            <!-- Departamento Field -->
            <div class="form-group col-sm-6">
                <label for="departamento_id">Departamento</label>
                <select class="form-control" data-old="{{ old('departamento_id') }}" id="departamento_id" name="departamento_id" required>
                    <option value= ''> </option>
                </select>
                @if ($errors->has('departamento_id'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('departamento_id') }}</i></small>
                    </span>
                @endif
            </div>

            <!-- Municipio Field -->
            <div class="form-group col-sm-6">
                <label for="municipio_id">Municipio</label>
                <select class="form-control" data-old="{{ old('municipio_id') }}" id="municipio_id" name="municipio_id" required>
                    <option value= ''> </option>
                </select>
                @if ($errors->has('municipio_id'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('municipio_id') }}</i></small>
                    </span>
                @endif
            </div>

            <!-- Dirección -->
            <div class="form-group col-sm-6">
                <label for="direccion_cliente">Dirección :</label>
                <input class="form-control" name="direccion_cliente" value="{{old('direccion_cliente')}}" type="text" id="direccion_cliente" required>
                @if ($errors->has('direccion_cliente'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('direccion_cliente') }}</i></small>
                    </span>
                @endif
            </div>


            <!-- Plazo para el pago de las facturas-->
            <div class="form-group col-sm-6">
                <label for="plazo_pagoFactura">Plazo para el pago de las facturas:</label>
                <input class="form-control" name="plazo_pagoFactura" type="text" value="{{old('plazo_pagoFactura')}}" id="plazo_pagoFactura" required>
                @if ($errors->has('plazo_pagoFactura'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('plazo_pagoFactura') }}</i></small>
                    </span>
                @endif 
            </div>

        </div> 

        <div>
            <br> <h4 class="help-block" style="color:#F3A100";> <strong> Información de Pruebas </strong></h4> <hr>

        
            <!-- Modelo de Servicio -->
            <div class="form-group col-sm-6">
                <label for="modeloServicio_id">Modelo de Servicio :</label>
                <select class="form-control" id="modeloServicio_id" name="modeloServicio_id">
                    @foreach($modeloServicios->get() as $index => $modeloServicios)
                        <option value = "{{ $index }}" {{old('modeloServicio_id') == $index ? 'selected' : ''}} > {{  $modeloServicios }}</option>
                    @endforeach
                </select>
                    @if ($errors->has('modeloServicio_id'))
                        <span class="help-block" style="color:#F3A100"; >
                            <small><i>{{ $errors->first('modeloServicio_id') }}</i></small>
                        </span>
                    @endif
            </div>

            <!-- Nombre del Contacto de Pruebas-->
            <div class="form-group col-sm-6">
                <label for="nombre_contactoPruebas">Nombre del Contacto de Pruebas :</label>
                <input class="form-control" name="nombre_contactoPruebas" value="{{old('nombre_contactoPruebas')}}" type="text" id="nombre_contactoPruebas" required>
                @if ($errors->has('nombre_contactoPruebas'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('nombre_contactoPruebas') }}</i></small>
                    </span>
                @endif
            </div>

            <!-- Telefono del Contacto de Pruebas-->
            <div class="form-group col-sm-6">
                <label for="telefono_contactoPruebas">Telefono del Contacto de Pruebas :</label>
                <input class="form-control" name="telefono_contactoPruebas" type="text" value="{{old('telefono_contactoPruebas')}}" id="telefono_contactoPruebas" required>
                @if ($errors->has('telefono_contactoPruebas'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('telefono_contactoPruebas') }}</i></small>
                    </span>
                @endif
            </div>

            <!-- Correo del Contacto de Pruebas-->
            <div class="form-group col-sm-6">
                <label for="correo_contactoPruebas">Correo del Contacto de Pruebas :</label>
                <input class="form-control" name="correo_contactoPruebas" type="email" value="{{old('correo_contactoPruebas')}}" id="correo_contactoPruebas">
                @if ($errors->has('correo_contactoPruebas'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('correo_contactoPruebas') }}</i></small>
                    </span>
                @endif
            </div>

        </div>

        <div>
            <br> <h4 class="help-block" style="color:#F3A100";> <strong> Información de Facturación </strong> </h4> <hr>

            <!-- Nombre del Contacto de Facturación-->
            <div class="form-group col-sm-6">
                <label for="nombre_contactoFacturacion">Nombre del Contacto de Facturación :</label>
                <input class="form-control" name="nombre_contactoFacturacion" type="text" value="{{old('nombre_contactoFacturacion')}}" id="nombre_contactoFacturacion" required>
                @if ($errors->has('nombre_contactoFacturacion'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('nombre_contactoFacturacion') }}</i></small>
                    </span>
                @endif  
            </div>

            <!-- Telefono del Contacto de Facturación-->
            <div class="form-group col-sm-6">
                <label for="telefono_contactoFacturacion">Telefono del Contacto de Facturación :</label>
                <input class="form-control" name="telefono_contactoFacturacion" type="text" value="{{old('telefono_contactoFacturacion')}}" id="telefono_contactoFacturacion" required>
                @if ($errors->has('telefono_contactoFacturacion'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('telefono_contactoFacturacion') }}</i></small>
                    </span>
                @endif  
            </div>

            <!-- Correo del Contacto de Facturacion-->
            <div class="form-group col-sm-6">
                <label for="correo_contactoFacturacion">Correo del Contacto de Facturación :</label>
                <input class="form-control" name="correo_contactoFacturacion" type="email" value="{{old('correo_contactoFacturacion')}}"  id="correo_contactoFacturacion" required>
                @if ($errors->has('correo_contactoFacturacion'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('correo_contactoFacturacion') }}</i></small>
                    </span>
                @endif 
            </div>
            </div>

            <!-- Correo para envio de facturas-->
            <div class="form-group col-sm-6">
                <label for="correo_envioFactura">Correo para envio de facturas :</label>
                <input class="form-control" name="correo_envioFactura" type="text" value="{{old('correo_envioFactura')}}" id="correo_envioFactura" required>
                @if ($errors->has('correo_envioFactura'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('correo_envioFactura') }}</i></small>
                    </span>
                @endif 
            </div>

            <!-- Fecha recibido facturas-->
            <div class="form-group col-sm-6">
                <label for="fecha_recibidoFactura">Fecha máxima recibido Facturas:</label>
                <input class="form-control" name="fecha_recibidoFactura" type="text" value="{{old('fecha_recibidoFactura')}}" id="fecha_recibidoFactura" required>
                @if ($errors->has('fecha_recibidoFactura'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('fecha_recibidoFactura') }}</i></small>
                    </span>
                @endif 
            </div>

            <!-- Adjuntos-->
            <div class="form-group col-sm-6">
                <label for="url_anexoCliente" >Anexos para el cliente</label>
                <small><input type="file" id="url_anexoCliente" name="url_anexoCliente[]" accept="image/png, image/jpeg, image/jpg, application/pdf" title="Este campo es obligatorio, debes adjuntar uno o más archivos" multiple= "multiple"></small>
                @if ($errors->has('url_anexoCliente'))
                <span class="help-block" style="color:#F3A100"; >
                    <small><i>{{ $errors->first('url_anexoCliente') }}</i></small>
                </span>
                @endif 
            </div>

           <!-- Acuerdos adicionales-->
            <div class="form-group col-sm-12">
                <label for="acuerdos_adicionales">Comentarios y/o acuerdos adicionales :</label>
                <textarea class="form-control" name="acuerdos_adicionales" type="text" value="{{old('acuerdos_adicionales')}}" id="acuerdos_adicionales" required minlength="4" pattern="[A-Za-z0-9 ]{4,250}" title="Este campo es obligatorio, puede contener letras, números y espacios. Y entre 4 y 250 caracteres"> </textarea>
                @if ($errors->has('acuerdos_adicionales'))
                    <span class="help-block" style="color:#F3A100"; >
                        <small><i>{{ $errors->first('acuerdos_adicionales') }}</i></small>
                    </span>
                @endif 
            </div>


            
        </div>
    
    </div>
  
    <div class="centrar">
        <div class="form-group col-sm-12">
            <input class="btn btn-primary" type="submit" value="Guardar">
            <a href={{ route('clientes.index') }} class="btn btn-default">Cancelar</a>
        </div>
    </div>

    <div class="form-group col-sm-2"></div>
    </div>

</div>


<!-- Script BuscarDepartamento -->
<script>
    $(document).ready(function(){
        function cargarDepartamento() {
            var pais_id = $('#pais_id').val();
            if ($.trim(pais_id) != '') {
				console.log("llamare a findMunicipio con ",pais_id);
                $.get('{!!URL::to('buscarDepartamento')!!}', {id_pais: pais_id}, function (data) {
					console.log("departamentos = ",data);
                    var old = $('#departamento_id').data('old') != '' ? $('#departamento_id').data('old') : '';

                    $('#departamento_id').empty();
                    $('#departamento_id').append("<option value='' selected> Selecciona una </option>");

					
                    $.each(data, function (index, value) {

                        $('#departamento_id').append("<option value='" + value.id_departamento + "'" + (old == value.id_departamento ? 'selected' : '') + ">" + value.nombre_departamento+"</option>");
                    })
                });
            }
        }
        cargarDepartamento();
        $('#pais_id').on('change', cargarDepartamento);
    });

</script>

<!-- Script BuscarMunicipio-->
<script>
    $(document).ready(function(){
        function cargarMunicipio() {
            var departamento_id = $('#departamento_id').val();
            if ($.trim(departamento_id) != '') {
				console.log("llamare a findMunicipio con ",departamento_id);
                $.get('{!!URL::to('buscarMunicipio')!!}', {id_departamento: departamento_id}, function (data) {
					console.log("municipios = ",data);
                    var old = $('#municipio_id').data('old') != '' ? $('#municipio_id').data('old') : '';

                    $('#municipio_id').empty();
                    $('#municipio_id').append("<option value='' selected> Selecciona una </option>");

					
                    $.each(data, function (index, value) {

                        $('#municipio_id').append("<option value='" + value.id_municipio + "'" + (old == value.id_municipio ? 'selected' : '') + ">" + value.nombre_municipio+"</option>");
                    })
                });
            }
        }
        cargarMunicipio();
        $('#departamento_id').on('change', cargarMunicipio);
    });

</script>
