@if(auth()->user()->hasRoles(['1','3']))
@extends('layouts.app')

        @section('content')
               
        <div>
                @include('flash::message')
                @include('clientes.table')
                
        </div>
        @endsection
@else
<h1> No tiene permisos </h1>
@endif
    