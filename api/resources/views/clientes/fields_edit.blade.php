@csrf

<div class="form-group col-sm-12">
    <div class="form-group col-sm-2"> </div>
    <div class="form-group col-sm-8">
    
<div>
    <br> <h4 class="help-block" style="color:#F3A100";><strong > Información Básica </strong></h4> <hr>
            
    <!-- NIT-->
    <div class="form-group col-sm-6">
        <label for="nit_cliente">NIT :</label>
        <input class="form-control" value="{{old('nit_cliente', $cliente->nit_cliente)}}" name="nit_cliente" type="text" id="nit_cliente" required>
        @if ($errors->has('nit_cliente'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('nit_cliente') }}</i></small>
            </span>
        @endif
    </div>

    <!-- Nombre del cliente-->
    <div class="form-group col-sm-6">
        <label for="nombre_cliente">Nombre del cliente :</label>
        <input class="form-control" value="{{old('nombre_cliente', $cliente->nombre_cliente)}}" name="nombre_cliente" type="text" id="nombre_cliente" required>
        @if ($errors->has('nombre_cliente'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('nombre_cliente') }}</i></small>
            </span>
        @endif
    </div>

    <!-- Descripción del cliente-->
    <div class="form-group col-sm-12">
        <label for="descripcion">Breve descripción del cliente :</label>
        <input class="form-control" value="{{old('descripcion', $cliente->descripcion)}}" name="descripcion" type="text" id="descripcion" required>
        @if ($errors->has('descripcion'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('descripcion') }}</i></small>
            </span>
        @endif
    </div>

    <!-- Sector -->
    <div class="form-group col-sm-6">
        <label for="sector_id">Sector:</label>
        <select class="form-control" id="sector_id" name="sector_id">
            @foreach($sectores as $key => $value)
                <option value="{{ $key }}" {{ ( $key == $sectorSeleccionado) ? 'selected' : '' }}> 
                    {{ $value }} 
            @endforeach
        </select>
            @if ($errors->has('sector_id'))
                <span class="help-block" style="color:#F3A100"; >
                    <small><i>{{ $errors->first('sector_id') }}</i></small>
                </span>
            @endif
    </div>

        <!-- Pais Field -->
    <div class="form-group col-sm-6">
        <label for="pais_id">Pais</label>
        <select class="form-control" id="pais_id" name="pais_id">
            <option value= ''> Selecciona uno </option>
            @foreach($paises as $key => $value)
                <option value="{{ $key }}" {{ ( $key == $paisSeleccionado) ? 'selected' : '' }}> 
                {{ $value }} 
            @endforeach
        </select>
        @if ($errors->has('pais_id'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('pais_id') }}</i></small>
            </span>
        @endif
    </div>

    <!-- Departamento Field -->
    <div class="form-group col-sm-6">
        <label for="departamento_id">Departamento</label>
        <select class="form-control" data-old= {{ $departamentoSeleccionado}} id="departamento_id" name="departamento_id">
            <option value= ''> </option>
        </select>
        @if ($errors->has('departamento_id'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('departamento_id') }}</i></small>
            </span>
        @endif
    </div>

    <!-- Municipio Field -->
    <div class="form-group col-sm-6">
        <label for="municipio_id">Municipio</label>
        <select class="form-control" data-old= {{ $municipioSeleccionado}} id="municipio_id" name="municipio_id">
            <option value= ''> </option>
        </select>
        @if ($errors->has('municipio_id'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('municipio_id') }}</i></small>
            </span>
        @endif
    </div>

    <!-- Dirección -->
    <div class="form-group col-sm-6">
        <label for="direccion_cliente">Dirección :</label>
        <input class="form-control" value="{{old('direccion_cliente', $cliente->direccion_cliente)}}" name="direccion_cliente" type="text" id="direccion_cliente" required>
        @if ($errors->has('direccion_cliente'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('direccion_cliente') }}</i></small>
            </span>
        @endif
    </div>

    
    <!-- Plazo pago facturas-->
    <div class="form-group col-sm-6">
        <label for="plazo_pagoFactura">Plazo para pago Facturas :</label>
        <input class="form-control" value="{{old('plazo_pagoFactura', $cliente->plazo_pagoFactura)}}" name="plazo_pagoFactura" type="text" id="plazo_pagoFactura" required>
        @if ($errors->has('plazo_pagoFactura'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('plazo_pagoFactura') }}</i></small>
            </span>
        @endif 
    </div>
    

</div> 

<div>

    <br> <h4 class="help-block" style="color:#F3A100";> <strong> Información de Pruebas </strong></h4> <hr>

    <!-- Modelo de Servicio -->
    <div class="form-group col-sm-6">
        <label for="modeloServicio_id">Modelo de Servicio :</label>
        <select class="form-control" id="modeloServicio_id" name="modeloServicio_id">
            @foreach($modeloServicio as $key => $value)
                <option value="{{ $key }}" {{ ( $key == $modeloSeleccionado) ? 'selected' : '' }}> 
                    {{ $value }} 
            @endforeach
        </select>
            @if ($errors->has('modeloServicio_id'))
                <span class="help-block" style="color:#F3A100"; >
                    <small><i>{{ $errors->first('modeloServicio_id') }}</i></small>
                </span>
        @endif
    </div>

    <!-- Nombre del Contacto de Pruebas-->
    <div class="form-group col-sm-6">
        <label for="nombre_contactoPruebas">Nombre del Contacto de Pruebas :</label>
        <input class="form-control" value="{{old('nombre_contactoPruebas', $cliente->nombre_contactoPruebas)}}" name="nombre_contactoPruebas" type="text" id="nombre_contactoPruebas" required>
        @if ($errors->has('nombre_contactoPruebas'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('nombre_contactoPruebas') }}</i></small>
            </span>
        @endif
    </div>

    <!-- Telefono del Contacto de Pruebas-->
    <div class="form-group col-sm-6">
        <label for="telefono_contactoPruebas">Telefono del Contacto de Pruebas :</label>
        <input class="form-control" value="{{old('telefono_contactoPruebas', $cliente->telefono_contactoPruebas)}}" name="telefono_contactoPruebas" type="text" id="telefono_contactoPruebas" required>
        @if ($errors->has('telefono_contactoPruebas'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('telefono_contactoPruebas') }}</i></small>
            </span>
        @endif
    </div>

    <!-- Correo del Contacto de Pruebas-->
    <div class="form-group col-sm-6">
        <label for="correo_contactoPruebas">Correo del Contacto de Pruebas :</label>
        <input class="form-control" value="{{old('correo_contactoPruebas', $cliente->correo_contactoPruebas)}}" name="correo_contactoPruebas" type="text" id="correo_contactoPruebas" required>
        @if ($errors->has('correo_contactoPruebas'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('correo_contactoPruebas') }}</i></small>
        </span>
        @endif
    </div>

</div>

<div>
    <br> <h4 class="help-block" style="color:#F3A100";> <strong> Información de Facturación </strong> </h4> <hr>

    <!-- Nombre del Contacto de Facturación-->
    <div class="form-group col-sm-6">
        <label for="nombre_contactoFacturacion">Nombre del Contacto de Facturación :</label>
        <input class="form-control" value="{{old('nombre_contactoFacturacion', $cliente->nombre_contactoFacturacion)}}" name="nombre_contactoFacturacion" type="text" id="nombre_contactoFacturacion" required>
        @if ($errors->has('nombre_contactoFacturacion'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('nombre_contactoFacturacion') }}</i></small>
            </span>
        @endif  
    </div>

    <!-- Telefono del Contacto de Facturación-->
    <div class="form-group col-sm-6">
        <label for="telefono_contactoFacturacion">Telefono del Contacto de Facturación :</label>
        <input class="form-control" value="{{old('telefono_contactoFacturacion', $cliente->telefono_contactoFacturacion)}}" name="telefono_contactoFacturacion" type="text" id="telefono_contactoFacturacion" required>
        @if ($errors->has('telefono_contactoFacturacion'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('telefono_contactoFacturacion') }}</i></small>
            </span>
    @endif  
    </div>

    <!-- Correo del Contacto de Facturacion-->
    <div class="form-group col-sm-6">
        <label for="correo_contactoFacturacion">Correo del Contacto de Facturación :</label>
        <input class="form-control" value="{{old('correo_contactoFacturacion', $cliente->correo_contactoFacturacion)}}" name="correo_contactoFacturacion" type="text" id="correo_contactoFacturacion" required>
        @if ($errors->has('correo_contactoFacturacion'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('correo_contactoFacturacion') }}</i></small>
            </span>
        @endif 
    </div>

    <!-- Correo para envio de facturas-->
    <div class="form-group col-sm-6">
        <label for="correo_envioFactura">Plazo para pago Facturas :</label>
        <input class="form-control" value="{{old('correo_envioFactura', $cliente->correo_envioFactura)}}" name="correo_envioFactura" type="text" id="correo_envioFactura" required>
        @if ($errors->has('correo_envioFactura'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('correo_envioFactura') }}</i></small>
            </span>
        @endif 
    </div>


    <!-- Fecha recibido facturas-->
    <div class="form-group col-sm-6">
        <label for="fecha_recibidoFactura">Fecha máxima recibido Facturas:</label>
        <input class="form-control" value="{{old('fecha_recibidoFactura', $cliente->fecha_recibidoFactura)}}" name="fecha_recibidoFactura" type="text" id="fecha_recibidoFactura" required>
        @if ($errors->has('fecha_recibidoFactura'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('fecha_recibidoFactura') }}</i></small>
            </span>
        @endif 
    </div>


    <!-- Adjuntos-->
    <div class="form-group col-sm-6">
        <label for="url_anexoCliente" >Anexos para el cliente</label>
        <small><input type="file" id="url_anexoCliente" name="url_anexoCliente[]" accept="image/png, image/jpeg, image/jpg, application/pdf" title="Este campo es obligatorio, debes adjuntar uno o más archivos" multiple= "multiple"></small>
        @if ($errors->has('url_anexoCliente'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('url_anexoCliente') }}</i></small>
        </span>
        @endif 
    </div>

    <!-- Acuerdos adicionales-->
    <div class="form-group col-sm-6">
        <label for="acuerdos_adicionales">Comentarios y/o acuerdos adicionales :</label>
        <input class="form-control" value="{{old('acuerdos_adicionales', $cliente->acuerdos_adicionales)}}" name="acuerdos_adicionales" type="text" id="acuerdos_adicionales" required>
        @if ($errors->has('acuerdos_adicionales'))
            <span class="help-block" style="color:#F3A100"; >
                <small><i>{{ $errors->first('acuerdos_adicionales') }}</i></small>
            </span>
        @endif 
    </div>
</div>

<div class="centrar">
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        <input class="btn btn-primary" type="submit" value="Guardar">
        <a href={{ route('clientes.index') }} class="btn btn-default">Cancelar</a>
    </div>
</div>

</div>

<div class="form-group col-sm-2"></div>
</div>

</div>



<!-- Script BuscarDepartamento -->
<script>
    $(document).ready(function(){
        function cargarDepartamento() {
            var pais_id = $('#pais_id').val();
            if ($.trim(pais_id) != '') {
				console.log("llamare a findMunicipio con ",pais_id);
                $.get('{!!URL::to('buscarDepartamento')!!}', {id_pais: pais_id}, function (data) {
					console.log("departamentos = ",data);
                    var old = $('#departamento_id').data('old') != '' ? $('#departamento_id').data('old') : '';

                    $('#departamento_id').empty();
                    $('#departamento_id').append("<option value='' selected> Selecciona una </option>");

					
                    $.each(data, function (index, value) {

                        $('#departamento_id').append("<option value='" + value.id_departamento + "'" + (old == value.id_departamento ? 'selected' : '') + ">" + value.nombre_departamento+"</option>");
                    })
                });
            }
        }
        cargarDepartamento();
        $('#pais_id').on('change', cargarDepartamento);
    });

</script>

<!-- Script BuscarMunicipio-->
<script>
    $(document).ready(function(){
        function cargarMunicipio() {
        	let departamentoEle = $('#departamento_id');
            var departamento_id = $('#departamento_id').val();
            let departamento_old = departamentoEle.data('old');
            if ($.trim(departamento_id) != '' || $.trim(departamento_old) != '') {
				//console.log("llamare a findMunicipio con ",departamento_id);
                $.get('{!!URL::to('buscarMunicipio')!!}', {id_departamento: (departamento_id !== '' ? departamento_id: departamento_old )}, function (data) {
					console.log("municipios = ",data);
                    var old = $('#municipio_id').data('old') != '' ? $('#municipio_id').data('old') : '';

                    $('#municipio_id').empty();
                    $('#municipio_id').append("<option value='' selected> Selecciona una </option>");


                    $.each(data, function (index, value) {

                        $('#municipio_id').append("<option value='" + value.id_municipio + "'" + (old == value.id_municipio ? 'selected' : '') + ">" + value.nombre_municipio+"</option>");
                    })
                });
            }
        }
        cargarMunicipio();
        $('#departamento_id').on('change', cargarMunicipio);
    });

</script>

