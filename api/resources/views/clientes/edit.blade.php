@extends('layouts.app')

@section('content')

<div>
    <h3 class="centrar"> <strong> Editar cliente </strong></h3>
</div> <br>

@foreach($clientes as $cliente)
<form action="{{ route('clientes.update', $cliente->id_cliente) }}" method="POST" enctype="multipart/form-data">

    @method('PUT')
    @include('clientes.fields_edit')

</form>
@endforeach
@endsection
