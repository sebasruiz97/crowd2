<div class="form-group col-sm-12">

    <div class="form-group col-sm-12">

<div>
    <div class="form-group col-sm-12">
        <div class="form-group col-sm-3">
            <br> <h4 class="help-block" style="color:#304457";> <strong> Datos Básicos del cliente </strong></h4><hr>

                <!-- Nombre del Cliente-->
                <div class="form-group">
                    <label for="nombre_cliente">Nombre del Cliente:</label>
                    <p>{{ $cliente->nombre_cliente }}</p>
                </div>

                <!-- Breve descripción del cliente-->
                <div class="form-group">
                    <label for="descrpcion">Breve descripción del cliente:</label>
                    <p>{{ $cliente->descripcion }}</p>
                </div>

                <!-- Sector-->
                <div class="form-group">
                    <label for="nombre_cliente">Nombre del Cliente:</label>
                    <p>{{ $cliente->sector->nombre_sector }}</p>
                </div>

                <!-- Nit del cliente-->
                <div class="form-group">
                    <label for="nit_cliente">NIT:</label>
                    <p>{{ $cliente->nit_cliente }}</p>
                </div>

                <!-- Pais-->
                <div class="form-group">
                    <label for="pais_id">Pais:</label>
                    <p>{{ $cliente->pais->nombre_pais }}</p>
                </div>

                <!-- Departamento-->
                <div class="form-group">
                    <label for="departamento_id">Departamento:</label>
                    <p>{{ $cliente->departamento->nombre_departamento }}</p>
                </div>

                 <!-- Municipio-->
                 <div class="form-group">
                    <label for="municipio_id">Municipio:</label>
                    <p>{{ $cliente->municipio->nombre_municipio }}</p>
                </div>

                <!-- Dirección-->
                <div class="form-group">
                    <label for="direccion_cliente">Dirección del cliente:</label>
                    <p>{{ $cliente->direccion_cliente }}</p>
                </div>
        </div>

        <div class="form-group col-sm-3">
            <br> <h4 class="help-block" style="color:#304457";> <strong> Información de Pruebas </strong> </h4> <hr>


            <!-- Modelo de Servicio-->
            <div class="form-group">
                <label for="modeloServicio_id">Modelo de Servicio:</label>
                <p>{{ $cliente->tipoModelo->nombre_modeloServicio }}</p>
            </div>

            <!-- Nombre Contacto Pruebas -->
            <div class="form-group">
                <label for="nombre_contactoPruebas">Nombre Contacto Pruebas:</label>
                <p>{{ $cliente->nombre_contactoPruebas }}</p>
            </div>

            <!-- Telefono Contacto Pruebas -->
            <div class="form-group">
                <label for="telefono_contactoPruebas">Telefono Contacto Pruebas:</label>
                <p>{{ $cliente->telefono_contactoPruebas }}</p>
            </div>

            <!-- Correo Contacto Pruebas -->
            <div class="form-group">
                <label for="correo_contactoPruebas">Correo Contacto Pruebas:</label>
                <p>{{ $cliente->correo_contactoPruebas }}</p>
            </div>
        </div>

        <div class="form-group col-sm-3">

            <br> <h4 class="help-block" style="color:#304457";> <strong> Información de Facturación </strong> </h4> <hr>

            <!-- Nombre Contacto Facturacion -->
            <div class="form-group">
                <label for="nombre_contactoFacturacion">Nombre Contacto Facturacion:</label>
                <p>{{ $cliente->nombre_contactoFacturacion }}</p>
            </div>

            <!-- Telefono Contacto Facturacion -->
            <div class="form-group">
                <label for="telefono_contactoFacturacion">Telefono Contacto Facturacion:</label>
                <p>{{ $cliente->telefono_contactoFacturacion }}</p>
            </div>

            <!-- Correo Contacto Facturacion -->
            <div class="form-group">
                <label for="correo_contactoFacturacion">Correo Contacto Pruebas:</label>
                <p>{{ $cliente->correo_contactoPruebas }}</p>
            </div>

            <!-- Fecha Recibido Factura -->
            <div class="form-group">
                <label for="fecha_recibidoFactura">Fecha máxima para recibir facturas</label>
                <p>{{ $cliente->fecha_recibidoFactura }}</p>
            </div>

            <!-- Correo envio de Facturas -->
            <div class="form-group">
                <label for="correo_envioFactura">Correo para envio de facturas</label>
                <p>{{ $cliente->correo_envioFactura }}</p>
            </div>

            <!-- Plazo de Facturación -->
            <div class="form-group">
                <label for="plazo_pagoFactura">Plazo para pago de facturas</label>
                <p>{{ $cliente->plazo_pagoFactura }}</p>
            </div>

            <!-- Acuerdos adicionales -->
            <div class="form-group">
                <label for="acuerdos_adicionales">Acuerdos adicionales</label>
                <p>{{ $cliente->acuerdos_adicionales }}</p>
            </div>
        </div>

        <div class="form-group col-sm-3">

            <br> <h4 class="help-block" style="color:#304457";> <strong> Anexos cargados para el cliente </strong> </h4> <hr>

            <div class="container.fluid py-0 px-1">
                <div class="row">
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                    <div class="col-sm col-12 col-lg-12">

                        @if ($anexosCliente->isEmpty()) 
                            No se han agregado anexos para el cliente
                        @else
                        <div class="table-responsive">
                            <table class="table" id="anexos-table" >
                                <thead>
                                    <tr>
                                        <th>Tipo de Anexo</th>
                                        <th colspan="3">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
            
                                   
                                    @foreach($anexosCliente as $anexo)
                                    <tr>
                                        <td>{{ $anexo->url_anexoCliente }}</td>
                                        <td>
            
                                            <form
                                                action="{{ route('borrarAnexoCliente',$anexo->id_anexoCliente) }}"
                                                method="POST">
                                                @csrf

                                                <div class='pull left'>
                                                    <a href="{{ route('verAnexoCliente',$anexo->id_anexoCliente) }}" class="btn btn-default btn-xs" style="margin-rigth: 3px" ><i class="lnr lnr-download"></i></a>
                                                    <button type="submit" class="btn btn-danger btn-xs" style="margin-left: 3px" onclick = "return confirm('¿Estás seguro?')"><i class="lnr lnr-cross"></i></button>
                                                </div>
                                            </form>
            
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                        @endif
                    </div>
                    <div class="col-sm col-1 col-lg-1">
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
    <div>

    </div>
</div>
