
<div class="container.fluid py-1 px-1">

    <h3 class="centrar" style="margin-top:5px;margin-bottom: 10px; "> Clientes registrados </h3>
    <br> <br>

    <div class="pull-left" style="padding-left: 25px">
    <nav class="navbar navbar-light float-right">
        <form class="form-inline">
    
        <input name="buscarpor" class="form-control mr-sm-2" type="search" placeholder="Buscar por nombre" aria-label="Search">
    
            <button class="btn btn-default btn-xs" type="submit">Buscar</button>
            <a href={{ route('clientes.index') }} class="btn btn-default btn-xs">Ver todos</a>
        </form>
    </nav>
</div>
    
<div class="col-sm col-12 col-lg-12">
    <div class="col-sm col-1 col-lg-1">
    </div>

    <div class="col-sm col-12 col-lg-12">
    
        <div class="table-responsive">
    <table class="table" id="clientes-table">
        

        @if(auth()->user()->hasRoles(['1']))
        <div class="pull-right">
            <a class="btn btn-default btn-xs pull-right" style="margin-top:10px;margin-bottom: 10px; "
            href="{{ route('clientes.create') }}">Añadir otro</a>
        </div>
        @endif
    
    
        <thead>
            <tr>
                <th>NIT</th>
                <th>Nombre Cliente</th>
                <th>Breve Descripción</th>
                <th>Sector</th>
                <th>Departamento</th>
                <th>Municipio</th>
                <th>Modelo de Servicio</th>
                <th>Fecha de Creación</th>
                <th colspan="3">Acciones</th>
            </tr>
        </thead>
        <tbody>

            @foreach($clientes as $cliente)
            <tr>
                <td>{{ $cliente->nit_cliente }}</td>
                <td>{{ $cliente->nombre_cliente }}</td>
                <td>{{ $cliente->descripcion }}</td>
                <td>{{ $cliente->sector->nombre_sector }}</td>
                <td>{{ $cliente->departamento->nombre_departamento }}</td>
                <td>{{ $cliente->municipio->nombre_municipio }}</td>
                <td>{{ $cliente->tipoModelo->nombre_modeloServicio }}</td>
                <td>{{ $cliente->created_at }}</td>
                
                <td>

                    
                    <form action="{{ route('clientes.destroy',$cliente->id_cliente) }}" method="POST">
                        @csrf
                        @method('DELETE')
                    
                        <div class='btn-group'>
                            <a href="{{ route('clientes.show',$cliente->id_cliente) }}" class="btn btn-default btn-xs" style="margin-rigth: 3px" ><i class="lnr lnr-magnifier"></i></a>
                            @if(auth()->user()->hasRoles(['1']))
                            <a  href="{{ route('clientes.edit',$cliente->id_cliente) }}" class="btn btn-default btn-xs" style="margin-left: 3px"><i class="lnr lnr-pencil"></i></a>
                            <button type="submit" class="btn btn-danger btn-xs" style="margin-left: 3px" onclick = "return confirm('¿Estás seguro?')"><i class="lnr lnr-cross"></i></button>
                            @endif
                        </div>
                    </form>
                    
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="pull-right">
        <small> {{$clientes->links()}} </small>
    </div>

    <div class="col-sm col-1 col-lg-1">
    </div>
</div>
</div>






