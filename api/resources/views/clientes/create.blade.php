@extends('layouts.app')

@section('content')

<div>
    <h3 class="centrar"> <strong> Añadir nuevo cliente </strong></h3>
</div> <br>

<form method="POST" action="{{ route('clientes.store') }}" enctype="multipart/form-data">

    @include('clientes.fields_create')

</form>
@endsection