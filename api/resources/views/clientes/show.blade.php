@extends('layouts.app')

@section('content')

    <div class="row" style="padding-right: 10px">
        <a href="{{ route('clientes.index') }}" class="btn btn-default btn-xs pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px">Volver</a>
    </div>
                    
         @foreach($clientes as $cliente)
            @include('clientes.fields_show')
        @endforeach
              
@endsection
