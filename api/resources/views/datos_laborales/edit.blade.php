@extends('layouts.app2')

@section('content')

<div>
    <h3 class="centrar"> <strong> Editar mi experiencia laboral </strong></h3>
</div> <br>

@foreach($datosLaborales as $datoLaboral)
<form action="{{ route('datosLaborales.update', $datoLaboral->id_datoLaboral) }}" method="POST" id="actualizar_datoLaboral">

    @method('PUT')
    @include('datos_laborales.fields_edit')

</form>
@endforeach
@endsection