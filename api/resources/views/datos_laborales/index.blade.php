@extends('layouts.app2')

@section('content')

@if ($datosLaborales->isEmpty())
<h1 class="pull-right">
    <a class="btn btn-default btn-xs pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px"
        href="{{ route('datosLaborales.create') }}">Añadir experiencia laboral</a>
</h1>
<br>
    <br>
    <br>
    <br>
    <br>
<div  style="text-align:center">
    <h3>Aún no has agregado tus datos laborales</h3>
 <h5>
	Aunque esta información no es necesaria para quedar activo en la plataforma, nos sirve para contactarte cuando realicemos búsquedas para proyectos específicos.
</h5>
    <div>
@else
    <h3  style="text-align:center"> Información Laboral </h3>
    <h1 class="pull-right">
        <a class="btn btn-default btn-xs pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px"
            href="{{ route('datosLaborales.create') }}">Añadir otra</a>
    </h1>
    @include('flash::message')
    @include('datos_laborales.table')
    @endif
    @endsection