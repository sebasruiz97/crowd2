@csrf

<style>
    /* The container */
    .container1 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 12px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default checkbox */
    .container1 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        background-color: #E6E6E6;

    }

    /* On mouse-over, add a grey background color */
    .container1:hover input~.checkmark {
        background-color: #304457;
    }

    /* When the checkbox is checked, add a blue background */
    .container1 input:checked~.checkmark {
        background-color: #304457;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .container1 input:checked~.checkmark:after {
        display: block;
    }

    /* Style the checkmark/indicator */
    .container1 .checkmark:after {
        left: 9px;
        top: 5px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>

<script>
    function comprobar() {
        if (document.getElementById("trabajo_actual").checked) {
            document.getElementById('fechaFin_datoLaboral').disabled = true;
            document.getElementById('fechaFin_datoLaboral').style.display = "none";
        } else {
            document.getElementById('fechaFin_datoLaboral').disabled = false;
            document.getElementById('fechaFin_datoLaboral').style.display = "block";
        }
    }
    $(window).on('load', function() {
        comprobar();
    });
</script>

<div class="form-group col-sm-12">
    <div class="form-group col-sm-2"> </div>
    <div class="form-group col-sm-8">


        <!-- Empresa -->
        <div class="form-group col-sm-6">
            <label for="empresa">¿En que empresa laboraste?*</label>
            <input class="form-control" value="{{old('empresa', $datoLaboral->empresa)}}" name="empresa" type="text" id="empresa" required>
            @if ($errors->has('empresa'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('empresa') }}</i></small>
            </span>
            @endif
        </div>

        <!-- Sector -->
        <div class="form-group col-sm-6">
            <label for="sector_id">¿A que sector pertenece la empresa?*</label>
            <select class="form-control" id="sector_id" name="sector_id">
                <option value=''> Selecciona uno </option>
                @foreach($sector as $key => $value)
                <option value="{{ $key }}" {{ ( $key == $sectorSeleccionado) ? 'selected' : '' }}>
                    {{ $value }}
                    @endforeach
            </select>
            @if ($errors->has('sector_id'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('sector_id') }}</i></small>
            </span>
            @endif
        </div>

        <!-- Cargo -->
        <div class="form-group col-sm-6">
            <label for="cargo_id">¿Que cargo tenías en la empresa?*</label>
            <select class="form-control" id="cargo_id" name="cargo_id">
                <option value=''> Selecciona uno </option>
                @foreach($cargo as $key => $value)
                <option value="{{ $key }}" {{ ( $key == $cargoSeleccionado) ? 'selected' : '' }}>
                    {{ $value }}
                    @endforeach
            </select>
            @if ($errors->has('cargo_id'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('cargo_id') }}</i></small>
            </span>
            @endif
        </div>

        <!-- Fecha de Inicio -->
        <div class="form-group col-sm-6">
            <label for="fechaInicio_datoLaboral">¿Cuando inició tu relación laboral con esta empresa?*</label>
            <input class="form-control" value="{{old('fechaInicio_datoLaboral', $datoLaboral->fechaInicio_datoLaboral)}}" name="fechaInicio_datoLaboral" type="month" id="fechaInicio_datoLaboral">
            @if ($errors->has('fechaInicio_datoLaboral'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('fechaInicio_datoLaboral') }}</i></small>
            </span>
            @endif
        </div>

        <!-- Fecha Fin -->
        <div class="form-group col-sm-6">
            <label for="fechaFin_datoLaboral">¿Cuando finalizo tu relación laboral con esta empresa?</label>
            <input class="form-control" value="{{old('fechaFin_datoLaboral', $datoLaboral->fechaFin_datoLaboral)}}" name="fechaFin_datoLaboral" type="month" id="fechaFin_datoLaboral">
            @if ($errors->has('fechaFin_datoLaboral'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('fechaFin_datoLaboral') }}</i></small>
            </span>
            @endif
            <br>
            <label class="container1" for="trabajo_actual"> Trabajo actualmente
                <input type="checkbox" id="trabajo_actual" name="trabajo_actual" onChange="comprobar(this);" accepted
                {{ (old('trabajo_actual', $datoLaboral->trabajo_actual) != null ) ? 'checked' : '' }}>
                <span class="checkmark"></span>
            </label>
        </div>

        <!-- Responsabilidades-->
        <div class="form-group col-sm-6">
            <label for="responsabilidades">¿Cuáles eran tus principales responsabilidades?</label>
            <input class="form-control" name="responsabilidades" value="{{old('responsabilidades', $datoLaboral->responsabilidades)}}" id="responsabilidades" cols="10" rows="1" style="resize: both">
            @if ($errors->has('responsabilidades'))
            <span class="help-block" style="color:#F3A100" ;>
                <small><i>{{ $errors->first('responsabilidades') }}</i></small>
            </span>
            @endif
        </div>

        <!-- UserID Field -->
        <div class="form-group col-sm-6">
            <label for="user_id"> </label>
            <input class="form-control" name="user_id" type="hidden" value={{auth()->user()->id}} readonly="readonly">
        </div>

        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <div class="centrar">
                <input class="btn btn-primary" type="submit" value="Guardar">
                <a href={{ route('datosLaborales.index') }} class="btn btn-default">Cancelar</a>
            </div>
        </div>

    </div>
</div>
<div class="form-group col-sm-2"></div>
</div>

@section('scripts')
<script type="text/javascript">
    $('#fechaInicio_datoLaboral').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent: false
    })
</script>

<script type="text/javascript">
    $('#fechaFin_datoLaboral').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent: false
    })
</script>


@endsection