@extends('layouts.app2')

@section('content')

<div>
    <h3 class="centrar"> <strong> Añadir mi experiencia laboral </strong></h3>
</div> <br>

<form method="POST" action="{{ route('datosLaborales.store') }}">

    @include('datos_laborales.fields_create')

</form>
@endsection