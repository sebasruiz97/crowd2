<!-- Empresa -->
<div class="form-group">
    <label for="empresa">Empresa:</label>
    <p>{{ $datoLaboral->empresa }}</p>
</div>

<!-- Sector -->
<div class="form-group">
    <label for="empresa">Sector:</label>
    <p>{{ $datoLaboral->sector->nombre_sector }}</p>
</div>

<!-- Cargo -->
<div class="form-group">
    <label for="cargo">Cargo:</label>
    <p>{{ $datoLaboral->cargo->nombre_cargo }}</p>
</div>

<!-- Fecha Inicio-->
<div class="form-group">
    <label for="fechaInicio_datoLaboral">Fecha de Inicio:</label>
    <p>{{ $datoLaboral->fechaInicio_datoLaboral }}</p>
</div>

<!-- Fecha Fin-->
<div class="form-group">
    <label for="fechaFin_datolaboral">Fecha Fin:</label>
    <p>{{ $datoLaboral->fechaFin_datolaboral }}</p>
</div>


<!-- Responsabilidades-->
<div class="form-group">
    <label for="responsabilidades">Responsabilidades:</label>
    <p>{{ $datoLaboral->responsabilidades }}</p>
</div>