@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Experiencia Laboral
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    
                    @foreach($datosLaborales as $datoLaboral)
                    @include('datos_laborales.fields_show')
                    @endforeach
                    <a href="{{ route('datosLaborales.index') }}" class="btn btn-default">Volver</a>
                </div>
            </div>
        </div>
    </div>
@endsection
