
<div class="container.fluid py-0 px-1">
    <div class="row" >
        <div class="col-sm col-1 col-lg-1">
        </div>
        <div class="col-sm col-12 col-lg-12">
            <div class="table-responsive">
                <table class="table" id="datosLaborales-table">
                    <thead>
                        <tr>
                            <th>Empresa</th>
                            <th>Sector</th>
                            <th>Cargo</th>
                            <th>Fecha de Inicio</th>
                            <th>Fecha Fin</th>
                            <th>Responsabilidades</th>        
                            <th colspan="3">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($datosLaborales as $datoLaboral)
                        <tr>
                            <td>{{ $datoLaboral->empresa }}</td>
                            <td>{{ $datoLaboral->sector->nombre_sector }}</td>
                            <td>{{ $datoLaboral->cargo->nombre_cargo }}</td>
                            <td>{{ $datoLaboral->fechaInicio_datoLaboral }}</td>
                            <td>
                                @if(($datoLaboral->fechaFin_datoLaboral) === null)
                                    Trabajo actual
                                @else
                                {{ $datoLaboral->fechaFin_datoLaboral }}
                                @endif
                            </td>
                            <td>{{ $datoLaboral->responsabilidades }}</td>
                            <td>

                                <form action="{{ route('datosLaborales.destroy',$datoLaboral->id_datoLaboral) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                
                                    <div class='pull-center'>
                                        <a  href="{{ route('datosLaborales.edit',$datoLaboral->id_datoLaboral) }}" class="btn btn-default btn-xs"><i class="lnr lnr-pencil"></i></a>
                                        <button type="submit" class="btn btn-danger btn-xs" onclick = "return confirm('¿Estás seguro?')"><i class="lnr lnr-cross"></i></button>
                                    </div>
                                </form>
                                
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pull-right">
                    <small>{{ $datosLaborales->links() }}</small>
                </div>
            </div>
        </div>
        <div class="col-sm col-1 col-lg-1">
        </div>
    </div>
</div>



