<div class="container.fluid py-0 px-1">
    <div class="row" >
        <div class="col-sm col-1 col-lg-1">
        </div>
        <div class="col-sm col-12 col-lg-12">
            <div class="table-responsive">
                <table class="table" id="estudioFormal-table">
                    <thead>
                        <tr>
                            <th>Tipo de Estudio</th>
                            <th>Nombre de Estudio</th>
                            <th>Fecha de Finalización</th>
                            <th>Duración en meses</th>
                            <th>Lugar de estudio</th>
                            <th>Universidad, Instituto o Plataforma</th>
                            <th colspan="3">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($estudiosNoFormales as $estudioNoFormal)
                        <tr>
                            <td>{{ $estudioNoFormal->tipoEstudio->nombre_tipoEstudio }}</td>
                            <td>{{ $estudioNoFormal->nombre_estudioNoFormal}}</td>
                            <td>{{ $estudioNoFormal->fechaFin_estudioNoFormal }}</td>
                            <td>{{ $estudioNoFormal->duracion }}</td>
                            <td>{{ $estudioNoFormal->lugar_estudio }}</td>
                            <td>
                                @if(($estudioNoFormal->universidad_id)==361||($estudioNoFormal->plataforma_id)==6)
                                    {{ $estudioNoFormal->otra_universidad }}
                                    @elseif(($estudioNoFormal->universidad_id)==null)
                                        {{ $estudioNoFormal->plataforma->nombre_plataforma }}
                                    @else
                                        {{ $estudioNoFormal->universidad->nombre_universidad }}
                                @endif                                    
                            </td>
                            <td>

                                <form action="{{ route('estudiosNoFormales.destroy',$estudioNoFormal->id_estudioNoFormal) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                
                                    <div class='pull-center'>
                                        <a  href="{{ route('estudiosNoFormales.edit',$estudioNoFormal->id_estudioNoFormal) }}" class="btn btn-default btn-xs"><i class="lnr lnr-pencil"></i></a>
                                        <button type="submit" class="btn btn-danger btn-xs" onclick = "return confirm('¿Estás seguro?')"><i class="lnr lnr-cross"></i></button>
                                    </div>
                                </form>
                                
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pull-right">
                    <small>{{ $estudiosNoFormales->links() }}</small>
                </div>
            </div>
        </div>
        <div class="col-sm col-1 col-lg-1">
        </div>
    </div>
</div>



