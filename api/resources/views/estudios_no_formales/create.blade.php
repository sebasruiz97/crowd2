@extends('layouts.app2')

@section('content')

<div> 
    <h3 class="centrar"> <strong>  Añadir estudio no formal </strong></h3>
</div> <br>

                    <form method="POST" action="{{ route('estudiosNoFormales.store') }}">

                        @include('estudios_no_formales.fields_create')

                    </form>
@endsection
