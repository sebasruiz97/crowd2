@extends('layouts.app2')

@section('content')

<div> 
    <h3 class="centrar"> <strong>  Editar mi estudio no formal </strong></h3>
</div> <br>

                @foreach($estudiosNoFormales as $estudioNoFormal)
                        <form action="{{ route('estudiosNoFormales.update', $estudioNoFormal->id_estudioNoFormal) }}" method="POST" id="actualizar_EstudioNoFormal">

                            @method('PUT')
                            @include('estudios_no_formales.fields_edit')
                    
                        </form>
                @endforeach
@endsection

