<!-- Tipo de Estudio -->
<div class="form-group">
    <label for="tipoEstudio_id">Tipo de Estudio:</label>
    <p>{{ $estudioNoFormal->tipoEstudio->nombre_tipoEstudio }}</p>
</div>

<!-- Nombre de Estudio -->
<div class="form-group">
    <label for="nombre_estudioNoFormal">Nombre del Estudio:</label>
    <p>{{ $estudioNoFormal->nombre_estudioNoFormal }}</p>
</div>

<!-- Fecha de finalización del estudio -->
<div class="form-group">
    <label for="fechaFin_estudioNoFormal">Fecha de finalización:</label>
    <p>{{ $estudioNoFormal->fechaFin_estudioNoFormal }}</p>
</div>

<!-- Duración -->
<div class="form-group">
    <label for="duracion">Duración:</label>
    <p>{{ $estudioNoFormal->duracion }}</p>
</div>

<!-- Lugar -->
<div class="form-group">
    <label for="lugar">Lugar:</label>
    <p>{{ $estudioNoFormal->lugar }}</p>
</div>