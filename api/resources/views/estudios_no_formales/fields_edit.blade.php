@csrf

<script>
    function habilitar(value)
    {
        if(value=="En una universidad o instituto" || value==true)
        {
            // habilitamos
            document.getElementById("universidad_id").disabled=false;
            document.getElementById("plataforma_id").disabled=true;

        }else if(value=="En una plataforma educativa" || value==false){
            // deshabilitamos
            document.getElementById("universidad_id").disabled=true;
            document.getElementById("plataforma_id").disabled=false;
        }
    }
</script>

<script type="text/javascript">
    function showDiv(select){
          if(select.value=="En una universidad o instituto"){
           document.getElementById('universidad_id').style.display = "block";
           document.getElementById('plataforma_id').style.display = "none";
          } else{
            document.getElementById('plataforma_id').style.display = "block";
            document.getElementById('universidad_id').style.display = "none";
          }
    } 
</script>

<script type="text/javascript">
    function showDivi(select){
          if(select.value==361){
           document.getElementById('otra_universidad').style.display = "block";
           document.getElementById('otra_universidad').disabled = false;
          } else{
            document.getElementById('otra_universidad').style.display = "none";
            document.getElementById('otra_universidad').disabled = true;
          }
    } 
</script>

<script type="text/javascript">
    function showDivid(select){
          if(select.value==6){
           document.getElementById('otra_universidad').style.display = "block";
           document.getElementById('otra_universidad').disabled = false;
          } else{
            document.getElementById('otra_universidad').style.display = "none";
            document.getElementById('otra_universidad').disabled = true;
          }
    } 
</script>

<div class="form-group col-sm-12">
    <div class="form-group col-sm-2"> </div>
    <div class="form-group col-sm-8">


<!-- Tipo de Estudio -->
<div class="form-group col-sm-6">
    <label for="tipoEstudio_id">¿Qué tipo de estudio es?*</label>
    <select class="form-control" id="tipoEstudio_id" name="tipoEstudio_id">
        <option value= ''> Selecciona uno  </option>
        @foreach($tipoEstudio as $key => $value)
             <option value="{{ $key }}" {{ ( $key == $tipoEstudioSeleccionado) ? 'selected' : '' }}> 
                {{ $value }} 
        @endforeach
    </select>
    @if ($errors->has('tipoEstudio_id'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('tipoEstudio_id') }}</i></small>
        </span>
    @endif
</div>

<!-- Nombre estudio -->
<div class="form-group col-sm-6">
    <label for="nombre_estudioNoFormal">¿Cuál es el nombre del estudio?*</label>
    <input class="form-control" value="{{old('nombre_estudioNoFormal', $estudioNoFormal->nombre_estudioNoFormal)}}" name="nombre_estudioNoFormal" type="text" id="nombre_estudioNoFormal" required >
    @if ($errors->has('nombre_estudioNoFormal'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('nombre_estudioNoFormal') }}</i></small>
        </span>
    @endif
</div>

<!-- Fecha Fin del Estudio-->
<div class="form-group col-sm-6">
    <label for="fechaFin_estudioNoFormal">¿Cuando finalizaste el estudio?*</label>
    <input type="month" class="form-control" value="{{old('fechaFin_estudioNoFormal', $estudioNoFormal->fechaFin_estudioNoFormal)}}" name="fechaFin_estudioNoFormal"  type="date" id="fechaFin_estudioNoFormal">
    @if ($errors->has('fechaFin_estudioNoFormal'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('fechaFin_estudioNoFormal') }}</i></small>
        </span>
    @endif
</div>

<!-- Duración estudio -->
<div class="form-group col-sm-6">
    <label for="duracion">¿Cuál fue la duración del estudio (en meses)?*</label>
    <input class="form-control" value="{{old('duracion', $estudioNoFormal->duracion)}}" name="duracion" type="text" id="duracion" required >
    @if ($errors->has('duracion'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('duracion') }}</i></small>
        </span>
    @endif
</div>

<!-- Lugar Estudio -->
<div class="form-group col-sm-6">
    <label for="lugar_estudio">¿Donde realizaste el estudio?*</label>
    <select class="form-control" id="lugar_estudio" name="lugar_estudio" onchange="showDiv(this)" onchange="habilitar(this)">
        <option value= ''> Selecciona uno  </option>
        <option value="En una universidad o instituto"@if($lugarSeleccionado=='En una universidad o instituto') selected='selected' @endif >En una universidad o instituto</option>
        <option value="En una plataforma educativa"@if($lugarSeleccionado=='En una plataforma educativa') selected='selected' @endif >En una plataforma educativa</option>
    </select>
    @if ($errors->has('lugar_estudio'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('modalidad_estudio') }}</i></small>
        </span>
    @endif
</div>

<!-- Universidad del Estudio -->
<div class="form-group col-sm-6" id="universidad_id" style="display:none;">
    <label for="universidad_id">¿En cual universidad o instituto lo realizaste?*</label>
    <select class="form-control" id="universidad_id" name="universidad_id" onchange="showDivi(this)">
        <option value= ''> Selecciona uno  </option>
        @foreach($universidad as $key => $value)
             <option value="{{ $key }}" {{ ( $key == $universidadSeleccionada) ? 'selected' : '' }}> 
                {{ $value }} 
        @endforeach
    </select>
    @if ($errors->has('universidad_id'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('universidad_id') }}</i></small>
        </span>
    @endif
</div>

<!-- Plataforma del Estudio -->
<div class="form-group col-sm-6" id="plataforma_id" style="display:none;">
    <label for="plataforma_id">¿En que plataforma lo realizaste?*</label>
    <select class="form-control" id="plataforma_id" name="plataforma_id" onchange="showDivid(this)">
        <option value= ''> Selecciona uno  </option>
        @foreach($plataforma as $key => $value)
             <option value="{{ $key }}" {{ ( $key == $plataformaSeleccionada) ? 'selected' : '' }}> 
                {{ $value }} 
        @endforeach
    </select>
    @if ($errors->has('plataforma_id'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('plataforma_id') }}</i></small>
        </span>
    @endif
</div>

<!-- Otra universidad -->
<div class="form-group col-sm-6" id="otra_universidad" style="display:none;" >
    <label for="otra_universidad">¿Cuál es el nombre de tu universidad, instituto o plataforma?*</label>
    <input class="form-control" name="otra_universidad" value="{{old('otra_universidad')}}" type="text" id="otra_universidad" pattern="[A-Za-z0-9 ]{4,15}" title="Puede contener letras, números y espacios. Y entre 4 y 15 caracteres" >
    @if ($errors->has('otra_universidad'))
        <span class="help-block" style="color:#F3A100"; >
            <small><i>{{ $errors->first('otra_universidad') }}</i></small>
        </span>
    @endif
</div>

<!-- UserID Field -->
<div class="form-group col-sm-6">
    <label for="user_id" > </label>
    <input class="form-control" name="user_id" type="hidden" value={{auth()->user()->id}} readonly="readonly" >
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <div class="centrar">
    <input class="btn btn-primary" type="submit" value="Guardar">
    <a href={{ route('estudiosNoFormales.index') }} class="btn btn-default">Cancelar</a>
    </div>
</div>

</div>
</div>
<div class="form-group col-sm-2"></div>
</div>

