
@extends('layouts.app2')

@section('content')

    @if ($estudiosNoFormales->isEmpty()) 
    <h1 class="pull-right">
        <a class="btn btn-default btn-xs pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px" href="{{ route('estudiosNoFormales.create') }}">Añadir estudios no formales</a>
    </h1>
<br>
    <br>
    <br>
    <br>
    <br>
    <div  style="text-align:center">
        <h3>Aún no has agregado tus estudios no formales</h3>
        <h5> Aunque esta información no es necesaria para quedar activo en la plataforma, nos sirve para contactarte cuando realicemos búsquedas para proyectos específicos. </h5>
    <div>
    @else
        <h3  style="text-align:center"> Estudios no Formales </h3>
        <h1 class="pull-right">
            <a class="btn btn-default btn-xs pull-right" style="margin-top: -10px;margin-bottom: 5px; margin: 10px" href="{{ route('estudiosNoFormales.create') }}">Añadir otro</a>
        </h1>
        <div>
            @include('flash::message')
            @include('estudios_no_formales.table')
    @endif
@endsection



