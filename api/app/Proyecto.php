<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proyecto extends Model
{
    use SoftDeletes;

    public $table = 'proyectos';
    protected $primaryKey='id_proyecto';
    protected $guarded = [];
}
