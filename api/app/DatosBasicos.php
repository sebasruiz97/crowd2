<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatosBasicos extends Model
{
    use SoftDeletes;

    public $table = 'datos_basicos';
    protected $primaryKey='id_datoBasico';
    protected $guarded = [];

}
