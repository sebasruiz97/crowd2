<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstudioFormal extends Model
{
    use SoftDeletes;

    public $table = 'estudios_formales';
    protected $primaryKey='id_estudioFormal';
    protected $guarded = [];
}
