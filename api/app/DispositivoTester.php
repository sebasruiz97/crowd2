<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DispositivoTester extends Model
{
    use SoftDeletes;

    public $table = 'dispositivos_tester';
    protected $primaryKey='id_dispositivoTester';
    protected $guarded = [];
}
