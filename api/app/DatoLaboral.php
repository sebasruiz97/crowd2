<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatoLaboral extends Model
{
    use SoftDeletes;

    public $table = 'datos_laborales';
    protected $primaryKey='id_datoLaboral';
    protected $guarded = [];
}
