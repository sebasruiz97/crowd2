<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;


class MyVerifyEmail extends VerifyEmail
{
    public function toMail($notifiable)
    {
        $verificationUrl = $this->verificationUrl($notifiable);

        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $verificationUrl);
        }

        return (new MailMessage)
            ->subject(Lang::get('Verificar correo electrónico'))
            ->greeting('Hola')
            ->line(Lang::get('Por favor, da click abajo para verificar tu dirección de correo'))
            ->action(Lang::get('Verificar correo electrónico'), $verificationUrl)
            ->line(Lang::get('Si no has creado una cuenta, ninguna acción es requerida'))
            ->line(Lang::get('Este enlace estará activo hasta: ' . Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60))))
            ->salutation('Saludos, '. config('app.name'));
    }
}
