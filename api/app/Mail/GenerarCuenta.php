<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GenerarCuenta extends Mailable
{

    public $user_id;
    public $nombre;
    public $fecha;
    public $id_cuentaCobro;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_id, $nombre, $fecha, $id_cuentaCobro )
    {
        
        $this->user_id = $user_id;
        $this->nombre = $nombre;
        $this->fecha = $fecha;
        $this->id_cuentaCobro = $id_cuentaCobro;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('mail.generar_cuenta')
                    ->subject('Se han generado una nueva cuenta de cobro en CROWD');                    
    }
}
