<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EventoProyecto extends Mailable
{
    public $name;
    public $proyectoNombre;
    public $evento;
    public $titulo;
    public $indicacion;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$proyectoNombre,$evento,$titulo, $indicacion)
    {
        $this->name = $name;
        $this->proyectoNombre = $proyectoNombre;
        $this->evento = $evento;
        $this->titulo = $titulo;
        $this->indicacion = $indicacion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.evento_proyecto')
                    ->subject('Novedad para un proyecto en CROWD'); 
    }
}
