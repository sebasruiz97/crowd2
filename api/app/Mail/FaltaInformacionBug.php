<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FaltaInformacionBug extends Mailable
{

    public $id;
    public $usuario;
    public $proyecto;
    public $tipoBug;
    public $tituloBug;
    public $estadoBug;
    public $cambio;
 
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id,$usuario,$proyecto,$tipoBug,$tituloBug,$estadoBug,$cambio)
    {
        
        $this->id = $id;
        $this->usuario = $usuario;
        $this->proyecto = $proyecto;
        $this->tipoBug = $tipoBug;
        $this->tituloBug = $tituloBug;
        $this->estadoBug = $estadoBug;
        $this->cambio = $cambio;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('mail.falta_info_bug')
                    ->subject('Actualización del estado de un hallazgo en Crowd');                    
    }
}
