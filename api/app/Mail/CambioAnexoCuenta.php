<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CambioAnexoCuenta extends Mailable
{

    public $cuentaCobro_id;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($cuentaCobro_id )
    {
        
        $this->cuentaCobro_id = $cuentaCobro_id;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('mail.nuevo_anexo_cuenta')
                    ->subject('Novedades respecto a una cuenta de cobro en CROWD');                    
    }
}
