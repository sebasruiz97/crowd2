<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;




use Luecano\NumeroALetras\NumeroALetras;

use Carbon\Carbon;
use Exception;

class CuentaCobroMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = "Sebastian Ruiz";

    public $msg;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($msg)
    {
        //
        $this->msg = $msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    $datos = [
     
            "fecha" => $this->msg["fecha"],
            "fecha_max" => $this->msg["fecha_max"],
            "nombre_usuario" => $this->msg["nombre_usuario"],
            "nombre_titular" =>$this->msg["nombre_titular"],
            "tipoDocumento_beneficiario" => $this->msg["tipoDocumento_beneficiario"],
            "correo" => $this->msg["correo"],
            "documento_beneficiario" => $this->msg["documento_beneficiario"],
            "monto_cuentaCobro" => $this->msg["monto_cuentaCobro"],
            "valor_cuentaCobro" => $this->msg["valor_cuentaCobro"],
            "mes_reto" => $this->msg["mes_reto"],
            "concepto" => $this->msg["concepto"],
            "descripcion_cuentaCobro" => $this->msg["descripcion_cuentaCobro"],
            "cuentaBancaria_beneficiario" =>$this->msg["cuentaBancaria_beneficiario"],
            "tipoCuenta_beneficiario" =>$this->msg["tipoCuenta_beneficiario"],
            "banco_beneficiario" => $this->msg["banco_beneficiario"] ,
            "direccion" => $this->msg["direccion"],
            "celular_beneficiario" => $this->msg["celular_beneficiario"],
            "estado" => $this->msg["estado"],
            "valor_letras_cuentaCobro" => $this->msg["valor_letras_cuentaCobro"],
            "ciudad" => $this->msg["ciudad"]
            
        ];
       
     
    $pdf = \PDF::loadView('cobros.cuentacobro',compact('datos'));

    /* return $this->view('personas',compact('mes_reto','motivo_cc','fecha_pago','fecha_max'))
     ->subject('CROWDSQA - 2021 '.$mes_reto.' - Cuenta de cobro de '.$motivo_cc)
      ->attach(public_path('Cuenta de Cobro.pdf'),[
          "as" => 'Cuenta de Cobro.pdf'
      ]);*/


      return $this->view('cobros.correo')
     ->subject('CROWDSQA - 2021 '.$datos["mes_reto"].' - Cuenta de cobro de '.$datos["descripcion_cuentaCobro"])
      ->attachData($this->msg["pdf"]->output(),"Cuenta Cobro.pdf");

    }

   



}
