<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvitacionTester extends Mailable
{
    public $name;
    public $proyectoNombre;
    public $proyectoDescripcion;
    public $proyectoInicio;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$proyectoNombre,$proyectoDescripcion,$proyectoInicio)
    {
        $this->name = $name;
        $this->proyectoNombre = $proyectoNombre;
        $this->proyectoDescripcion = $proyectoDescripcion;
        $this->proyectoInicio = $proyectoInicio;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.invitacion_tester')
                    ->subject('Tienes una nueva invitación en CROWD'); 
    }
}
