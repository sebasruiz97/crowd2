<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PagoTester extends Mailable
{

    public $user_id;
    public $nombre;
    public $fecha;
    public $cuentaCobro_id;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_id, $nombre, $fecha, $cuentaCobro_id )
    {
        
        $this->user_id = $user_id;
        $this->nombre = $nombre;
        $this->fecha = $fecha;
        $this->cuentaCobro_id = $cuentaCobro_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('mail.pagar_cuenta')
                    ->subject('Se ha realizado el pago de una cuenta de cobro en CROWD');                    
    }
}
