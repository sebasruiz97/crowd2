<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AprobarBug extends Mailable
{

    public $id;
    public $usuario;
    public $proyecto;
    public $tipoBug;
    public $tituloBug;
    public $estadoBug;
    public $fechaCreacion;
 
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id,$usuario,$proyecto,$tipoBug,$tituloBug,$estadoBug,$fechaCreacion)
    {
        
        $this->id = $id;
        $this->usuario = $usuario;
        $this->proyecto = $proyecto;
        $this->tipoBug = $tipoBug;
        $this->tituloBug = $tituloBug;
        $this->estadoBug = $estadoBug;
        $this->fechaCreacion = $fechaCreacion;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('mail.aprobar_bug')
                    ->subject('Aprobación de un hallazgo en Crowd');                    
    }
}
