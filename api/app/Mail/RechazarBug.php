<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RechazarBug extends Mailable
{

    public $id;
    public $usuario;
    public $proyecto;
    public $tipoBug;
    public $tituloBug;
    public $estadoBug;
    public $motivoRechazo;
    public $fechaCreacion;
 
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id,$usuario,$proyecto,$tipoBug,$tituloBug,$estadoBug,$motivoRechazo,$fechaCreacion)
    {
        
        $this->id = $id;
        $this->usuario = $usuario;
        $this->proyecto = $proyecto;
        $this->tipoBug = $tipoBug;
        $this->tituloBug = $tituloBug;
        $this->estadoBug = $estadoBug;
        $this->fechaCreacion = $fechaCreacion;
        $this->motivoRechazo = $motivoRechazo;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('mail.rechazar_bug')
                    ->subject('Rechazo de un hallazgo en Crowd');                    
    }
}
