<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RechazoCuenta extends Mailable
{

    public $fecha;
    public $id_cuentaCobro;
    public $motivoRechazo;
    public $quienRechaza;
    public $mensaje;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fecha, $id_cuentaCobro, $motivoRechazo, $quienRechaza, $mensaje )
    {
        
        $this->fecha = $fecha;
        $this->id_cuentaCobro = $id_cuentaCobro;
        $this->motivoRechazo = $motivoRechazo;
        $this->quienRechaza = $quienRechaza;
        $this->mensaje = $mensaje;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('mail.rechazo_cuenta')
                    ->subject('Rechazo de una cuenta de cobro en CROWD');                    
    }
}
