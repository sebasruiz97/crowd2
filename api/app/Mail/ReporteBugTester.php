<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReporteBugTester extends Mailable
{

    public $id;
    public $usuario;
    public $proyecto;
    public $tipoBug;
    public $tituloBug;
    
 
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id,$usuarioNombre,$proyecto,$tipoBug,$tituloBug)
    {        
        $this->id = $id;
        $this->usuario = $usuarioNombre;
        $this->proyecto = $proyecto;
        $this->tipoBug = $tipoBug;
        $this->tituloBug = $tituloBug;        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('mail.nuevo_bug')
                    ->subject('Reporte de un nuevo hallazgo en Crowd');                    
    }
}
