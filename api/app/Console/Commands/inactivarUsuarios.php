<?php

namespace App\Console\Commands;

use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class inactivarUsuarios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inactivarUsuarios:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inactiva los usuarios que tengan el perfil incompleto por mas de 3 meses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            //code...
            User::where('estado_id',1)
                ->where('created_at','>',Carbon::now())
                ->update(['estado_id' => 4]);
        } catch (\Throwable $th) {
            \Log::info($th->getMessage());
        }
        return 0;
    }
}
