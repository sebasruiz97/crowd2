<?php

namespace App\Services;

use App\Models\Pais;

class paises
{
    public function get()
    {
        $paises = Pais::orderBy('nombre_pais')->get();
        $paisesArray[''] = 'Selecciona uno';
        foreach ($paises as $pais) {
            $paisesArray[$pais->id_pais] = $pais->nombre_pais;
        }
        return $paisesArray;
    }
}
