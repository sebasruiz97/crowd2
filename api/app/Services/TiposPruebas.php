<?php

namespace App\Services;

use App\Models\TipoPruebas;

class tiposPruebas
{
    public function get()
    {
        $tiposPruebas = TipoPruebas::orderBy('nombre_prueba')->get();
        $tiposPruebasArray[''] = 'Selecciona una';
        foreach ($tiposPruebas as $tipoPrueba) {
            $tiposPruebasArray[$tipoPrueba->id_prueba] = $tipoPrueba->nombre_prueba;
        }
        return $tiposPruebasArray;
    }
}
