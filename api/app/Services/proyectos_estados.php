<?php

namespace App\Services;


class proyectos_estados
{
    public function get()
    {

        $proyectos_estados = \DB::table('proyectos_estados')->orderBy('estadoProyecto_Nombre')->get();
        $proyectos_estadosArray[''] = 'Selecciona uno';
        foreach ($proyectos_estados as $proyecto_estado) {
                $proyectos_estadosArray[$proyecto_estado->id_estadoProyecto] = $proyecto_estado->estadoProyecto_Nombre;
        }
        return $proyectos_estadosArray;
    }
}
