<?php

namespace App\Services;

class crypto
{
    public static function encrypt($data){
        $encrypt_data = '';
        $key       = file_get_contents(base_path('keys/publicKey.pem'));
        $key_eol   = (string) implode("\n", str_split((string) $key, 64));
        $publicKey = (string) "-----BEGIN PUBLIC KEY-----\n" . $key_eol . "\n-----END PUBLIC KEY-----";
        openssl_public_encrypt($data, $encrypt_data, $publicKey);
        $encrypt_data = base64_encode($encrypt_data);
        return $encrypt_data;
    }

    public function decrypt($data){
        $data = str_replace(" ","+",$data);
        $decrypted = '';
        $key       = file_get_contents(base_path('keys/privateKey.pem'));
        $key_eol   = (string) implode("\n", str_split((string) $key, 64));
        $privateKey = (string) "-----BEGIN RSA PRIVATE KEY-----\n" . $key_eol . "\n-----END RSA PRIVATE KEY-----";
        openssl_private_decrypt(base64_decode($data), $decrypted, $privateKey);
        return $decrypted;
    }

    public function validDecrypt($data){
        if(is_array($data) || substr($data,0,1) == "["){
            $dataDecrypt = '';
            $arrayDecrypt = is_array($data)? $data : explode(',', substr($data,1,-1));
            foreach ($arrayDecrypt as $s) {
                $dataDecrypt .= $this->decrypt($s);
            }
            return $dataDecrypt;
        } else {
            return $this->decrypt($data);
        }
    }

    public static function arrayEncrypt($data){
        $dataString = json_encode($data);
        return self::divideString($dataString);
    }

    public static function error($message){
        return self::arrayEncrypt(['error' => mb_convert_encoding($message, 'UTF-8', 'UTF-8')]);
    }

    protected static function divideString($string){
        $arrayString = [];
        $nIterate = round(strlen($string)/255);
        while($nIterate >= 0){
            $subString = substr($string, 255*$nIterate, 255);
            $stringEncrypted = self::encrypt($subString);
            array_unshift($arrayString, $stringEncrypted);
            if($nIterate == 0){
                break;
            }
            $nIterate--;
        }
        return $arrayString;
    }

}