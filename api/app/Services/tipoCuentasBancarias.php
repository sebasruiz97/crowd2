<?php

namespace App\Services;

use App\Models\TipoCuentaBancaria;

class tipoCuentasBancarias
{
    public function get()
    {
        $tipoCuentasBancarias = TipoCuentaBancaria::orderBy('nombre_cuentaBancaria','asc')->get();
        $tipoCuentasBancariasArray[''] = 'Selecciona una';
        foreach ($tipoCuentasBancarias as $tipoCuentaBancaria) {
                $tipoCuentasBancariasArray[$tipoCuentaBancaria->id_cuentaBancaria] = $tipoCuentaBancaria->nombre_cuentaBancaria;
        }
        return $tipoCuentasBancariasArray;
    }
}
