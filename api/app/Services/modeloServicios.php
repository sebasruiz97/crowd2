<?php

namespace App\Services;

use App\Models\ModeloServicio;

class modeloServicios
{
    public function get()
    {
        $modeloServicios = ModeloServicio::orderBy('nombre_modeloServicio')->get();
        $modeloServiciosArray[''] = 'Selecciona una';
        foreach ($modeloServicios as $modeloServicio) {
            $modeloServiciosArray[$modeloServicio->id_modeloServicio] = $modeloServicio->nombre_modeloServicio;
        }
        return $modeloServiciosArray;
    }
}