<?php

namespace App\Services;

use App\Models\TipoEjecucion;

class tiposEjecucion
{
    public function get()
    {
        $tiposEjecucion = TipoEjecucion::orderBy('nombre_ejecucion')->get();
        $tiposEjecucionArray[''] = 'Selecciona una';
        foreach ($tiposEjecucion as $tipoEjecucion) {
            $tiposEjecucionArray[$tipoEjecucion->id_ejecucion] = $tipoEjecucion->nombre_ejecucion;
        }
        return $tiposEjecucionArray;
    }
}
