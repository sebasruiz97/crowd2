<?php

namespace App\Services;

use App\Models\Plataforma;

class plataformas
{
    public function get()
    {
        $plataformas = Plataforma::orderBy('nombre_plataforma','asc')->get();
        $plataformasArray[''] = 'Selecciona uno';
        foreach ($plataformas as $plataforma) {
            $plataformasArray[$plataforma->id_plataforma] = $plataforma->nombre_plataforma;
        }
        return $plataformasArray;
    }
}
