<?php

namespace App\Services;


class rechazosCuentas
{
    public function get()
    {
        if(auth()->user()->hasRoles(['2'])){
            $rechazosCuentas = \DB::table('cuentas_cobro_rechazos')
                            ->where('id_rechazo_cuentaCobro','=',1)
                            ->orWhere('id_rechazo_cuentaCobro','=',4)                   
                            ->get();
        }elseif(auth()->user()->hasRoles(['4'])){
            $rechazosCuentas = \DB::table('cuentas_cobro_rechazos')
                            ->where('id_rechazo_cuentaCobro','=',2)
                            ->orWhere('id_rechazo_cuentaCobro','=',3)                   
                            ->get();

        }else{
            $rechazosCuentas = \DB::table('cuentas_cobro_rechazos')              
                            ->get();
        }       

        $rechazosCuentasArray[''] = 'Selecciona uno';
        foreach ($rechazosCuentas as $rechazoCuenta) {
                $rechazosCuentasArray[$rechazoCuenta->id_rechazo_cuentaCobro] = $rechazoCuenta->nombre_rechazo_cuentaCobro;
        }
        return $rechazosCuentasArray;
    }
}
