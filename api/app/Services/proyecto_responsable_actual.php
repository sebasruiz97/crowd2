<?php

namespace App\Services;


class proyecto_responsable_actual
{
    public function get()
    {

        $proyecto_responsable_actual = \DB::table('users')->where([
                ['rol_id','=', 3],
                ])->orderBy('name')->get();
        $proyecto_responsable_actualArray[''] = 'Selecciona uno';
        foreach ($proyecto_responsable_actual as $proyecto_responsable_actual) {
                $proyecto_responsable_actualArray[$proyecto_responsable_actual->id] = $proyecto_responsable_actual->name;
        }
        return $proyecto_responsable_actualArray;
    }
}


