<?php

namespace App\Services;

use App\Models\TipoMetodologia;

class tiposMetodologias
{
    public function get()
    {
        $tiposMetodologias = TipoMetodologia::get();
        $tiposMetodologiasArray[''] = 'Selecciona una';
        foreach ($tiposMetodologias as $tipoMetodologia) {
            $tiposMetodologiasArray[$tipoMetodologia->id_metodologia] = $tipoMetodologia->nombre_metodologia;
        }
        return $tiposMetodologiasArray;
    }
}
