<?php

namespace App\Services;


class proyectos_responsabes_tipos_novedades
{
    public function get()
    {

        $proyectos_responsabes_tipos_novedades = \DB::table('proyectos_responsabes_tipos_novedades')->orderBy('tipoNovedad_nombre')->get();
        $proyectos_responsabes_tipos_novedadesArray[''] = 'Selecciona uno';
        foreach ($proyectos_responsabes_tipos_novedades as $proyectos_responsabes_tipo_novedad) 
        {
            $proyectos_responsabes_tipos_novedadesArray[$proyectos_responsabes_tipo_novedad->id_tipoNovedad] = $proyectos_responsabes_tipo_novedad->tipoNovedad_Nombre;
        }
        return $proyectos_responsabes_tipos_novedadesArray;
    }
}
