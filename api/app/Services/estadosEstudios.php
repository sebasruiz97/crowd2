<?php

namespace App\Services;

use App\Models\EstadoEstudio;

class estadosEstudios
{
    public function get()
    {
        $estadosEstudios = EstadoEstudio::orderBy('nombre_estadoEstudio')->get();
        $estadosEstudiosArray[''] = 'Selecciona una';
        foreach ($estadosEstudios as $estadoEstudio) {
            $estadosEstudiosArray[$estadoEstudio->id_estadoEstudio] = $estadoEstudio->nombre_estadoEstudio;
        }
        return $estadosEstudiosArray;
    }
}