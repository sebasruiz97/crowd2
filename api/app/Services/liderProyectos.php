<?php

namespace App\Services;


class liderProyectos
{
    public function get()
    {

        $liderProyectos = \DB::table('vista_proyectos')->where([
                ['proyecto_responsable_actual', '=',auth()->user()->id],
                ['proyectoEstado_id','=', 2],
                ])->get();
        $liderProyectosArray[''] = 'Filtrar por proyecto';
        foreach ($liderProyectos as $liderProyecto) {
                $liderProyectosArray[$liderProyecto->id_proyecto] = $liderProyecto->proyecto_nombre;
        }
        return $liderProyectosArray;
    }
}
