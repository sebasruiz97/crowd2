<?php

namespace App\Services;


class estadosBugs
{
    public function get()
    {

        $estadosBugs = \DB::table('bug_estados')->where('id_estadoBug','!=',1)->orderBy('nombre_estadoBug')->get();
        $estadosBugsArray[''] = 'Selecciona uno';
        foreach ($estadosBugs as $estadoBug) {
                $estadosBugsArray[$estadoBug->id_estadoBug] = $estadoBug->nombre_estadoBug;
        }
        return $estadosBugsArray;
    }
}
