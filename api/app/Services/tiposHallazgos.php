<?php

namespace App\Services;


class tiposHallazgos
{
    public function get()
    {

        $tipoBugs = \DB::table('bug_tipos')->orderBy('nombre_tipoBug')->get();
        $tipoBugsArray[''] = 'Selecciona uno';
        foreach ($tipoBugs as $tipoBug) {
                $tipoBugsArray[$tipoBug->id_tipoBug] = $tipoBug->nombre_tipoBug;
        }
        return $tipoBugsArray;
    }
}
