<?php

namespace App\Services;

use App\Models\TipoEstudio;

class tiposEstudios
{
    public function get()
    {
        $tiposEstudios = TipoEstudio::orderBy('nombre_tipoEstudio')->get();
        $tiposEstudiosArray[''] = 'Selecciona uno';
        foreach ($tiposEstudios as $tipoEstudio) {
            $tiposEstudiosArray[$tipoEstudio->id_tipoEstudio] = $tipoEstudio->nombre_tipoEstudio;
        }
        return $tiposEstudiosArray;
    }
}