<?php

namespace App\Services;

use App\Models\Cargo;

class cargos
{
    public function get()
    {
        $cargos = Cargo::orderBy('nombre_cargo')->get();
        $cargosArray[''] = 'Selecciona uno';
        foreach ($cargos as $cargo) {
                $cargosArray[$cargo->id_cargo] = $cargo->nombre_cargo;
        }
        return $cargosArray;
    }
}
