<?php

namespace App\Services;


class estadosCuentasCobro
{
    public function get()
    {
        if(auth()->user()->hasRoles(['2'])){

            $estadosCuentasCobro = \DB::table('cuentas_cobro_estados')
                                    ->where('id_estado_cuentaCobro','=',4)
                                    ->orWhere('id_estado_cuentaCobro','=',5)
                                    ->get();            

        }elseif(auth()->user()->hasRoles(['4'])){

            $estadosCuentasCobro = \DB::table('cuentas_cobro_estados')
                                    ->where('id_estado_cuentaCobro','=',2)
                                    ->orWhere('id_estado_cuentaCobro','=',3)
                                    ->orWhere('id_estado_cuentaCobro','=',6)
                                    ->get();

        }elseif(auth()->user()->hasRoles(['3'])){

            $estadosCuentasCobro = \DB::table('cuentas_cobro_estados')
                                    ->where('id_estado_cuentaCobro','=',7)
                                    ->get();

        }else{

            $estadosCuentasCobro = \DB::table('cuentas_cobro_estados')->where('id_estado_cuentaCobro','!=',1)->get();

        }

        $estadosCuentasCobroArray[''] = 'Selecciona uno';
        foreach ($estadosCuentasCobro as $estadoCobro) {
                $estadosCuentasCobroArray[$estadoCobro->id_estado_cuentaCobro] = $estadoCobro->nombre_estado_cuentaCobro;
        }
        return $estadosCuentasCobroArray;
        
    }
}
