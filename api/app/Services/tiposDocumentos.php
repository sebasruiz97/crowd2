<?php

namespace App\Services;

use App\Models\TipoDocumento;

class tiposDocumentos
{
    public function get()
    {
        $tiposDocumentos = TipoDocumento::orderBy('nombre_tipoDocumento')->get();
        $tiposDocumentosArray[''] = 'Seleccione uno';
        foreach ($tiposDocumentos as $tipoDocumento) {
                $tiposDocumentosArray[$tipoDocumento->id_tipoDocumento] = $tipoDocumento->nombre_tipoDocumento;
        }
        return $tiposDocumentosArray;
    }
}
