<?php

namespace App\Services;


class rechazosBugs
{
    public function get()
    {

        $rechazosBugs = \DB::table('bug_rechazos')->orderBy('motivo_rechazoBug')->get();
        $rechazosBugsArray[''] = 'Selecciona uno';
        foreach ($rechazosBugs as $rechazoBug) {
                $rechazosBugsArray[$rechazoBug->id_rechazoBug] = $rechazoBug->motivo_rechazoBug;
        }
        return $rechazosBugsArray;
    }
}
