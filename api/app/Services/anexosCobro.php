<?php

namespace App\Services;


class anexosCobro
{
    public function get()
    {
        if(auth()->user()->hasRoles(['2'])){
            $anexos = \DB::table('vista_anexos_por_modulo')
                        ->where('modulo_id','=','2')
                        ->where('tipoAnexo_id','=','8')
                        ->orWhere('tipoAnexo_id','=','9')
                        ->get();

        }elseif(auth()->user()->hasRoles(['4'])){
            $anexos = \DB::table('vista_anexos_por_modulo')
                        ->where('modulo_id','=','2')
                        ->where('tipoAnexo_id','=','10')
                        ->get();

        }else{            
            $anexos = \DB::table('vista_anexos_por_modulo')->where('modulo_id','=','2')->get();
           
        }
        
        $anexosArray[''] = 'Selecciona uno';
        foreach ($anexos as $anexo) {
            $anexosArray[$anexo->tipoAnexo_id] = $anexo->nombre_anexo;
        }
        return $anexosArray;
    }
}
