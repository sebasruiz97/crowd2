<?php

namespace App\Services;

use App\Models\Ocupacion;

class ocupaciones
{
    public function get()
    {
        $ocupaciones = Ocupacion::orderBy('nombre_ocupacion')->get();
        $ocupacionesArray[''] = 'Selecciona una';
        foreach ($ocupaciones as $ocupacion) {
            $ocupacionesArray[$ocupacion->id_ocupacion] = $ocupacion->nombre_ocupacion;
        }
        return $ocupacionesArray;
    }
}
