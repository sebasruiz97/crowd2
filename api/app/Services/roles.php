<?php

namespace App\Services;

class roles
{
    public function get()
    {

        $roles = \DB::table('roles')->orderBy('nombre_rol')->get();
        $rolesArray[''] = 'Seleccione uno';
        foreach ($roles as $rol) {
                $rolesArray[$rol->id_rol] = $rol->descripcion;
        }
        return $rolesArray;
    }
}
