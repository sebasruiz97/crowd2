<?php

namespace App\Services;

use App\Models\Departamento;

class departamentos
{
    public function get()
    {
        $departamentos = Departamento::orderBy('nombre_departamento')->get();
        $departamentosArray[''] = 'Selecciona uno';
        foreach ($departamentos as $departamento) {
            $departamentosArray[$departamento->id_departamento] = $departamento->nombre_departamento;
        }
        return $departamentosArray;
    }
}
