<?php

namespace App\Services;

use App\Models\Universidad;

class universidades
{
    public function get()
    {
        $universidades = Universidad::orderBy('nombre_universidad')->get();
        $universidadesArray[''] = 'Selecciona uno';
        foreach ($universidades as $universidad) {
            $universidadesArray[$universidad->id_universidad] = $universidad->nombre_universidad;
        }
        return $universidadesArray;
    }
}
