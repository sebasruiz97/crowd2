<?php

namespace App\Services;

use App\Models\AreaEstudio;

class areasEstudios
{
    public function get()
    {
        $areasEstudios = AreaEstudio::orderBy('nombre_areaEstudio')->get();
        $areasEstudiosArray[''] = 'Selecciona una';
        foreach ($areasEstudios as $areaEstudio) {
            $areasEstudiosArray[$areaEstudio->id_areaEstudio] = $areaEstudio->nombre_areaEstudio;
        }
        return $areasEstudiosArray;
    }
}
