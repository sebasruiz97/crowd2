<?php

namespace App\Services;

use App\Models\Sector;

class sectores
{
    public function get()
    {
        $sectores = Sector::orderBy('nombre_sector')->get();
        $sectoresArray[''] = 'Selecciona una';
        foreach ($sectores as $sector) {
                $sectoresArray[$sector->id_sector] = $sector->nombre_sector;
        }
        return $sectoresArray;
    }
}
