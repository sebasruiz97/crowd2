<?php

namespace App\Services;

use App\Models\TiempoExperiencia;

class tiemposExperiencias
{
    public function get()
    {
        $tiemposExperiencias = TiempoExperiencia::orderBy('nombre_tiempoExperiencia')->get();
        $tiemposExperienciasArray[''] = 'Selecciona una';
        foreach ($tiemposExperiencias as $tiempoExperiencia) {
                $tiemposExperienciasArray[$tiempoExperiencia->id_tiempoExperiencia] = $tiempoExperiencia->nombre_tiempoExperiencia;
        }
        return $tiemposExperienciasArray;
    }
}
