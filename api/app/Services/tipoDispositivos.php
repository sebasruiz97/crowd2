<?php

namespace App\Services;

use App\Models\TipoDispositivo;

class tipoDispositivos
{
    public function get()
    {
        $tipoDispositivos = TipoDispositivo::orderBy('nombre_dispositivo')->get();
        $tipoDispositivosArray[''] = 'Selecciona uno';
        foreach ($tipoDispositivos as $tipoDispositivo) {
            $tipoDispositivosArray[$tipoDispositivo->id_dispositivo] = $tipoDispositivo->nombre_dispositivo;
        }
        return $tipoDispositivosArray;
    }
}
