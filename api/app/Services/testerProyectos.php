<?php

namespace App\Services;


class testerProyectos
{
    public function get()
    {
        if(auth()->user()->hasRoles(['2'])){

            $testerProyectos = \DB::table('vista_proyectos_responsables')->where([
                ['id_proyecto_responsable', '=',auth()->user()->id],
                ['ProyectoEstado_id','=', 2],
                ['estado_r','=', "Asignada"],
                ])->get();
            $testerProyectosArray[''] = 'Selecciona uno';
            foreach ($testerProyectos as $testerProyecto) {
                    $testerProyectosArray[$testerProyecto->id_proyecto] = $testerProyecto->proyecto_nombre;
            }
            return $testerProyectosArray;

        }

            $testerProyectos = \DB::table('vista_proyectos_responsables')->where([
                ['ProyectoEstado_id','=', 2],
                ])->get();
            $testerProyectosArray[''] = 'Selecciona uno';
            foreach ($testerProyectos as $testerProyecto) {
                    $testerProyectosArray[$testerProyecto->id_proyecto] = $testerProyecto->proyecto_nombre;
            }
            return $testerProyectosArray;
       
    }
}
