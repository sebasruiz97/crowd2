<?php

namespace App\Services;


class clientes
{
    public function get()
    {
        $clientes = \DB::table('clientes')->orderBy('nombre_cliente')->get();
        $clientesArray[''] = 'Selecciona uno';
        foreach ($clientes as $cliente) {
            $clientesArray[$cliente->id_cliente] = $cliente->nombre_cliente;
        }
        return $clientesArray;
    }
}
