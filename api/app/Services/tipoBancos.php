<?php

namespace App\Services;

use App\Models\TipoBanco;

class tipoBancos
{
    public function get()
    {
        $tipoBancos = TipoBanco::orderBy('nombre_banco','asc')->get();
        $tipoBancosArray[''] = 'Selecciona una';
        foreach ($tipoBancos as $tipoBanco) {
                $tipoBancosArray[$tipoBanco->id_banco] = $tipoBanco->nombre_banco;
        }
        return $tipoBancosArray;
    }
}
