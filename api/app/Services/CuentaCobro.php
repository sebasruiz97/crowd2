<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CuentaCobro extends Model
{
    use SoftDeletes;

    public $table = 'cuentas_cobro';
    protected $primaryKey='id_cuentaCobro';
    protected $guarded = [];
}
