<?php

namespace App\Services;


class anexos
{
    public function get()
    {
        $anexos = \DB::table('vista_anexos_por_modulo')->where('modulo_id','=','1')->orderBy('nombre_anexo')->get();
        $anexosArray[''] = 'Selecciona uno';
        foreach ($anexos as $anexo) {
            $anexosArray[$anexo->tipoAnexo_id] = $anexo->nombre_anexo;
        }
        return $anexosArray;
    }
}
