<?php

namespace App\Services;

use App\Models\NivelEducacion;

class nivelesEducacion
{
    public function get()
    {
        $nivelesEducacion = NivelEducacion::orderBy('nombre_nivelEducacion')->get();
        $nivelesEducacionArray[''] = 'Selecciona una';
        foreach ($nivelesEducacion as $nivelEducacion) {
            $nivelesEducacionArray[$nivelEducacion->id_nivelEducacion] = $nivelEducacion->nombre_nivelEducacion;
        }
        return $nivelesEducacionArray;
    }
}
