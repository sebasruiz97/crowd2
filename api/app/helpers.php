<?php

use App\Models\Anexo;
use App\DatosBasicos;
use App\DatoLaboral;
use App\Models\AlertaUsuario;
use App\Models\Messages;
use App\Models\Secciones;

function validarSeccion($seccion)
{
    if (!in_array($seccion, getArrayObligatorios())) {
        return '';
    }
    $puntos = \DB::table('puntos_tester')->where([
        ['user_id', '=', auth()->user()->id],
        ['motivo', '=', "Agregar $seccion"],
    ])->orWhere([
        ['user_id', '=', auth()->user()->id],
        ['motivo', '=', "Eliminar $seccion"],
    ])->sum('puntos');
    return $puntos > 0 ?  'completed' : 'incompleted';
}

function active($path)
{
    return request()->is($path) ? 'activeIH' : '';
}

function datosObligatorios()
{
    $puntosPerfil = 0;

    $datosObligatorios = getArrayObligatorios();

    foreach ($datosObligatorios as $seccion) {
        # code...
        $puntos = \DB::table('puntos_tester')->where([
            ['user_id', '=', Auth()->user()->id],
            ['motivo', '=', "Agregar $seccion"],
        ])->orWhere('motivo', '=', "Eliminar $seccion")
            ->sum('puntos');
        if ($puntos > 0) {
            $puntosPerfil++;
        }
    }
    return $puntosPerfil;
}

function anexosObligatorios($onlyId=false)
{
    $user = auth()->user()->id;
    $anexos = Anexo::query()->where('user_id', $user)->paginate(6);
    $anexosRequeridos = [1, 3, 4, 6, 12];

    $datos = DatoLaboral::where('user_id', $user)->whereNull('fechaFin_datoLaboral')->first();
    if(isset($datos->empresa)){
        if (substr($datos->empresa, 0, 3) == "SQA") {
            array_push($anexosRequeridos, 11);
        }
    }

    foreach ($anexos as $anexo) {
        $index = array_search($anexo->tipoAnexo_id, $anexosRequeridos);
        if (isset($index)) {
            array_splice($anexosRequeridos, $index, 1);
        }
        if ($anexo->tipoAnexo_id == 2 && $anexosRequeridos[0] == 1) {
            array_splice($anexosRequeridos, 0, 1);
        }
    }

    if($onlyId){
        return $anexosRequeridos;
    }

    $anexosFaltantes = \DB::table('tipos_anexos')->whereIn('id_tipoAnexo', $anexosRequeridos)->get();

    return $anexosFaltantes;
}

function getArrayObligatorios()
{
    $id = auth()->user()->id;
    $datosBasicos = DatosBasicos::where("user_id", "=", $id)->first();
    $datosObligatorios = ['datos basicos', 'dispositivo disponible para probar', 'datos financieros', 'Anexos'];

    if(isset($datosBasicos)){
        if ($datosBasicos->experienciaPruebas != 'No') {
            array_push($datosObligatorios, 'conocimiento en pruebas');
        } else {
            if (($clave = array_search('conocimiento en pruebas', $datosObligatorios)) !== false) {
                unset($datosObligatorios[$clave]);
            }
        }
    }
    return $datosObligatorios;
}

function alerts($type = null)
{
    $user = auth()->user()->id;
    if($type == 'contador'){
        $alertas = AlertaUsuario::where(function($query) use ($user){
            $query->where('id_usuario', $user);
            $query->where('estado','enviada');
        })
        ->orWhere(function($query){
            $query->where('id_usuario', '1');
            $query->where('estado','enviada');
        })->get();
        return $alertas;
    }
    $alertas = AlertaUsuario::where(function($query) use ($user){
                    $query->where('id_usuario', $user);
                    $query->where('estado','<>','asignada');
                })
                ->orWhere(function($query){
                    $query->where('id_usuario', '1');
                    $query->where('estado','<>','asignada');
                })
                ->orderByDesc('created_at')
                ->get();
    return $alertas;
}

function newMessages()
{
    $user = auth()->user()->id;
    $newMensajes = Messages::query()
        ->where([
            'to_id' => $user,
            'seen' => 0
        ])
        ->groupBy('from_id')->get();
    return $newMensajes;
}

function seccionIncompleta()
{
    switch ('incompleted') {
        case(validarSeccion('dispositivo disponible para probar')):
            return route('sinDispositivos');
        break;
        case(validarSeccion('datos financieros')):
            return route('datosFinancieros.index');
        break;
        case(validarSeccion('conocimiento en pruebas')):
            return route('sinPruebas');
        break;
        case(validarSeccion('Anexos')):
            return route('anexos.index');
        break;
    }
    return route('datosBasicos.index');
}

/* NUEVO PROCESO SECCIONES */
function validarSecciones($user_id)
{
    $secciones = Secciones::all();

    // Data
    $datosBasicos = Datosbasicos::where('user_id',$user_id)->first();

    foreach ($secciones as &$seccion) {
        switch ($seccion->nombre_seccion) {
            case 'datos basicos':
                $seccion['valid'] = isset($datosBasicos);
                break;
            case 'conocimiento en pruebas':
                # code...
                if(isset($datosBasicos) && $datosBasicos->experienciaPruebas !== 'No'){
                    $conocimiento = \DB::table('indice_conocimiento_pruebas')->where('user_id',$user_id)->first();
                    $seccion['valid'] = isset($conocimiento);
                } else {
                    $seccion['obligatory'] = null;
                    //$seccion['valid'] = false;
                }
                break;
            case 'mis dispositivos':
                $dispositivo = \DB::table('info_dispositivos')->where('user_id',$user_id)->first();
                $seccion['valid'] = isset($dispositivo);
                break;
            case 'informacion financiera':
                # code...
                break;
            case 'anexos':
                $anexos = \DB::table('vista_anexos')->where('user_id',$user_id)->pluck('tipoAnexo_id')->toArray();
                $anexosObligatorios = anexosObligatorios(true);
                $seccion['valid'] = count(array_diff($anexosObligatorios,$anexos)) === 0;
                break;
        }
    }
    return $secciones;
}