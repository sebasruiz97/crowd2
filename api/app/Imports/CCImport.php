<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

use App\Models\Users;
use App\Models\TipoDocumento;
use App\Models\VistaDatosBasicos;
use App\Models\VistaDatosFinancieros;
use App\Models\Estado;
use App\Models\Departamento;
use Carbon\Carbon;
use App\CuentaCobro;
use App\Services\CuentaCobro as ServicesCuentaCobro;

use Luecano\NumeroALetras\NumeroALetras;

use Maatwebsite\Excel\Concerns\ToModel;

use Illuminate\Support\Facades\Mail;

use App\Mail\CuentaCobroMail;

use Illuminate\Support\Str;

use App\Http\Controllers\Controller;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Throwable;

class CCImport implements ToCollection,WithHeadingRow
{
    protected $fecha;
    protected $fechamax;
    protected $mes_reto;
    protected $concepto;
    protected $descripcion_cuentacobro;

    
    // Método Constructor parámetros de los datos 

    public function __constructor()
    {
    }


    public function __construct($fecha, $fechamax, $mes_reto, $concepto, $descripcion_cuentacobro)
    {
        $this->fecha = $fecha;
        $this->fechamax = $fechamax;
        $this->mes_reto = $mes_reto;
        $this->concepto = $concepto;
        $this->descripcion_cuentacobro = $descripcion_cuentacobro;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {

  // 1: Leer Lista con los destinatarios
  $documento = $collection["documento"];
  $valor = $collection["valor"];
 
  // 2: Buscar Destinatario x en base de datos
  // Consultas a Base de Datos

  $usuario = Users::where('numeroDocumento', $collection["documento"])
   ->first();

$tipo_documento = TipoDocumento::where('id_tipoDocumento', $usuario->tipoDocumento_id)
   ->first();

$datos_financieros = VistaDatosFinancieros::where('user_id', $usuario->id)
   ->first();

$datos_basicos = VistaDatosBasicos::where('user_id', $usuario->id)
   ->first();

$estado = Estado::where('id_estado', $usuario->estado_id)
   ->first();


$departamento = Departamento::where('id_departamento', $datos_basicos->departamento_id)
   ->first();

  // 3. Obtener sus datos para armar el archivo

  //Fecha
  if ($this->fecha != "") {
      $dia = $this->fecha;
  } else {
      $dia = Carbon::now()->toDateTimeString();
  }

  // Convertidor de letras a números
  $convertidor = new NumeroALetras();
  $letras = $convertidor->toString($collection["valor"], 2, "PESOS COLOMBIANOS", "");

   // Entrada de datos al correo
   $columnas = [
      "fecha" => $dia,
      "fecha_max" => $this->fechamax,
      "nombre_usuario" => $usuario->name,
      "nombre_titular" => $datos_financieros->nombreTitular,
      "tipoDocumento_beneficiario" => $tipo_documento->nombre_tipoDocumento,
      "correo" =>  $usuario->email,
      "documento_beneficiario" => $usuario->numeroDocumento,
      "monto_cuentaCobro" => $collection["valor"],
      "valor_cuentaCobro" => $collection["valor"],
      "mes_reto" => $this->mes_reto,
      "concepto" => $this->concepto,
      "descripcion_cuentaCobro" => $this->descripcion_cuentacobro,
      "cuentaBancaria_beneficiario" => $datos_financieros->numeroCuenta,
      "tipoCuenta_beneficiario" => $datos_financieros->nombre_cuentaBancaria,
      "banco_beneficiario" =>  $datos_financieros->nombre_banco,
      "direccion" => $datos_basicos->direcion,
      "celular_beneficiario" => $datos_basicos->telefonoCelular,
      "estado" => $estado->nombre_estado,
      "valor_letras_cuentaCobro" => $letras,
      "ciudad" => $departamento->nombre_departamento
  ];

  // 4. Generar el Archivo

  // 5. Enviar mensaje con Archivo Adjunto
  Mail::to($columnas["correo"])->send(new CuentaCobroMail($columnas));

  return response()->json([
      "mensaje" => $columnas["correo"]
  ]);
    


       
    }   
}