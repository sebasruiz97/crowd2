<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConocimientoPruebas extends Model
{
    use SoftDeletes;

    public $table = 'conocimiento_pruebas';
    protected $primaryKey='id_conocimientoPruebas';
    protected $guarded = [];
}
