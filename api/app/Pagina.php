<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
// esta clase contiene metodos que pueden ser llamados desde diferentes controladores y ser enviados como parametros a las vistas
// ejemplo de llamado desde el controlador: 
// use App\Pagina;
// $datosPaises['data'] = Pagina::getPaises();
// return view('ensayo.index', compact('datosPaises'));

// ejemplo uso desde la vista

//<!-- Leer paises y crear select -->
//            @foreach($datosPaises['data'] as $pais)
//            <option value='{{ $pais->id_pais }}'>{{ $pais->nombre_pais }}</option>
//           @endforeach

// llamar cuando cambie valor

// AJAX request 
//                $.ajax({
//                   url: 'getciudadesxPais/'+id,
//                   type: 'get',
//                   dataType: 'json',
//                   success: function(response){

//                   var len = 0;
//                   if(response['data'] != null){
//                       len = response['data'].length;
//                   }

//                   if(len > 0){
// leer datos y crear <option >
//                       for(var i=0; i<len; i++)
//                       {
//                          var id = response['data'][i].id_ciudad;
//                          var name = response['data'][i].nombre_ciudad;
//                          var option = "<option value='"+id+"'>"+name+"</option>"; 
//
//                            $("#sel_ciudad").append(option); 
//                       }
//                   }

//                   }
//              });

class Pagina extends Model
{

  // Obtner lista de Paises
  public static function getPaises()
  {

    $value = DB::table('paises')->orderBy('nombre_pais', 'asc')->get();

    return $value;
  }

  // Obtener Lista de Ciudades x pais
  public static function getciudadesxPais($pais_id = 0)
  {

    $value = DB::table('ciudades')->where('pais_id', $pais_id)->get();

    return $value;
  }

  //*********************************** */

  // Obtener Tipos de pruebas
  public static function gettiposPruebas($id_prueba = 0)
  {

    $value = DB::table('tipo_pruebas')->orderBy('nombre_prueba', 'asc')->get();

    return $value;
  }

  // Obtener Tipos de pruebas x id_prueba
  public static function gettiposPruebaSeleccionada($id_prueba = 0)
  {

    $value = DB::table('tipo_pruebas')->where('id_prueba', $id_prueba)->get();

    return $value;
  }

  // obtener herramientas x tipo de prueba seleccionada
  public static function getherramientasxTipo($id_prueba = 0)
  {

    $value = DB::table('vista_herramientas_por_prueba')->where('prueba_id', $id_prueba)->get();


    return $value;
  }

  // obtener dispositivos x tipo de prueba seleccionada
  public static function getDispositivosxTipo($id_prueba = 0)
  {

    $value = DB::table('vista_dispositivos_por_prueba')->where('prueba_id', $id_prueba)->get();


    return $value;
  }

  public static function getDispositivos()
  {

    $value = DB::table('tipo_dispositivos')->get();


    return $value;
  }

  public static function getMarcas($term=false)
  {
    $value = DB::table('dispositivos');
    if($term){
      $value->where('nombre','like',"$term%");
    }
    $value = $value->orderBy('nombre')->get();

    return $value;
  }

  public static function getModelos($marca, $term=false)
  {
    $where = [['dispositivo_id',$marca]];
    if($term && $term !== "false"){
      array_push($where, ['nombre','like',"$term%"]);
    };
    $value = DB::table('modelos_dispositivo')->where($where)->orderBy('nombre')->get();
    return $value;
  }

  // obtener so x dispositivos seleccionada
  public static function getSoxDispxTipo($id_dispositivo = 0)
  {

    $value = DB::table('vista_so_por_dispositivos')->where('dispositivo_id', $id_dispositivo)->get();


    return $value;
  }


  // obtener nav x dispositivos seleccionada
  public static function getNavDispxTipo($id_dispositivo = 0)
  {

    $value = DB::table('vista_navegadores_x_dispositivo')->where('dispositivo_id', $id_dispositivo)->get();


    return $value;
  }

  //listado de tiempo_experiencia
  public static function gettiempo_experiencias($id_tiempoExperiencia = 0)
  {

    $value = DB::table('tiempo_experiencias')->orderBy('nombre_tiempoExperiencia', 'asc')->get();

    return $value;
  }

  //listado tipo_ejecucion
  public static function gettipo_ejecucion($id_ejecucion = 0)
  {

    $value = DB::table('tipo_ejecucion')->orderBy('nombre_ejecucion', 'asc')->get();

    return $value;
  }


  //listado de tiempo_experiencia
  public static function gettipoPruebasDisponiblesxUsuario($id_usuario = 0)
  {

    $misql = "";
    $misql =  "select user_id AS userid, prueba_id AS id_prueba, nombre_prueba AS nombre_prueba from tipos_pruebas_por_usuarios as tpu";
    $misql = $misql . "   WHERE     exists";
    $misql = $misql . "    (select id_prueba,nombre_prueba   from tipo_pruebas as tp";
    $misql = $misql . " where tpu.prueba_id = tp.id_prueba  and tpu.user_id = 10 ";
    $misql = $misql . "     )";
    $misql = $misql . "   union";
    $misql = $misql . "  SELECT '' AS userid, id_prueba AS id_prueba,nombre_prueba AS nombre_prueba";
    $misql = $misql . "   from tipo_pruebas as tp";
    $misql = $misql . "   where not exists";
    $misql = $misql . "    (select * from tipos_pruebas_por_usuarios as tpu";
    $misql = $misql . "      where tp.id_prueba=tpu.prueba_id  AND tpu.user_id = 10";
    $misql = $misql . "   )";
    $misql = $misql . "   order by id_prueba";

    $vars = \DB::select($misql, [$id_usuario, $id_usuario]);



    return $vars;
  }
}
