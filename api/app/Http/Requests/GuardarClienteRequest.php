<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuardarClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_cliente'=>'required|max:50', 
            'sector_id'=>'required', 
            'descripcion'=>'required|max:250', 
            'nit_cliente'=>'required|max:50', 
            'modeloServicio_id'=>'required', 
            'pais_id'=>'required',
            'departamento_id'=>'required', 
            'municipio_id'=>'required', 
            'direccion_cliente'=>'required|max:100',
            'nombre_contactoPruebas'=>'required|max:50',
            'telefono_contactoPruebas'=>'required|alpha_dash|max:50',
            'correo_contactoPruebas'=>'required|email|max:50',
            'nombre_contactoFacturacion'=>'required|max:50',
            'telefono_contactoFacturacion'=>'required|alpha_dash|max:50',
            'correo_contactoFacturacion'=>'required|email|max:50',
            'plazo_pagoFactura'=>'required|max:50',
            'fecha_recibidoFactura'=>'required|max:50',
            'correo_envioFactura'=>'required|email|max:50',
            'acuerdos_adicionales' => 'required',
        ];
    }

    public function messages()

    {
        return [
            'nombre_cliente.required' => 'Es obligatorio registrar el nombre del cliente',
            'sector_id.required' => 'Es obligatorio registrear el sector al que el cliente pertenece',
            'nit_cliente.required' => 'Es obligatorio registrar el NIT del cliente',
            'tipoPrueba_id.required' => 'Es obligatorio seleccionar los tipos de prueba que conforman el alcance definido con el cliente',
            'modeloServicio_id.required' => 'Es obligatorio seleccionar el modelo de servicio que se tiene con el cliente',
            'metodologia_id.required' => 'ES obligatorio registrar que metodología(s) usa el cliente en sus proyectos',
            'pais_id.required' => 'Es obligatorio registrar el país donde está localizado el cliente',
            'departamento_id.required' => 'Es obligatorio registrar el departamento donde está localizado el cliente',
            'municipio_id.required' => 'Es obligatorio registrar el municipio donde está localizado el cliente',
            'direccion_cliente.required' => 'Es obligatorio registrar la dirección del cliente',
            'nombre_contactoPruebas.required' => 'Es obligatorio registrar un contacto para tratar temas de pruebas con el cliente',
            'telefono_contactoPruebas.required' => 'Es obligatorio registrar el teléfono de la persona contacto para pruebas',
            'correo_contactoPruebas.required' => 'Es obligatorio registrar el correo de la persona contacto para pruebas',
            'nombre_contactoFacturacion.required' => 'Es obligatorio registrar un contacto para tratar temas de factureación con el cliente',
            'telefono_contactoFacturacion.required' => 'Es obligatorio registrar el teléfono de la persona contacto para facturación',
            'correo_contactoFacturacion.required' => 'Es obligatorio registrar el correo de la persona contacto para facturación',
            'plazo_pagoFactura.required' => 'Es obligatorio registrar el plazo de pago de facturas pactado con el cliente',
            'fecha_recibidoFactura.required' => 'Es obligatorio registrar la fecha máxima de recibido de facturar en el cliente',
            'correo_envioFactura.required' => 'Es obligatorio registrar el correo para envío de facturas al cliente',
            'nombre_cliente.max' => 'El nombre del cliente no debe contener más de 50 carácteres',
            'descripcion.max' => 'La descripción del cliente no debe contener más de 250 carácteres',
            'nit_cliente.max' => 'El NIT del cliente no debe contener más de 50 carácteres',
            'direccion_cliente.max' => 'La dirección del cliente no debe contener más de 100 carácteres',
            'nombre_contactoPruebas.max' => 'El nombre del contacto de pruebas no debe contener más de 50 carácteres',
            'telefono_contactoPruebas.max' => 'El teléfono del contacto de pruebas no debe contener más de 50 carácteres',
            'correo_contactoPruebas.max' => 'El correo del contacto de pruebas no debe contener más de 50 carácteres',
            'nombre_contactoFacturacion.max' => 'El nombre del contacto de facturación no debe contener más de 50 carácteres',
            'correo_contactoFacturacion.max' => 'El correo del contacto de facturación no debe contener más de 50 carácteres',
            'telefono_contactoFacturacion.max' => 'El teléfono del contacto de facturación no debe contener más de 50 carácteres',
            'plazo_pagoFactura.max' => 'El plazo del pago de la factura no debe contener más de 50 carácteres',
            'fecha_recibidoFactura.max' => 'La fecha máxima de recibido de facturas no debe contener más de 50 carácteres',
            'correo_envioFactura.max' => 'El correo para envío de factura no debe contener más de 50 carácteres',

        ];
    }
    
}
