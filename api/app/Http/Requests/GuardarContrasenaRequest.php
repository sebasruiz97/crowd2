<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuardarContrasenaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $expresionRegular = 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}$/';

        return [
            'current_password' => ['required','string','password'],
            'password' => ['required', 'string', 'min:8', 'confirmed', $expresionRegular],
            'password_confirmation' => ['required','string']
        ];
    }


    public function messages()

    {
        return [
            'current_password.required' =>'La contraseña ingresada, no corresponde a tu contraseña actual',
            'password.required' => 'Es obligatorio que digites una contraseña que contenga entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&',
            'password.min' => 'La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&',
            'password.confirmed' => 'Las contraseñas no coinciden y debe contener entre 8 y 15 caracteres, al menos una letra mayúscula, una minúscula y al menos unos de los siguientes caracteres especiales: $@$!%*?&',
       ];
    }
}
