<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class GuardarProyectoAnexoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        return [
            'url_anexoProyecto'=> "required|array|min:1",    
            'url_anexoProyecto.*'=>'mimes:pdf,png,jpeg,jpg|max:4002',  
            'nota_anexoProyecto'=>'required', 

        ];
    }

    public function messages()

    {
        return [
            'url_anexoProyecto.required' => 'Es obligatorio adjuntar un archivo',
            'nota_anexoProyecto.required' => 'Es obligatorio poner una nota para el archivo cargado',
        ];
    }
}

