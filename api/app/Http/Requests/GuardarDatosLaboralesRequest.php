<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class GuardarDatosLaboralesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        if(($request->input('trabajo_actual'))==null){

            return [
            'user_id'=>'required',  
            'empresa'=>'required|max:50|min:5',  
            'sector_id'=>'required',  
            'cargo_id'=>'required',  
            'fechaInicio_datoLaboral'=>'required|before:today|date|date_format:Y-m',  
            'fechaFin_datoLaboral'=>'required|date|date_format:Y-m|after_or_equal:fechaInicio_datoLaboral', 
            'responsabilidades'=>'nullable|max:200',  
            'trabajo_actual'=>'nullable',
            ];

            }else{
          
            return [
            'user_id'=>'required',  
            'empresa'=>'required|max:50|min:5',  
            'sector_id'=>'required',  
            'cargo_id'=>'required',  
            'fechaInicio_datoLaboral'=>'required|before:today|date|date_format:Y-m',  
            'fechaFin_datoLaboral'=>'nullable',
            'responsabilidades'=>'nullable|max:200', 
            'trabajo_actual'=>'required',
            ];
            }
    }

    public function messages()

    {
        return [
            'empresa.required' => 'Necesitamos que registres el nombre de la empresa',
            'empresa.max:50' => 'El nombre de la empresa debe tener máximo 50 carácteres',
            'sector_id.required' => 'Necesitamos que registres el sector de tu experiencia laboral',
            'cargo_id.required' => 'Necesitamos que registres el cargo que tenias',
            'fechaInicio_datoLaboral.required' => 'Necesitamos que registres cuando iniciaste esta experiencia',
            'fechaInicio_datoLaboral.date'=>'El formato de la fecha ingresada no es válido.',
            'fechaInicio_datoLaboral.before' => 'La fecha de inicio debe ser antes de hoy',
            'fechaFin_datoLaboral.required' => 'La fecha de finalización es obligatoria a menos que sea tu trabajo actual',
            'fechaFin_datoLaboral.date'=>'El formato de la fecha ingresada no es válido',
            'fechaFin_datoLaboral.after_or_equal' => 'La fecha de finalización, no puede ser anterior a la fecha de inicio',
            //'responsabilidades.required' => 'Necesitamos que registres cuáles eran tus responsabilidades',
            'responsabilidades.max' => 'La descripción de tus responsabilidades no debe contener más de 200 carácteres',
            'empresa.min' => 'El nombre de la empresa debe contener mínimo 5 caracteres',
        ];
    }

}
