<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class GuardarEstudiosNoFormalesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if(($request->input('universidad_id'))==361){
        
            return [
            'user_id'=>'required',  
            'tipoEstudio_id'=>'required',  
            'nombre_estudioNoFormal'=>'required|max:50',  
            'fechaFin_estudioNoFormal'=>'required|before:today|date|date_format:Y-m',  
            'duracion'=>'required|numeric',  
            'lugar_estudio'=>'required|max:50',  
            'universidad_id'=>'required', 
            'plataforma_id'=>'nullable', 
            'otra_universidad'=>'required', 
            ];

        }elseif(($request->input('plataforma_id'))==6){

            return [
            'user_id'=>'required',  
            'tipoEstudio_id'=>'required',  
            'nombre_estudioNoFormal'=>'required|max:50',  
            'fechaFin_estudioNoFormal'=>'required|before:today|date|date_format:Y-m',  
            'duracion'=>'required|numeric',  
            'lugar_estudio'=>'required|max:50',  
            'universidad_id'=>'nullable', 
            'plataforma_id'=>'required', 
            'otra_universidad'=>'required', 
            ];
        }else{
            
            return [
            'user_id'=>'required',  
            'tipoEstudio_id'=>'required',  
            'nombre_estudioNoFormal'=>'required|max:50',  
            'fechaFin_estudioNoFormal'=>'required|before:today|date|date_format:Y-m',  
            'duracion'=>'required|numeric',  
            'lugar_estudio'=>'required|max:50',  
            'universidad_id'=>'required_without:plataforma_id', 
            'plataforma_id'=>'required_without:universidad_id', 
            'otra_universidad'=>'nullable',

            ];
        }
    
    }

    public function messages()

    {
        return [
            'tipoEstudio_id.required' => 'Necesitamos que registres que tipo de estudio es',
            'nombre_estudioNoFormal.required' => 'Necesitamos que registres el nombre del estudio',
            'fechaFin_estudioNoFormal.required' => 'Necesitamos que registres la fecha de finalización del estudio',
            'fechaFin_EstudioNoFormal.date'=>'El formato de la fecha ingresada no es válido',
            'fechaFin_estudioNoFormal.before' => 'La fecha de finalización del estudio debe ser anterior a hoy',
            'duracion.required' => 'Necesitamos que registres la duración del estudio en meses. ',
            'duracion.numeric' => 'Debes registrar un número de meses ',
            'lugar.required' => 'Necesitamos que registres la universidad o instituto en que realizaste el estudio',
            'modalidad_estudio.required' => 'Necesitamos que registres la modalidad del estudio',
            'nombre_estudioNoFormal.max:50' => 'El nombre del estudio no debe superar los 50 carácteres',
            'duracion.max:50' => 'La duración no debe superar los 50 carácteres',
            'lugar.max:50' => 'El nombre de la universidad o instituto no debe superar los 50 carácteres',
            'universidad_id.required_without'=>'Es obligatorio que indiques la universidad o instituto donde realizaste el estudio',
            'plataforma_id.requiered_without'=>'Es obligatorio que indiques la plataforma donde realizaste el estudio',

            
        ];
    }

}
