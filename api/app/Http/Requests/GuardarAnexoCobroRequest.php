<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class GuardarAnexoCobroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'url_anexo_cuentaCobro'=> "required",    
            // 'nota_anexo_cuentaCobro'=>'required', 

        ];
    }

    public function messages()

    {
        return [
            // 'url_anexo_cuentaCobro.required' => 'Es obligatorio adjuntar un archivo',
            // 'nota_anexo_cuentaCobro.required' => 'Es obligatorio poner una nota para el archivo cargado',
        ];
    }
}

