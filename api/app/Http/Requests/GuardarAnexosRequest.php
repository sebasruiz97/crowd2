<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuardarAnexosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipoAnexo_id' => 'required',
            'modulo_id' => 'required',
            'comentarios'=> 'nullable|string',
            'user_id'=>'nullable|numeric',
            'cliente_id'=>'nullable|numeric',
            'proyecto_id'=>'nullable|numeric',
            'url'=> 'required|mimes:pdf,png, jpeg|max:3002',
            //el valor del tamaño del archivo es en kilobytes
        ];
    }

    public function messages()

    {
        return [
            'tipoAnexo_id.required' => 'Es obligatorio que elijas el tipo de anexo',
            'url.required' => 'Es obligatorio seleccionar un adjunto',
            'url.mimes' => 'Sólo está permitido subir anexos en formato pdf, png, jpg o jpeg. ',
            'url.max' => 'Sólo está permitido subir anexos de máximo 2 megas. ',
        ];
    }
}
