<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class GuardarEstudiosFormalesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if(($request->input('universidad_id'))==361){

        return [
    
            'user_id'=>'required',  
            'nivelEducacion_id'=>'required',  
            'areaEstudio_id'=>'required',  
            'estadoEstudio_id'=>'required',  
            'fechaInicio_estudioFormal'=>'required|before:today|date|date_format:Y-m',  
            'fechaFin_estudioFormal'=>'after_or_equal:fechaInicio_estudioFormal|date|date_format:Y-m',  
            'universidad_id'=>'required',    
            'departamento_id'=>'required',  
            'otra_universidad'=>'required',             
        ];

        }else{

			
			if (($request->input('estadoEstudio_id')) != 2)
				// En proceso
			$fechaFin_obligatorio = 'required|after_or_equal:fechaInicio_estudioFormal|date|date_format:Y-m';
			else
				$fechaFin_obligatorio = 'nullable'; 
            return [
    
            'user_id'=>'required',  
            'nivelEducacion_id'=>'required',  
            'areaEstudio_id'=>'required',  
            'estadoEstudio_id'=>'required',  
            'fechaInicio_estudioFormal'=>'required|before:today|date|date_format:Y-m',  
            'fechaFin_estudioFormal'=>$fechaFin_obligatorio,  
            'universidad_id'=>'required',    
            'departamento_id'=>'required',  
            'otra_universidad'=>'nullable',             
        ];
        }
    }

    public function messages()

    {
        return [
            'nivelEducacion_id.required' => 'Necesitamos que registres tu nivel de educación',
            'areaEstudio_id.required' => 'Necesitamos que registress tu área de estudio',
            'estadoEstudio_id.required' => 'Necesitamos que registres el estado actual del estudio',
            'fechaInicio_estudioFormal.required' => 'Necesitamos que registres la fecha de inicio del estudio',
            'fechaInicio_estudioFormal.before' => 'La fecha de inicio del estudio debe ser anterior a hoy',
            'fechaInicio_estudioFormal.date'=>'El formato de la fecha ingresada no es válido',
            'fechaFin_estudioFormal.required' => 'Necesitamos que registres la fecha fin del estudio',
            'fechaFin_estudioFormal.date' => 'El formato de la fecha ingresada no es válido',
            'fechaFin_estudioFormal.after_or_equal' => 'La fecha fin del estudio debe ser igual o posterior a la fecha de inicio',
            'universidad_id.required' => 'Necesitamos que registres la universidad en que realizaste el estudio',
            'departamento_id.required' => 'Necesitamos que registres en que departamento realizaste el estudio',
    
        ];
    }

}
