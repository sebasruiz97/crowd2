<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuardarDatosBasicosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required|max:50|regex:/^[\pL\s\-]+$/u',
            'fechaNacimiento'=> 'required|before:today|date|date_format:Y-m-d',
            'genero'=> 'required',
            'pais_id'=> 'required',
            'departamento_id'=> 'required',
            'municipio_id'=> 'required',
			'direcion'=> 'max:100',
            'telefonoFijo'=> 'nullable|numeric',
            'telefonoCelular'=> 'required|numeric',
            //'ocupacion_id'=> 'required',
            'experienciaPruebas'=> 'required',
            'user_id'=> 'required',       
            'avatar' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',     
        ];
    }


    public function messages()

    {
        return [
            'name.required' => 'Es obligatorio que registres nu nombre',
            'name.max' => 'Tu nombre debe ser de máximo 50 caracteres',
            'name.regex' => 'Tu nombre sólo debe contener letras y espacios',
            'fechaNacimiento.required' => 'Necesitamos saber cuado naciste',
            'fechaNacimiento.date'=>'La fecha de nacimiento debe ser una fecha válida',
            'fechaNacimiento.date_format'=>'El formato de la fecha ingresada no es valido.',
            'fechaNacimiento.before' => 'Tu fecha de nacimiento debe ser anterior a hoy',
            'genero.required' => 'Necesitamos que registres tu género',
            'pais_id.required' => 'Necesitamos que registres el país donde vives ',
            'departamento_id.required' => 'Necesitamos que registres el departamento donde vives',
            'municipio_id.required' => 'Necesitamos que registres el municipio donde vives',
            //'direcion.required' => 'Necesitamos que registres tu dirección',
            'direcion.max' => 'La dirección no debe contener más de 100 carácteres',
            'telefonoCelular.required' => 'Necesitamos que registres tu número celular',
            'telefonoCelular.required' => 'Necesitamos que registres tu número celular',
            'telefonoCelular.numeric' => 'El número de teléfono celular debe ser un campo numérico',
            'telefonoFijo.numeric' => 'El número de teléfono fijo debe ser un campo numérico',
            'ocupacion_id.required' => 'Necesitamos que  registres tu ocupación actual',
            'experienciaPruebas.required' => 'Necesitamos que registres si tienes experiencia en pruebas',
            'telefonoCelular.alpha_dash' => 'El número de celular sólo debe contener letras, número y guiones',

       ];
    }
}
