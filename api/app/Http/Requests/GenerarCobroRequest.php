<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class GenerarCobroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       
        return [
    
            'fecha_inicio'=>'required|before:today|date',  
            'fecha_fin'=>'required|after_or_equal:fecha_inicio|date',  
                       
        ];

     
    }

    public function messages()

    {
        return [
            'fecha_inicio.required' => 'Necesitamos que registres la fecha de inicio',
            'fecha_inicio.before' => 'La fecha de inicio debe ser anterior a hoy',
            'fecha_inicio.date'=>'El formato de la fecha ingresada no es válido',
            'fecha_fin.required' => 'Necesitamos que registres la fecha fin ',
            'fecha_fin.date' => 'El formato de la fecha ingresada no es válido',
            'fecha_fin.after_or_equal' => 'La fecha fin debe ser igual o posterior a la fecha de inicio',
    
        ];
    }

}
