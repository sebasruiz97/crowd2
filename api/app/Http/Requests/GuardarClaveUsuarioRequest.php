<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class GuardarClaveUsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $expresionRegular = 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}$/';

        return [
            'cambio_password' => ['required', 'string', 'min:8', 'max:15', $expresionRegular],
        ];
    }

    public function messages()

    {
        return [
            'cambio_password.required' => 'Es obligatorio que digites una contraseña que cumpla con el formato válido',
            'cambio_password.min' => 'La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&',
            'cambio_password.confirmed' => 'Las contraseñas no coinciden',
        ];
    }
}

