<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class GuardarBugAnexoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        return [
            'url_anexoBug'=> "required|array|min:1",    
            'url_anexoBug.*'=>'mimes:pdf,png,jpeg,jpg,mp4|max:8002',  
            'nota_anexoBug'=>'required', 

        ];
    }

    public function messages()

    {
        return [
            'url_anexoBug.required' => 'Es obligatorio adjuntar un archivo',
            'nota_anexoBug.required' => 'Es obligatorio poner una nota para el archivo cargado',
        ];
    }
}

