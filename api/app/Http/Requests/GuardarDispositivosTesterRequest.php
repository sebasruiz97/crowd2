<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuardarDispositivosTesterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'=>'required',  
            'dispositivo_id'=>'required',  
        ];
    }

    public function messages()

    {
        return [
            'dispositivo_id.required' => 'Necesitamos que registres que tipo de dispositivo tienes',
        ];
    }
}
