<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class GuardarUsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $expresionRegular = 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}$/';

        return [
            'name' => ['required', 'string', 'max:50', 'regex:/^[\pL\s\-]+$/u'],
            'rol_id' => ['required', 'numeric'],
            'tipoDocumento_id' => ['required', 'string', 'max:50'],
            'numeroDocumento' => ['required', 'numeric', 'digits_between:6,15'],
            'email' => ['required', 'string', 'email', 'max:50'],
            'password' => ['required', 'string', 'min:8', 'confirmed', $expresionRegular],
        ];
    }

    public function messages()

    {
        return [
            'name.required' => 'Es obligatorio que registres tu nombre',
            'rol_id.required' => 'Es obligatorio que selecciones un rol para el usuario',
            'name.max' => 'Tu nombre debe ser de máximo 50 caracteres',
            'name.regex' => 'Tu nombre sólo debe contener letras y espacios',
            'tipoDocumento_id.required' => 'Es obligatorio que registres tu tipo de documento',
            'numeroDocumento.required' => 'Es obligatorio que registres tu número de documento',
            'numeroDocumento.numeric' => 'Tu número de documento debe ser un valor numérico',
            'numeroDocumento.digits_between' => 'Tu número de documento debe tener cómo mínimo 6 caracteres',
            'email.required' => 'Es obligatorio que registres una dirección de correo',
            'email.max' => 'La dirección de correo no debe contener más de 50 caracteres',
            'password.required' => 'La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula, un dígito y al menos unos de los siguientes caracteres especiales: $@$!%*?&',
            'password.min' => 'La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula. Y al menos, un dígito y un caracter especial',
            'password.confirmed' => 'Las contraseñas no coinciden',
        ];
    }
}

