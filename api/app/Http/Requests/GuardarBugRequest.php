<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class GuardarBugRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $estado = $request->input('bug_estado_id');

        if($estado == null){

            return [
            'bug_tipo_id'=>'required',  
            'proyecto_id'=>'required',  
            'bug_titulo'=>'required|max:250',  
            'bug_descripcion'=>'required',  
            'bug_pasos'=>'required',  
            'bug_reproducible'=>'required',  
            'bug_gravedad'=>'required',  

             ];
        }else{

            if($estado == 3 || $estado == 5){

                return [
                'bug_tipo_id'=>'required',  
                'proyecto_id'=>'required',  
                'bug_titulo'=>'required|max:250',  
                'bug_descripcion'=>'required',  
                'bug_pasos'=>'required',  
                'bug_reproducible'=>'required',  
                'bug_gravedad'=>'required',  
                'bug_estado_id'=>'required',  
                'bug_rechazo_id'=>'required',  
                'bug_notaCambio'=>'required',  

                ];

            }

                return [
                'bug_tipo_id'=>'required',  
                'proyecto_id'=>'required',  
                'bug_titulo'=>'required|max:250',  
                'bug_descripcion'=>'required',  
                'bug_pasos'=>'required',  
                'bug_reproducible'=>'required',  
                'bug_gravedad'=>'required',  
                'bug_estado_id'=>'required',  
                'bug_notaCambio'=>'required',  

                ];
                


        }


        
    }

    public function messages()

    {
        return [
            'bug_tipo_id.required' => 'Es obligatorio especificar el tipo de hallazgo',
            'proyecto_id.required' => 'Es obligatorio especificar el proyecto al que reportarás el hallazgo',
            'bug_titulo.required' => 'Es obligatorio especificar un título para el hallazgo',
            'bug_descripcion.required' => 'Es obligatorio especificar euna descripción para el hallazgo',
            'bug_pasos.required' => 'Es obligatorio especificar los pasos para reproducir el hallazgo',
            'bug_reproducible.required' => 'Es obligatorio especificar si el hallazgo es o no reproducible',
            'bug_gravedad.required' => 'Es obligatorio especificar la gravedad del hallazgo',
        ];
    }
}

