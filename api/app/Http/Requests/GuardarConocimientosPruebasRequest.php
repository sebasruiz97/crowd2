<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuardarConocimientosPruebasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'=>'required',  
            'prueba_id'=>'required',  
            'tiempoExperiencia_id'=>'required',  
            'ejecucion_id'=>'required',  
            'metodologia_id'=>'required',  
            'herramienta_id'=>'required_if:ejecucion_id,2',  
            'dispositivo_id'=>'required', 
            'sistemaOperativo_id'=>'required',  
            'navegador_id'=>'required',              
        ];
    }

    public function messages()

    {
        return [
            'prueba_id.required' => 'Elige el tipo de pruebas para el cuál quiere agregar tu conocimiento',
            'tiempoExperiencia_id.required' => 'Necesitamos saber cuánto tiempo llevas ejecutando este tipo de pruebas',
            'ejecucion_id.required' => 'Necesitamos saber cómo has ejecutado este tipo de pruebas',
            'metodologia_id.required' => 'Necesitamos saber en que marcos de trabajo has ejecutado este tipo de pruebas',
            'herramienta_id.required' => 'Necesitamos saber en cual(es) herramienta(s) saber ejecutar este tipo de pruebas',
            'dispositivo_id.required' => 'Necesitamos saber, en que dispositivo has ejecutado este tipo de pruebas',
            'navegador_id.required' => 'Necesitamos saber que navegador tiene el dispositivo seleccionado',
            'sistemaOperativo_id.required' => 'Necesitamos saber que sistema operativo tiene el dispositivo seleccionado',
        ];
    }

}
