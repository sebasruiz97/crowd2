<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class EditarUsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        return [
            'name_usuario' => ['required', 'string', 'max:50', 'regex:/^[\pL\s\-]+$/u'],
            'rol_usuario' => ['required', 'numeric'],
            'tipoDocumento_usuario' => ['required', 'string', 'max:50'],
            'numeroDocumento_usuario' => ['required', 'numeric', 'digits_between:6,15'],
            'email_usuario' => ['required', 'string', 'email', 'max:50'],
        ];
    }

    public function messages()

    {
        return [
            'name_usuario.required' => 'Es obligatorio que registres tu nombre',
            'rol_usuario.required' => 'Es obligatorio que selecciones un rol para el usuario',
            'name_usuario.max' => 'Tu nombre debe ser de máximo 50 caracteres',
            'name_usuario.regex' => 'Tu nombre sólo debe contener letras y espacios',
            'tipoDocumento_usuario.required' => 'Es obligatorio que registres tu tipo de documento',
            'numeroDocumento_usuario.required' => 'Es obligatorio que registres tu número de documento',
            'numeroDocumento_usuario.numeric' => 'Tu número de documento debe ser un valor numérico',
            'numeroDocumento_usuario.digits_between' => 'Tu número de documento debe tener cómo mínimo 6 caracteres',
            'email_usuario.required' => 'Es obligatorio que registres una dirección de correo',
            'email_usuario.max' => 'La dirección de correo no debe contener más de 50 caracteres',
        ];
    }
}

