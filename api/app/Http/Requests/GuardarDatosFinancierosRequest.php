<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuardarDatosFinancierosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'=>'required',  
            'banco_id'=>'required',  
            'cuentaBancaria_id'=>'required',  
            'numeroCuenta'=>'required|numeric',  
            'nombreTitular'=>'required|max:50',  
            'sucursal'=>'required|max:50',  
            'no_retencion_sin_minimo'=>'required',
            'no_personas_vinculadas'=>'required',

        ];
    }

    public function messages()

    {
        return [
            'banco_id.required' => 'Necesitamos que registres tu entidad bancaria',
            'cuentaBancaria_id.required' => 'Necesitamos que registres tu tipo de cuenta bancaria',
            'numeroCuenta.required' => 'Necesitamos que registres tu número de cuenta',
            'nombreTitular.required' => 'Necesitamos que registres el nombre del titular de la cuenta',
            'sucursal.required' => 'Necesitamos que registres la sucursal',
            'numeroCuenta.numeric' => 'El número de cuenta debe ser una valor numérico',
            'nombreTitular.max:50' => 'El nombre del titular debe contener máximo 50 carácteres',
            'sucursal.max:50' => 'La sucursal debe contener máximo 50 carácteres',
            'no_retencion_sin_minimo.required'=>'Es obligatorio que marques esta casilla',
            'no_personas_vinculadas.required'=>'Es obligatorio que marques esta casilla',
        ];
    }
}
