<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class GuardarCobroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_cuentaCobro'=>'required',  
            'estado_id_cuentaCobro'=>'required',  

        ];
    }

    public function messages()

    {
        return [
            // 'bug_tipo_id.required' => 'Es obligatorio especificar el tipo de hallazgo',
        ];
    }
}

