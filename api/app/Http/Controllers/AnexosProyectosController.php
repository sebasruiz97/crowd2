<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Http\Requests\GuardarProyectoAnexoRequest;
use Redirect,Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Flash;
 
class AnexosProyectosController extends Controller
{

    public function __construct()
    {        
        $this->middleware([
            'auth','roles:1,2,3']);    
    }
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
public function index($id)
{  
    //$id = 1;
    $anexos = \DB::table('proyectos_anexos')
                    ->where([
                    ['deleted_at', '=', null],
                    ['proyecto_id', '=', $id],
                    ])
                    ->get();
    
    if(request()->ajax()) {     
      
        return datatables()->of($anexos)
        ->addColumn('action', 'anexos_proyectos.action_button')
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);   

        return view('anexos_proyectos.index', compact('id'));

    }else{

        return view('anexos_proyectos.index', compact('id'));

    }      
   
    
}
 
 
/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(GuardarProyectoAnexoRequest $request)
{  

    if(auth()->user()->hasRoles(['1','3'])){

        $user_id =  auth()->user()->id;   
        $rol_id = auth()->user()->rol_id;

        $id_proyecto = $request->input('proyecto_id');

        $proyectos = \DB::table('proyectos')->where('id_proyecto', '=',$id_proyecto)->get();
        foreach($proyectos as $proyecto){
            $clienteID = $proyecto->cliente_id;
            $estadoProyecto = $proyecto->ProyectoEstado_id;
            $responsableActual = $proyecto->proyecto_responsable_actual;
        }  

        if($responsableActual === $user_id || $rol_id === 1){

            if($estadoProyecto ===1 || $estadoProyecto ===2){

            $data = $request->validated();

            $files = $request->file('url_anexoProyecto');
            foreach($files as $file){
                
                $nombre      =   time().'.'.$file->getClientOriginalName();
                $target_path    =   storage_path('clientes'.'/cliente_'.$clienteID.'/proyectos'.'/proyecto_'.$id_proyecto.'/anexos'.'/');

                if($file->move($target_path, $nombre)) {
                    $data = array();
                    $data['proyecto_id'] = $id_proyecto;
                    $data['user_id'] = $user_id;
                    $data['url_anexoProyecto'] = $nombre;
                    $data['nota_anexoProyecto'] = $request->input('nota_anexoProyecto');
                    $data['created_at'] = Carbon::now();
                    $data['updated_at'] = Carbon::now();
                    \DB::table('proyectos_anexos')->insert($data);   
                }           
            
            } 
                
            Flash::success('Anexo(s) agregado(s) correctamente');
            return Response::json($data);

            }
        }

        Flash::error('No puede agregar anexos a un proyecto con este estado');
        return Response::json();   

    }

    Flash::error('No está autorizado a agregar anexos a un proyecto');
    return;
                  

}
 
 
/**
 * Show the form for editing the specified resource.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */


public function show($id)
{   

    $rol_id = auth()->user()->rol_id;
    $id_user = auth()->user()->id;

    $anexos = \DB::table('proyectos_anexos')->where('id_anexoProyecto', '=',$id)->get();  
    foreach($anexos as $anexo){
        $usuario = $anexo->user_id;
        $url = $anexo->url_anexoProyecto;
        $idProyecto = $anexo->proyecto_id;             
    }

    $clientes = \DB::table('proyectos')->where('id_proyecto', '=',$idProyecto)->get('cliente_id');  
    foreach($clientes as $cliente){
        $clienteID = $cliente->cliente_id;
    }

    if($id_user == $usuario ||$rol_id ==2 ||$rol_id ==1 ){

        $enlace = storage_path('clientes'.'/cliente_'.$clienteID.'/proyectos'.'/proyecto_'.$idProyecto.'/anexos'.'/');
        return response()->download($enlace.$url);

    } else {
        return ('Sólo el propietario puede descargar el archivo');
    }
     
        
}


 
/**
 * Remove the specified resource from storage.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
{
    $user_id = auth()->user()->id;
    $rol_id = auth()->user()->rol_id;

    $anexos = \DB::table('proyectos_anexos')->where('id_anexoProyecto', '=',$id)->get();
    foreach($anexos as $anexo){
             $usuario = $anexo->user_id;
             $url = $anexo->url_anexoProyecto;
             $idProyecto = $anexo->proyecto_id;             
        }

        $clientes = \DB::table('proyectos')->where('id_proyecto', '=',$idProyecto)->get(); 
        
        foreach($clientes as $cliente){
            $clienteID = $cliente->cliente_id;
            $estadoProyecto = $cliente->ProyectoEstado_id;
        }
   
        if (empty($anexos)) {
            return Response::json($anexos);
        }

        if($user_id == $usuario || $rol_id ==1 ){

            if($estadoProyecto !==4 || $estadoProyecto !==3){

                $anexos = \DB::table('proyectos_anexos')->where('id_anexoProyecto', '=',$id)->delete();
                $data = storage_path('clientes'.'/cliente_'.$clienteID.'/proyectos'.'/proyecto_'.$idProyecto.'/anexos'.'/'.$url);
                unlink($data);

                Flash::success('Anexo eliminado correctamente');
                return Response::json($anexos);                
            }               
            

        } else {
                Flash::error('Sólo el propietario puede eliminar el archivo');
                return Response::json(); 
            }

        
    }


}
