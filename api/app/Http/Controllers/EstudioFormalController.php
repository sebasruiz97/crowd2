<?php

namespace App\Http\Controllers;

use App\EstudioFormal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\GuardarEstudiosFormalesRequest;
use App\Http\Controllers\AppBaseController;
use App\User;
use App\Models\AreaEstudio;
use App\Models\Universidad;
use App\Models\EstadoEstudio;
use App\Models\NivelEducacion;
use App\Models\Departamento;
use App\DatosBasicos;
use App\PuntosTester;
use Flash;
use Response;
use Carbon\Carbon;



class EstudioFormalController extends Controller
{
    public function __construct()
    {        
        $this->middleware([
            'auth','roles:1,2,3']);    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user()->id;
        $estudiosFormales = EstudioFormal::where("user_id","=",$user)->paginate(6);

        foreach($estudiosFormales as $estudioFormal){
                $areaEstudio = AreaEstudio::find($estudioFormal->areaEstudio_id);
                $nivelEducacion = NivelEducacion::find($estudioFormal->nivelEducacion_id);
                $estadoEstudio = EstadoEstudio::find($estudioFormal->estadoEstudio_id);
                $universidad = Universidad::find($estudioFormal->universidad_id);
                $estudioFormal["areaEstudio"] = $areaEstudio;
                $estudioFormal["nivelEducacion"] = $nivelEducacion;
                $estudioFormal["estadoEstudio"] = $estadoEstudio;
                $estudioFormal["universidad"] = $universidad;
        }

        return view('estudios_formales.index', compact('estudiosFormales'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estudios_formales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GuardarEstudiosFormalesRequest $request, User $user)
    {

        $repetido =  EstudioFormal::query()->where([
            ['user_id', '=',  $request->input('user_id')],
            ['nivelEducacion_id', '=', $request->input('nivelEducacion_id')],
            ['areaEstudio_id', '=', $request->input('areaEstudio_id')],
            ['universidad_id', '=', $request->input('universidad_id')],

        ])->get();

        if($repetido->isEmpty()){
            $id = 'Agregar estudios formales';
            $data = $request->validated();
            $estudioFormal = new EstudioFormal($data);
            $estudioFormal->saveOrFail();       
            return redirect(route('asignarPuntos',$id));
        }
            Flash::error('Ya tienes registrado este estudio formal, puedes editarlo si lo deseas.');
            return redirect(route('estudiosFormales.index'));
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EstudioFormal  $estudioFormal
     * @return \Illuminate\Http\Response
     */
    public function show($id_estudioFormal)
    {
        $estudiosFormales = EstudioFormal::where("id_estudioFormal","=",$id_estudioFormal)->get();

        foreach($estudiosFormales as $estudioFormal){
                $areaEstudio = AreaEstudio::find($estudioFormal->areaEstudio_id);
                $nivelEducacion = NivelEducacion::find($estudioFormal->nivelEducacion_id);
                $estadoEstudio = EstadoEstudio::find($estudioFormal->estadoEstudio_id);
                $estudioFormal["areaEstudio"] = $areaEstudio;
                $estudioFormal["nivelEducacion"] = $nivelEducacion;
                $estudioFormal["estadoEstudio"] = $estadoEstudio;
        }

        if (empty($estudiosFormales)) {
                Flash::error('Estudio formal no encontrado');
                return redirect(route('estudiosFormales.index'));
            }

        return view('estudios_formales.show', compact('estudiosFormales'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EstudioFormal  $estudioFormal
     * @return \Illuminate\Http\Response
     */
    public function edit($id_estudioFormal)
    {
        $estudiosFormales = EstudioFormal::where("id_estudioFormal","=",$id_estudioFormal)->get();
		$areaEstudio = AreaEstudio::orderBy('nombre_areaEstudio')->pluck('nombre_areaEstudio','id_areaEstudio');
        $nivelEducacion = NivelEducacion::pluck('nombre_nivelEducacion','id_nivelEducacion');
        $estadoEstudio = EstadoEstudio::pluck('nombre_estadoEstudio','id_estadoEstudio');
        $departamentos = Departamento::pluck('nombre_departamento','id_departamento');
        
    
        //para dejar seleccionado
        $selAreaEstudio = EstudioFormal::where("id_estudioFormal","=",$id_estudioFormal)->select("areaEstudio_id")->first();
        $areaSeleccionada = $selAreaEstudio->areaEstudio_id;
        $selnivelEducacion = EstudioFormal::where("id_estudioFormal","=",$id_estudioFormal)->select("nivelEducacion_id")->first();
        $nivelSeleccionado = $selnivelEducacion->nivelEducacion_id;
        $selestadoEstudio = EstudioFormal::where("id_estudioFormal","=",$id_estudioFormal)->select("estadoEstudio_id")->first();
        $estadoSeleccionado = $selestadoEstudio->estadoEstudio_id;
        $selDepartamento = EstudioFormal::where("id_estudioFormal","=",$id_estudioFormal)->select("departamento_id")->first();
        $departamentoSeleccionado = $selDepartamento->departamento_id;
        $selUniversidad = EstudioFormal::where("id_estudioFormal","=",$id_estudioFormal)->select("universidad_id")->first();
        $universidadSeleccionada = $selUniversidad->universidad_id;
        $selOtraUniversidad = EstudioFormal::where("id_estudioFormal","=",$id_estudioFormal)->select("otra_universidad")->first();
        $otraUniversidadSeleccionada = $selOtraUniversidad->otra_universidad;
        
        if (empty($estudiosFormales)) {
            Flash::error('Estudio formal no encontrado');
            return redirect(route('estudiosFormales.index'));
        }

        return view('estudios_formales.edit',compact('estudiosFormales','areaEstudio','departamentos','departamentoSeleccionado','universidadSeleccionada','nivelEducacion','estadoEstudio','areaSeleccionada','nivelSeleccionado','estadoSeleccionado','otraUniversidadSeleccionada'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EstudioFormal  $estudioFormal
     * @return \Illuminate\Http\Response
     */
    public function update($id_estudioFormal, GuardarEstudiosFormalesRequest $request)
    {

        if(($request->input('universidad_id'))==361){
            $data = $request->validated();
            $data = array();
            $data['user_id'] = $request->input('user_id');
            $data['nivelEducacion_id'] = $request->input('nivelEducacion_id');
            $data['estadoEstudio_id'] = $request->input('estadoEstudio_id');
            $data['areaEstudio_id'] = $request->input('areaEstudio_id');
            $data['fechaInicio_estudioFormal'] = $request->input('fechaInicio_estudioFormal');
            $data['fechaFin_estudioFormal'] = $request->input('fechaFin_estudioFormal');
            $data['departamento_id'] = $request->input('departamento_id');
            $data['universidad_id'] = $request->input('universidad_id');
            $data['otra_universidad'] = $request->input('otra_universidad');
            \DB::table('estudios_formales')->where('id_estudioFormal','=',$id_estudioFormal)->update($data);
            Flash::success('Estudio formal agregado correctamente');
            return redirect(route('estudiosFormales.index'));

            }else{
            $data = $request->validated();
            $data = array();
            $data['user_id'] = $request->input('user_id');
            $data['nivelEducacion_id'] = $request->input('nivelEducacion_id');
            $data['areaEstudio_id'] = $request->input('areaEstudio_id');
            $data['estadoEstudio_id'] = $request->input('estadoEstudio_id');
            $data['fechaInicio_estudioFormal'] = $request->input('fechaInicio_estudioFormal');
            $data['fechaFin_estudioFormal'] = $request->input('fechaFin_estudioFormal');
            $data['departamento_id'] = $request->input('departamento_id');
            $data['universidad_id'] = $request->input('universidad_id');
            $data['otra_universidad'] = null;
            \DB::table('estudios_formales')->where('id_estudioFormal','=',$id_estudioFormal)->update($data);
            Flash::success('Estudio formal agregado correctamente');
            return redirect(route('estudiosFormales.index'));                }
        return redirect(route('estudiosFormales.index'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstudioFormal  $estudioFormal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_estudioFormal)
    {
        $id = 'Eliminar estudios formales';
        $estudiosFormales = EstudioFormal::find($id_estudioFormal);
        if (empty($estudiosFormales)) {
            Flash::error('Estudio Formal no encontrado');
            return redirect(route('estudiosFormales.index'));
        }
        $estudiosFormales->delete($id_estudioFormal);
        return redirect(route('quitarPuntos',$id));
 
    }

    public function buscarUniversidad(Request $request)
    {
        $data = Universidad::select('nombre_universidad','id_universidad')->where('departamento_id',$request->id_departamento)->get();

        return response()->json($data);//then sent this data to ajax success*/
    }
}
