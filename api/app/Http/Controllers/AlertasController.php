<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alerta;
use App\Models\AlertaUsuario;
use Chatify\Facades\ChatifyMessenger as Chatify;
use Flash;

class AlertasController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth', 'roles:1'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = \DB::table('users')->get();
        $alertas = Alerta::get();
        if (request()->ajax()) {
            return datatables()->of($alertas)
                ->addColumn('action_usuarios', 'alertas.buttons.actions_usuarios')
                ->addColumn('action', 'alertas.buttons.actions')
                ->editColumn('created_at', function($alerta){
                    return $alerta->created_at->format('d-m-Y');
                })
                ->editColumn('enlace', function ($alerta) {
                    return '<a href="'.$alerta->enlace.'">'.$alerta->enlace.'</a>';
                })
                ->rawColumns(['action_usuarios','action','enlace'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('alertas.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $data = $request->only('titulo', 'mensaje', 'enlace');
            Alerta::create($data);
            //\DB::table('alertas')->insert($data);
            Flash::success('Alerta creada correctamente');
            return redirect(route('alertas.index'));
            //code...
        } catch (\Throwable $th) {
            Flash::error('Hubo un error al crear la alerta');
            return redirect(route('alertas.index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return "hola store";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return "Vista sin funcion";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return "ola edit";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return "ola update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return "ola destro";
    }

    public function asignarUsuarios(Request $request)
    {
        try {
            if (request()->ajax()) {
                $usuarios = AlertaUsuario::where('id_alerta',$request->id_alerta)->get();
                return \Response::json($usuarios);
            }
            $data = $request->only('alerta','usuarios');
            if(isset($data['usuarios'])){
                $usuarios = AlertaUsuario::where('id_alerta',$request->alerta)->get()->pluck('id_usuario')->toArray();
                $usuariosAdd = array_diff($data['usuarios'],$usuarios);
                $usuariosRem = array_diff($usuarios,$data['usuarios']);
                foreach ($usuariosAdd as $userID) {
                    AlertaUsuario::create([
                        'id_usuario' => $userID,
                        'id_alerta' => $data['alerta'],
                        'estado' => 'asignada'
                    ]);
                }
                foreach ($usuariosRem as $userID) {
                    AlertaUsuario::where([
                        'id_usuario' => $userID,
                        'id_alerta' => $data['alerta']
                    ])->delete();
                }
            }
            \Flash::success('Se guardaron los usuarios correctamente');
            return redirect(route('alertas.index'));
        } catch (\Throwable $th) {
            //throw $th;
            \Flash::error('Hubo un error al asignar los usuarios.');
            return redirect(route('alertas.index'));
        }
    }

    public function enviarAlerta(Request $request)
    {
        $alerta = Alerta::find($request->id_alerta);
        $usuarios = AlertaUsuario::where('id_alerta',$request->id_alerta)->get();
        foreach ($usuarios as $usuario) {
            // send to user using pusher
            AlertaUsuario::find($usuario->id_alertaUsuario)->update(['estado' => 'enviada']);
            Chatify::push('private-chatify', 'alert.'.$usuario->id_usuario, [
                'alerta' => $alerta,
                'usuario' => $usuario->id_usuario
            ]);
        }
        if($usuarios->isEmpty()){
            // Crear alerta general (Se crea al Admin)
            AlertaUsuario::create([
                'id_usuario' => '1',
                'id_alerta' => $alerta->id_alerta,
                'estado' => 'enviada'
            ]);
            // Se envia alerta a todos los usuarios
            Chatify::push('private-chatify', 'alert', [
                'alerta' => $alerta
            ]);
        }
    }

}
