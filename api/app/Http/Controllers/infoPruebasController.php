<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use App\info_pruebas;
use App\User;
use App\Models\Users;
use App\Models\infoPruebas;
use Flash;
use Response;


class infoPruebasController extends Controller
{
    public function __construct()
    {        
        $this->middleware([
            'auth','roles:1,2,3']);
    
    }
    
    public function sinPruebas()
    {
        $user=auth()->user()->id;
        $tipospruebas =  \DB::table('vista_info_pruebas')->where('user_id',$user)->get();
        if($tipospruebas->isEmpty()){
            return view('info_pruebas.sindatos');
            
        }else{
            return(redirect(route('info_pruebas')));
        
        }
    }
    
    public function index()
    {

        $user=auth()->user()->id;
        $tipospruebas = \DB::select('call SP_infoPruebas(?)',array($user));
        foreach ($tipospruebas as &$info) {
            # code...
            $info->herramientas = infoPruebas::find($info->id_info_prueba)->herramientas()->with('tipoHerramienta')->get();
        }
        //$lista_datos_pruebas  =   \DB::table('indice_conocimiento_pruebas')->where('user_id',$user)->get();
        $lista_datos_pruebas = \DB::select('call SP_indiceConocimientoPruebas(?)',array($user));
        $conocimientoPruebas = $lista_datos_pruebas;

        $lista_tipo_pruebas  =  \DB::table('tipo_pruebas')->get();
        $lista_tiempo_experiencia  =      \DB::table('tiempo_experiencias')->get();
        $lista_tipo_ejecucion  =      \DB::table('tipo_ejecucion')->get();

        return view('info_pruebas.index', compact('tipospruebas', 'conocimientoPruebas','user','lista_tipo_pruebas','lista_tiempo_experiencia','lista_tipo_ejecucion' ));
        
    }
    
}