<?php
 
namespace App\Http\Controllers;
 
use App\Bug;
use Illuminate\Http\Request;
use Redirect,Response;
use Carbon\Carbon;

 
class ValidarBugsController extends Controller
{

    public function __construct()
    {        
        $this->middleware([
            'auth','roles:1,3']);    
    }
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
public function index()
{
    if(request()->ajax()) {
        return datatables()->of(Bug::select('*'))
        ->addColumn('action', 'bugs.action_button')
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);
    }
    return view('bugs.index');
}
 
 
/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(Request $request)
{  
    
    $BugID = $request->id_bug;
    $estado_id = 1;        
    $buscarCliente = \DB::table('proyectos')->where('id_proyecto','=',$request->input('proyecto_id'))->get('cliente_id');
    
    foreach($buscarCliente as $buscar){
        $cliente = $buscar->cliente_id;
    }

    $data = array();   
    $data['user_id'] = $request->input('user_id');
    $data['cliente_id'] = $cliente;
    $data['proyecto_id'] = $request->input('proyecto_id');
    $data['bug_tipo_id'] = $request->input('bug_tipo_id');
    $data['bug_titulo'] = $request->input('bug_titulo');
    $data['bug_descripcion'] = $request->input('bug_descripcion');
    $data['bug_pasos'] = $request->input('bug_pasos');
    $data['bug_reproducible'] = $request->input('bug_reproducible');
    $data['bug_gravedad'] = $request->input('bug_gravedad');

        if (($BugID==null))
    {
        $data['created_at'] = Carbon::now();
        $data['bug_estado_id'] = 1;
        \DB::table('bugs')->insert($data);
    }
    else
    {
         $data['updated_at'] = Carbon::now();
         $data['bug_estado_id'] = $estado_id;
        \DB::table('bugs')->where('id_bug', $BugID)->update($data);
    }
    return Response::json($data);

}
 
 
/**
 * Show the form for editing the specified resource.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{   
    $where = array('id_bug' => $id);

    $Bug  = Bug::where($where)->first();
 
    return Response::json($Bug);
}
 
 
/**
 * Remove the specified resource from storage.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
{
    $Bug = Bug::where('id_bug',$id)->delete();
 
    return Response::json($Bug);
}


/**
 * Remove the specified resource from storage.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function tester($id)
{
   
    list($id_proyecto, $id_responsale) = explode(",", $id);
    $id_proyecto = intval($id_proyecto);
    $id_responsale = intval($id_responsale);

    $Bug =  \DB::table('vista_bugs')
                        ->where('proyecto_id', $id_proyecto)
                        ->where('user_id', $id_responsale)
                        ->where(function ($query) {
                            $query->where('bug_estado_id', '=', 1)
                                  ->orWhere('bug_estado_id', '=', 6);
                        })->get();

 

    return Response::json($Bug);
}
public function proyecto($id)
{
   
    $Bug =  \DB::table('vista_bugs')
                        ->where('proyecto_id', $id)
                        ->where(function ($query) {
                            $query->where('bug_estado_id', '=', 1)
                                  ->orWhere('bug_estado_id', '=', 6);
                        })->get();

 

    return Response::json($Bug);
}

}