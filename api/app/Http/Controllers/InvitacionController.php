<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\EventoProyecto;
use Illuminate\Support\Facades\Mail;

class InvitacionController extends Controller
{

    public function inicio($id)
    {
        $proyecto = $id;

        $responsables = \DB::table('vista_proyectos_responsables')->where([
            ['proyecto_id', '=', $proyecto],
            ])->get();
            

        foreach($responsables as $responsable){
            $emailTester = $responsable->email;
            $name = $responsable->nombre_responsable;
            $proyectoNombre = $responsable->proyecto_nombre;
            $evento = 'inicio';
            $titulo = 'Notificación de inicio de proyecto';
            $indicacion = 'ya puedes empezar a reportar hallazgos y podrás hacerlo hasta que el proyecto sea cerrado';

            Mail::to($emailTester)
                ->queue(new EventoProyecto($name,$proyectoNombre, $evento, $titulo, $indicacion));

        }        
        
    }


    public function fin($id)
    {
        $proyecto = $id;

        $responsables = \DB::table('vista_proyectos_responsables')->where([
            ['proyecto_id', '=', $proyecto],
            ])->get();
            

        foreach($responsables as $responsable){
            $emailTester = $responsable->email;
            $name = $responsable->nombre_responsable;
            $proyectoNombre = $responsable->proyecto_nombre;
            $evento = 'cierre';
            $titulo = 'Notificación de cierre de proyecto';
            $indicacion = 'ya no podrás reportar hallazgos sobre el proyecto y se procederá a generar cuentas de cobro para los hallazgos aprobados por el cliente';

            Mail::to($emailTester)
                ->queue(new EventoProyecto($name,$proyectoNombre, $evento, $titulo, $indicacion));

        }        
        
    }
}
