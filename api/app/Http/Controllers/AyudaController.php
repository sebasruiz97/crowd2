<?php

namespace App\Http\Controllers;

use App\Models\Messages;
use Illuminate\Http\Request;

class AyudaController extends Controller
{
    //
    public function index()
    {
        $this->middleware([
            'auth', 'roles:1,4'
        ]);
        $mensajes = messages();
        return view('ayuda.index', compact('mensajes'));
    }

    public function sendMsg(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:users',
            'body' => 'required|string'
        ]);

        Messages::create([ 'user_to_id' => $request->id, 'body' => $request->body, 'user_from_id' => auth()->user()->id ]);

        return response()->json(['to' => $request->id, 'body' => $request->body]);

    }

    public function getMsg(Request $request)
    { 
        $request->validate([
            'id' => 'required|exists:users'
        ]);

        try {
            $mensajes = messages($request->id, 'asc');
            return response()->json($mensajes);
        } catch (\Throwable $th) {
            return response()->json([ 'error' => 1, 'mensaje' => $th->getMessage() ]);
        }

    }

}