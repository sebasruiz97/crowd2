<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GuardarDatosBasicosRequest;
use App\Http\Requests\GuardarContrasenaRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\DatosBasicos;
use App\PuntosTester;
use App\User;
Use App\Models\TipoDocumento;
Use App\Models\Pais;
Use App\Models\Departamento;
Use App\Models\Municipio;
Use App\Models\Ocupacion;
use App\Models\Modulo;
use Flash;
use Carbon\Carbon;



class DatosBasicosController extends Controller
{
    public function __construct()
    {        
        $this->middleware([
            'auth','roles:1,2,3']);    
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request)
    {
        $id = auth()->user()->id;
        $datosBasicos = DatosBasicos::where("user_id","=",$id)->get();
        $puntosAcumulados = PuntosTester::where("user_id","=",auth()->user()->id)->sum('puntos');
        $tiposDocumento = TipoDocumento::where("id_tipoDocumento","=",auth()->user()->tipoDocumento_id)->get();

        foreach($datosBasicos as $datoBasico){
                $pais = Pais::find($datoBasico->pais_id);
                $departamento = Departamento::find($datoBasico->departamento_id);
                $municipio = Municipio::find($datoBasico->municipio_id);
                $ocupacion = Ocupacion::find($datoBasico->ocupacion_id);
                $datoBasico["pais"] = $pais;
                $datoBasico["departamento"] = $departamento;
                $datoBasico["municipio"] = $municipio;
                $datoBasico["ocupacion"] = $ocupacion;
        }
        return view('datos_basicos.index', compact('datosBasicos','tiposDocumento','puntosAcumulados'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $puntosAcumulados = PuntosTester::where("user_id","=",auth()->user()->id)->sum('puntos');
        return view('datos_basicos.create', compact('puntosAcumulados','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GuardarDatosBasicosRequest $request, User $user)
    {
    
        $existe = \DB::table('datos_basicos')->where('user_id','=',auth()->user()->id)->get();

        if($existe->isEmpty()){

        $edad = Carbon::parse($request->input('fechaNacimiento'))->age;
        $id = 'Agregar datos basicos';

        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $nombre = time().$file->getClientOriginalName();
            $file->move(public_path().'/storage/avatars/',$nombre);
        
        $data = $request->validated();

        $data = array();
        $data['user_id'] = $request->input('user_id');
        $data['experienciaPruebas'] = $request->input('experienciaPruebas');
        $data['genero'] = $request->input('genero');
        $data['pais_id'] = $request->input('pais_id');
        $data['departamento_id'] = $request->input('departamento_id');
        $data['municipio_id'] = $request->input('municipio_id');
        $data['direcion'] = $request->input('direcion');
        $data['telefonoFijo'] = $request->input('telefonoFijo');
        $data['telefonoCelular'] = $request->input('telefonoCelular');
        $data['ocupacion_id'] = $request->input('ocupacion_id');
        $data['fechaNacimiento'] = $request->input('fechaNacimiento');
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();
        $data['edad'] = $edad;
        \DB::table('datos_basicos')->insert($data);

        $data2 = array();
        $data2['name'] = $request->input('name');
        $data2['avatar'] = $nombre;
        $data2['updated_at'] = Carbon::now();
        \DB::table('users')->where('id','=',auth()->user()->id)->update($data2);

        }else{
            $data = $request->validated();
    
            $data = array();
            $data['user_id'] = $request->input('user_id');
            $data['experienciaPruebas'] = $request->input('experienciaPruebas');
            $data['genero'] = $request->input('genero');
            $data['pais_id'] = $request->input('pais_id');
            $data['departamento_id'] = $request->input('departamento_id');
            $data['municipio_id'] = $request->input('municipio_id');
            $data['direcion'] = $request->input('direcion');
            $data['telefonoFijo'] = $request->input('telefonoFijo');
            $data['telefonoCelular'] = $request->input('telefonoCelular');
            $data['ocupacion_id'] = $request->input('ocupacion_id');
            $data['fechaNacimiento'] = $request->input('fechaNacimiento');
            $data['created_at'] = Carbon::now();
            $data['updated_at'] = Carbon::now();
            $data['edad'] = $edad;
            \DB::table('datos_basicos')->insert($data);

            $data2 = array();
            $data2['name'] = $request->input('name');
            $data2['updated_at'] = Carbon::now();
            \DB::table('users')->where('id','=',auth()->user()->id)->update($data2);                
        }

        return redirect(route('asignarPuntos',$id));

    }else{
            Flash::error('Ya tienes tus datos básicos registrados, puedes editarlo si lo deseas');
            return redirect(route('datosBasicos.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DatosBasicos  $datosBasicos
     * @return \Illuminate\Http\Response
     */
    public function show(DatosBasicos $datosBasicos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DatosBasicos  $datosBasicos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    
    {
        $users = Auth::user();
        $datosBasicos = DatosBasicos::where("user_id","=",$id)->get();
        $paises = Pais::orderBy('nombre_pais')->pluck('nombre_pais','id_pais');
        $departamentos = Departamento::orderBy('nombre_departamento')->pluck('nombre_departamento','id_departamento');
        $municipios = Municipio::orderBy('nombre_municipio','asc')->pluck('nombre_municipio','id_municipio');
        $ocupaciones = Ocupacion::orderBy('nombre_ocupacion')->pluck('nombre_ocupacion','id_ocupacion');

        //para dejar seleccionado
        $generoSeleccionado = DatosBasicos::where("user_id","=",$users->id)->select("genero")->first();
        $selpais = DatosBasicos::where("user_id","=",$users->id)->select("pais_id")->first();
        $paisSeleccionado =   $selpais->pais_id;
        $selOcupacion = DatosBasicos::where("user_id","=",$users->id)->select("ocupacion_id")->first();
        $ocupacionSeleccionado =   $selOcupacion->ocupacion_id;
        $selGenero = DatosBasicos::where("user_id","=",$users->id)->select("genero")->first();
        $generoSeleccionado =   $selGenero->genero;
        $selExperienciaPruebas = DatosBasicos::where("user_id","=",$users->id)->select("experienciaPruebas")->first();
        $experienciaSeleccionada =   $selExperienciaPruebas->experienciaPruebas;
        $seldepartamento = DatosBasicos::where("user_id","=",$users->id)->select("departamento_id")->first();
        $departamentoSeleccionado =   $seldepartamento->departamento_id;
        $selmunicipio = DatosBasicos::where("user_id","=",$users->id)->select("municipio_id")->first();
        $municipioSeleccionado =   $selmunicipio->municipio_id;

        

        if (empty($datosBasicos)) {
            Flash::error('Datos básicos no encontrados');
            return redirect()->route('datosBasicos.index');
        }
        return view('datos_basicos.edit', compact('users','datosBasicos','paises','departamentos','municipios','ocupaciones','paisSeleccionado','ocupacionSeleccionado','generoSeleccionado','generoSeleccionado','experienciaSeleccionada','departamentoSeleccionado','municipioSeleccionado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DatosBasicos  $datosBasicos
     * @return \Illuminate\Http\Response
     */
    public function update($id_datoBasico, GuardarDatosBasicosRequest $request )
    {

        if($request->hasFile('avatar')){
                $file = $request->file('avatar');
                $nombre = time().$file->getClientOriginalName();
                $file->move(public_path().'/storage/avatars/',$nombre);

                $data = $request->validated();

                $data = array();
                $data['user_id'] = $request->input('user_id');
                $data['experienciaPruebas'] = $request->input('experienciaPruebas');
                $data['genero'] = $request->input('genero');
                $data['pais_id'] = $request->input('pais_id');
                $data['departamento_id'] = $request->input('departamento_id');
                $data['municipio_id'] = $request->input('municipio_id');
                $data['direcion'] = $request->input('direcion');
                $data['telefonoFijo'] = $request->input('telefonoFijo');
                $data['telefonoCelular'] = $request->input('telefonoCelular');
                $data['ocupacion_id'] = $request->input('ocupacion_id');
                $data['fechaNacimiento'] = $request->input('fechaNacimiento');
                $data['created_at'] = Carbon::now();
                $data['updated_at'] = Carbon::now();
                \DB::table('datos_basicos')->where('user_id','=',auth()->user()->id)->update($data);

                $data2 = array();
                $data2['name'] = $request->input('name');
                $data2['avatar'] = $nombre;
                $data2['updated_at'] = Carbon::now();
                \DB::table('users')->where('id','=',auth()->user()->id)->update($data2);

            }
        

        $data = $request->validated();

        $data = array();
        $data['user_id'] = $request->input('user_id');
        $data['experienciaPruebas'] = $request->input('experienciaPruebas');
        $data['genero'] = $request->input('genero');
        $data['pais_id'] = $request->input('pais_id');
        $data['departamento_id'] = $request->input('departamento_id');
        $data['municipio_id'] = $request->input('municipio_id');
        $data['direcion'] = $request->input('direcion');
        $data['telefonoFijo'] = $request->input('telefonoFijo');
        $data['telefonoCelular'] = $request->input('telefonoCelular');
        $data['ocupacion_id'] = $request->input('ocupacion_id');
        $data['fechaNacimiento'] = $request->input('fechaNacimiento');
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();
        \DB::table('datos_basicos')->where('user_id','=',auth()->user()->id)->update($data);

        $data2 = array();
        $data2['name'] = $request->input('name');
        $data2['updated_at'] = Carbon::now();
        \DB::table('users')->where('id','=',auth()->user()->id)->update($data2);

        Flash::success('Datos básicos actualizados correctamente.');

        return redirect(route('datosBasicos.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DatosBasicos  $datosBasicos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_datoBasico)
    {
        $id = 'Eliminar datos basicos';
        $user = auth()->user()->id;
        $datosBasicos = DatosBasicos::find($id_datoBasico);
        if (empty($datosBasicos)) {
            Flash::error('Datos Básicos no encontrados');
            return redirect(route('datosBasicos.index'));
        }
        $datosBasicos->delete($id_datoBasico);
        
        return redirect(route('quitarPuntos',$id));
    }

    // Esta funcion es para el select dinamico de pais/departamento /municipios

    public function buscarDepartamento(Request $request)
    {
        $data = Departamento::select('nombre_departamento','id_departamento')->where('pais_id',$request->id_pais)->get();

        return response()->json($data);//then sent this data to ajax success*/
    }

    public function buscarMunicipio(Request $request)
    {
        $data = Municipio::select('nombre_municipio','id_municipio')->where('departamento_id',$request->id_departamento)->orderBy('nombre_municipio')->get();

        return response()->json($data);//then sent this data to ajax success*/
    }

    public function desactivar($id)
    {
        $usuarios = User::where("id","=",$id)->get();
        foreach($usuarios as $usuario){
            $data = array();
            $data['estado_id'] = 4;
            $data['deleted_at'] = Carbon::now();
            \DB::table('users')->where("id","=",$id)->update($data);

            Auth::logout();
            return redirect(route('login'));
        }

    }

    public function editarContrasena($id){

        return view('datos_basicos.editar_contrasena');

    }

    public function actualizarContrasena($id, GuardarContrasenaRequest $request ){

        $data = array();
        $data['password'] = Hash::make($request->input('password'));
        $data['updated_at'] = Carbon::now();
       \DB::table('users')->where('id','=',auth()->user()->id)->update($data);

        Flash::success('Contraseña actualizada correctamente.');
        return redirect(route('datosBasicos.index'));

    }
}
