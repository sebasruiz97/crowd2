<?php
 
namespace App\Http\Controllers;
 
use App\CuentaCobro;
use App\PuntosTester;
use Illuminate\Http\Request;
use App\Http\Requests\GuardarCobroRequest;
use App\Http\Requests\GenerarCobroRequest;
use Redirect,Response;
use Carbon\Carbon;
use App\Mail\GenerarCuenta;
use App\Mail\RechazoCuenta;
use Illuminate\Support\Facades\Mail;
use Flash;

use App\Imports\CuentaCobroImport;

use App\Imports\CCImport;

use Maatwebsite\Excel\Facades\Excel;
 
class CobrosController extends Controller
{

    public function __construct()
    {
        $this->middleware([
            'auth','roles:1,2,3,4']);
    }
    
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
 public function generar(GenerarCobroRequest $request){

    if(auth()->user()->hasRoles(['1','3'])){

    $fechaInicio = $request->input('fecha_inicio');
    $fechaFin =  $request->input('fecha_fin');
    $usuario = auth()->user()->id;

    $bugsAprobados =  \DB::table('vista_bugs_aprobados')->where([
                    ['proyecto_responsable_actual', '=', $usuario],
                    ['proyectoEstado_id', '=', 4],
                    ['fecha_aprobacion_cliente', '>=', $fechaInicio],
                    ['fecha_aprobacion_cliente', '<=', $fechaFin],
                    ])->get();

    if($bugsAprobados->isEmpty()){
        Flash::error('No se encontraron hallazgos aprobados pendientes por generarles cuenta de cobro');       
        return; 
    }

        \DB::table('bugs_pendientes_pago')->delete();
        foreach($bugsAprobados as $bugAprobado ){

            $id_bug=  $bugAprobado->id_bug;

            $data = array();   
            $data['id_bug'] = $id_bug;
            $data['user_id'] = $bugAprobado->user_id;
            $data['name'] = $bugAprobado->name;
            $data['cliente_id'] = $bugAprobado->cliente_id;
            $data['nombre_cliente'] = $bugAprobado->nombre_cliente;
            $data['proyecto_id'] = $bugAprobado->proyecto_id;
            $data['proyecto_nombre'] = $bugAprobado->proyecto_nombre;
            $data['proyecto_responsable_actual'] = $bugAprobado->proyecto_responsable_actual;
            $data['proyectoEstado_id'] = $bugAprobado->proyectoEstado_id;
            $data['nombre_tipoBug'] = $bugAprobado->nombre_tipoBug;
            $data['bug_titulo'] = $bugAprobado->bug_titulo;
            $data['bug_estado_id'] = $bugAprobado->bug_estado_id;
            $data['nombre_estadoBug'] = $bugAprobado->nombre_estadoBug;
            $data['bug_estadoCobro'] = $bugAprobado->bug_estadoCobro;
            $data['fecha_creacion'] = $bugAprobado->fecha_creacion;
            $data['fecha_aprobacion_cliente'] = $bugAprobado->fecha_aprobacion_cliente;
            $data['id_bugTarifa'] = $bugAprobado->id_bugTarifa;
            $data['tarifa_bug'] = $bugAprobado->tarifa_bug;
            \DB::table('bugs_pendientes_pago')->insert($data);

        }


        $cuentasPorPagar = \DB::table('vista_bugs_pagar')->get();

        foreach($cuentasPorPagar as $cuentaPorPagar){

            $data = array();   
            $data['ciudad_cuentaCobro'] = "Medellín";
            $data['nombre_usuario'] =  $cuentaPorPagar->nombre;
            $data['user_id'] = $cuentaPorPagar->user_id;
            $data['nombre_titular'] =$cuentaPorPagar->nombreTitular;
            $data['tipoDocumento_beneficiario'] = $cuentaPorPagar->nombre_tipoDocumento;
            $data['documento_beneficiario'] = $cuentaPorPagar->numeroDocumento;
            $data['monto_cuentaCobro'] = $cuentaPorPagar->monto;
            $data['valor_cuentaCobro'] = null;
            $data['concepto_cuentaCobro'] = "Servicios";
            $data['descripcion_cuentaCobro'] = $cuentaPorPagar->descripcion;
            $data['cuentaBancaria_beneficiario'] = $cuentaPorPagar->numeroCuentaBancaria;
            $data['tipoCuenta_beneficiario'] =  $cuentaPorPagar->nombre_cuentaBancaria;
            $data['banco_beneficiario'] = $cuentaPorPagar->nombre_banco;
            $data['direccion_beneficiario'] = $cuentaPorPagar->residencia;
            $data['celular_beneficiario'] = $cuentaPorPagar->telefonoCelular;
            $data['estado_id_cuentaCobro'] = 1;
            $data['generada_por'] = auth()->user()->id;
            $data['created_at'] = Carbon::now();
            $insert = \DB::table('cuentas_cobro')->insert($data);

            $ultimaCuenta = \DB::table('cuentas_cobro')->max('id_cuentaCobro');
            $ultimasCuentas = \DB::table('cuentas_cobro')
                                ->where([
                                    ['id_cuentaCobro','=',$ultimaCuenta]
                                    ])
                                ->get();

                foreach($ultimasCuentas as $uc){
                    $fechaCreacion = $uc->created_at;
                }

            $data2 = array();    
            $data2['cuentaCobro_id'] = $ultimaCuenta;
            $data2['user_id'] =auth()->user()->id;
            $data2['cuentaCobro_cambio'] = "Cuenta de cobro generada automáticamente";
            $data2['nota_cuentaCobro_cambio'] = "Se ha generado la cuenta de cobro.";
            $data2['created_at'] = $fechaCreacion;
            \DB::table('cuentas_cobro_cambios')->insert($data2);              
        }

        foreach($bugsAprobados as $bugAprobado){

            $idBug = $bugAprobado->id_bug;
            $cuentas = \DB::table('cuentas_cobro')->where('descripcion_cuentaCobro', 'like',"%#$idBug%")->get();

            foreach($cuentas as $cuenta){
                $id_cuenta = $cuenta->id_cuentaCobro;
                $fecha_cuenta = $cuenta->created_at;
            }

            $data3 = array(); 
            $data3['bug_estadoCobro'] = 2;
            $data3['cuentaCobro_id'] = $id_cuenta;
            \DB::table('bugs')->where('id_bug', $idBug)->update($data3);

            $data4 = array();    
            $data4['bug_id'] = $idBug;
            $data4['user_id'] = auth()->user()->id;
            $data4['bug_cambio'] = "Cuenta de cobro generada";
            $data4['bug_notaCambio'] = "Se ha generado la cuenta de cobro #".$id_cuenta;
            $data4['created_at'] = Carbon::now();
            \DB::table('bug_cambios')->insert($data4);

        }
        Flash::success('Cuentas de cobro generadas, recuerde que debe revisar y aprobar las cuentas de cobro, para que le sean notificadas al tester');
        return; 
    }

    Flash::error('No está autorizado para generar cuentas de cobro');
    
}


 public function index()
{
    
    $rol = auth()->user()->rol_id;
    $user = auth()->user()->id;

    if($rol == 1){

        if(request()->ajax()) {
        return datatables()->of(\DB::table('vista_cuentas_cobro')->where('deleted_at','=',null))
                ->addColumn('action', 'cobros.action_button')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

    }elseif($rol == 3){

        if(request()->ajax()) {
        return datatables()->of(\DB::table('vista_cuentas_cobro')->where([
            ['deleted_at','=',null],
            ['generada_por','=',auth()->user()->id],
            ]))
            ->addColumn('action', 'cobros.action_button')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
    
    }elseif($rol == 4){

        if(request()->ajax()) {
        return datatables()->of(\DB::table('vista_cuentas_cobro')->where([
            ['deleted_at','=',null],
            ['id_estado_cuentaCobro', '=', 2],
            ])->orWhere([
            ['deleted_at','=',null],
            ['id_estado_cuentaCobro', '=', 3],
            ])->orWhere([
            ['deleted_at','=',null],
            ['id_estado_cuentaCobro', '=', 4],
            ])->orWhere([
            ['deleted_at','=',null],
            ['id_estado_cuentaCobro', '=', 6],
            ]))
            ->addColumn('action', 'cobros.action_button')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
     }else{

        if(request()->ajax()) {
            return datatables()->of(\DB::table('vista_cuentas_cobro')->where([
            ['user_id', '=', auth()->user()->id],
            ['id_estado_cuentaCobro', '!=', 1],
            ['deleted_at', '=', null]]))
            ->addColumn('action', 'cobros.action_button')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);    
        }

    }

    return view('cobros.index');
}
 
 
/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(GuardarCobroRequest $request)
{  
    $user_id = auth()->user()->id;
    $rol_id = auth()->user()->rol_id;
    $rechazo = $request->input('motivo_rechazo_cuentaCobro');
    $motivos= \DB::table('cuentas_cobro_estados')->where('id_estado_cuentaCobro','=',$request->input('estado_id_cuentaCobro'))->get();

    foreach($motivos as $motivo){
        $cambio = $motivo->nombre_estado_cuentaCobro;
    }

    $id_cuentaCobro = $request->input('id_cuentaCobro');
    $id_estado = $request->input('estado_id_cuentaCobro');

    $cuentas = \DB::table('cuentas_cobro')->where('id_cuentaCobro','=',$id_cuentaCobro)->get();
    foreach($cuentas as $cuenta){
        $estadoActual_cuentaCobro = $cuenta->estado_id_cuentaCobro;
    }

    if($rol_id === 3){

        if($estadoActual_cuentaCobro == 1 || $estadoActual_cuentaCobro == 7){

            if($id_estado == 7){

                $data = array();   
                $data['estado_id_cuentaCobro'] = $request->input('estado_id_cuentaCobro');
                $data['updated_at'] = Carbon::now();
                \DB::table('cuentas_cobro')->where('id_cuentaCobro', $id_cuentaCobro)->update($data);

                $data2 = array();   
                $data2['cuentaCobro_id'] = $id_cuentaCobro;
                $data2['user_id'] = $user_id;
                $data2['cuentaCobro_cambio'] = $cambio;
                $data2['nota_cuentaCobro_cambio'] = $request->input('cobro_notaCambio');
                $data2['created_at'] = Carbon::now();
                \DB::table('cuentas_cobro_cambios')->insert($data2);  

                //Mail para notificar al tester generacion de cuenta de cobro

                $testers = \DB::table('vista_cuentas_cobro')->where('id_cuentaCobro', $id_cuentaCobro)->get();
                foreach($testers as $tester){
                    $user_id = $tester->user_id;
                    $nombre = $tester->nombre_titular;
                    $fecha = $tester->created_at;
                }

                $correos = \DB::table('users')->where('id', $user_id)->get('email');
                foreach($correos as $correo){
                    $emailTester = $correo->email;
                }

                Mail::to($emailTester)
                    ->queue(new GenerarCuenta($user_id, $nombre, $fecha, $id_cuentaCobro));

                Flash::success('Se ha actualizado correctamente el estado de la cuenta de cobro');
                return; 
            }

        }

        Flash::error('No puede actualizar el estado de una cuenta de cobro, si el tester o el contador ya lo han cambiado');
        return Response::json();

    }elseif($rol_id === 2){

        if($estadoActual_cuentaCobro == 7 || $estadoActual_cuentaCobro == 5 || $estadoActual_cuentaCobro == 4){

            if($id_estado == 4 || $id_estado == 5){

                $data = array();   
                $data['estado_id_cuentaCobro'] = $request->input('estado_id_cuentaCobro');

                if($rechazo===null){
                    $data['motivo_rechazo_tester'] = null;
                }else{
                    $data['motivo_rechazo_tester'] = $request->input('motivo_rechazo_cuentaCobro');            
                }
                $data['updated_at'] = Carbon::now();
                \DB::table('cuentas_cobro')->where('id_cuentaCobro', $id_cuentaCobro)->update($data);

                $data2 = array();   
                $data2['cuentaCobro_id'] = $id_cuentaCobro;
                $data2['user_id'] = $user_id;
                $data2['cuentaCobro_cambio'] = $cambio;
                $data2['nota_cuentaCobro_cambio'] = $request->input('cobro_notaCambio');
                $data2['created_at'] = Carbon::now();
                \DB::table('cuentas_cobro_cambios')->insert($data2);  

                if($id_estado == 5){

                    $lideres = \DB::table('cuentas_cobro_cambios')
                                    ->where([
                                        ['cuentaCobro_id', $id_cuentaCobro],
                                        ['cuentaCobro_cambio', 'Cuenta de cobro generada automáticamente'],
                                    ])->get();

                    foreach($lideres as $lider){
                        $user_id = $lider->user_id;
                    }
                    
                    $correos = \DB::table('users')->where('id', $user_id)->get('email');
                    foreach($correos as $correo){
                        $emailLider = $correo->email;
                    }

                    $cuentas = \DB::table('vista_cuentas_cobro')->where('id_cuentaCobro', $id_cuentaCobro)->get();
                    foreach($cuentas as $cuenta){
                        $user_id = $cuenta->user_id;
                        $fecha = $cuenta->created_at;
                        $motivoRechazo = $cuenta->nombre_rechazo_cuentaCobro;
                    }

                    $quienRechaza = 'tester ';
                    $mensaje = 'Por favor contactar al tester, para obtener más detalles.';

                    Mail::to($emailLider)
                        ->queue(new RechazoCuenta($fecha, $id_cuentaCobro, $motivoRechazo, $quienRechaza, $mensaje));
                    }

            }
            
            Flash::success('Se ha actualizado correctamente el estado de la cuenta de cobro');
            return;                

        }

        Flash::error('No puede actualizar el estado de una cuenta de cobro, si el contador ya lo ha cambiado');
        return Response::json();
       
    }
    
    if($id_estado == 2 || $id_estado == 3 || $id_estado == 6){

        $data = array();   
        $data['estado_id_cuentaCobro'] = $request->input('estado_id_cuentaCobro');

        if($rechazo===null){
            $data['motivo_rechazo_contador'] = null;
        }else{
            $data['motivo_rechazo_contador'] = $request->input('motivo_rechazo_cuentaCobro');            
        }
        $data['updated_at'] = Carbon::now();
        \DB::table('cuentas_cobro')->where('id_cuentaCobro', $id_cuentaCobro)->update($data);

        $data2 = array();   
        $data2['cuentaCobro_id'] = $id_cuentaCobro;
        $data2['user_id'] = $user_id;
        $data2['cuentaCobro_cambio'] = $cambio;
        $data2['nota_cuentaCobro_cambio'] = $request->input('cobro_notaCambio');
        $data2['created_at'] = Carbon::now();
        \DB::table('cuentas_cobro_cambios')->insert($data2);  

        if($id_estado == 2){
            
            //Quitar puntos porque se rechazo por culpa del tester

            $cuentas = \DB::table('vista_cuentas_cobro')->where('id_cuentaCobro','=',$id_cuentaCobro)->get();

            foreach($cuentas as $cuenta){
                        $user_id = $cuenta->user_id;
                        $fecha = $cuenta->created_at;
                        $motivoRechazo = $cuenta->nombre_rechazo_cuentaCobro;
                        $generadaPor = $cuenta->generada_por;
                    }

            $datosTester = \DB::table('users')->where("id","=",$user_id)->get();
            foreach($datosTester as $datoTester){
                $emailTester = $datoTester->email;
            }

            $datosLider = \DB::table('users')->where("id","=",$generadaPor)->get();
            foreach($datosLider as $datoLider){
                $emailLider = $datoLider->email;
            }

            $data3 = array();   
            $data3['user_id'] = $user_id;
            $data3['puntos'] = -1;
            $data3['motivo'] = 'Cuenta de cobro rechazada por motivos atribuibles al tester';
            $data3['created_at'] = Carbon::now();
            \DB::table('puntos_tester')->insert($data3);  

            $puntosAcumulados = PuntosTester::where("user_id","=",$user_id)->sum('puntos');
            $users = \DB::table('users')->where("id","=",$user_id)->get();
            foreach($users as $user){
                $data5 = array();
                $data5['puntosAcumulados'] = $puntosAcumulados;
                $data5['updated_at'] = Carbon::now();
                \DB::table('users')->where("id","=",$user_id)->update($data5);
            }

            $quienRechaza = 'contador ';
            $emailContador = auth()->user()->email;
            $mensaje = "En caso de requerir información adicional, puede contar al lider al correo ".$emailLider." o al contador al correo ".$emailContador;
            

                    Mail::to($emailTester)
                        ->cc($emailLider)
                        ->queue(new RechazoCuenta($fecha, $id_cuentaCobro, $motivoRechazo, $quienRechaza, $mensaje));
        }
 
        if($id_estado == 6){

            $bugs = \DB::table('bugs')->where('cuentaCobro_id','=',$id_cuentaCobro)->get();

            foreach($bugs as $bug){

                $idBUG = $bug->id_bug;
                $cuentaCobro = $bug->cuentaCobro_id;

                $data3 = array();   
                $data3['bug_estadoCobro'] = 3;
                $data3['updated_at'] = Carbon::now();
                \DB::table('bugs')->where('id_bug','=', $idBUG)->update($data3);  

                $data4 = array();    
                $data4['bug_id'] = $idBUG;
                $data4['user_id'] = auth()->user()->id;
                $data4['bug_cambio'] = "Cuenta de cobro pagada";
                $data4['bug_notaCambio'] = "Se ha pagado la cuenta de cobro #".$cuentaCobro;
                $data4['created_at'] = Carbon::now();
                \DB::table('bug_cambios')->insert($data4);

            }
        }

        Flash::success('Se ha actualizado correctamente el estado de la cuenta de cobro');
        return; 

    }

    Flash::error('No puede cambiar la cuenta de cobro a este estado');
    return; 

}
 
 
/**
 * Show the form for editing the specified resource.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */

public function show($id)
{    
    $data = \DB::table('vista_cuentas_cobro_cambios')->where('id_cuentaCobro','=',$id)->get();
    return Response::json($data);
}

public function edit($id)
{   
    $where = array('id_cuentaCobro' => $id);

    $cuenta  = CuentaCobro::where($where)->first();
 
    return Response::json($cuenta);
}

 
/**
 * Remove the specified resource from storage.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
    {
        if(auth()->user()->rol_id === 3){

            $cuentas = \DB::table('cuentas_cobro')->where('id_cuentaCobro','=',$id)->get();

            foreach ($cuentas as $cuenta){
                $estado = $cuenta->estado_id_cuentaCobro;
            }

            if($estado===1 || $estado===7 || $estado===5){

                $data = array();    
                $data['deleted_at'] = Carbon::now();
                $cobros = \DB::table('cuentas_cobro')
                            ->where('id_cuentaCobro','=',$id)
                            ->update($data);

                $bugs = \DB::table('bugs')->where('cuentaCobro_id','=',$id)->get();
                foreach($bugs as $bug){

                    $data4 = array();    
                    $data4['bug_id'] = $bug->id_bug;
                    $data4['user_id'] = auth()->user()->id;
                    $data4['bug_cambio'] = "Cuenta de cobro eliminada";
                    $data4['bug_notaCambio'] = "Se ha eliminado una cuenta de cobro";
                    $data4['created_at'] = Carbon::now();
                    \DB::table('bug_cambios')->insert($data4);

                }

                $data2 = array();
                $data2['bug_estadoCobro'] = 1;
                $data2['cuentaCobro_id'] = null;
                $cobros = \DB::table('bugs')
                            ->where('cuentaCobro_id','=',$id)
                            ->update($data2);
               
                Flash::success('Cuenta de cobro eliminada correctamente');
                return Response::json($cobros);
            }

            Flash::error('No puede eliminar una cuenta de cobro que tenga este estado');
            return Response::json();

            
        }

        Flash::error('No está autorizado a eliminar cuentas de cobro');
        return Response::json();
    }


public function imprimir($id)
    {
    
    $cuentas = \DB::table('cuentas_cobro')->where('id_cuentaCobro','=',$id)->get();
    $pdf = \PDF::loadView('cobros.descargar', compact('cuentas'));
    return $pdf->download('CuentaCobro.pdf');

   }

public function usuarioCobro(Request $request)
    {

        $id_cuentaCobro= $request->input('cuentaCobro_id');
        $tipo = $request->input('tipo');

        $cuentasCobro = \DB::table('cuentas_cobro')->where('id_cuentaCobro','=',$id_cuentaCobro)->get();

        foreach($cuentasCobro as $cuenta){
            $usuario = $cuenta->user_id;
        }

        if($tipo === 'Documento de identificación'){

            $anexosUsuario = \DB::table('anexos')
                                ->where([
                                    ['user_id','=',$usuario],
                                    ['tipoAnexo_id','=',1]
                                ])
                                ->get('url');
            
            if($anexosUsuario->isEmpty()){
                Flash::error('Anexo no encontrado, es posible que el tester no lo haya cargado o lo haya eliminado de la plataforma');
                return redirect(route('cobros.index'));
            }

            foreach($anexosUsuario as $anexo){
                $url = $anexo->url;
                $enlace = storage_path('testers/tester_'.$usuario.'/');
                return response()->download($enlace.$url);
            }
        
        }elseif($tipo === 'Certificación Bancaria'){
            
            $anexosUsuario = \DB::table('anexos')
                                ->where([
                                    ['user_id','=',$usuario],
                                    ['tipoAnexo_id','=',4]
                                ])->get('url');
        
            if($anexosUsuario->isEmpty()){
                Flash::error('Anexo no encontrado, es posible que el tester no lo haya cargado o lo haya eliminado de la plataforma');
                return redirect(route('cobros.index'));
            }

            foreach($anexosUsuario as $anexo){
                $url = $anexo->url;
                $enlace = storage_path('testers/tester_'.$usuario.'/');
                return response()->download($enlace.$url);
            }

        }elseif($tipo === 'RUT'){
            
            $anexosUsuario = \DB::table('anexos')
                                ->where([
                                    ['user_id','=',$usuario],
                                    ['tipoAnexo_id','=',3]
                                ])->get('url');
        
            if($anexosUsuario->isEmpty()){
                Flash::error('Anexo no encontrado, es posible que el tester no lo haya cargado o lo haya eliminado de la plataforma');
                return redirect(route('cobros.index'));
            }

            foreach($anexosUsuario as $anexo){
                $url = $anexo->url;
                $enlace = storage_path('testers/tester_'.$usuario.'/');
                return response()->download($enlace.$url);
            }

        }else{

            $anexosUsuario = \DB::table('anexos')
                                ->where([
                                    ['user_id','=',$usuario],
                                    ['tipoAnexo_id','=',6]
                                ])->get('url');
        
            if($anexosUsuario->isEmpty()){
                Flash::error('Anexo no encontrado, es posible que el tester no lo haya cargado o lo haya eliminado de la plataforma');
                return redirect(route('cobros.index'));
            }

            foreach($anexosUsuario as $anexo){
                $url = $anexo->url;
                $enlace = storage_path('testers/tester_'.$usuario.'/');
                return response()->download($enlace.$url);
            }
        }
    }


    public function enviar(){


        return view('cobros.cobros');
        
    }

    public function enviarpdf(Request $request){

        $archivo = $request->file("archivo");
        $fecha = $request->input("fecha");
        $fechamax = $request->input("fechamax");
        $mes_reto = $request->input("mes_reto");
        $descripcion = $request->input("descripcion");
        $concepto = $request->input("concepto");

        if($fecha != ""){
            $fecha = $request->input("fecha");
        }else {
            $fecha= Carbon::now()->toDateTimeString();
        }
        
        $excel = Excel::import(new CuentaCobroImport($fecha,$fechamax,$mes_reto,$concepto,$descripcion),$archivo);
    
        $cuentaCobro= CuentaCobro::where('created_at',$fecha)
        ->where('concepto_cuentaCobro',$concepto)
        ->where('descripcion_cuentaCobro',$descripcion)->get();
        //return  view('cobros.cobros');
        return response()->json([
            "mensaje" => $cuentaCobro
        ]);
    }
}



