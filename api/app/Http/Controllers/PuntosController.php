<?php

namespace App\Http\Controllers;

use App\PuntosTester;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Comunes\actualizarPuntos;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\ConocimientoPruebas;
use App\DatoFinanciero;
use App\DatoLaboral;
use App\DatosBasicos;
use App\DispositivoTester;
use App\EstudioFormal;
use App\EstudioNoFormal;
use App\Models\NivelEducacion;
use App\Models\AreaEstudio;
use App\Models\EstadoEstudio;
use Flash;
use Carbon\Carbon;
use App\Helpers;

class PuntosController extends Controller

{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function asignarPuntos($id)
    {

        $user = auth()->user()->id;
        $puntos = new PuntosTester();
        $puntos->user_id = $user;
        $puntos->puntos = 1;
        $puntos->motivo = $id;
        $puntos->save();
        Flash::success('Felicitaciones! Has ganado 1 punto por completar esta información');

        if ($id == 'Agregar datos basicos') {

            $this->puntos();
            $this->cambiarEstado();

            return redirect(route('datosBasicos.index'));
        } elseif ($id == 'Agregar estudios formales') {

            $this->puntos();
            $this->cambiarEstado();
            return redirect(route('estudiosFormales.index'));
        } elseif ($id == 'Agregar estudios no formales') {

            //$this->puntos();
            $this->cambiarEstado();
            return redirect(route('estudiosNoFormales.index'));
        } elseif ($id == 'Agregar datos laborales') {

            //$this->puntos();
            $this->cambiarEstado();
            return redirect(route('datosLaborales.index'));
        } elseif ($id == 'Agregar datos financieros') {

            $this->puntos();
            $this->cambiarEstado();
            return redirect(route('datosFinancieros.index'));
        } elseif ($id == 'Agregar Anexos') {

            $this->puntos();
            $this->cambiarEstado();
            return redirect(route('anexos.index'));
        } else
            return redirect(route('home'));
    }

    public function quitarPuntos($id)
    {

        $user = auth()->user()->id;

        $puntos = new PuntosTester();
        $puntos->user_id = $user;
        $puntos->puntos = -1;
        $puntos->motivo = $id;
        $puntos->save();
        Flash::error('Que lástima! Has perdido 1 punto por eliminar esta información');

        if ($id == 'Eliminar datos basicos') {

            $this->puntos();
            $this->cambiarEstado();
            return redirect(route('datosBasicos.index'));
        } elseif ($id == 'Eliminar estudios formales') {

            $this->puntos();
            $this->cambiarEstado();
            return redirect(route('estudiosFormales.index'));
        } elseif ($id == 'Eliminar estudios no formales') {

            //$this->puntos();
            $this->cambiarEstado();
            return redirect(route('estudiosNoFormales.index'));
        } elseif ($id == 'Eliminar datos laborales') {

            //$this->puntos();
            $this->cambiarEstado();
            return redirect(route('datosLaborales.index'));
        } elseif ($id == 'Eliminar datos financieros') {

            $this->puntos();
            $this->cambiarEstado();
            return redirect(route('datosFinancieros.index'));
        } elseif ($id == 'Eliminar Anexos') {

            $this->puntos();
            $this->cambiarEstado();
            return redirect(route('anexos.index'));
        } else
            return redirect(route('home'));
    }


    private function puntos()
    {

        $puntosAcumulados = PuntosTester::where("user_id", "=", auth()->user()->id)->sum('puntos');
        $users = User::query()->where("id", "=", auth()->user()->id)->get();
        foreach ($users as $user) {
            $data3 = array();
            $data3['puntosAcumulados'] = $puntosAcumulados;
            $data3['updated_at'] = Carbon::now();
            \DB::table('users')->where("id", "=", auth()->user()->id)->update($data3);
        }
    }

    public function mostrarPuntos($id)
    {
        $puntosTester = PuntosTester::query()->where("user_id", "=", $id)->orderBy('created_at', 'DESC')->paginate(10);
        if (empty($puntosTester)) {
            Flash::error('Aún no has acumulado puntos');
            return redirect(route('inicioTester'));
        }
        $puntosTotal = PuntosTester::query()
            ->where("user_id", "=", $id)->sum('puntos');
        return view('home_tester.index', compact('puntosTester', 'puntosTotal'));
    }

    private function cambiarEstado()
    {

        // Uso funcion Helpers
        $puntosPerfil = datosObligatorios();

        $usuarios = User::where("id", "=", auth()->user()->id)->get();
        //if($puntosPerfil > 8){
        $estado = 2;
        if($puntosPerfil >= count(getArrayObligatorios())){
            $estado = 3;
        }
        foreach ($usuarios as $usuario) {
            $data = array();
            $data['estado_id'] = $estado;
            $data['updated_at'] = Carbon::now();
            \DB::table('users')->where("id", "=", auth()->user()->id)->update($data);
        }
    }
}
