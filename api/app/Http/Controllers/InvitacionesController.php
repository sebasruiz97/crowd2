<?php
 
namespace App\Http\Controllers;
 
use App\Proyecto;
use Illuminate\Http\Request;
use Redirect,Response;
use Carbon\Carbon;
use Flash;
 
class InvitacionesController extends Controller
{
/**


 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */

public function __construct()
{        
    $this->middleware([
        'auth','roles:1,2']);    
}

 
public function index()
{
    $usuario_actual = (auth()->user()->id);
    if(request()->ajax()) {
        $query = \DB::table('vista_lista_invitaciones_index')->where([
            ["invitacion_tester_id","=", $usuario_actual]
            ]);
        //return datatables()->of(proyecto::select('*'))
        return datatables()->of( $query)
        ->addColumn('action', 'invitaciones.action_button')
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);
    }
    return view('invitaciones.index');
}


/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(Request $request)
{  

}

 
/**
 * Show the form for editing the specified resource.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{   
    $datos_invitacion = $invitacion = \DB::table('proyectos_invitaciones')->where('id_invitacion', '=',  $id)->first();
  
    
    $invitacion_tester_id = $datos_invitacion->invitacion_tester_id;
    $proyecto = $datos_invitacion->invitacion_proyecto_id;

    $invitacion = \DB::table('proyectos_invitaciones')->where('id_invitacion', '=',  $id)->update(
        ['invitacion_estado' =>"Aceptada",
        'invitacion_fecha_aceptacion'=> date('Y-m-d H:i:s')]
    );
// debe insertar en tabla de responsables del proyecto

        $data = array();
        $data['id_proyecto_responsable'] =  $invitacion_tester_id;
        $data['proyecto_id'] = $proyecto;
        $data['proyectorole_id'] = 2;
        $data['proyectoResponsable_fecha_inicio'] = date('Y-m-d H:i:s');
        $data['proyectoResponsable_fecha_fin'] = NULL;
        $data['proyectoResponsable_porcentaje_participacion'] = 100;
        $data['proyectoResponsable_Activo_SN'] =1;
        $data['proyectoResponsable_motivoFin'] = NULL;
 
        $responsables = \DB::table("proyectos_responsables")->where([
            ["id_proyecto_responsable","=", $invitacion_tester_id],
            ["proyecto_id", "=", $proyecto]
            ])->first();
            
            

            if(!$responsables)
            {
                //insertar nuevo
                \DB::table('proyectos_responsables')->insert($data);
            }

    Flash::success('Invitación aceptada correctamente');
    return Response::json($invitacion);
}
 
 
/**
 * Remove the specified resource from storage.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
{
    $invitacion = \DB::table('proyectos_invitaciones')->where('id_invitacion', '=',  $id)->update(
        ['invitacion_estado' =>"Rechazada",
        'invitacion_fecha_rechazo'=> date('Y-m-d H:i:s')]
    );
   // $invitacion = \DB::table('proyectos_invitaciones')->where('id_invitacion', '=',  $id)->update(['invitacion_estado' =>"Rechazada"]);
   Flash::success('Invitación rechazada correctamente'); 
   return Response::json($invitacion);
}
}