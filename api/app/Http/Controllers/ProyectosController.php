<?php
 
namespace App\Http\Controllers;
 
use App\Proyecto;
use Illuminate\Http\Request;
use Redirect,Response;
use Carbon\Carbon;
use Flash;
 
class ProyectosController extends Controller
{

    public function __construct()
    {        
        $this->middleware([
            'auth','roles:1,3']);    
    }
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
public function index()

{
    
    $user_id =auth()->user()->id;

    if(auth()->user()->hasRoles(['1'])){
        $query = \DB::table('vista_proyectos_index');
    }else{
        $query = \DB::table('vista_proyectos_index')->where('user_id','=',$user_id);
    }
    
    if(request()->ajax()) {
        return datatables()->of( $query)
        ->addColumn('action', 'proyectos.action_button')
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);
    }
    return view('proyectos.index');
}


/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(Request $request)
{  

    $proyectoId = $request->id_proyecto;
    $data = array();
   
      
    $data['cliente_id'] = $request->input('cliente_id2');
    $data['ProyectoEstado_id'] = $request->input('ProyectoEstado_id2');
    $data['proyecto_nombre'] = $request->input('proyecto_nombre');
    $data['proyecto_descripcion'] = $request->input('proyecto_descripcion');
    $data['proyecto_fecha_inicio'] = $request->input('proyecto_fecha_inicio');
    $data['proyecto_fecha_entrega'] = $request->input('proyecto_fecha_entrega');
    $data['proyecto_presupuesto'] = $request->input('proyecto_presupuesto');
    $data['proyecto_responsable_actual'] = $request->input('proyecto_responsable_actual2');
    $rutaCorreo=null;
    $estado= $request->input('ProyectoEstado_id2');
    $NombreCorreo =  \DB::table('proyectos_estados')->where('id_estadoProyecto', $estado)->get('estadoProyecto_EnviarCorreo');
    foreach ($NombreCorreo as $correo) {
        $rutaCorreo =  $correo->estadoProyecto_EnviarCorreo;
    }
    if ($rutaCorreo!=null)
    {
        $enviar_invitacion = new \App\Http\Controllers\InvitacionController();
        if ($rutaCorreo!="INICIO")
        {
            $enviar_invitacion->inicio($proyectoId);
        }
        if ($rutaCorreo!="FIN")
        {
            $enviar_invitacion->inicio($proyectoId);
        }
    }

    if($request->has('id_proyecto') && !empty($request->input('id_proyecto')))
    
    {
        $data['updated_at'] = Carbon::now();
        \DB::table('proyectos')->where('id_proyecto', $proyectoId)->update($data);
        Flash::success('Proyecto actualizado correctamente');

    }
    else
    {
        $data['created_at'] = Carbon::now();
        \DB::table('proyectos')->insert($data);  
        Flash::success('Proyecto creado correctamente');
    }
    
    return Response::json($data);
}

 
/**
 * Show the form for editing the specified resource.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{   
    $where = array('id_proyecto' => $id);
    $proyecto = \DB::table('vista_proyectos_index')->where('id_proyecto', '=',  $id)->get();
    //$proyecto = \DB::table('vista_proyectos_index')::where($where)->first();
    //$proyecto  = proyecto::where($where)->first();
 
    return Response::json($proyecto);
}
 
 
/**
 * Remove the specified resource from storage.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
{
    if(auth()->user()->hasRoles(['1'])){
        $cantidadbugs=0;
        $cantidades =\DB::table('vista_bugsxproyecto')->where('proyecto_id', '=', $id)->get('totalbugs');
        foreach ($cantidades as $cantidad) {
            $cantidadbugs =  $cantidad->totalbugs;
        }

        if($cantidadbugs==0)
        {
            $proyecto = Proyecto::where('id_proyecto',$id)->delete();
            Flash::success('Proyecto eliminado correctamente');
            return Response::json();
        }
        else
        {
            
             Flash::error('No puede eliminar un proyecto que tiene hallazgos pendientes por aprobar o rechazar');
            return Response::json();
        }

    }

        Flash::error('No está autorizado para eliminar proyectos');
        return Response::json();
}
}