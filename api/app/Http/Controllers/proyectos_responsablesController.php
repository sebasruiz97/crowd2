<?php
 
namespace App\Http\Controllers;
 
use App\Proyecto;
use Illuminate\Http\Request;
use Redirect,Response;
use Carbon\Carbon;
use Flash;
 
class proyectos_responsablesController extends Controller
{

public function __construct()
{        
    $this->middleware([
        'auth','roles:1,3']);    
}

/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
public function index(Request $request)
{

    //Print the values
   
    if(request()->ajax()) {
  
        $query = \DB::table('vista_proyectos_responsables');
        //return datatables()->of(proyecto::select('*'))
        return datatables()->of( $query)
        ->addColumn('action', 'proyectos_responsables.action_button')
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);
    }
    return view('proyectos_responsables.index');
}


/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(Request $request)
{  
    $id_pr = $request->input('id_pr');
    $tester = $request->input('id_proyecto_responsable');
    $estadobugs ="";

    $estadosBugs = \DB::table('vista_bugs')->where([
        ['user_id', $tester],
        ['proyecto_id',$id_pr],
        ['bug_estado_id','=',1]
    ])->orWhere([
        ['user_id', $tester],
        ['proyecto_id',$id_pr],
        ['bug_estado_id','=',6]
    ])->get();

    foreach ($estadosBugs as $estado) {
        $estadobugs =  $estado->bug_estado_id;
    }
    if ($estadobugs==1 || $estadobugs==6 )
    {
        $data ="No se Puede DesAsignar.. Posee Bugs en estado <Nuevo>";
        Flash::error('No puede desasignar al tester porque tiene hallazgos reportados a este proyecto, pendientes de aprobar o rechazar');
    }
    else
    {
        $data = array();
    
        $data['id_proyecto_responsable'] = $request->input('id_proyecto_responsable');
        $data['proyecto_id'] = $request->input('id_proyecto');
        $data['proyectorole_id'] = $request->input('proyectorole_id');

        $data['proyectoResponsable_fecha_inicio'] = $request->input('proyectoResponsable_fecha_inicio2');
        $data['proyectoResponsable_fecha_fin'] = $request->input('proyectoResponsable_fecha_fin2');
        $data['proyectoResponsable_Activo_SN'] = $request->input('proyectoResponsable_Activo_SN2');;
        $data['proyectoResponsable_motivoFin'] = $request->input('proyectoResponsable_motivoFin2');
        $motivo= $request->input('proyectoResponsable_motivoFin2');
        $NuevoEstado =  \DB::table('proyectos_responsabes_tipos_novedades')->where('id_tipoNovedad', $motivo)->get('tipoNovedad_NuevoEstado');
        foreach ($NuevoEstado as $user) {
            $estado =  $user->tipoNovedad_NuevoEstado;
        }
        
        $data['proyectoResponsable_Activo_SN'] = ($estado);
        if ($estado==1)
        {
            $data['proyectoResponsable_fecha_fin'] = null;
            $data['proyectoResponsable_motivoFin'] = null;
        }


        $encontrado =  \DB::table('proyectos_responsables')->where('id_pr', $id_pr)->first();

        if($encontrado)
        {
            $data['updated_at'] = Carbon::now();
            \DB::table('proyectos_responsables')->where('id_pr', $id_pr)->update($data);
            Flash::success('Estado del tester en el proyecto actualizado correctamente');

        }
        else
        {
            $data['created_at'] = Carbon::now();
            \DB::table('proyectos_responsables')->insert($data);  
        }
    }
    return Response::json();
}

 
/**
 * Show the form for editing the specified resource.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{   
    list($id_proyecto, $id_responsale) = explode(",", $id);
    $id_proyecto = intval($id_proyecto);
    $id_responsale = intval($id_responsale);
    // $proyecto = \DB::table('vista_proyectos_responsables')->where([
    //     ['id_proyecto_responsable', '=',$id_responsale],
    //     ['id_proyecto', '=', $id_proyecto],
    // ])->get();
   
    $proyecto = \DB::table('vista_proyectos_responsables')->where(
        'id_proyecto_responsable', '=',$id_responsale)->where(
    'id_proyecto', '=', $id_proyecto
    )->get();
    //dd(($proyecto));
    //$proyecto = \DB::table('vista_proyectos_responsables')->where('id_proyecto_responsable', '=',  $id)->get();
    //$proyecto = \DB::table('vista_buscar_tester')::where($where)->first();
    //$proyecto  = proyecto::where($where)->first();
 
    return Response::json($proyecto);
}
 
 
/**
 * Remove the specified resource from storage.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
{
    $proyecto = Proyecto::where('user_id',$id)->delete();
    //$proyecto =\DB::table('proyectos')->where('user_id', '=', $id)->delete();
 
    return Response::json($proyecto);
}


    public function ver(Request $request)
    {
        $input = $request->all();
        $id = $request->id;
        //  \Log::info($input);
    // dd($id);

        // dd($values);
        //Print the values
        
        if(request()->ajax()) {
        
            $query = \DB::table('vista_proyectos_responsables')->where('id_proyecto', '=', $id)->get();
    //   $query = \DB::table('vista_proyectos_responsables');
            //return datatables()->of(proyecto::select('*'))
            return datatables()->of( $query)
            ->addColumn('action', 'proyectos_responsables.action_button')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        
        }
        return view('proyectos_responsables.index', compact('id'));
    
    }
}