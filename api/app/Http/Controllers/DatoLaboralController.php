<?php

namespace App\Http\Controllers;

use App\DatoLaboral;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\GuardarDatosLaboralesRequest;
use App\Http\Controllers\AppBaseController;
use App\User;
use App\Models\Sector;
use App\Models\Cargo;
use App\DatosBasicos;
use App\PuntosTester;
use Flash;
use Response;
use Carbon\Carbon;



class DatoLaboralController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth', 'roles:1,2,3'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = auth()->user()->id;
        $datosLaborales = DatoLaboral::where("user_id", "=", $user)->paginate(6);;
        foreach ($datosLaborales as $datoLaboral) {
            $sector = Sector::find($datoLaboral->sector_id);
            $cargo = Cargo::find($datoLaboral->cargo_id);
            $datoLaboral["sector"] = $sector;
            $datoLaboral["cargo"] = $cargo;
        }
        return view('datos_laborales.index', compact('datosLaborales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('datos_laborales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GuardarDatosLaboralesRequest $request, User $user)
    {
        $repetido =  DatoLaboral::query()->where([
            ['user_id', '=',  $request->input('user_id')],
            ['empresa', '=', $request->input('empresa')],
            ['sector_id', '=', $request->input('sector_id')],
            ['fechaInicio_datoLaboral', '=', $request->input('fechaInicio_datoLaboral')],
            ['fechaFin_datoLaboral', '=', $request->input('fechaFin_datoLaboral')],
        ])->get();

        if ($repetido->isEmpty()) {
            $id = 'Agregar datos laborales';
            $data = $request->validated();
            $data = array();
            $data['user_id'] = $request->input('user_id');
            $data['empresa'] = $request->input('empresa');
            $data['sector_id'] = $request->input('sector_id');
            $data['cargo_id'] = $request->input('cargo_id');
            $data['fechaInicio_datoLaboral'] = $request->input('fechaInicio_datoLaboral');
            $data['fechaFin_datoLaboral'] = $request->input('trabajo_actual') == null? $request->input('fechaFin_datoLaboral') : null;
            $data['trabajo_actual'] = $request->input('trabajo_actual') == null? null : $request->input('trabajo_actual');
            $data['responsabilidades'] = $request->input('responsabilidades');
            \DB::table('datos_laborales')->insert($data);
            Flash::success('Experiencia laboral agregada correctamente');
            $datosLaborales = DatoLaboral::where('user_id',$request->input('user_id'))->get();
            if(count($datosLaborales) == 1){
                return redirect(route('asignarPuntos', $id));
            }
        } else {
            Flash::error('Ya tienes registrado una experiencia laboral en esta empresa durante esas fechas');
        }
        return redirect(route('datosLaborales.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DatoLaboral  $datoLaboral
     * @return \Illuminate\Http\Response
     */
    public function show($id_datoLaboral)
    {
        $datosLaborales = DatoLaboral::where("id_datoLaboral", "=", $id_datoLaboral)->get();

        foreach ($datosLaborales as $datoLaboral) {
            $cargo = Cargo::find($datoLaboral->cargo_id);
            $sector = Sector::find($datoLaboral->sector_id);
            $datoLaboral["cargo"] = $cargo;
            $datoLaboral["sector"] = $sector;
        }

        if (empty($datosLaborales)) {
            Flash::error('Experiencia no encontrada');
            return redirect(route('datosLaborales.index'));
        }

        return view('datos_laborales.show', compact('datosLaborales'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DatoLaboral  $datoLaboral
     * @return \Illuminate\Http\Response
     */
    public function edit($id_datoLaboral)
    {
        $datosLaborales = DatoLaboral::where("id_datoLaboral", "=", $id_datoLaboral)->get();
        $sector = Sector::orderBy('nombre_sector')->pluck('nombre_sector', 'id_sector');
        $cargo = Cargo::orderBy('nombre_cargo')->pluck('nombre_cargo', 'id_cargo');

        //para dejar seleccionado
        $selSector = DatoLaboral::where("id_datoLaboral", "=", $id_datoLaboral)->select("sector_id")->first();
        $sectorSeleccionado = $selSector->sector_id;
        $selCargo = DatoLaboral::where("id_datoLaboral", "=", $id_datoLaboral)->select("cargo_id")->first();
        $cargoSeleccionado = $selCargo->cargo_id;

        if (empty($datosLaborales)) {
            Flash::error('Experiencia laboral no encontrada');
            return redirect(route('datosLaborales.index'));
        }

        return view('datos_laborales.edit', compact('datosLaborales', 'sector', 'cargo', 'sectorSeleccionado', 'cargoSeleccionado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DatoLaboral  $datoLaboral
     * @return \Illuminate\Http\Response
     */
    public function update($id_datoLaboral, GuardarDatosLaboralesRequest $request)
    {


        if (($request->input('trabajo_actual')) == null) {
            $data = $request->validated();
            $data = array();
            $data['user_id'] = $request->input('user_id');
            $data['empresa'] = $request->input('empresa');
            $data['sector_id'] = $request->input('sector_id');
            $data['cargo_id'] = $request->input('cargo_id');
            $data['fechaInicio_datoLaboral'] = $request->input('fechaInicio_datoLaboral');
            $data['fechaFin_datoLaboral'] = $request->input('fechaFin_datoLaboral');
            $data['trabajo_actual'] = null;
            $data['responsabilidades'] = $request->input('responsabilidades');
            \DB::table('datos_laborales')->where('id_datoLaboral', '=', $id_datoLaboral)->update($data);
            Flash::success('Experiencia laboral actualizada correctamente');
            return redirect(route('datosLaborales.index'));
        } else {
            $data = $request->validated();
            $data = array();
            $data['user_id'] = $request->input('user_id');
            $data['empresa'] = $request->input('empresa');
            $data['sector_id'] = $request->input('sector_id');
            $data['cargo_id'] = $request->input('cargo_id');
            $data['fechaInicio_datoLaboral'] = $request->input('fechaInicio_datoLaboral');
            $data['fechaFin_datoLaboral'] = null;
            $data['trabajo_actual'] = $request->input('trabajo_actual');
            $data['responsabilidades'] = $request->input('responsabilidades');
            \DB::table('datos_laborales')->where('id_datoLaboral', '=', $id_datoLaboral)->update($data);
            Flash::success('Experiencia laboral actualiazada correctamente');
            return redirect(route('datosLaborales.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DatoLaboral  $datoLaboral
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_datoLaboral)
    {
        $id = 'Eliminar datos laborales';
        $datosLaborales = DatoLaboral::find($id_datoLaboral);
        $usuario = $datosLaborales->user_id;
        if (empty($datosLaborales)) {
            Flash::error('Experiencia laboral no encontrada');
            return redirect(route('datosLaborales.index'));
        }
        $datosLaborales->delete($id_datoLaboral);

        $datosLaborales = DatoLaboral::where('user_id',$usuario)->get();
        if($datosLaborales->isEmpty()){
            return redirect(route('quitarPuntos', $id));
        } else {
            return redirect(route('datosLaborales.index'));
        }
    }
}
