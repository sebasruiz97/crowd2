<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\DatosBasicos;
use App\EstudioFormal;
use Flash;
use Response;
use Carbon\Carbon;

class InicioTesterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    
    {
        //Esta parte mira si el perfil esta lleno

        $puntosDatosBasicos = \DB::table('puntos_tester')->where([
        ['user_id', '=', auth()->user()->id],
        ['motivo', '=', 'Agregar datos basicos'],
        ])->orWhere('motivo', '=', 'Eliminar datos basicos')
        ->sum('puntos');
                            
        $puntosEstudioFormal = \DB::table('puntos_tester')->where([
            ['user_id', '=', auth()->user()->id],
            ['motivo', '=', 'Agregar estudios formales'],
            ])->orWhere('motivo', '=', 'Eliminar estudios formales')
            ->sum('puntos');
                                
        $puntosEstudioNoFormal = \DB::table('puntos_tester')->where([
            ['user_id', '=', auth()->user()->id],
            ['motivo', '=', 'Agregar estudios no formales'],
            ])->orWhere('motivo', '=', 'Eliminar estudios no formales')
            ->sum('puntos');
                                
        $puntosDatosLaborales = \DB::table('puntos_tester')->where([
            ['user_id', '=', auth()->user()->id],
            ['motivo', '=', 'Agregar datos laborales'],
            ])->orWhere('motivo', '=', 'Eliminar datos laborales')
            ->sum('puntos');
                                
        $puntosConocimientoPruebas = \DB::table('puntos_tester')->where([
            ['user_id', '=', auth()->user()->id],
            ['motivo', '=', 'Agregar conocimiento en pruebas'],
            ])->orWhere('motivo', '=', 'Eliminar conocimiento en pruebas')
            ->sum('puntos');
                                
        $puntosDispositivos = \DB::table('puntos_tester')->where([
            ['user_id', '=', auth()->user()->id],
            ['motivo', '=', 'Agregar dispositivo disponible para probar'],
            ])->orWhere('motivo', '=', 'Eliminar dispositivo disponible para probar')
            ->sum('puntos');
                                
        $puntosDatosFinancieros = \DB::table('puntos_tester')->where([
            ['user_id', '=', auth()->user()->id],
            ['motivo', '=', 'Agregar datos financieros'],
            ])->orWhere('motivo', '=', 'Eliminar datos financieros')
            ->sum('puntos');
                                
        $puntosPerfil = $puntosDatosBasicos +  $puntosEstudioFormal + $puntosEstudioNoFormal +$puntosDatosLaborales +  $puntosConocimientoPruebas + $puntosDispositivos + $puntosDatosFinancieros;

        //Esta parte define variables y las manda las variables a la vista.

    if($puntosPerfil>4){

        $user = auth()->user()->id;

        //Informacion del perfil

        $datosBasicos = DatosBasicos::where("user_id","=",$user)->get();
        $estudiosFormales = EstudioFormal::where("user_id","=",$user)->get();
        $conocimientoPruebas = DB::table('vista_info_pruebas')->where("user_id","=",$user)->get();
        $dispositivos = DB::table('vista_info_dispositivos')->where("user_id","=",$user)->get();
        $nivelEducacion = DB::table('vista_informacion_tester')->where('user_id','=',$user)->max('nivelEducacion_id');
        $nivelEducacion = DB::table('vista_informacion_tester')->select('nombre_nivelEducacion')->where('nivelEducacion_id','=',$nivelEducacion)->first();
        $areaEstudio = DB::table('vista_informacion_tester')->where('user_id','=',$user)->max('areaEstudio_id');
        $areaEstudio = DB::table('vista_informacion_tester')->select('nombre_areaEstudio')->where('areaEstudio_id','=',$areaEstudio)->first();
        $ocupacion = DB::table('vista_informacion_tester')->where('user_id','=',$user)->max('ocupacion_id');
        $ocupacion = DB::table('vista_informacion_tester')->select('nombre_ocupacion')->where('ocupacion_id','=',$ocupacion)->first();
        $sectores = DB::table('vista_informacion_tester')->where('user_id','=',$user)->select('nombre_sector')->distinct()->limit(5)->get();
        $departamentos = DB::table('vista_informacion_tester')->where('user_id','=',$user)->select('nombre_departamento')->distinct()->get();
        $municipios = DB::table('vista_informacion_tester')->where('user_id','=',$user)->select('nombre_municipio')->distinct()->get();
        $paises = DB::table('vista_informacion_tester')->where('user_id','=',$user)->select('nombre_pais')->distinct()->get();
        $pruebas = DB::table('vista_info_pruebas')->where('user_id','=',$user)->distinct()->limit(6)->get();
        $dispositivos = DB::table('vista_info_dispositivos')->where('user_id','=',$user)->distinct()->limit(6)->get();
        
    //Puntos acumulados
        $puntosAcumulados = DB::table('puntos_tester')->where("user_id","=",auth()->user()->id)->sum('puntos');
        $puntos =DB::table('puntos_tester')->where("user_id","=",auth()->user()->id)->simplePaginate(3);
        
    //Informacion de bugs
        $proyectosParticipado = DB::table('proyectos_responsables')->where('id_proyecto_responsable','=',$user)->count();
        $bugsAprobados = DB::table('bugs')
                            ->where([
                                ['user_id','=',$user],
                                ['bug_estado_id','=',2]
                            ])
                            ->orWhere([
                                ['user_id','=',$user],
                                ['bug_estado_id','=',4]
                            ])
                            ->count();

        $bugsReportados = DB::table('bugs')->where('user_id','=',$user)->count();
        $bugs = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_tipo_id','=',1]])
                                ->count();
        $notas = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_tipo_id','=',2]])
                                ->count();
        $ideas = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_tipo_id','=',3]])
                                ->count();
        $preguntas = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_tipo_id','=',4]])
                                ->count();
        $bugsNuevos = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_estado_id','=',1]])
                                ->count();
        $bugsAprobadosLider = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_estado_id','=',2]])
                                ->count();
        $bugsRechazadosLider = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_estado_id','=',3]])
                                ->count();
        $bugsAprobadosCliente = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_estado_id','=',4]])
                                ->count();
        $bugsRechazadosCliente = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_estado_id','=',5]])
                                ->count();
        $bugsGravedadMenor = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_gravedad','=','Menor, no afecta la regla de negocio']])
                                ->count();
        $bugsGravedadMayor = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_gravedad','=','Mayor, no cumple con la regla de negocio']])
                                ->count();
        $bugsStopper = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_gravedad','=','Stopper, no permite continuar con la ejecución de la prueba']])
                                ->count();
        $rechazoDuplicado = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_rechazo_id','=',1]])
                                ->count();
        $rechazoFueraAlcance = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_rechazo_id','=',2]])
                                ->count();
        $rechazoNoDefecto = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_rechazo_id','=',3]])
                                ->count();
        $rechazoNoReproducible = DB::table('bugs')->where([
                                    ['user_id','=',$user],
                                    ['bug_rechazo_id','=',4]])
                                ->count();

        $bugsRechazados = DB::table('bugs')
                            ->where([
                                ['user_id','=',$user],
                                ['bug_estado_id','=',3]
                            ])
                            ->orWhere([
                                ['user_id','=',$user],
                                ['bug_estado_id','=',5]
                            ])
                            ->count();
        $proyectosAbiertos = DB::table('vista_proyectos')
                            ->where([
                                ['user_id','=',$user],
                                ['proyectoEstado_id','=',2]
                            ])
                            ->count();
        $proyectosCerrados = DB::table('vista_proyectos')
                        ->where([
                            ['user_id','=',$user],
                            ['proyectoEstado_id','=',3]
                        ])
                        ->count();
        $proyectosAnulados = DB::table('vista_proyectos')
                        ->where([
                            ['user_id','=',$user],
                            ['proyectoEstado_id','=',5]
                        ])
                        ->count();
        $proyectosSuspendidos = DB::table('vista_proyectos')
                        ->where([
                            ['user_id','=',$user],
                            ['proyectoEstado_id','=',6]
                        ])
                        ->count();
        $proyectosCancelados = DB::table('vista_proyectos')
                        ->where([
                            ['user_id','=',$user],
                            ['proyectoEstado_id','=',7]
                        ])
                        ->count();
        
        
        return  view('home_tester.inicio_tester', 
                compact('user', 'puntosPerfil','datosBasicos','estudiosFormales','conocimientoPruebas',
                        'dispositivos','edad','nivelEducacion','areaEstudio', 'ocupacion','sectores',
                        'departamentos','municipios','paises','puntosAcumulados', 'puntos','pruebas',
                        'dispositivos', 'proyectosParticipado','bugsAprobados','bugsReportados','bugs',
                        'notas', 'ideas','preguntas','bugsNuevos','bugsAprobadosLider','bugsRechazadosLider',
                        'bugsAprobadosCliente','bugsRechazadosCliente','bugsGravedadMenor','bugsGravedadMayor',
                        'bugsStopper','rechazoDuplicado','rechazoFueraAlcance','rechazoNoDefecto',
                        'rechazoNoReproducible','bugsRechazados','proyectosAbiertos','proyectosCerrados',
                        'proyectosAnulados','proyectosSuspendidos','proyectosCancelados'));

    }
    return view('home_tester.sin_perfil');
    }
}
