<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\DatosBasicos;
use App\VistaBugs;
use App\EstudioFormal;
use Flash;
use Response;
use Carbon\Carbon;

class InicioLiderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    
    {       
        $buscar = $request->get('buscarpor');
        $tipo = $request->get('tipo');
        $proyecto = 1;

        if($tipo === 'Proyecto'){
            $tipo = 'id_proyecto';
        }elseif($tipo === 'Usuario'){
            $tipo = 'id_proyecto_responsable';
        }
        
            // $usuarios = \DB::table('users')->select('users.id','users.name','users.email','vista_datos_basicos.experienciaPruebas','vista_datos_basicos.genero','vista_datos_basicos.nombre_departamento','vista_datos_basicos.nombre_municipio','vista_datos_basicos.nombre_ocupacion','vista_datos_basicos.edad','vista_datos_basicos.puntosAcumulados')
            //             ->leftJoin('vista_datos_basicos', 'users.id', '=', 'vista_datos_basicos.user_id')
            //             ->where($tipo, 'like',"%$buscar%")
            //             ->paginate(15);
    

        $buscar = $request->get('buscarpor');

        if($buscar === null){

              //Informacion de plataforma

            $user = auth()->user()->id;

            $proyectosAsignados = DB::table('proyectos')->where([
                                                    ['proyecto_responsable_actual','=',$user],
                                                    ['deleted_at','=',null],
                                                    ])->get();

            $detalleProyectos = null;
            $numeroTesters = DB::table('vista_proyectos')->where([
                                                    ['proyecto_responsable_actual','=',$user],
                                                    ])->distinct('id_proyecto')->count();

            $proyectosAsignados = DB::table('proyectos')->where([
                                                    ['proyecto_responsable_actual','=',$user],
                                                    ['deleted_at','=',null],
                                                    ])->count();
            $bugsReportados = DB::table('vista_bugs')->where([
                                                    ['proyecto_responsable_actual','=',$user],
                                                    ['deleted_at','=',null],
                                                    ])->count();
            $bugsAprobadosCliente = DB::table('vista_bugs')->where([
                                                    ['proyecto_responsable_actual','=',$user],
                                                    ['bug_estado_id','=',4],
                                                    ['deleted_at','=',null],
                                                    ])->count();
            $bugsAprobadosLider = DB::table('vista_bugs')->where([
                                                    ['proyecto_responsable_actual','=',$user],
                                                    ['bug_estado_id','=',2],
                                                    ['deleted_at','=',null],
                                                    ])->count();
            $bugs = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_tipo_id','=',1]])
                                    ->count();
            $notas = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_tipo_id','=',2]])
                                    ->count();
            $ideas = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_tipo_id','=',3]])
                                    ->count();
            $preguntas = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_tipo_id','=',4]])
                                    ->count();
            $bugsNuevos = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_estado_id','=',1]])
                                    ->count();
            $bugsAprobadosLider = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_estado_id','=',2]])
                                    ->count();
            $bugsRechazadosLider = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_estado_id','=',3]])
                                    ->count();
            $bugsAprobadosCliente = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_estado_id','=',4]])
                                    ->count();
            $bugsRechazadosCliente = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_estado_id','=',5]])
                                    ->count();
            $bugsGravedadMenor = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_gravedad','=','Menor, no afecta la regla de negocio']])
                                    ->count();
            $bugsGravedadMayor = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_gravedad','=','Mayor, no cumple con la regla de negocio']])
                                    ->count();
            $bugsStopper = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_gravedad','=','Stopper, no permite continuar con la ejecución de la prueba']])
                                    ->count();
            $rechazoDuplicado = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_rechazo_id','=',1]])
                                    ->count();
            $rechazoFueraAlcance = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_rechazo_id','=',2]])
                                    ->count();
            $rechazoNoDefecto = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_rechazo_id','=',3]])
                                    ->count();
            $rechazoNoReproducible = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['bug_rechazo_id','=',4]])
                                    ->count();

            $bugsRechazados = DB::table('vista_bugs')
                                ->where([
                                    ['proyecto_responsable_actual','=',$user],
                                    ['bug_estado_id','=',3]
                                ])
                                ->orWhere([
                                    ['proyecto_responsable_actual','=',$user],
                                    ['bug_estado_id','=',5]
                                ])
                                ->count();

            
            return  view('home_tester.inicio_lider', 
                    compact('user',  'buscar','numeroTesters','detalleProyectos','proyectosAsignados','bugsReportados','bugsAprobadosCliente','bugsAprobadosLider','bugs',
                            'notas', 'ideas','preguntas','bugsNuevos','bugsAprobadosLider','bugsRechazadosLider',
                            'bugsAprobadosCliente','bugsRechazadosCliente','bugsGravedadMenor','bugsGravedadMayor',
                            'bugsStopper','rechazoDuplicado','rechazoFueraAlcance','rechazoNoDefecto',
                            'rechazoNoReproducible','bugsRechazados','vistaBugs'
                        ));
        }else{

             //Informacion de plataforma

            $user = auth()->user()->id;

            $detalleProyectos = DB::table('vista_proyectos')->where([
                                                    ['id_proyecto','=',$proyecto],
                                                    ])->distinct('id_proyecto')->limit(1)
                                                    ->get();

            $numeroTesters = DB::table('proyectos_responsables')->where([
                                                    ['proyecto_id','=',$proyecto],
                                                    ])->count();

            $proyectosAsignados = DB::table('proyectos')->where([
                                                    ['proyecto_responsable_actual','=',$user],
                                                    ['deleted_at','=',null],
                                                    ])->count();
            $bugsReportados = DB::table('vista_bugs')->where([
                                                    ['proyecto_responsable_actual','=',$user],
                                                    ['proyecto_id','=',$proyecto],
                                                    ['deleted_at','=',null],
                                                    ])->count();
            $bugsAprobadosCliente = DB::table('vista_bugs')->where([
                                                    ['proyecto_responsable_actual','=',$user],
                                                    ['proyecto_id','=',$proyecto],
                                                    ['bug_estado_id','=',4],
                                                    ['deleted_at','=',null],
                                                    ])->count();
            $bugsAprobadosLider = DB::table('vista_bugs')->where([
                                                    ['proyecto_responsable_actual','=',$user],
                                                    ['proyecto_id','=',$proyecto],
                                                    ['bug_estado_id','=',2],
                                                    ['deleted_at','=',null],
                                                    ])->count();

            $bugs = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_tipo_id','=',1]])
                                    ->count();
            $notas = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_tipo_id','=',2]])
                                    ->count();
            $ideas = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_tipo_id','=',3]])
                                    ->count();
            $preguntas = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_tipo_id','=',4]])
                                    ->count();
            $bugsNuevos = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_estado_id','=',1]])
                                    ->count();
            $bugsAprobadosLider = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_estado_id','=',2]])
                                    ->count();
            $bugsRechazadosLider = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_estado_id','=',3]])
                                    ->count();
            $bugsAprobadosCliente = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_estado_id','=',4]])
                                    ->count();
            $bugsRechazadosCliente = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_estado_id','=',5]])
                                    ->count();
            $bugsGravedadMenor = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_gravedad','=','Menor, no afecta la regla de negocio']])
                                    ->count();
            $bugsGravedadMayor = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_gravedad','=','Mayor, no cumple con la regla de negocio']])
                                    ->count();
            $bugsStopper = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_gravedad','=','Stopper, no permite continuar con la ejecución de la prueba']])
                                    ->count();
            $rechazoDuplicado = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_rechazo_id','=',1]])
                                    ->count();
            $rechazoFueraAlcance = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_rechazo_id','=',2]])
                                    ->count();
            $rechazoNoDefecto = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_rechazo_id','=',3]])
                                    ->count();
            $rechazoNoReproducible = DB::table('vista_bugs')->where([
                                        ['proyecto_responsable_actual','=',$user],
                                        ['proyecto_id','=',$proyecto],
                                        ['bug_rechazo_id','=',4]])
                                    ->count();

            $bugsRechazados = DB::table('vista_bugs')
                                ->where([
                                    ['proyecto_responsable_actual','=',$user],
                                    ['proyecto_id','=',$proyecto],
                                    ['bug_estado_id','=',3]
                                ])
                                ->orWhere([
                                    ['proyecto_responsable_actual','=',$user],
                                    ['proyecto_id','=',$proyecto],
                                    ['bug_estado_id','=',5]
                                ])
                                ->count();
                
            return  view('home_tester.inicio_lider', 
                    compact('user',  'numeroTesters','detalleProyectos','proyectosAsignados','bugsReportados','bugsAprobadosCliente',
                    'bugsAprobadosLider','vistaBugs','bugs','notas', 'ideas','preguntas','bugsNuevos','bugsAprobadosLider','bugsRechazadosLider',
                    'bugsAprobadosCliente','bugsRechazadosCliente','bugsGravedadMenor','bugsGravedadMayor',
                    'bugsStopper','rechazoDuplicado','rechazoFueraAlcance','rechazoNoDefecto',
                    'rechazoNoReproducible','bugsRechazados'));
            }

        }


       
}
