<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function rules()
    {
        $expresionRegular = 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}$/';

        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => ['required', 'string', 'min:8', 'confirmed', $expresionRegular],
        ];
    }

    protected function validationErrorMessages()
    {
        return [
            'password.required' => 'Es obligatorio que digites una contraseña que contenga entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula. Y al menos, un dígito y un caracter especial',
            'password.min' => 'La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula. Y al menos, un dígito y un caracter especial',
            'password.confirmed' => 'Las contraseñas no coinciden y debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula. Y al menos, un dígito y un caracter especial',
            'email.required' => 'Es obligatorio que registres una dirección de correo',
        ];
    }

}
