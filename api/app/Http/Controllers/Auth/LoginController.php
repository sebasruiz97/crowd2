<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/datosBasicos';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }

    public function username()
    {
        return 'numeroDocumento';
    }

        /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    protected function credentials(Request $request)
    {
        $credenciales = $request->only($this->username(), 'password');
        //añadimos el valor de activo a S, para que sea correcto
        $credenciales['estado_id'] = [1,2,3];
        return $credenciales;
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];

        // Load user from database
        $user = \App\User::where($this->username(), $request->{$this->username()})->first();

        // Check if user was successfully loaded, that the password matches
        // and active is not 1. If so, override the default error message.
        if ($user && \Hash::check($request->password, $user->password) && $user->estado_id == 4) {
            $errors = [$this->username() => 'Tu cuenta está inactiva, debes enviar un correo a info@crowdsqa.co para activarte de nuevo'];
        }

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    public function authenticated()
       {
           if(Auth::check()) {
               if(\Auth::user()->rol_id === 1) {
                    return redirect(route('usuarios.index'));
               } elseif(\Auth::user()->rol_id === 2) {
                    return redirect(route('datosBasicos.index'));
               } elseif(\Auth::user()->rol_id === 4){
                    return redirect(route('cobros.index'));
               }else{
                    return redirect(route('bugs.index'));
               }                   

           }    
       }


}
