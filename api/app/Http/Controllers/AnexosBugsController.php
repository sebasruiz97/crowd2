<?php

namespace App\Http\Controllers;

use App\Bug;
use Illuminate\Http\Request;
use App\Http\Requests\GuardarBugAnexoRequest;
use Redirect, Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReporteBugTester;
use Flash;


class AnexosBugsController extends Controller
{

    public function __construct()
    {
        $this->middleware([
            'auth', 'roles:1,2,3'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $id = $id;
        $anexos = \DB::table('bug_anexos')
            ->where([
                ['deleted_at', '=', null],
                ['bug_id', '=', $id],
            ])
            ->get();

        if (request()->ajax()) {

            return datatables()->of($anexos)
                ->addColumn('action', 'anexos_bugs.action_button')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);

            return view('anexos_bugs.index', compact('id'));
        } else {

            return view('anexos_bugs.index', compact('id'));
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GuardarBugAnexoRequest $request)
    {
        $user_id =  auth()->user()->id;
        $BugID = $request->input('bug_id');

        $proyectos = \DB::table('vista_bugs')->where('id_bug', '=', $BugID)->get();
        foreach ($proyectos as $proyecto) {
            $proyectoID = $proyecto->proyecto_id;
            $clienteID = $proyecto->cliente_id;
            $estadoBug = $proyecto->bug_estado_id;
            $creadorBug = $proyecto->user_id;
            $estadoProyecto = $proyecto->ProyectoEstado_id;
        }

        if ($estadoProyecto == 2) {

            $responsablesProyecto = \DB::table('proyectos')->where('id_proyecto', '=', $proyectoID)->get('proyecto_responsable_actual');
            foreach ($responsablesProyecto as $responsable) {
                $lider = $responsable->proyecto_responsable_actual;
            }

            $correosLider = \DB::table('users')->where('id', '=', $lider)->get('email');
            foreach ($correosLider as $correoLider) {
                $correoFront = $correoLider->email;
            }

            $reportes = \DB::table('vista_bugs')->where('id_bug', '=', $BugID)->get();
            foreach ($reportes as $reporte) {
                $id = $reporte->id_bug;
                $usuario = $reporte->name;
                $usuarioReporta = $reporte->user_id;
                $proyecto = $reporte->proyecto_nombre;
                $tipoBug = $reporte->nombre_tipoBug;
                $tituloBug = $reporte->bug_titulo;
            }

            if ($creadorBug === $user_id) {

                if ($estadoBug === 1 || $estadoBug === 6) {

                    $anexosCargados = \DB::table('bug_anexos')->where('bug_id', '=', $BugID)->get();

                    $data = $request->validated();

                    $files = $request->file('url_anexoBug');
                    foreach ($files as $file) {

                        $nombre      =   time() . '.' . $file->getClientOriginalName();
                        $target_path    =   storage_path('clientes' . '/cliente_' . $clienteID . '/proyectos' . '/proyecto_' . $proyectoID . '/bugs' . '/bug_' . $BugID);

                        if ($file->move($target_path, $nombre)) {
                            $data = array();
                            $data['bug_id'] = $BugID;
                            $data['user_id'] = $user_id;
                            $data['url_anexoBug'] = $nombre;
                            $data['nota_anexoBug'] = $request->input('nota_anexoBug');
                            $data['created_at'] = Carbon::now();
                            $data['updated_at'] = Carbon::now();
                            \DB::table('bug_anexos')->insert($data);
                        }
                    }

                    $data2 = array();
                    $data2['bug_id'] = $BugID;
                    $data2['user_id'] = $user_id;
                    $data2['bug_cambio'] = 'Anexos para el hallazgo';
                    $data2['bug_notaCambio'] = 'Tester ha agregado anexos al hallazgo';
                    $data2['created_at'] = Carbon::now();
                    \DB::table('bug_cambios')->insert($data2);


                    if ($anexosCargados->isEmpty()) {
                        Mail::to($correoFront)
                            ->queue(new ReporteBugTester($id, $usuario, $proyecto, $tipoBug, $tituloBug));
                    }

                    Flash::success('Anexo agregado correctamente');
                    return Response::json($data);
                }

                Flash::error('No puede agregar anexos a un hallazgo con este estado');
                return Response::json();
            }

            Flash::error('Sólo el creador del hallazgo puede agregarle anexos');
            return Response::json();
        }

        Flash::error('No puede agregar anexos a un hallazgo, cuando el proyecto ya no se encuentra en ejecución');
        return Response::json();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */


    public function show($id)
    {

        $rol_id = auth()->user()->rol_id;
        $anexos = \DB::table('bug_anexos')->where('id_anexoBug', '=', $id)->get();
        $id = auth()->user()->id;

        foreach ($anexos as $anexo) {
            $usuario = $anexo->user_id;
            $url = $anexo->url_anexoBug;
            $idBug = $anexo->bug_id;
        }

        $bugs = \DB::table('bugs')->where('id_bug', '=', $idBug)->get();

        foreach ($bugs as $bug) {
            $clienteID = $bug->cliente_id;
            $proyectoID = $bug->proyecto_id;
        }

        if ($id == $usuario || $rol_id == 3 || $rol_id == 1) {

            $enlace = storage_path('clientes' . '/cliente_' . $clienteID . '/proyectos' . '/proyecto_' . $proyectoID . '/bugs' . '/bug_' . $idBug . '/');
            return response()->download($enlace . $url);
        } else {
            return ('Sólo el propietario puede descargar el archivo');
        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_id = auth()->user()->id;
        $rol_id = auth()->user()->rol_id;

        $anexos = \DB::table('bug_anexos')->where('id_anexoBug', '=', $id)->get();
        foreach ($anexos as $anexo) {
            $usuario = $anexo->user_id;
            $url = $anexo->url_anexoBug;
            $idBug = $anexo->bug_id;
        }

        $bugs = \DB::table('vista_bugs')->where('id_bug', '=', $idBug)->get();

        foreach ($bugs as $bug) {
            $clienteID = $bug->cliente_id;
            $proyectoID = $bug->proyecto_id;
            $estadoBug = $bug->bug_estado_id;
            $estadoProyecto = $bug->ProyectoEstado_id;
        }

        if (empty($anexos)) {
            return Response::json($anexos);
        }

        if ($estadoProyecto == 2) {

            if ($user_id == $usuario || $rol_id == 1) {

                if ($estadoBug === 1) {

                    $anexos = \DB::table('bug_anexos')->where('id_anexoBug', '=', $id)->delete();
                    $data = storage_path('clientes' . '/cliente_' . $clienteID . '/proyectos' . '/proyecto_' . $proyectoID . '/bugs' . '/bug_' . $idBug . '/' . $url);
                    unlink($data);

                    $data2 = array();
                    $data2['bug_id'] = $idBug;
                    $data2['user_id'] = $user_id;
                    $data2['bug_cambio'] = 'Anexos eliminados para el hallazgo';
                    $data2['bug_notaCambio'] = 'Tester ha eliminado anexos del hallazgo';
                    $data2['created_at'] = Carbon::now();
                    \DB::table('bug_cambios')->insert($data2);

                    Flash::success('Anexo eliminado correctamente');
                    return Response::json($anexos);
                }

                Flash::error('No puede eliminar un anexo para un hallazgo con este estado');
                return Response::json();
            }
            Flash::error('Sólo el propietario del anexo puede eliminarlo');
            return Response::json();
        }

        Flash::error('No puede eliminar los anexos del hallazgo si el proyecto ya no se encuentra en ejecución');
        return Response::json();
    }
}
