<?php

namespace App\Http\Controllers;

use App\Http\Requests\GuardarAnexosRequest;
use Carbon\Carbon;
use App\Models\Anexo;
use Flash;

class AnexoController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth', 'roles:1,2,3,4'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user()->id;
        $anexos = Anexo::query()->where('user_id', $user)->paginate(6);
        $anexosFaltantes = anexosObligatorios();
        return view('anexos.index', compact(['anexos', 'anexosFaltantes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$anexosFaltantes = anexosObligatorios();
        //return $anexosFaltantes;
        //return $anexos;
        return view('anexos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GuardarAnexosRequest $request)
    {

        $id_user = auth()->user()->id;
        $id = 'Agregar Anexos';
        $tipoAnexos = $request->input('tipoAnexo_id');
        if ($tipoAnexos == 1 || $tipoAnexos == 4 || $tipoAnexos == 3 || $tipoAnexos == 6) {

            $existe = \DB::table('anexos')
                ->where([
                    ['user_id', '=', $id_user],
                    ['tipoAnexo_id', '=', $tipoAnexos],
                ])->get();

            if ($existe->isEmpty() == 1) {

                $data = $request->validated();
                $file = $request->file('url');

                $nombre = time() . '.' . $file->getClientOriginalName();

                $target_path = storage_path('/testers/tester_' . $id_user);

                if ($file->move($target_path, $nombre)) {

                    $data = array();
                    $data['tipoAnexo_id'] = $request->input('tipoAnexo_id');
                    $data['modulo_id'] = $request->input('modulo_id');
                    $data['url'] = $nombre;
                    $data['comentarios'] = $request->input('comentarios');
                    $data['user_id'] = $request->input('user_id');
                    $data['created_at'] = Carbon::now();
                    $data['updated_at'] = Carbon::now();
                    \DB::table('anexos')->insert($data);
                    Flash::success('Anexo agregado correctamente');
                    $anexosPorSubir = anexosObligatorios();
                    if ($anexosPorSubir->isEmpty()) {
                        return redirect(route('asignarPuntos', $id));
                    }
                }
            } else {
                Flash::success('Ya agregaste este tipo de anexo');
            }
            return redirect(route('anexos.index'));
        }

        $data = $request->validated();
        $file = $request->file('url');

        $nombre = time() . '.' . $file->getClientOriginalName();

        $target_path = storage_path('/testers/tester_' . $id_user);

        if ($file->move($target_path, $nombre)){
            $data = array();
            $data['url'] = $nombre;
            $data['user_id'] = $request->input('user_id');
            $data['modulo_id'] = $request->input('modulo_id');
            $data['comentarios'] = $request->input('comentarios');
            $data['tipoAnexo_id'] = $request->input('tipoAnexo_id');
            $data['created_at'] = Carbon::now();
            $data['updated_at'] = Carbon::now();
            \DB::table('anexos')->insert($data);
            Flash::success('Anexo agregado correctamente');
            $anexosPorSubir = anexosObligatorios();
            if ($anexosPorSubir->isEmpty()) {
                return redirect(route('asignarPuntos', $id));
            }
            return redirect(route('anexos.index'));
        }

        Flash::error('Hubo un error al subir el anexo!');
        return redirect(route('anexos.index'));

    }

    public function show($url)
    {

        $anexos = Anexo::where("url", "=", $url)->get();
        $id = auth()->user()->id;
        $rol_id = auth()->user()->rol_id;

        foreach ($anexos as $anexo) {
            $usuario = $anexo->user_id;
            $url = $anexo->url;
        }


        if ($id == $usuario || $rol_id == 3 || $rol_id == 1 || $rol_id == 4) {

            $enlace = storage_path('testers/tester_' . $usuario . '/');

            return response()->download($enlace . $url);
        } else {
            return ('Sólo el propietario puede visualizar el archivo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_anexo)
    {
        $id_user = auth()->user()->id;

        $id = 'Eliminar Anexos';
        $anexos = \DB::table('anexos')->where('id_anexo', '=', $id_anexo)->get();
        foreach ($anexos as $anexo) {
            $url = $anexo->url;
            $tipoAnexos = $anexo->tipoAnexo_id;
        }

        if (empty($anexos)) {
            Flash::error('Anexo no encontrado');
            return redirect(route('anexos.index'));
        }

        \DB::table('anexos')->where('id_anexo', '=', $id_anexo)->delete();
        $enlace = storage_path('testers/tester_' . $id_user . '/');
        unlink($enlace . $url);

        $anexosPorSubir = anexosObligatorios();
        if (!$anexosPorSubir->isEmpty()) {
            return redirect(route('quitarPuntos', $id));
        }

        /*if (count(anexosObligatorios()) == 4) {
            if ($tipoAnexos == 1 || $tipoAnexos == 4 || $tipoAnexos == 3 || $tipoAnexos == 6) {
                return redirect(route('quitarPuntos', $id));
            }
        }*/

        Flash::error('Anexo eliminado correctamente');
        return redirect(route('anexos.index'));
    }

    public function datos()
    {

        $enlace = public_path('images/' . 'Aceptacion Tratamiento de Datos.pdf');
        return response()->download($enlace);
    }

    public function terminosCondiciones()
    {

        $enlace = public_path('images/' . 'Terminos y Condiciones Plataforma CROWDSQA_ v_1_1.pdf');
        return response()->download($enlace);
    }

    public function approve($id_anexo)
    {
        $id_user = auth()->user()->id;

        $anexo = \DB::table('anexos')->where("id_anexo", "=", $id_anexo)->first();

        $data = ['approved' => ($anexo->approved == 1 ? 0 : 1)];

        \DB::table('anexos')->where("id_anexo", "=", $id_anexo)->update($data);

        Flash::success($anexo->approved == 0 ? 'Anexo aprobado correctamente' : 'Se revirtió la aprobación correctamente');
        return redirect(route('usuarios.show', $anexo->user_id));
    }
}
