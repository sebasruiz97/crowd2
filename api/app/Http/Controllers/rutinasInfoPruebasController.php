<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator, Redirect, Response;
use App\Pagina;
use Auth;

class rutinasInfoPruebasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        if (Auth::check()) {
            $user = Auth::id();
        } else {

            $user = '';
        }

        if (($user != '')) {
            $lista_datos_pruebas  =   \DB::table('indice_conocimiento_pruebas')->where('user_id', $user)->get();
            $tipospruebas = tipos_pruebas_por_usuarios::where('user_id', $user)->get();

            $lista_herramientas = herramientas_conocidas_por_pruebas_por_usuario::where('user_id', $user)->get();
            $lista_disp_prueba_usuario = lista_dispositivos_x_tipo_prueba_x_usuario::where('user_id', $user)->get();
            $lista_Sodisp_prueba_usuario = lista_so_por_disp_por_pruebas_por_usuario::where('user_id', $user)->get();
            // dd($tipospruebas); 
        } else {
            $lista_datos_pruebas  =   \DB::table('indice_conocimiento_pruebas')->get();
            $tipospruebas = tipos_pruebas_por_usuarios::select("*")->orderBy('nombre_prueba', 'asc')->get();

            $lista_herramientas = herramientas_conocidas_por_pruebas_por_usuario::select("*")->get();
            $lista_disp_prueba_usuario = lista_dispositivos_x_tipo_prueba_x_usuario::select("*")->get();
            $lista_Sodisp_prueba_usuario = lista_so_por_disp_por_pruebas_por_usuario::select("*")->get();
        }

        $variable = "2";
        // listado tipo tipo_pruebas
        $lista_tipo_pruebas  =      \DB::table('tipo_pruebas')->orderBy('nombre_prueba', 'asc')->get();
        //listado de tiempo_experiencia
        $lista_tiempo_experiencia  =      \DB::table('tiempo_experiencias')->get();
        //listado tipo_ejecucion
        $lista_tipo_ejecucion  =      \DB::table('tipo_ejecucion')->get();
        // Obtener lista de Paises llamando a funcion dentro del mimsmo controlador
        //  $datosPaises['data'] = Self::getPaises_de_formalocal();
        // Obtener lista de Paises llamando al metodo almacenado dentro del modelo PAGINA
        $datosPaises['data'] = Pagina::getPaises();

        return view('info_pruebas.index', compact('variable', 'lista_tipo_pruebas', 'lista_tiempo_experiencia', 'lista_tipo_ejecucion', 'datosPaises'));
    }

    public static function getPaises_de_formalocal()
    {

        $value = \DB::table('paises')->orderBy('id_pais', 'asc')->get();

        return $value;
    }

    // obtener registros
    public function getciudadesxPais($pais_id = 0)
    {

        // obtener ciudades por pais  
        $datosCiudades['data'] = Pagina::getciudadesxPais($pais_id);

        echo json_encode($datosCiudades);
        exit;
    }

    //***************************************** */
    // obtener registros
    public function gettiposPruebas($id_prueba = 0)
    {

        // obtener TIPOS DE PRUEBAS  
        $lista_tipo_pruebas['data'] = Pagina::gettiposPruebas($id_prueba);

        echo json_encode($lista_tipo_pruebas);
        exit;
    }

    // obtener tipo de prueba seleccionada
    public function gettiposPruebaSeleccionada($id_prueba = 0)
    {

        // obtener TIPOS DE PRUEBAS  
        $lista_tipo_pruebas['data'] = Pagina::gettiposPruebaSeleccionada($id_prueba);

        echo json_encode($lista_tipo_pruebas);
        exit;
    }

    // obtener herramientas x tipo de prueba seleccionada
    public function getherramientasxTipo($id_prueba)
    {

        //dd($id_prueba);
        $lista_tipo_herramientas['data'] = Pagina::getherramientasxTipo($id_prueba);

        echo json_encode($lista_tipo_herramientas);
        exit;
    }

    // obtener dispositivos  x tipo de prueba seleccionada
    public function getDispositivosxTipo($id_prueba)
    {

        //dd($id_prueba);
        $lista_dispositivos['data'] = Pagina::getDispositivosxTipo($id_prueba);

        echo json_encode($lista_dispositivos);
        exit;
    }

    public function getDispositivos()
    {

        $lista_dispositivos_solo['data'] = Pagina::getDispositivos();

        echo json_encode($lista_dispositivos_solo);
        exit;
    }

    public function getMarcas(Request $request)
    {
        $data = $request->only(['term']);
        $term = false;
        if(isset($data['term'])){
            $term = $data['term'];
        }
        $lista_dispositivos['data'] = Pagina::getMarcas($term);

        echo json_encode($lista_dispositivos);
        exit;
    }

    public function getModelos(Request $request)
    {
        $data = $request->only(['idMarca','term']);
        $marca = $data['idMarca'];
        $term = $data['term'];
        $lista_modelos['data'] = Pagina::getModelos($marca,$term);

        echo json_encode($lista_modelos);
        exit;
    }

    // obtener dispositivos  x tipo de dispositivo seleccionada
    public function getSoxDispxTipo($id_dispositivo)
    {

        //dd($id_prueba);
        $lista_dispositivos['data'] = Pagina::getSoxDispxTipo($id_dispositivo);

        echo json_encode($lista_dispositivos);
        exit;
    }
    // obtener navegadores   x tipo de dispositivo seleccionada
    public function getNavDispxTipo($id_dispositivo)
    {

        //dd($id_prueba);
        $lista_dispositivos['data'] = Pagina::getNavDispxTipo($id_dispositivo);

        echo json_encode($lista_dispositivos);
        exit;
    }
    //***************************************** */

    //listado de tiempo_experiencia
    public function gettiempo_experiencias($id_tiempoExperiencia = 0)
    {

        // obtener TIPOS DE PRUEBAS  
        $lista_tiempo_experiencia['data'] = Pagina::gettiempo_experiencias($id_tiempoExperiencia);

        echo json_encode($lista_tiempo_experiencia);
        exit;
    }

    //listado de tiempo_experiencia
    public function gettipo_ejecucion($id_ejecucion = 0)
    {

        // obtener TIPOS DE PRUEBAS  
        $lista_tipo_ejecucion['data'] = Pagina::gettipo_ejecucion($id_ejecucion);

        echo json_encode($lista_tipo_ejecucion);
        exit;
    }

    /* insert and update todo into mysql database*/

    public function store(Request $request)
    {
    }

    /* display edit todo form with data */

    public function edit(Request $request, $id)
    {
    }

    /* delete todo from mysql database */

    public function delete($id)
    {
    }
}
