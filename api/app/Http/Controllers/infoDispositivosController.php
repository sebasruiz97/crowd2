<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use App\info_dispositivos;
use App\User;
use Flash;
use Response;


class infoDispositivosController extends Controller

{
    public function __construct()
    {        
        $this->middleware([
            'auth','roles:1,2,3']);    
    }
    
    public function sinDispositivos()
    {
        $user=auth()->user()->id;
        $tiposdispositivos =  \DB::table('vista_info_dispositivos')->where('user_id',$user)->get();
        if($tiposdispositivos->isEmpty()){
            return view('info_dispositivos.sindatos');
        }else{
            return(redirect(route('info_dispositivos')));
        }
    }
    
    public function index()
    {

        $user=auth()->user()->id;
        $tiposdispositivos =  \DB::table('vista_info_dispositivos')->where('user_id',$user)->get();
        $lista_datos_dispositivos  =   \DB::table('indice_dispositivos')->where('user_id',$user)->get();
        $dispositivos = $lista_datos_dispositivos;

        return view('info_dispositivos.index', compact('tiposdispositivos', 'dispositivos','user' ));
        

    }
    
}