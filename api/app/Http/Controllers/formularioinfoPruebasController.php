<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Validator,Redirect,Response;
use App\info_pruebas;
use Auth;
use Yajra\Datatables\Datatables;
use App\Pagina;
use app\User;
use app\DatosBasicos;
use App\listaconocimientopruebas;
use Flash;
use Carbon\Carbon;
use Response;

use App\Models\HerramientaUser;
use App\Models\infoPruebas;
use App\Models\navegadoresUser;
use App\Models\Users;
use Illuminate\Support\Facades\DB;


class formularioinfoPruebasController extends Controller
{

    public function __construct()
    {
        $this->middleware([
            'auth', 'roles:1,2,3'
        ]);
    }


    // obtener registros
    public  function gettipoPruebasDisponiblesxUsuario1($id_usuario = 0)
    {

        // obtener ciudades por pais  
        $listado_tipo_pruebas['data'] = Pagina::gettipoPruebasDisponiblesxUsuario($id_usuario);

        return json_encode($listado_tipo_pruebas);
        exit;
    }



    /* Display todo list */

    public function index(Request $request)
    {
        // $user_id = $request->input('user_id');



        if (Auth::check()) {
            $user = Auth::id();
        } else {

            $user = '';
        }

        ///paa efectos de prueba
        $user = auth()->user()->id;

        if (($user != '')) {
            // $tipospruebas = info_pruebas::where('user_id',$user)->get();
            $tipospruebas =  DB::table('vista_info_pruebas')->where('user_id', $user)->get();
            $lista_datos_pruebas  =   DB::table('indice_conocimiento_pruebas')->where('user_id', $user)->get();
            // dd($tipospruebas); 
        } else {
            // $tipospruebas = info_pruebas::select("*")->get();
            $tipospruebas =  DB::table('vista_info_pruebas')->get();
            $lista_datos_pruebas  =   DB::table('indice_conocimiento_pruebas')->get();
        }
        // dd($tipospruebas);
        $prueba_id = $tipospruebas->pluck('id_info_prueba')->first();
        // listado tipo tipo_pruebas
        $lista_tipo_pruebas  =      DB::table('tipo_pruebas')->get();
        // $lista_tipo_pruebas =  pagina::gettipoPruebasDisponiblesxUsuario($user);
        // dd($lista_tipo_pruebas);
        //listado de tiempo_experiencia
        $lista_tiempo_experiencia  =      DB::table('tiempo_experiencias')->get();
        //listado tipo_ejecucion
        $lista_tipo_ejecucion  =      DB::table('tipo_ejecucion')->get();


        if (request()->ajax()) {

            return datatables()->of($tipospruebas)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    if (($data->id_info_prueba != '')) {

                        // $btn = '<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Edit" class="edit btn btn-primary btn-sm" onclick=editTodo("'.$data->id_info_prueba.'","'.$data->user_id.'","'.$data->prueba_id.'","'.$data->tiempoExperiencia_id.'","'.$data->ejecucion_id.'") >Editar</a>';
                        $btn = "";
                        $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-original-title="Delete" class="btn btn-danger lnr lnr-cross " onclick=deleteTodo("' . $data->id_info_prueba . '")></a>';

                        return $btn;
                    }
                })
                ->rawColumns(['action'])
                ->make(true);
        }


        return view('info_pruebas.formularioinfopruebas', compact('tipospruebas', 'lista_tipo_pruebas', 'lista_tiempo_experiencia', 'lista_tipo_ejecucion', 'prueba_id', 'lista_datos_pruebas', 'user'));
    }



    /* insert and update todo into mysql database*/
    public function buscar($condiciones)
    {
        $resultados = DB::table('info_pruebas')->where($condiciones)->orderByRaw("user_id ASC, 
        prueba_id ASC, 
        tiempoExperiencia_id ASC, 
        ejecucion_id ASC,
        herramienta_id ASC,
        dispositivo_id ASC,
        navegador_id ASC,
        sistemaOperativo_id asc")->get();
        return $resultados;
    }

    public function store(Request $request)
    {

        $user_id = $request->input('user_id');
        $prueba_id = $request->input('prueba_id');
        $tiempoExperiencia_id = $request->input('tiempoExperiencia_id');
        $ejecucion_id = $request->input('ejecucion_id');
        $herramientasA = $request->input('herramientas');
        $dispositivo_id = $request->input('dispositivo_id1');
        $navegadoresA = $request->input('navegadores');
        $sistemaOperativo_id = $request->input('sistemaOperativo_id1');

        // Datos info pruebas
        $data = array(
            'user_id' => $user_id,
            "prueba_id" => $prueba_id,
            "tiempoExperiencia_id" => $tiempoExperiencia_id,
            "ejecucion_id" => $ejecucion_id,
            "dispositivo_id" => $dispositivo_id,
            "sistemaOperativo_id" => $sistemaOperativo_id
        );

        //siempre deben venir
        $basicos = [
            'user_id' => $user_id,
            'prueba_id' => $prueba_id,
            'tiempoExperiencia_id' => $tiempoExperiencia_id,
            'ejecucion_id' => $ejecucion_id
        ];

        $infoPrueba = infoPruebas::where($basicos)->orderByRaw("
            id_info_prueba ASC,
            user_id ASC, 
            prueba_id ASC, 
            tiempoExperiencia_id ASC, 
            ejecucion_id ASC,
            dispositivo_id ASC,
            sistemaOperativo_id asc
        ");
        if(!isset($herramientasA)){
            $herramientasA = [];
        }
        if(!isset($navegadoresA)){
            $navegadoresA = [];
        }

        // $h = Users::find(7);
        // return $h->infoPruebas()->first()->herramientas()->first()->tipoHerramienta;

        if (count($infoPrueba->get())) {
            $infoPruebaI = $infoPrueba->first()->update($data);

            $navegadores = $infoPrueba->first()->navegadores()->pluck('navegador_id')->toArray();
            $herramientas = $infoPrueba->first()->herramientas()->pluck('herramienta_id')->toArray();

            // return $herramientas;

            // Actualizar herramientas
            $herramientasAdd = array_diff($herramientasA,$herramientas);
            $herramientasRem = array_diff($herramientas,$herramientasA);
            // Nuevas herramietnas
            foreach ($herramientasAdd as $herramienta) {
                HerramientaUser::create([
                    'user_id' => $user_id,
                    "info_prueba_id" => $infoPrueba->first()->id_info_prueba,
                    "herramienta_id" => $herramienta
                ])->save();
            }
            // eliminar herramientas
            foreach ($herramientasRem as $herramienta) {
                $infoPrueba->first()->herramientas()->where(['herramienta_id' => $herramienta])->delete();
            }

            // Actualizar navegadores
            $navegadoreAdd = array_diff($navegadoresA,$navegadores);
            $navegadoresRem = array_diff($navegadores,$navegadoresA);
            // Nuevas herramietnas
            foreach ($navegadoreAdd as $navegador) {
                navegadoresUser::create([
                    'user_id' => $user_id,
                    "info_prueba_id" => $infoPrueba->first()->id_info_prueba,
                    "navegador_id" => $navegador
                ])->save();
            }
            // eliminar herramientas
            foreach ($navegadoresRem as $navegador) {
                $infoPrueba->first()->navegadores()->where(['navegador_id' => $navegador])->delete();
            }

            //return $herramientasAdd;

        } else {
            // buscar registros anteriores
            $listInfoPruebas = infoPruebas::where([ 'user_id' => $user_id ])->get();
            //no encuentra encuentra basicos 
            $infoPruebaI = infoPruebas::insertGetId($data);

            // Crear herramientas
            foreach ($herramientasA as $herramienta) {
                HerramientaUser::create([
                    'user_id' => $user_id,
                    "info_prueba_id" => $infoPruebaI,
                    "herramienta_id" => $herramienta
                ])->save();
            }

            // Crear navegadores
            foreach ($navegadoresA as $navegador) {
                navegadoresUser::create([
                    'user_id' => $user_id,
                    "info_prueba_id" => $infoPruebaI,
                    "navegador_id" => $navegador
                ])->save();
            }

            if(count($listInfoPruebas) == 0){
                //Con esto se agregan los puntos
                $dataPuntos = array(
                    'user_id' => auth()->user()->id,
                    'puntos' => 1,
                    'motivo' => 'Agregar conocimiento en pruebas',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                );
                DB::table('puntos_tester')->insert($dataPuntos);
            }

            // $check = info_pruebas::create($data);
        }
        
        Flash::success('Acción ejecutada correctamente. Si añadiste todos los campos, tienes un punto adicional.');
        return Response::json($infoPruebaI);

    }

    /* display edit todo form with data */

    public function edit(Request $request, $id)
    {
        $user_id = $request->input('user_id');
        $prueba_id = $request->input('prueba_id');
        $tiempoExperiencia_id = $request->input('tiempoExperiencia_id');
        $ejecucion_id = $request->input('ejecucion_id');
        $data['todo'] = info_pruebas::where('id_info_prueba', $id)->first();
        dd();
        return Response::json($data['todo']);
    }

    /* delete todo from mysql database */

    public function delete($id)
    {
        //  chequear que no exista 

        if (Auth::check()) {
            $user = Auth::id();
        } else {

            $user = 0;
        }

        // $ff = listaconocimientopruebas::where('user_id',$user)->first();

        $existente = DB::table('info_pruebas')->where('id_info_prueba', '=', $id)->get();

        foreach ($existente as $lleno) {
            $herramienta = $lleno->herramienta_id;
            $dispositivo = $lleno->dispositivo_id;
            $navegador = $lleno->navegador_id;
            $so = $lleno->sistemaOperativo_id;
        }

        
        if (($herramienta == null) || ($dispositivo == null) || ($navegador == null) || ($so == null)) {
            $check = DB::table('info_pruebas')->where('id_info_prueba', '=', $id)->delete();
        } else {
            $check = DB::table('info_pruebas')->where('id_info_prueba', '=', $id)->delete();
        }

        $listInfoPruebas = infoPruebas::where([ 'user_id' => auth()->user()->id ])->get();
        if(!isset($listInfoPruebas) || count($listInfoPruebas) == 0){
            //Con esto se agregan los puntos
            $dataPuntos = array(
                'user_id' => auth()->user()->id,
                'puntos' => -1,
                'motivo' => 'Eliminar conocimiento en pruebas',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );
            $puntos = DB::table('puntos_tester')->insert($dataPuntos);
        }


        //Esta parte, actualiza los puntos acumulados
        $puntosAcumulados = DB::table('puntos_tester')->where("user_id", "=", auth()->user()->id)->sum('puntos');
        // dd($puntosAcumulados);
        $users = DB::table('users')->where("id", "=", auth()->user()->id)->get();
        foreach ($users as $user) {
            $data3 = array();
            $data3['puntosAcumulados'] = $puntosAcumulados;
            $data3['updated_at'] = Carbon::now();
            DB::table('users')->where("id", "=", auth()->user()->id)->update($data3);
        }

        Flash::error('Registro eliminado correctamente. Si lo tenias completamente diligenciado, has perdido un punto por eliminarlo');
        // Response::json($check);
    }
}
