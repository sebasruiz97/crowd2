<?php
 
namespace App\Http\Controllers;
 
use App\Bug;
use Illuminate\Http\Request;
use App\Http\Requests\GuardarBugRequest;
use Redirect,Response;
use Carbon\Carbon;
use App\Mail\ReporteBugTester;
use App\Mail\AprobarBug;
use App\Mail\RechazarBug;
use App\Mail\FaltaInformacionBug;
use Illuminate\Support\Facades\Mail;
use Flash;
 
class BugsController extends Controller
{

    public function __construct()
    {        
        $this->middleware([
            'auth','roles:1,2,3']);    
    }
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
public function index()
{

    $activado = auth()->user()->estado_id;   
    $rol = auth()->user()->rol_id;
    $user = auth()->user()->id;
    
    if($rol == 1){

        if(request()->ajax()) {
        return datatables()->of(\DB::table('vista_bugs')->where('deleted_at','=',null))
                ->addColumn('action', 'bugs.action_button')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

    }elseif($rol == 3){

        if(request()->ajax()) {
        return datatables()->of(\DB::table('vista_bugs')->where([
            ['deleted_at','=',null],
            ['proyecto_responsable_actual','=',$user],
            ['proyectoEstado_id', '!=', 3],
            ]))
            ->addColumn('action', 'bugs.action_button')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
    
    }else{
        
        if($activado === 3){
        
        if(request()->ajax()) {
            return datatables()->of(\DB::table('vista_bugs_testers')->where([
            ['proyecto_tester', '=', auth()->user()->id],
            ['proyectoEstado_id', '!=', 3],
            ['deleted_at', '=', null]]))
            ->addColumn('action', 'bugs.action_button')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);    
        }
        return view('bugs.index');
        }
        return view('home_tester.sin_perfil');
    }

    return view('bugs.index');

}

 
 
/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(GuardarBugRequest $request)
{  

    $user_id = auth()->user()->id;      
    $rol =  auth()->user()->rol_id;
    $reporte = $request->validated();
    $BugID = $request->id_bug;     

    $buscarCliente = \DB::table('proyectos')->where('id_proyecto','=',$request->input('proyecto_id'))->get();
        foreach($buscarCliente as $buscar){
        $cliente = $buscar->cliente_id;
        $estadoProyecto = $buscar->ProyectoEstado_id; 
    }

   
    if($rol == 2 ||$rol == 1){

        if($estadoProyecto ==2){

            $data = array();   
            $data['user_id'] = $user_id;
            $data['cliente_id'] = $cliente;
            $data['proyecto_id'] = $request->input('proyecto_id');
            $data['bug_tipo_id'] = $request->input('bug_tipo_id');
            $data['bug_titulo'] = $request->input('bug_titulo');
            $data['bug_descripcion'] = $request->input('bug_descripcion');
            $data['bug_pasos'] = $request->input('bug_pasos');
            $data['bug_reproducible'] = $request->input('bug_reproducible');
            $data['bug_gravedad'] = $request->input('bug_gravedad');
            $data['bug_dispositivo'] = $request->input('bug_dispositivo');
            $data['bug_dispositivo_so'] = $request->input('bug_dispositivo_so');
            $data['bug_dispositivo_navegador'] = $request->input('bug_dispositivo_navegador');

            if (($BugID==null))
            {
                $data['created_at'] = Carbon::now();
                $data['bug_estado_id'] = 1;
                $insert = \DB::table('bugs')->insert($data);

                $ultimoBug = \DB::table('vista_bugs')->where('user_id','=',$user_id)->max('id_bug');
                $ultimosBUG = \DB::table('vista_bugs')
                                ->where([
                                    ['user_id','=',$user_id],
                                    ['id_bug','=',$ultimoBug]
                                    ])
                                ->get();

                foreach($ultimosBUG as $uB){
                    $fechaCreacion = $uB->fecha_creacion;
                }

                $data2 = array();    
                $data2['bug_id'] = $ultimoBug;
                $data2['user_id'] = $user_id;
                $data2['bug_cambio'] = "Hallazgo creado";
                $data2['bug_notaCambio'] = "Tester ha reportado un nuevo hallazgo";
                $data2['created_at'] = $fechaCreacion;
                \DB::table('bug_cambios')->insert($data2);       
                
                Flash::success('Hallazgo creado correctamente. Recuerda que debes agregar los anexos para que el hallazgo sea tenido en cuenta');
                return Response::json();  
                        
            }
            
            else
            {
                $usuariosReporta = \DB::table('bugs')->where('id_bug', $BugID)->get();
                foreach($usuariosReporta as $usuario){
                    $creadorBug = $usuario->user_id;
                }

                $cuentasBug = \DB::table('bugs')->where('id_bug','=',$BugID)->get('cuentaCobro_id');
                foreach($cuentasBug as $cuentaBug){
                    $cuenta = $cuentaBug->cuentaCobro_id; 
                }
                if($cuenta == null){

                    if($creadorBug === $user_id){

                        $estados = \DB::table('bugs')->where('id_bug', $BugID)->get();
                        foreach($estados as $estado){
                        $estadoBug = $estado->bug_estado_id;
                        }

                        if($estadoBug === 1 || $estadoBug === 6){

                        $data['updated_at'] = Carbon::now();
                        \DB::table('bugs')->where('id_bug', $BugID)->update($data);

                        $data2 = array();    
                        $data2['bug_id'] = $BugID;
                        $data2['user_id'] = $user_id;
                        $data2['bug_cambio'] = "Hallazgo modificado";
                        $data2['bug_notaCambio'] = "Tester ha cambiado el detalle del hallazgo";
                        $data2['created_at'] = Carbon::now();
                        \DB::table('bug_cambios')->insert($data2);

                        Flash::success('Hallazgo editado correctamente');
                        return Response::json();  
                        }

                        Flash::error('No puede editar un hallazgo con este estado');
                        return Response::json(); 

                        }
                        Flash::error('Sólo el creador del hallazgo puede editarlo');
                        return Response::json(); 

                }

                Flash::error('No puede editar un hallazgo que ya tiene asociada una cuenta de cobro');
                return Response::json(); 
                        
            }

        }

        Flash::error('No puede crear o editar un hallazgo cuando el proyecto ya no se encuentra en ejecución');

        }else{

        if($estadoProyecto ==2){

            $bug_estado = $request->bug_estado_id;    
            $testerReportes = \DB::table('bugs')->where('id_bug','=',$request->input('id_bug'))->get('user_id');
                
                foreach($testerReportes as $testerReporta){
                    $id_reporta = $testerReporta->user_id;
                } 

            $emailReportes = \DB::table('users')->where('id','=',$id_reporta)->get('email');
                foreach($emailReportes as $emailReporta){
                        $emailReporta = $emailReporta->email;
                    }

                if($bug_estado == 2){

                    $data = array();   
                    $data['bug_estado_id'] = $bug_estado;
                    $data['updated_at'] = Carbon::now();
                    $data['bug_rechazo_id'] = null;
                    \DB::table('bugs')->where('id_bug', $BugID)->update($data);
                    
                    $data2 = array();    
                    $data2['bug_id'] = $BugID;
                    $data2['user_id'] = $user_id;
                    $data2['bug_cambio'] = "Hallazgo aprobado por líder del proyecto";
                    $data2['bug_notaCambio'] = $request->input('bug_notaCambio');
                    $data2['created_at'] = Carbon::now();
                    \DB::table('bug_cambios')->insert($data2);

                    //Con esto se agregan los puntos

                    $BugsAprobados = \DB::table('vista_bugs')->where('id_bug','=',$BugID)->get('user_id'); 
                    
                    foreach( $BugsAprobados as $BugAprobado){
                        $usuarioPuntos =  $BugAprobado->user_id;
                    }
                    
                    $dataPuntos = array(
                    'user_id'=>$usuarioPuntos,
                    'puntos' => 1,
                    'motivo' => 'Hallazgo aprobado por líder del proyecto',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    );
                    $puntos=\DB::table('puntos_tester')->insert($dataPuntos);

                    //Esta parte, actualiza los puntos acumulados

                    $puntosAcumulados = \DB::table('puntos_tester')->where("user_id","=",$usuarioPuntos)->sum('puntos');
                    $users = \DB::table('users')->where("id","=",$usuarioPuntos)->get();
                    foreach($users as $user){
                        $data3 = array();
                        $data3['puntosAcumulados'] = $puntosAcumulados;
                        $data3['updated_at'] = Carbon::now();
                        \DB::table('users')->where("id","=",$usuarioPuntos)->update($data3);
                    }

            
                }elseif($bug_estado == 3){

                    $data = array();   
                    $data['bug_estado_id'] = $bug_estado;
                    $data['updated_at'] = Carbon::now();
                    $data['bug_rechazo_id'] = $request->input('bug_rechazo_id');;
                    \DB::table('bugs')->where('id_bug', $BugID)->update($data);
                    
                    $data2 = array();    
                    $data2['bug_id'] = $BugID;
                    $data2['user_id'] = $user_id;
                    $data2['bug_cambio'] = "Hallazgo rechazado por líder del proyecto";
                    $data2['bug_notaCambio'] = $request->input('bug_notaCambio');
                    $data2['created_at'] = Carbon::now();
                    \DB::table('bug_cambios')->insert($data2);

                    //Con esto se quitan los puntos

                    $BugsAprobados = \DB::table('vista_bugs')->where('id_bug','=',$BugID)->get('user_id'); 
                    
                    foreach( $BugsAprobados as $BugAprobado){
                        $usuarioPuntos =  $BugAprobado->user_id;
                    }
                    
                    $dataPuntos = array(
                    'user_id'=>$usuarioPuntos,
                    'puntos' => -1,
                    'motivo' => 'Hallazgo rechazado por líder del proyecto',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    );
                    $puntos=\DB::table('puntos_tester')->insert($dataPuntos);

                    //Esta parte, actualiza los puntos acumulados

                    $puntosAcumulados = \DB::table('puntos_tester')->where("user_id","=",$usuarioPuntos)->sum('puntos');
                    $users = \DB::table('users')->where("id","=",$usuarioPuntos)->get();
                    foreach($users as $user){
                        $data3 = array();
                        $data3['puntosAcumulados'] = $puntosAcumulados;
                        $data3['updated_at'] = Carbon::now();
                        \DB::table('users')->where("id","=",$usuarioPuntos)->update($data3);
                    }


                }elseif($bug_estado == 4){

                    $data = array();   
                    $data['bug_estado_id'] = $bug_estado;
                    $data['updated_at'] = Carbon::now();
                    $data['fecha_aprobacion_cliente'] = Carbon::now();
                    $data['fecha_rechazo_cliente'] = null;
                    $data['bug_estadoCobro'] = 1;
                    $data['bug_rechazo_id'] = null;
                    \DB::table('bugs')->where('id_bug', $BugID)->update($data);
                    
                    $data2 = array();    
                    $data2['bug_id'] = $BugID;
                    $data2['user_id'] = $user_id;
                    $data2['bug_cambio'] = "Hallazgo aprobado por el cliente";
                    $data2['bug_notaCambio'] = $request->input('bug_notaCambio');
                    $data2['created_at'] = Carbon::now();
                    \DB::table('bug_cambios')->insert($data2);

                    //Con esto se agregan los puntos

                    $BugsAprobados = \DB::table('vista_bugs')->where('id_bug','=',$BugID)->get('user_id'); 
                    
                    foreach( $BugsAprobados as $BugAprobado){
                        $usuarioPuntos =  $BugAprobado->user_id;
                    }
                    
                    $dataPuntos = array(
                    'user_id'=>$usuarioPuntos,
                    'puntos' => 1,
                    'motivo' => 'Hallazgo aprobado por el cliente',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    );
                    $puntos=\DB::table('puntos_tester')->insert($dataPuntos);

                    //Esta parte, actualiza los puntos acumulados

                    $puntosAcumulados = \DB::table('puntos_tester')->where("user_id","=",$usuarioPuntos)->sum('puntos');
                    $users = \DB::table('users')->where("id","=",$usuarioPuntos)->get();
                    foreach($users as $user){
                        $data3 = array();
                        $data3['puntosAcumulados'] = $puntosAcumulados;
                        $data3['updated_at'] = Carbon::now();
                        \DB::table('users')->where("id","=",$usuarioPuntos)->update($data3);
                    }

                    //Esta parte envía notificacion
                    $reportes = \DB::table('vista_bugs')->where('id_bug','=',$BugID)->get();
                    foreach($reportes as $reporte){
                        $id = $reporte->id_bug;
                        $usuario = $reporte->name;   
                        $proyecto = $reporte->proyecto_nombre;   
                        $tipoBug = $reporte->nombre_tipoBug;   
                        $tituloBug = $reporte->bug_titulo;
                        $estadoBug = $reporte->nombre_estadoBug;        
                        $fechaCreacion = $reporte->fecha_creacion;  
                    }  

                    Mail::to($emailReporta)
                    ->queue(new AprobarBug($id,$usuario,$proyecto,$tipoBug,$tituloBug,$estadoBug,$fechaCreacion));

                }elseif($bug_estado == 5){

                    $data = array();   
                    $data['bug_estado_id'] = $bug_estado;
                    $data['updated_at'] = Carbon::now();
                    $data['fecha_rechazo_cliente'] = Carbon::now();
                    $data['fecha_aprobacion_cliente'] = null;
                    $data['bug_rechazo_id'] = $request->input('bug_rechazo_id');;
                    \DB::table('bugs')->where('id_bug', $BugID)->update($data);
                    
                    $data2 = array();    
                    $data2['bug_id'] = $BugID;
                    $data2['user_id'] = $user_id;
                    $data2['bug_cambio'] = "Hallazgo rechazado por  el cliente";
                    $data2['bug_notaCambio'] = $request->input('bug_notaCambio');
                    $data2['created_at'] = Carbon::now();
                    \DB::table('bug_cambios')->insert($data2);

                    //Con esto se quitan los puntos

                    $BugsAprobados = \DB::table('vista_bugs')->where('id_bug','=',$BugID)->get('user_id'); 
                    
                    foreach( $BugsAprobados as $BugAprobado){
                        $usuarioPuntos =  $BugAprobado->user_id;
                    }
                    
                    $dataPuntos = array(
                    'user_id'=>$usuarioPuntos,
                    'puntos' => -1,
                    'motivo' => 'Hallazgo rechazado por el cliente',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    );
                    $puntos=\DB::table('puntos_tester')->insert($dataPuntos);

                    //Esta parte, actualiza los puntos acumulados

                    $puntosAcumulados = \DB::table('puntos_tester')->where("user_id","=",$usuarioPuntos)->sum('puntos');
                    $users = \DB::table('users')->where("id","=",$usuarioPuntos)->get();
                    foreach($users as $user){
                        $data3 = array();
                        $data3['puntosAcumulados'] = $puntosAcumulados;
                        $data3['updated_at'] = Carbon::now();
                        \DB::table('users')->where("id","=",$usuarioPuntos)->update($data3);
                    }

                    $reportes = \DB::table('vista_bugs')->where('id_bug','=',$BugID)->get();
                    foreach($reportes as $reporte){
                        $id = $reporte->id_bug;
                        $usuario = $reporte->name;   
                        $proyecto = $reporte->proyecto_nombre;   
                        $tipoBug = $reporte->nombre_tipoBug;   
                        $tituloBug = $reporte->bug_titulo;
                        $estadoBug = $reporte->nombre_estadoBug;    
                        $motivoRechazo = $reporte->motivo_rechazoBug;    
                        $fechaCreacion = $reporte->fecha_creacion;            
                    }  
                    
                    Mail::to($emailReporta)
                    ->queue(new RechazarBug($id,$usuario,$proyecto,$tipoBug,$tituloBug,$estadoBug,$motivoRechazo, $fechaCreacion));

                }else{

                    $data = array();   
                    $data['bug_estado_id'] = $bug_estado;
                    $data['updated_at'] = Carbon::now();
                    $data['bug_rechazo_id'] = null;
                    \DB::table('bugs')->where('id_bug', $BugID)->update($data);
                    
                    $data2 = array();    
                    $data2['bug_id'] = $BugID;
                    $data2['user_id'] = $user_id;
                    $data2['bug_cambio'] = "Se requiere más información del hallazgo";
                    $data2['bug_notaCambio'] = $request->input('bug_notaCambio');
                    $data2['created_at'] = Carbon::now();
                    \DB::table('bug_cambios')->insert($data2);

                    $reportes = \DB::table('vista_bugs')->where('id_bug','=',$BugID)->get();
                    foreach($reportes as $reporte){
                        $id = $reporte->id_bug;
                        $usuario = $reporte->name;   
                        $proyecto = $reporte->proyecto_nombre;   
                        $tipoBug = $reporte->nombre_tipoBug;   
                        $tituloBug = $reporte->bug_titulo;
                        $estadoBug = $reporte->nombre_estadoBug;    
                    }  

                    $ultimoCambio =  \DB::table('vista_bugs_cambios')->where('id_bug','=',$BugID)->max('id_bugCambio');
                    $notas =  \DB::table('vista_bugs_cambios')->where('id_bugCambio','=',$ultimoCambio)->get();

                    foreach($notas as $nota){
                        $cambio = $nota->bug_notaCambio;
                    }

                    Mail::to($emailReporta)
                    ->queue(new FaltaInformacionBug($id,$usuario,$proyecto,$tipoBug,$tituloBug,$estadoBug,$cambio));

                }

                Flash::success('Hallazgo editado correctamente');
                return Response::json();  

        }

            Flash::error('No puede editar un hallazgo cuando el proyecto ya no se encuentra en ejecución');  
            return Response::json();   
            }


    

    Flash::error('No puede realizar cambios sobre un hallazgo que ya tiene una cuenta de cobro generada');
    return Response::json();

   

}
 
 
/**
 * Show the form for editing the specified resource.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */

public function show($id)
{   
    $bugs = \DB::table('vista_bugs_cambios')->where('id_bug','=',$id)->get();
    return Response::json($bugs);
}

public function edit($id)
{   
    $where = array('id_bug' => $id);

    $Bug  = Bug::where($where)->first();
 
    return Response::json($Bug);
}

 
/**
 * Remove the specified resource from storage.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
    {

        $hallazgos = \DB::table('vista_bugs')->where('id_bug','=',$id)->get();
        $user_id = auth()->user()->id;

        foreach ($hallazgos as $hallazgo){
            $estadoBug = $hallazgo->bug_estado_id;
            $creadorBug = $hallazgo->user_id;
            $estadoProyecto = $hallazgo->ProyectoEstado_id;
        }

        if($estadoProyecto == 2){

            if($creadorBug === $user_id){

                if($estadoBug===1){
                    $data = array();    
                    $data['deleted_at'] = Carbon::now();
                    $bugs = \DB::table('bugs')
                                ->where('id_bug','=',$id)
                                ->update($data);
                    Flash::error('Hallazgo eliminado correctamente');
                    return Response::json($bugs);
                }

                Flash::error('No puede eliminar un hallazgo con este estado');
                return Response::json();

            }

            Flash::error('Sólo el creador del hallazgo lo puede eliminar');
            return Response::json();
        }        

        Flash::error('No puede eliminar un hallazgo si el proyecto ya no se encuentra en ejecución');
        return Response::json();
    }

public function buscarVersionSistemaOperativo(Request $request){

    $data = \DB::table('vista_so_por_dispositivos')->select('nombre_sistemaOperativo','sistemaOperativo_id')->where('dispositivo_id',$request->bug_dispositivo)->get();
    return response()->json($data);//then sent this data to ajax success*/

}

public function buscarNavegador(Request $request){

    $data = \DB::table('vista_navegadores_x_dispositivo')->select('nombre_navegador','navegador_id')->where('dispositivo_id',$request->bug_dispositivo)->get();
    return response()->json($data);//then sent this data to ajax success*/

}

}





