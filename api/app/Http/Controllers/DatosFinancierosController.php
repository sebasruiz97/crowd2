<?php

namespace App\Http\Controllers;

use App\DatoFinanciero;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\GuardarDatosFinancierosRequest;
use App\Http\Controllers\AppBaseController;
use App\User;
use App\DatosBasicos;
use App\PuntosTester;
use App\Models\TipoBanco;
use App\Models\TipoCuentaBancaria;
use Flash;
use Response;
use Carbon\Carbon;


class DatosFinancierosController extends Controller
{
    public function __construct()
    {        
        $this->middleware([
            'auth','roles:1,2,3']);    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = auth()->user()->id;
        $datosFinancieros = DatoFinanciero::where("user_id","=",$user)->paginate(6);

        foreach($datosFinancieros as $datoFinanciero){
                $tipoBanco = TipoBanco::find($datoFinanciero->banco_id);
                $tipoCuentaBancaria = TipoCuentaBancaria::find($datoFinanciero->cuentaBancaria_id);
                $datoFinanciero["tipoBanco"] = $tipoBanco;
                $datoFinanciero["tipoCuentaBancaria"] = $tipoCuentaBancaria;
        }
        return view('datos_financieros.index', compact('datosFinancieros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('datos_financieros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GuardarDatosFinancierosRequest $request, User $user)
    {

        $existe =  DatoFinanciero::query()->where([
            ['user_id', '=',  $request->input('user_id')],
        ])->get();

        if($existe->isEmpty()){
            $id = 'Agregar datos financieros';
            $data = $request->validated();
            
            $data = array();
            $data['user_id'] = $request->input('user_id');
            $data['banco_id'] = $request->input('banco_id');
            $data['cuentaBancaria_id'] = $request->input('cuentaBancaria_id');
            $data['numeroCuenta'] = $request->input('numeroCuenta');
            $data['nombreTitular'] = auth()->user()->name;
            $data['sucursal'] = $request->input('sucursal');
            $data['no_retencion_sin_minimo'] = $request->input('no_retencion_sin_minimo');
            $data['no_personas_vinculadas'] = $request->input('no_personas_vinculadas');
            $data['created_at'] = Carbon::now();
            \DB::table('datos_financieros')->insert($data);
            return redirect(route('asignarPuntos',$id));
        }else{
            Flash::error('Ya tienes registrada una cuenta bancaria registrada, puedes editarla si lo deseas');
            return redirect(route('datosFinancieros.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DatosFinancieros  $datosFinancieros
     * @return \Illuminate\Http\Response
     */
    public function show($id_datoFinanciero)
    {
        
        $datosFinancieros = DatoFinanciero::where("id_datoFinanciero","=",$id_datoFinanciero)->get();

        foreach($datosFinancieros as $datoFinanciero){
                $tipoCuentaBancaria = TipoCuentaBancaria::find($datoFinanciero->cuentaBancaria_id);
                $tipoBanco = TipoBanco::find($datoFinanciero->banco_id);
                $datoFinanciero["tipoCuentaBancaria"] = $tipoCuentaBancaria;
                $datoFinanciero["tipoBanco"] = $tipoBanco;
        }

        if (empty($datosFinancieros)) {
                Flash::error('Dato Financiero no encontrado');
                return redirect(route('datosFinancieros.index'));
            }

        return view('datos_financieros.show', compact('datosFinancieros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DatosFinancieros  $datosFinancieros
     * @return \Illuminate\Http\Response
     */
    public function edit($id_datoFinanciero)
    {
        $datosFinancieros = DatoFinanciero::where("id_datoFinanciero","=",$id_datoFinanciero)->get();
        $tipoBanco = TipoBanco::pluck('nombre_banco','id_banco');
        $tipoCuentaBancaria = TipoCuentaBancaria::pluck('nombre_cuentaBancaria','id_cuentaBancaria');
    
        //para dejar seleccionado
        $selDatoFinanciero = DatoFinanciero::where("id_datoFinanciero","=",$id_datoFinanciero)->select("banco_id")->first();
        $tipoBancoSeleccionado = $selDatoFinanciero->banco_id;
        $selCuentaBancaria = DatoFinanciero::where("id_datoFinanciero","=",$id_datoFinanciero)->select("cuentaBancaria_id")->first();
        $tipoCuentaBancariaSeleccionado = $selCuentaBancaria->cuentaBancaria_id;
        
        if (empty($datosFinancieros)) {
            Flash::error('Dato Financiero no encontrado');
            return redirect(route('datosFinancieros.index'));
        }

        return view('datos_financieros.edit',compact('datosFinancieros','tipoBanco','tipoCuentaBancaria','tipoBancoSeleccionado','tipoCuentaBancariaSeleccionado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DatosFinancieros  $datosFinancieros
     * @return \Illuminate\Http\Response
     */
    public function update($id_datoFinanciero, GuardarDatosFinancierosRequest $request)
    {

        $repetido =  DatoFinanciero::query()->where([
            ['user_id', '=',  $request->input('user_id')],
            ['banco_id', '=', $request->input('banco_id')],
            ['cuentaBancaria_id', '=', $request->input('cuentaBancaria_id')],
            ['nombreTitular', '=', $request->input('numeroCuenta')],
            ['sucursal', '=', $request->input('sucursal')],
            ['no_retencion_sin_minimo', '=', $request->input('no_retencion_sin_minimo')],
            ['no_personas_vinculadas', '=', $request->input('no_personas_vinculadas')],            
            ])->get();

        if($repetido->isEmpty()){
            $datosFinancieros = DatoFinanciero::find($id_datoFinanciero);
            if (empty($datosFinancieros)) {
                Flash::error('Dato Financiero no encontrado');
                return redirect()->route('datosFinancieros.index');
            }

            $data = $request->validated();
            $data = array();
            $data['user_id'] = $request->input('user_id');
            $data['banco_id'] = $request->input('banco_id');
            $data['cuentaBancaria_id'] = $request->input('cuentaBancaria_id');
            $data['numeroCuenta'] = $request->input('numeroCuenta');
            $data['nombreTitular'] = auth()->user()->name;
            $data['sucursal'] = $request->input('sucursal');
            $data['no_retencion_sin_minimo'] = "$request->input('no_retencion_sin_minimo')";
            $data['no_personas_vinculadas'] = $request->input('no_personas_vinculadas');
            $data['created_at'] = Carbon::now();
            \DB::table('datos_financieros')->where('user_id','=',auth()->user()->id)->update($data);

            Flash::success('Dato Financiero actualizado correctamente.');
            return redirect(route('datosFinancieros.index'));

        }else{
            Flash::error('Ya tienes registrada una cuenta bancaria para este banco con el mismo número');
            return redirect(route('datosFinancieros.index'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DatosFinancieros  $datosFinancieros
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_datoFinanciero)
    {
        $id = 'Eliminar datos financieros';

        $datosFinancieros = DatoFinanciero::find($id_datoFinanciero);
        if (empty($datosFinancieros)) {
            Flash::error('Dato Financiero no encontrado');
            return redirect(route('datosFinancieros.index'));
        }
        $datosFinancieros->delete($id_datoFinanciero);

        return redirect(route('quitarPuntos',$id));
    }
}
