<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Mail\MessageReceived;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Flash;

class ContactoController extends Controller
{
    public function store(Request $request)
    {
      try {
        //$this->validator($request->all())->validate();
  
        $msg = request()->validate([
          'nombre'=>'required|max:50|regex:/^[\pL\s\-]+$/u',
          'email'=>'required|email',
          'asunto'=>'required|max:100',
          'mensaje'=>'required|min:4'
        ], [
              'nombre.required' => __('Necesitamos tu nombre'),
              'nombre.max' => __('Tu nombre debe contener máximo 100 caracteres'),
              'nombre.regex' => __('Tu nombre sólo puede contener letras y espacios'),
              'email.required' => __('Necesitamos tu dirección de correo'),
              'email.email' => __('Ingresa una dirección de correo válida'),
              'mensaje.required' => __('Especifica un mensaje'),
              'mensaje.min' => __('El mensaje debe contener mínimo 4 carácteres'),
              'asunto.required' => __('El asunto es requerido'),
              'asunto.max' => __('El asunto no debe contener máximo 100 carácteres'),
        ]);
  
        Mail::to( config('app.admin_email') )->queue(new MessageReceived($msg));

        return \Response::json(['message' => 'ok'], 200);
      } catch (\Throwable $th) {
        return response()->json([ 'error' => $th->getMessage() ]);
      }
    }

    protected function validator(array $data)
    {
      return Validator::make($data, [
        'g-recaptcha-response' => 'recaptcha'
      ]);
    }
}
