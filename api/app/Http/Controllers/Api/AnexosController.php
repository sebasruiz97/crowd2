<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Anexo;

use App\Services\crypto;

class AnexosController extends Controller
{
    public function index(Request $request)
    {
        try {
            $user = $request->user()->id;
            $anexos = Anexo::where('user_id',$user)->get();
    
            return \Response::json([
                'anexos' => crypto::arrayEncrypt($anexos)
            ]);
        } catch (\Throwable $th) {
            return response()->json([ 'error' => $th->getMessage() ]);
        }
    }

    public function insert()
    {

    }
    
    public function delete($id)
    {
        try {
            \DB::delete("DELETE FROM anexos WHERE id_anexo = '$id'");
            return \Response::json(['message' => 'ok'], 200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }
}
