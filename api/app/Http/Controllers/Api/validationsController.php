<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Mantis\ProjectController;
use App\Http\Controllers\Controller;
use App\Models\Mantis\UserProject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Mantis\Project;

class validationsController extends Controller
{
    //
    public function validReto(Request $request)
    {
        try {
            $data = $request->only('project');
            $validator = Validator::make($data, ['project' => 'required']);
            if ($validator->fails()) {
                return response()->json(["error" => $validator->messages(), 'assign' => false], 200);
            }
            $userProject = UserProject::where([ 'user_id' => $request->user()->id, 'project_id' => $data['project'] ]);
            if($userProject){
                $project = ProjectController::dataProject($data['project']);
                return response()->json(['assign' => isset($project)? true : false, 'project' => $project]);
            }
            return response()->json(['assign' => false]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['error' => $th->getMessage()]);
        }
    }
}
