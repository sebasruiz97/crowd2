<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\VistaInfoDispositivos;

use App\Services\crypto;

use Carbon\Carbon;

class InfoDispositivos extends Controller
{
    public function index(Request $request)
    {
        try {
            $user = $request->user()->id;
            $dispositivos = VistaInfoDispositivos::where('user_id',$user)->get();
            return \Response::json([
                'dispositivos' => crypto::arrayEncrypt($dispositivos)
            ]);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }

    public function crud(Request $request)
    {
        try {
            $datos = $request->only('dispositivo_id','navegador_id','sistemaOperativo_id','marca_id','modelo_id');
            $datos['user_id'] = $request->user()->id;
            $id = $request->input('id');
            if(isset($id)){
                $datos['updated_at'] = Carbon::now();
                $dispositivo = \DB::table('info_dispositivos')
                                ->where('id_info_dispositivo', $id)
                                ->update($datos);
            } else {
                $datos['created_at'] = Carbon::now();
                \DB::table('info_dispositivos')->insert($datos);
            }
            return response()->json(['message' => 'ok'], 200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }

    public function delete($id)
    {
        try {
            \DB::delete("DELETE FROM info_dispositivos WHERE id_info_dispositivo = '$id'");
            return \Response::json(['message' => 'ok'], 200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }
}
