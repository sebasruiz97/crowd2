<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Services\crypto;

use App\Models\Pais;
use App\Models\Ocupacion;
use App\Models\Departamento;
use App\Models\Municipio;
use App\Models\NivelEducacion;
use App\Models\AreaEstudio;
use App\Models\EstadoEstudio;
use App\Models\Universidad;
use App\Models\Plataforma;
use App\Models\TipoEstudio;
use App\Models\TipoPruebas;
use App\Models\infoPruebas;
use App\Models\TiempoExperiencia;
use App\Models\TipoEjecucion;
use App\Models\TipoDispositivo;

use App\Models\HerramientaUser;
use App\Models\navegadoresUser;

use App\EstudioFormal;
use App\EstudioNoFormal;

use App\Models\TipoBanco;
use App\Models\TipoCuentaBancaria;

use App\DatosBasicos;

class DatosFormulariosController extends Controller
{
    //
    public function datosBasicos(Request $request)
    {
        try {
            //code...
            $datos = [
                'paises' => Pais::all(),
                'ocupaciones' => Ocupacion::orderBy('nombre_ocupacion')->get(),
                'departamentos' => Departamento::orderBy('nombre_departamento')->get(),
                'municipios' => Municipio::orderBy('nombre_municipio')->get()
            ];
            $misDatos = datosBasicos::where('user_id',$request->user()->id)->first();
            if(isset($misDatos)){
                $datos['misDatos'] = crypto::arrayEncrypt($misDatos);
            }
            /*return response()->json([
                'res' => crypto::arrayEncrypt([
                    'data' => $datos
                ])
            ], 200);*/
            return response()->json($datos,200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }

    }

    public function estudioFormal(Request $request)
    {
        try {
            $id = $request->input('id');
            $datos = [
                'niveles' => NivelEducacion::orderBy('nombre_nivelEducacion')->get(),
                'areas' => AreaEstudio::orderBy('nombre_areaEstudio')->get(),
                'estados' => EstadoEstudio::orderBy('nombre_estadoEstudio')->get(),
                'departamentos' => Departamento::orderBy('nombre_departamento')->get(),
                'universidades' => Universidad::orderBy('nombre_universidad')->get()
            ];
            if(isset($id)){
                $datos['estudioFormal'] = crypto::arrayEncrypt(EstudioFormal::find($id)->first());
            }
            return response()->json($datos,200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }

    public function estudioNoFormal(Request $request)
    {
        try {
            $id = $request->input('id');
            $datos = [
                'tipo_estudios' => TipoEstudio::orderBy('nombre_tipoEstudio')->get(),
                'universidades' => Universidad::orderBy('nombre_universidad')->get(),
                'plataformas' => Plataforma::orderBy('nombre_plataforma')->get()
            ];
            if(isset($id)){
                $datos['estudioNoFormal'] = crypto::arrayEncrypt(EstudioNoFormal::find($id)->first());
            }
            if($request->input('encrypt') == true){
                return response()->json([
                    'res' => crypto::arrayEncrypt([
                        'data' => $datos
                    ])
                ], 200);
            }
            return response()->json($datos,200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }

    public function conocimientoPruebas(Request $request)
    {
        try {
            $id = $request->input('id');
            $id_usuario = $request->user()->id;
            $datos = [
                'tipo_pruebas' => TipoPruebas::orderBy('nombre_prueba')->get(),
                'tiempos_experiencia' => TiempoExperiencia::all(),
                'tipos_ejecucion' => TipoEjecucion::orderBy('nombre_ejecucion')->get(),
                'tipos_dispositivo' => TipoDispositivo::orderBy('nombre_dispositivo')->get(),
                'navegadores' => \DB::table('navegadores_por_dispositivo')
                                    ->join('tipo_navegadores','tipo_navegadores.id_navegador','navegadores_por_dispositivo.navegador_id')
                                    ->get(),
                'dispositivos' => \DB::table('dispositivos_por_prueba')
                                    ->join('dispositivos','dispositivos.id_dispositivo','dispositivos_por_prueba.dispositivo_id')
                                    ->get(),
                'herramientas' => \DB::table('herramientas_por_prueba')
                                    ->join('tipo_herramientas','tipo_herramientas.id_herramienta','herramientas_por_prueba.herramienta_id')
                                    ->get(),
                'sistemas_operativos' => \DB::table('sistemas_operativos_por_dispositivo')
                                    ->join('tipo_sistemas_operativos','tipo_sistemas_operativos.id_sistemaOperativo','sistemas_operativos_por_dispositivo.sistemaOperativo_id')
                                    ->get()
            ];
            if(isset($id)){
                $datos['conocimiento'] = infoPruebas::find($id);
                $datos['conocimiento']['navegadores'] = navegadoresUser::where([
                    'user_id' => $id_usuario,
                    'info_prueba_id' => $id
                ])->get()->pluck('navegador_id')->toArray();
                $datos['conocimiento']['herramientas'] = HerramientaUser::where([
                    'user_id' => $id_usuario,
                    'info_prueba_id' => $id
                ])->get()->pluck('herramienta_id')->toArray();
                $datos['conocimiento'] = crypto::arrayEncrypt($datos['conocimiento']);
            }
            if($request->input('encrypt') == true){
                return response()->json([
                    'res' => crypto::arrayEncrypt([
                        'data' => $datos
                    ])
                ], 200);
            }
            return response()->json($datos,200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }

    public function infoDispositivos(Request $request)
    {
        try {
            $id = $request->input('id');
            $user = $request->user()->id;
            $tipo_dispositivos = TipoDispositivo::all();
            $datos = [
                'tipos_dispositivos' => $tipo_dispositivos,
                'sistemas_operativos' => \DB::table('sistemas_operativos_por_dispositivo')
                                    ->join('tipo_sistemas_operativos','tipo_sistemas_operativos.id_sistemaOperativo','sistemas_operativos_por_dispositivo.sistemaOperativo_id')
                                    ->orderBy('tipo_sistemas_operativos.nombre_sistemaOperativo')
                                    ->get(),
                'navegadores' => \DB::table('navegadores_por_dispositivo')
                                    ->join('tipo_navegadores','tipo_navegadores.id_navegador','navegadores_por_dispositivo.navegador_id')
                                    ->orderBy('tipo_navegadores.nombre_navegador')
                                    ->get(),
                'dispositivos' => \DB::table('dispositivos')->orderBy('dispositivos.nombre')->get(),
                'modelos' => \DB::table('modelos_dispositivo')->orderBy('modelos_dispositivo.nombre')->get()
            ];
            if(isset($id)){
                $infoDispositivo = \DB::table('info_dispositivos')->where(['user_id' => $user, 'id_info_dispositivo' => $id])->first();
                $datos['infoDispositivo'] = crypto::arrayEncrypt($infoDispositivo);
            }
            return response()->json($datos,200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }

    public function datosFinancieros(Request $request)
    {
        try {
            $id = $request->input('id');
            $user = $request->user()->id;
            $datos = [
                'tipoBancos' => TipoBanco::orderBy('nombre_banco')->get(),
                'tipoCuentasBancarias' => TipoCuentaBancaria::orderBy('nombre_cuentaBancaria')->get()
            ];
            $datos['nombreTitular'] = $request->user()->name . " " . $request->user()->lastname;
            if(isset($id)){
                $infoDispositivo = \DB::table('datos_financieros')->where(['user_id' => $user, 'id_datoFinanciero' => $id])->first();
                $datos['datosFinancieros'] = crypto::arrayEncrypt($infoDispositivo);
            }
            return response()->json($datos,200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }

}
