<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use App\DatoFinanciero;
use App\Models\TipoBanco;
use App\Models\TipoCuentaBancaria;

use App\Services\crypto;
use Carbon\Carbon;

class DatosFinancierosController extends Controller
{
    public function index(Request $request){
        try {
            $user = $request->user()->id;
            $datosFinancieros = DatoFinanciero::where("user_id","=",$user)->get();
    
            foreach ($datosFinancieros as &$dato) {
                $dato->banco = TipoBanco::find($dato->banco_id);
                $dato->cuentaBancaria = TipoCuentaBancaria::find($dato->cuentaBancaria_id);
            }

            return \Response::json([
                'datosFinancieros' => crypto::arrayEncrypt($datosFinancieros)
            ]);
        } catch (\Throwable $th) {
            return response()->json([ 'error' => $th->getMessage() ]);
        }
    }

    public function crud(Request $request)
    {
        try {
            $datos = $request->only('banco_id','cuentaBancaria_id','numeroCuenta','sucursal');
            $user = User::where('email', $request->user()->email)->first();
            $datos['user_id'] = $user->id;
            $datos['nombreTitular'] = $user->name . " " . $user->lastname;
            $datos['no_retencion_sin_minimo'] = "on";
            $datos['no_personas_vinculadas'] = "on";
            $id = $request->input('id');
            if(isset($id)){
                $datos['updated_at'] = Carbon::now();
                $dispositivo = \DB::table('datos_financieros')
                                ->where('id_datoFinanciero', $id)
                                ->update($datos);
            } else {
                $datos['created_at'] = Carbon::now();
                \DB::table('datos_financieros')->insert($datos);
            }
            return response()->json(['message' => 'ok'], 200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }

    public function delete($id)
    {
        try {
            \DB::delete("DELETE FROM datos_financieros WHERE id_datoFinanciero = '$id'");
            return \Response::json(['message' => 'ok'], 200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }
}
