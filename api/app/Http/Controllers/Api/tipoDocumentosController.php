<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\TipoDocumento;
use App\Services\crypto;

class TipoDocumentosController extends Controller
{
    //
    public function index()
    {
        $tipoDocumentos = TipoDocumento::all();
        return \Response::json([
            'res' => crypto::arrayEncrypt([
                "data" => [
                    "tipo_documentos" => $tipoDocumentos
                ]
            ])
        ], 200);
    }
}
