<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\crypto;
use App\User;

class UserController extends Controller
{
    public function index()
    {

    }

    public function find(Request $request)
    {
        try {
            //code...
            $data = $request->only('email','numeroDocumento');
            $where = [];
            if(isset($data['email'])){
                array_push($where, ['email','=',$data['email']]);
            }
            if(isset($data['numeroDocumento'])){
                array_push($where, ['numeroDocumento','=',$data['numeroDocumento']]);
            }
            $userData = User::where($where)->get();
            return \Response::json([
                'res' => crypto::arrayEncrypt([
                    "data" => [
                        "usuario" => $userData
                    ]
                ])
            ], 200);
        } catch (\Throwable $th) {
            //throw $th;
            return \Response::json(['error' => $th->getMessage()]);
        }
    }
}
