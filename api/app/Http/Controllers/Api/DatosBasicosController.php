<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DatosBasicos;
use App\EstudioFormal;
use App\EstudioNoFormal;

use App\Models\VistaEstudiosFormales;
use App\Models\VistaEstudiosNoFormales;

use App\Services\crypto;
use Carbon\Carbon;
use DB;

class DatosBasicosController extends Controller
{
    public function getData(Request $request)
    {
        try {
            $user = $request->user();
            $user['tipoDocumento'] = DB::table('tipo_documentos')
                                                    ->select('nombre_tipoDocumento')
                                                    ->where('id_tipoDocumento',$user['tipoDocumento_id'])
                                                    ->first()->nombre_tipoDocumento;
            $data = DatosBasicos::where('user_id', $user['id'])
                        ->select('datos_basicos.*','ocupaciones.nombre_ocupacion','paises.nombre_pais', 'departamentos.nombre_departamento', 'municipios.nombre_municipio')
                        ->leftJoin('ocupaciones','id_ocupacion','datos_basicos.ocupacion_id')
                        ->leftJoin('paises','paises.id_pais','datos_basicos.pais_id')
                        ->leftJoin('departamentos','departamentos.id_departamento','datos_basicos.departamento_id')
                        ->leftJoin('municipios','municipios.id_municipio','datos_basicos.municipio_id')
                        ->first();
            
            return \Response::json([
                'res' => crypto::arrayEncrypt([
                    "data" => [
                        "usuario" => $request->user(),
                        "datos_basicos" => $data,
                        "estudiosFormales" => VistaEstudiosFormales::where('user_id',$request->user()->id)->get(),
                        "estudiosNoFormales" => VistaEstudiosNoFormales::where('user_id',$request->user()->id)->get()
                    ]
                ])
            ], 200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }

    public function saveData(Request $request)
    {
        try {
            $existe = DatosBasicos::where('user_id', auth()->user()->id)->first();
            $edad = Carbon::parse($request->input('fechaNacimiento'))->age;
    
            $datos = $request->only(
                'experienciaPruebas',
                'genero',
                'pais_id',
                'departamento_id',
                'municipio_id',
                'direcion',
                'telefonoFijo',
                'telefonoCelular',
                'ocupacion_id',
                'fechaNacimiento'
            );
            $datos['edad'] = $edad;
    
            if(isset($existe)){
                $existe->update($datos);
            } else {
                $datos['user_id'] = $request->user()->id;
                DatosBasicos::create($datos);
            }
            return \Response::json(['message' => 'ok'], 200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }

    }

    public function savePicture(Request $request)
    {
        try {
            $file = $request->file('avatar');
            if(isset($file)){
                $nombre = time().$file->getClientOriginalName();
                $file->move(public_path().'/storage/avatars/',$nombre);
    
                $data = array();
                $data['avatar'] = $nombre;
                \DB::table('users')->where('id', '=', $request->user()->id)->update($data);
    
                return response()->json([
                    'nombre' => $nombre
                ],200);
            } else {
                return response()->json([
                    'error' => 'no se encuentra el archivo'
                ],200);
            }
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }

}
