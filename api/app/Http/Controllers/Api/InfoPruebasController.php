<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\infoPruebas;
use App\Models\HerramientaUser;
use App\Models\navegadoresUser;

use App\info_pruebas;

use App\Services\crypto;

class InfoPruebasController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user()->id;
        $tipospruebas = \DB::select('call SP_infoPruebas(?)',array($user));
        foreach ($tipospruebas as &$info) {
            # code...
            $info->herramientas = infoPruebas::find($info->id_info_prueba)->herramientas()->with('tipoHerramienta')->get();
            $info->navegadores = infoPruebas::find($info->id_info_prueba)->navegadores()->with('tipoNavegador')->get();
        }

        $lista_tipo_pruebas  =  \DB::table('tipo_pruebas')->get();
        $lista_tiempo_experiencia  =      \DB::table('tiempo_experiencias')->get();
        $lista_tipo_ejecucion  =      \DB::table('tipo_ejecucion')->get();

        return \Response::json([
            'conocimientos' => crypto::arrayEncrypt($tipospruebas),
            'tipo_pruebas' => $lista_tipo_pruebas
        ]);
    }
    
    public function crud(Request $request)
    {
        try {
            $datos = $request->only('prueba_id','tiempoExperiencia_id','ejecucion_id','dispositivo_id',
            'sistemaOperativo_id');
            $herramientas = $request->input('herramientas');
            $navegadores = $request->input('navegadores');

            $datos['user_id'] = $request->user()->id;

            if($request->input('id')){
                $infoPrueba = infoPruebas::find($request->input('id'));
                $infoPrueba->update($datos);
            } else {
                // Create info pruebas
                $infoPrueba = infoPruebas::create($datos);
            }
            
            $herramientasNow = HerramientaUser::where([ 'user_id' => $datos['user_id'], 'info_prueba_id' => $infoPrueba->id_info_prueba ])
                                            ->get()
                                            ->pluck('herramienta_id')
                                            ->toArray();

            $herramientasToRemove = array_diff($herramientasNow,$herramientas);
            foreach ($herramientasToRemove as $hRemove) {
                \DB::delete("DELETE FROM herramientas_user hu WHERE hu.user_id=".$datos['user_id']." and hu.info_prueba_id=".$infoPrueba->id_info_prueba." and herramienta_id=$hRemove");
            }

            $herramientasToAdd = array_diff($herramientas,$herramientasNow);
            foreach ($herramientasToAdd as $hAdd) {
                HerramientaUser::create([
                    'user_id' => $datos['user_id'],
                    'info_prueba_id' => $infoPrueba->id_info_prueba,
                    'herramienta_id' => $hAdd
                ]);
            }

            $navegadoresNow = navegadoresUser::where([ 'user_id' => $datos['user_id'], 'info_prueba_id' => $infoPrueba->id_info_prueba ])
                                            ->get()
                                            ->pluck('navegador_id')
                                            ->toArray();
            $navegadoresToRemove = array_diff($navegadoresNow,$navegadores);
            foreach ($navegadoresToRemove as $nRemove) {
                \DB::delete("DELETE FROM navegadores_user nu WHERE nu.user_id=".$datos['user_id']." and nu.info_prueba_id=".$infoPrueba->id_info_prueba." and navegador_id=$nRemove");
            }

            $navegadoresToAdd = array_diff($navegadores,$navegadoresNow);
            foreach ($navegadoresToAdd as $nAdd) {
                navegadoresUser::create([
                    'user_id' => $datos['user_id'],
                    'info_prueba_id' => $infoPrueba->id_info_prueba,
                    'navegador_id' => $nAdd
                ]);
            }
            
            return response()->json([ 'message' => 'ok' ],200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }

    }

    public function delete($id)
    {
        try {
            \DB::delete("DELETE FROM info_pruebas WHERE id_info_prueba = '$id'");
            return \Response::json(['message' => 'ok'], 200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }
}
