<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\EstudioNoFormal;

class EstudioNoFormalController extends Controller
{
    public function crud(Request $request)
    {
        try {
            $datos = $request->only('tipoEstudio_id', 'nombre_estudioNoFormal', 'fechaFin_estudioNoFormal', 
                'duracion', 'lugar_estudio', 'otra_universidad', 'plataforma_id', 'universidad_id'
            );
            $id = $request->input('id');
            if(isset($id)){
                EstudioNoFormal::where([
                    'id_estudioNoFormal' => $id,
                    'user_id' => $request->user()->id
                ])->update($datos);
            } else {
                $datos['user_id'] = $request->user()->id;
                EstudioNoFormal::create($datos);
            }
            return \Response::json(['message' => 'ok'], 200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }

    }

    public function delete($id)
    {
        try {
            EstudioNoFormal::where([
                'id_estudioNoFormal' => $id,
                'user_id' => Auth()->user()->id
            ])->delete();
            return \Response::json(['message' => 'ok'], 200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }
}
