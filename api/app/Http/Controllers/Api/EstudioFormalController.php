<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\EstudioFormal;

class EstudioFormalController extends Controller
{
    public function crud(Request $request)
    {
        try {
            $datos = $request->only(
                'nivelEducacion_id',
                'areaEstudio_id',
                'estadoEstudio_id',
                'fechaInicio_estudioFormal',
                'fechaFin_estudioFormal',
                'departamento_id',
                'universidad_id'
            );
            $id = $request->input('id');
            if(isset($id)){
                EstudioFormal::where([
                    'id_estudioFormal' => $id,
                    'user_id' => $request->user()->id
                ])->update($datos);
            } else {
                $datos['user_id'] = $request->user()->id;
                EstudioFormal::create($datos);
            }
            return \Response::json(['message' => 'ok'], 200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }

    }

    public function delete($id)
    {
        try {
            EstudioFormal::where([
                'id_estudioFormal' => $id,
                'user_id' => Auth()->user()->id
            ])->delete();
            return \Response::json(['message' => 'ok'], 200);
        } catch (\Throwable $th) {
            return \Response::json(['error' => $th->getMessage()]);
        }
    }
}
