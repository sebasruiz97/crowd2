<?php

namespace App\Http\Controllers\Api\Mantis;

use App\Http\Controllers\Controller;
use App\Models\Mantis\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Services\crypto;

class ProjectController extends Controller
{
    protected static $priority = [
        10 => 'ninguna',
        20 => 'baja',
        30 => 'normal',
        40 => 'alta',
        50 => 'urgente',
        60 => 'inmediata'
    ];
    
    protected static $status = [
        10 => 'nueva',
        20 => 'se necesitan más datos',
        30 => 'aceptada',
        40 => 'confirmada',
        50 => 'asignada',
        80 => 'resuelta',
        90 => 'cerrada'
    ];
    
    public function data(Request $request){
        try {
            $data = $request->only('id');
            $validator = Validator::make($data, [ 'id' => 'required' ]);
            if ($validator->fails()) {
                return response()->json(["errors" => $validator->messages()], 200);
            }
            $project = $this->dataProject($data['id']);
            return response()->json(["project" => $project]);
        }
        catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()]);
        }
    }

    public static function dataProject($id){
        $project = Project::find($id);
        if(isset($project)){
            $project->bugs = $project->bugs;
            foreach ($project->bugs as &$bug) {
                $bug->status_label = self::$status[$bug->status];
                $bug->priority_label = self::$priority[$bug->priority];
            }
        }
        return $project;
    }

}