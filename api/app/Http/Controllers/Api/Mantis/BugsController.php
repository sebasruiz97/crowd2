<?php

namespace App\Http\Controllers\Api\Mantis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Mantis\UserMantis;
use App\Models\Mantis\BugText;
use App\Models\Mantis\Bug;

class BugsController extends Controller
{
    //
    public function reportBug(Request $request)
    {
        try {
            $data = $request->all();
            $userMantis = UserMantis::where('email', $request->user()->email)->first();

            $bugTextID = BugText::insertGetId([
                'description' => $data['descripcion'],
                'steps_to_reproduce' => $data['pasos'],
                'additional_information' => $data['info_adicional']
            ]);

            $dataReport = [
                'project_id' => $data['id'],
                'reporter_id' => $userMantis->id,
                'priority' => 30,
                'severity' => $data['severity'],
                'reproducibility' => $data['reproducibility'],
                'status' => 10,
                'bug_text_id' => $bugTextID,
                'summary' => $data ['resumen'],
                'date_submitted' => now()->timestamp,
                'last_updated' => now()->timestamp
            ];

            Bug::insert($dataReport);

            return response()->json([ 'message' => 'ok' ],200);
        } catch (\Throwable $th) {
            return response()->json(['errors' => $th->getMessage()]);
        }
    }
}