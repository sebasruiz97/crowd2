<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Mantis\UserMantis;
use Illuminate\Http\Request;

use App\Services\crypto;

class HomeController extends Controller
{
    //
    public function HomeData(Request $request)
    {
        try {
            //code...
            $user = UserMantis::where($this->relationData(),$request->user()->{$this->relationData()})->first();
            $user->projects_assign = $user->projectsAssign;
            /*$obligatorios = getArrayObligatorios();
            $seccionesIncompletas = [];
            foreach ($obligatorios as $section) {
                if(validarSeccion($section) === 'incompleted'){
                    array_push($seccionesIncompletas, $section);
                }
            }
            $user->seccionesIncompletas = $seccionesIncompletas;*/
            $user->secciones = validarSecciones($request->user()->id);
            foreach ($user->projects_assign as &$projectUser) {
                # code...
                $projectUser->project;
            }
            return \Response::json([
                'res' => crypto::arrayEncrypt([
                    "userData" => $user
                ])
            ], 200);
        } catch (\Throwable $th) {
            //throw $th;
            return \Response::json(['error' => $th->getMessage()]);
        }
    }

    protected function relationData(){
        return 'email';
    }
}
