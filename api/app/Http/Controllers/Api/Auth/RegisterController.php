<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Services\crypto;
use Flash;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    public function register(Request $request)
    {
        try {
            //code...
            $validator = $this->validator($request->all());

            if ($validator->fails()) {
                return response()->json([
                    'res' => crypto::arrayEncrypt([
                        'errors' => $validator->messages()
                    ])
                ], 200);
            }
            
            $user = $this->create($request->all());

            $user->sendEmailApiVerificationNotification();

            $token = $user->createToken('Auth-Sanctum')->plainTextToken;

            if ($response = $this->registered($request, $user)) {
                return $response;
            }

            return response()->json([
                'res' => crypto::arrayEncrypt([
                    'access_token' => $token,
                    'token_type' => 'Bearer',
                    'data' => $user
                ])
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return \Response::json($th->getMessage(), 200);
        }
    }

    /**
     * Get a validator for an incoming registration data.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $expresionRegular = 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}$/';

        $rules = [
            'name' => ['required', 'string', 'max:50', 'regex:/^[\pL\s\-]+$/u'],
            'lastname' => ['required', 'string', 'max:50', 'regex:/^[\pL\s\-]+$/u'],
            'tipoDocumento_id' => ['required', 'alpha_num', 'max:50'],
            'numeroDocumento' => ['required', 'numeric', 'digits_between:6,15', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:50', 'confirmed', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed', $expresionRegular],
            'terminosCondiciones' => ['required', 'string'],
            'g-recaptcha-response' => 'recaptcha',
        ];

        $messages = [
            'name.required' => 'Es obligatorio que registres tu nombre',
            'name.max' => 'Tu nombre debe ser de máximo 50 caracteres',
            'name.regex' => 'Tu nombre sólo debe contener letras y espacios',
            'lastname.required' => 'Es obligatorio que registres tu apellido',
            'lastname.max' => 'Tu apellido debe ser de máximo 50 caracteres',
            'lastname.regex' => 'Tu apellido sólo debe contener letras y espacios',
            'tipoDocumento_id.required' => 'Es obligatorio que registres tu tipo de documento',
            'numeroDocumento.required' => 'Es obligatorio que registres tu número de documento',
            'numeroDocumento.numeric' => 'Tu número de documento debe ser un valor numérico',
            'numeroDocumento.digits_between' => 'Tu número de documento debe tener cómo mínimo 6 caracteres',
            'numeroDocumento.unique' => 'Este número de documento ya ha sido registrado en Crowd',
            'email.required' => 'Es obligatorio que registres una dirección de correo',
            'email.max' => 'La dirección de correo no debe contener más de 50 caracteres',
            'email.unique' => 'Esta dirección de correo ya ha sido tomada',
            'password.required' => 'Es obligatorio que digites una contraseña que cumpla con el formato válido',
            'password.min' => 'La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula. Y al menos, un dígito y un caracter especial',
            'password.confirmed' => 'Las contraseñas no coinciden',
            'terminosCondiciones.required' => 'Es obligatorio que aceptes los términos y condiciones',
        ];

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'rol_id' => 2,
            'tipoDocumento_id' => $data['tipoDocumento_id'],
            'referido_id' => $data['referido_id'],
            'numeroDocumento' => $data['numeroDocumento'],
            'password' => Hash::make($data['password']),
            'terminosCondiciones' => $data['terminosCondiciones'],
            'estado_id' => 1,
        ]);

        //    $user->roles()->attach(Role::where('nombre_rol', 'tester')->first());
        Flash::success('Registro exitoso. Te hemos enviado un correo para que confirmes tu registro.');
        return $user;
    }
}
