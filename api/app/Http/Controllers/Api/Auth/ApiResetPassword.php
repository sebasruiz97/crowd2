<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\URL;

class ApiResetPassword extends ResetPassword
{
    public function toMail($notifiable)
    {
        $resetUrl = $this->resetApiUrl($notifiable);

        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $resetUrl);
        }

        return (new MailMessage)
            ->subject('Recuperar contraseña')
            ->greeting('Hola')
            ->line('Estás recibiendo este correo porque hiciste una solicitud de recuperación de contraseña para tu cuenta.')
            ->action('Recuperar contraseña', $resetUrl)
            ->line(Lang::get('Este enlace estará activo hasta: ' . Carbon::now()->addMinutes(Config::get('auth.verification.expire', 180))))
            ->line('Si no realizaste esta solicitud, no se requiere realizar ninguna otra acción.')
            ->salutation('Saludos, '. config('app.name'));
    }

    public function resetApiUrl($notifiable)
    {   
        $urlApi = base64_encode(URL::temporarySignedRoute(
            'api.resetPassword',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 180)),
            [
                'id' => $notifiable->getKey(),
                'hash' => sha1($notifiable->getEmailForPasswordReset())
            ]
        ));

        $host = config('app.front_url');
        $id = $notifiable->getKey();
        return "$host/password/reset/$id?dataUrl=$urlApi";
    }
}
