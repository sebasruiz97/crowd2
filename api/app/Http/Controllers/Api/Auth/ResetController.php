<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\crypto;

use App\User;

class ResetController extends Controller
{

    public function __construct(){
        $this->middleware('signed')->only('reset');
        $this->middleware('throttle:50,1')->only('forgot','reset');
    }

    public function forgot(Request $request)
    {
        try {
            $correo = $request->input('email');
            $user = User::where('email', $correo)->first();
    
            $token = $user->createToken('Auth-Sanctum')->plainTextToken;
    
            $user->sendPasswordApiResetNotification($token);

            return response()->json(['message' => 'ok'],200);
        } catch (\Throwable $th) {
            return response()->json([ 'error' => $th->getMessage() ]);
        }

    }

    public function reset(Request $request)
    {
        try {
            $data = $request->only('email','password');
            $user = User::where(['email' => $data['email']])->first();
            $validator = $this->validator($request->all());
    
            if (! hash_equals((string) $request->input('id'), (string) $user->getKey())) {
                throw new AuthorizationException;
            }
            
            if (! hash_equals((string) $request->input('hash'), sha1($user->getEmailForPasswordReset()))) {
                throw new AuthorizationException;
            }
    
            if ($validator->fails()) {
                return response()->json([
                    'res' => crypto::arrayEncrypt([
                        'errors' => $validator->messages()
                    ])
                ], 200);
            }
    
            $user->update([ 'password' => Hash::make($data['password']) ]);

            //send token to the register user
            $token = $user->createToken('Auth-Sanctum')->plainTextToken;

            Auth::logoutOtherDevices(\request('password'));

            return response()->json([
                'res' => crypto::arrayEncrypt([
                    'status_code' => 200,
                    'access_token' => $token,
                    'token_type' => 'Bearer',
                    'message' => 'ok',
                    'data' => $user
                ])
            ]);
        } catch (\Throwable $th) {
            return response()->json([ 'error' => $th->getMessage() ]);
        }
        

    }
    
    protected function validator(array $data)
    {

        $expresionRegular = 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}$/';

        $rules = [
            'email' => ['required', 'string', 'email', 'max:50', 'exists:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed', $expresionRegular],
            'g-recaptcha-response' => 'recaptcha',
        ];

        $messages = [
            'password.required' => 'Es obligatorio que digites una contraseña que contenga entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula. Y al menos, un dígito y un caracter especial',
            'password.min' => 'La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula. Y al menos, un dígito y un caracter especial',
            'password.confirmed' => 'Las contraseñas no coinciden y debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula. Y al menos, un dígito y un caracter especial',
            'email.required' => 'Es obligatorio que registres una dirección de correo',
            'email.max' => 'La dirección de correo no debe contener más de 50 caracteres',
            'email.exists' => 'No encontramos este correo entre nuestros crowdys',
            'password.required' => 'Es obligatorio que digites una contraseña que cumpla con el formato válido',
            'password.min' => 'La contraseña debe contener entre 8 y 15 caracteres, al menos una letra mayúscula y una minúscula. Y al menos, un dígito y un caracter especial',
            'password.confirmed' => 'Las contraseñas no coinciden',
            'g-recaptcha-response.required' => 'Es necesario que demuestres que no eres un robot'
        ];

        return Validator::make($data, $rules, $messages);
    }

}
