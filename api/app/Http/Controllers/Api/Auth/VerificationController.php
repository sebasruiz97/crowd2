<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\Mantis\UserMantis;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use App\Services\crypto;
use App\User;

class VerificationController extends Controller
{
    //
    use VerifiesEmails;

    public function __construct()
    {
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:50,1')->only('verify','resend','sendToVerify');
    }

    /**
     * Resend the email verification notification.
     * 
     *  @param \Illuminate\Http\Request $request
     *  @return \Illuminate\Http\Response
     */
    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json([
                'res' => crypto::arrayEncrypt([
                    'message' => 'Already verified'
                ])
            ], 200);
        }

        $request->user()->sendEmailApiVerificationNotification();

        if ($request->wantsJson()) {
            return response()->json([
                'res' => crypto::arrayEncrypt([
                    'message' => 'Email Sent'
                ])
            ], 200);
        }

        return back()->with('resent', true);
    }

    /**
     *  Mark the authenticated user's email address as verified.
     * 
     *  @param \Illuminate\Http\Request $request
     *  @return \Illuminate\Http\Response
     *  @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function verify(Request $request)
    {
        
        if (! hash_equals((string) $request->route('id'), (string) $request->user()->getKey())) {
            throw new AuthorizationException;
        }
        
        if (! hash_equals((string) $request->route('hash'), sha1($request->user()->getEmailForVerification()))) {
            throw new AuthorizationException;
        }
        
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json([
                'res' => crypto::arrayEncrypt([
                    'message' => 'Already verified'
                ])
            ], 200);
        }
        
        if ($request->user()->markEmailAsVerified()){
            event(new Verified($request->user()));
        }
        
        if ($response = $this->verified($request)) {
            return response()->json([
                'res' => crypto::arrayEncrypt([
                    'message' => $response
                ])
            ], 200);
        }
        
        User::where('id',$request->route('id'))->update(['estado_id' => 2]);
        
        // Create user in Mantis
        $user = User::find($request->route('id'));
        $userMantis = UserMantis::where('email', $user->email)->first();

        if($userMantis){
            \DB::connection('mysql_mantis')
                ->table('mantis_user')
                ->where('email', $userMantis->email)
                ->update([
                    'password' => $user->password
                ]);
        } else {
            UserMantis::insert([
                'username' => $user->numeroDocumento,
                'realname' => $user->name." ".$user->lastname,
                'email' => $user->email,
                'password' => $user->password,
                'enabled' => 1,
                'access_level' => 25,
                'cookie_string' => $user->numeroDocumento
            ]);
        }

        return response()->json([
            'res' => crypto::arrayEncrypt([
                'message' => 'Successfully verified'
            ])
        ], 200);
    }

}
