<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\URL;

class ApiVerifyEmail extends VerifyEmail
{
    public function toMail($notifiable)
    {
        $verificationUrl = $this->verificationApiUrl($notifiable);

        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $verificationUrl);
        }

        return (new MailMessage)
            ->subject(Lang::get('Verificar correo electrónico'))
            ->greeting('Hola')
            ->line(Lang::get('Por favor, da click abajo para verificar tu dirección de correo'))
            ->action(Lang::get('Verificar correo electrónico'), $verificationUrl)
            ->line(Lang::get('Si no has creado una cuenta, ninguna acción es requerida'))
            ->line(Lang::get('Este enlace estará activo hasta: ' . Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60))))
            ->salutation('Saludos, '. config('app.name'));
    }

    public function verificationApiUrl($notifiable)
    {
        $urlApi = base64_encode(URL::temporarySignedRoute(
            'api.verifyEmail',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
            [
                'id' => $notifiable->getKey(),
                'hash' => sha1($notifiable->getEmailForVerification()),
            ]
        ));
        $host = config('app.front_url');
        $id = $notifiable->getKey();
        return "$host/email/verify/$id?dataUrl=$urlApi";
    }
}
