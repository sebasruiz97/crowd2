<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Services\crypto;

class AuthController extends Controller
{

    //
    public function login(Request $request)
    {
        $request->validate([
            'numeroDocumento' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:4'],
        ]);

        try {
            $user = User::where('numeroDocumento', $request->numeroDocumento)->first();
            if (!$user || !Hash::check($request->password, $user->password)) {
                return response()->json([
                    'res' => crypto::arrayEncrypt([
                        'error' => 'Las credenciales proporcionadas son incorrectas.'
                    ])
                ], 200);
            }
            $user->tokens()->delete();

            if ($user->estado_id == 4) {
                return response()->json([
                    'res' => crypto::arrayEncrypt([
                        'error' => 'Tu cuenta está inactiva, debes enviar un correo a info@crowdsqa.co para activarte de nuevo.'
                    ])
                ], 200);
            }

            //send token to the register user
            $token = $user->createToken('Auth-Sanctum')->plainTextToken;

            Auth::logoutOtherDevices(\request('password'));

            return response()->json([
                'res' => crypto::arrayEncrypt([
                    'status_code' => 200,
                    'access_token' => $token,
                    'token_type' => 'Bearer',
                    'data' => $user
                ])
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'res' => crypto::error($e->getMessage())
            ], 200);
        }
    }

    public function logout(Request $request)
    {
        try {
            $user = User::find($request->user()->id);
            $user->tokens()->delete();
            return response()->json([
                'res' => crypto::arrayEncrypt(['logout' => true])
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'res' => crypto::error($th->getMessage())
            ], 400);
        }
    }

    public function getUserInfo(Request $request)
    {
        return response()->json([
            'res' => crypto::arrayEncrypt($request->user())
        ]);
    }
}
