<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;
use App\Http\Requests\GuardarClienteRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Sector;
use App\Models\TipoPruebas;
use App\Models\ModeloServicio;
use App\Models\TipoMetodologia;
use App\Models\Pais;
Use App\Models\Departamento;
Use App\Models\Municipio;
use Flash;
use Response;
use Carbon\Carbon;


class ClienteController extends Controller
{
    public function __construct()
    {        
        $this->middleware([
            'auth','roles:1,3']);    
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $buscar = $request->get('buscarpor');

        if($buscar === null){

            $clientes = Cliente::paginate(20);

            foreach($clientes as $cliente){
                    $sector = Sector::find($cliente->sector_id);
                    $tipoModelo = ModeloServicio::find($cliente->modeloServicio_id);
                    $modeloServicio = ModeloServicio::find($cliente->modeloServicio_id);
                    $pais = Pais::find($cliente->pais_id);
                    $departamento = Departamento::find($cliente->departamento_id);
                    $municipio = Municipio::find($cliente->municipio_id);
                    $cliente["sector"] = $sector;
                    $cliente["tipoModelo"] = $tipoModelo;
                    $cliente["pais"] = $pais;
                    $cliente["departamento"] = $departamento;
                    $cliente["municipio"] = $municipio;
            }

        }else{

            $clientes = Cliente::where('nombre_cliente','like',"%$buscar%")->paginate(5);

            foreach($clientes as $cliente){
                $sector = Sector::find($cliente->sector_id);
                $tipoModelo = ModeloServicio::find($cliente->modeloServicio_id);
                $modeloServicio = ModeloServicio::find($cliente->modeloServicio_id);
                $pais = Pais::find($cliente->pais_id);
                $departamento = Departamento::find($cliente->departamento_id);
                $municipio = Municipio::find($cliente->municipio_id);
                $cliente["sector"] = $sector;
                $cliente["tipoModelo"] = $tipoModelo;
                $cliente["pais"] = $pais;
                $cliente["departamento"] = $departamento;
                $cliente["municipio"] = $municipio;
            }
        }

        return view('clientes.index', compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GuardarClienteRequest $request)
    {
       
        if(auth()->user()->hasRoles(['1'])){

            $repetido =  Cliente::query()->where([
            ['nit_cliente', '=',  $request->input('nit_cliente')],
            ])->get();

            if($repetido->isEmpty()){
                $data = $request->validated();

                $files = $request->file('url_anexoCliente');
                $nitCliente = $request->input('nit_cliente');

                $data = array();
                    $data['nit_cliente'] = $request->input('nit_cliente');
                    $data['nombre_cliente'] = $request->input('nombre_cliente');
                    $data['descripcion'] =$request->input('descripcion');
                    $data['sector_id'] = $request->input('sector_id');
                    $data['pais_id'] = $request->input('pais_id');
                    $data['departamento_id'] = $request->input('departamento_id');
                    $data['municipio_id'] = $request->input('municipio_id');
                    $data['direccion_cliente'] = $request->input('direccion_cliente');
                    $data['modeloServicio_id'] =$request->input('modeloServicio_id');
                    $data['nombre_contactoPruebas'] = $request->input('nombre_contactoPruebas');
                    $data['telefono_contactoPruebas'] = $request->input('telefono_contactoPruebas');
                    $data['correo_contactoPruebas'] = $request->input('correo_contactoPruebas');
                    $data['nombre_contactoFacturacion'] = $request->input('nombre_contactoFacturacion');
                    $data['correo_contactoFacturacion'] = $request->input('correo_contactoFacturacion');
                    $data['telefono_contactoFacturacion'] = $request->input('telefono_contactoFacturacion');
                    $data['correo_envioFactura'] = $request->input('correo_envioFactura');
                    $data['fecha_recibidoFactura'] = $request->input('fecha_recibidoFactura');
                    $data['plazo_pagoFactura'] = $request->input('plazo_pagoFactura');
                    $data['acuerdos_adicionales'] = $request->input('acuerdos_adicionales');
                    $data['created_at'] = Carbon::now();
                    $data['updated_at'] = Carbon::now();
                \DB::table('clientes')->insert($data);    
                
            
                $clientesID = \DB::table('clientes')->where('nit_cliente','=',$nitCliente)->get('id_cliente');

                foreach($clientesID as $clienteID){
                    $id = $clienteID->id_cliente;
                }

                If($files == null){

                    Flash::success('Cliente agregado correctamente');
                    return redirect(route('clientes.index'));
                    
                }
                foreach($files as $file){
                    
                    $nombre      =   time().'.'.$file->getClientOriginalName();
                    $target_path    =   storage_path('clientes'.'/cliente_'.$id.'/anexos');

                    if($file->move($target_path, $nombre)) {
                        $data2 = array();
                        $data2['url_anexoCliente'] = $nombre;
                        $data2['user_id'] = auth()->user()->id;
                        $data2['cliente_id'] = $id;
                        $data2['created_at'] = Carbon::now();
                        \DB::table('clientes_anexos')->insert($data2);   
                    }
                }

                Flash::success('Cliente agregado correctamente');
                return redirect(route('clientes.index'));
                
            }else{
                Flash::error('Ya existe un cliente registrado con este NIT');
                return redirect(route('clientes.index'));
            }

        }

        Flash::error('No esta autorizado para crear clientes');
                return redirect(route('clientes.index'));

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show($id_cliente)
    {
        
        $anexosCliente = \DB::table('clientes_anexos')->where("cliente_id","=",$id_cliente)->get();

        $clientes = Cliente::where("id_cliente","=",$id_cliente)->get();
        foreach($clientes as $cliente){
            $sector = Sector::find($cliente->sector_id);
            $tipoModelo = ModeloServicio::find($cliente->modeloServicio_id);
            $modeloServicio = ModeloServicio::find($cliente->modeloServicio_id);
            $pais = Pais::find($cliente->pais_id);
            $departamento = Departamento::find($cliente->departamento_id);
            $municipio = Municipio::find($cliente->municipio_id);
            $cliente["sector"] = $sector;
            $cliente["tipoModelo"] = $tipoModelo;
            $cliente["pais"] = $pais;
            $cliente["departamento"] = $departamento;
            $cliente["municipio"] = $municipio;
        }

            if (empty($clientes)) {
                Flash::error('Cliente no encontrado');
                return redirect(route('clientes.index'));
            }

            return view('clientes.show', compact('clientes','anexosCliente'));

            
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit($id_cliente)
    {

        if(auth()->user()->hasRoles(['1'])){

           
        $clientes = Cliente::where("id_cliente","=",$id_cliente)->get();
        $sectores = Sector::pluck('nombre_sector','id_sector');
        $modeloServicio = ModeloServicio::pluck('nombre_modeloServicio','id_modeloServicio');
        $paises = Pais::pluck('nombre_pais','id_pais');
        $departamentos = Departamento::pluck('nombre_departamento','id_departamento');
        $municipios = Municipio::pluck('nombre_municipio','id_municipio');

        //para dejar seleccionado
        $selSector = Cliente::where("id_cliente","=",$id_cliente)->select("sector_id")->first();
        $sectorSeleccionado = $selSector->sector_id;
        $selModelo = Cliente::where("id_cliente","=",$id_cliente)->select("modeloServicio_id")->first();
        $modeloSeleccionado = $selModelo->modeloServicio_id;
        $selPais = Cliente::where("id_cliente","=",$id_cliente)->select("pais_id")->first();
        $paisSeleccionado = $selPais->pais_id;
        $selDepartamento = Cliente::where("id_cliente","=",$id_cliente)->select("departamento_id")->first();
        $departamentoSeleccionado = $selDepartamento->departamento_id;
        $selMunicipio = Cliente::where("id_cliente","=",$id_cliente)->select("municipio_id")->first();
        $municipioSeleccionado = $selMunicipio->municipio_id;

        if (empty($clientes)) {
            Flash::error('Cliente no encontrado');
            return redirect()->route('clientes.index');
        }
        return view('clientes.edit', compact('clientes','sectores','modeloServicio','paises','tipoPruebaSeleccionado','sectorSeleccionado','metodologiaSeleccionada','modeloSeleccionado','paisSeleccionado','departamentoSeleccionado','municipioSeleccionado'));

            
        }

        Flash::error('No esta autorizado para editar clientes');
        return redirect(route('clientes.index'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update($id_cliente, GuardarClienteRequest $request)
    {

        if(auth()->user()->hasRoles(['1'])){

            $files = $request->file('url_anexoCliente');
            $nitCliente = $request->input('nit_cliente');

            $clientes = Cliente::find($id_cliente);
            if (empty($clientes)) {
                Flash::error('Cliente no encontrado');
                return redirect()->route('clientes.index');
            }

            $data = array();
            $data['nit_cliente'] = $request->input('nit_cliente');
            $data['nombre_cliente'] = $request->input('nombre_cliente');
            $data['descripcion'] =$request->input('descripcion');
            $data['sector_id'] = $request->input('sector_id');
            $data['pais_id'] = $request->input('pais_id');
            $data['departamento_id'] = $request->input('departamento_id');
            $data['municipio_id'] = $request->input('municipio_id');
            $data['direccion_cliente'] = $request->input('direccion_cliente');
            $data['modeloServicio_id'] =$request->input('modeloServicio_id');
            $data['nombre_contactoPruebas'] = $request->input('nombre_contactoPruebas');
            $data['telefono_contactoPruebas'] = $request->input('telefono_contactoPruebas');
            $data['correo_contactoPruebas'] = $request->input('correo_contactoPruebas');
            $data['nombre_contactoFacturacion'] = $request->input('nombre_contactoFacturacion');
            $data['correo_contactoFacturacion'] = $request->input('correo_contactoFacturacion');
            $data['telefono_contactoFacturacion'] = $request->input('telefono_contactoFacturacion');
            $data['correo_envioFactura'] = $request->input('correo_envioFactura');
            $data['fecha_recibidoFactura'] = $request->input('fecha_recibidoFactura');
            $data['plazo_pagoFactura'] = $request->input('plazo_pagoFactura');
            $data['acuerdos_adicionales'] = $request->input('acuerdos_adicionales');
            $data['created_at'] = Carbon::now();
            $data['updated_at'] = Carbon::now();
            \DB::table('clientes')->where('id_cliente','=',$id_cliente)->update($data);    
        
        
            $clientesID = \DB::table('clientes')->where('nit_cliente','=',$nitCliente)->get('id_cliente');

            foreach($clientesID as $clienteID){
                $id = $clienteID->id_cliente;
            }

            If($files == null){
                
                Flash::success('Cliente actualizado correctamente.');
                return redirect(route('clientes.index'));

            }
            foreach($files as $file){
                
                $nombre      =   time().'.'.$file->getClientOriginalName();
                $target_path    =   storage_path('clientes'.'/cliente_'.$id.'/anexos');

                if($file->move($target_path, $nombre)) {
                    $data2 = array();
                    $data2['url_anexoCliente'] = $nombre;
                    $data2['user_id'] = auth()->user()->id;
                    $data2['cliente_id'] = $id;
                    $data2['created_at'] = Carbon::now();
                    \DB::table('clientes_anexos')->insert($data2);   
                }
            }

            Flash::success('Cliente actualizado correctamente.');
            return redirect(route('clientes.index'));

        

        return redirect()->route('clientes.index');
                

        }
        
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_cliente)
    {
        if(auth()->user()->hasRoles(['1'])){

            $proyectosCliente = \DB::table('proyectos')->where([
                ['cliente_id', '=',$id_cliente],
                ['deleted_at', '=',null],
                ])->get();

            if($proyectosCliente->isEmpty()){

                $clientes = Cliente::find($id_cliente);
                if (empty($clientes)) {
                    Flash::error('Cliente no encontrado');
                    return redirect(route('clientes.index'));
                }
                $clientes->delete($id_cliente);
                Flash::error('Cliente eliminado correctamente.');
                return redirect(route('clientes.index'));

            }

            Flash::error('No puede eliminar un cliente para el cual ya se han creado proyectos');
            return redirect(route('clientes.index'));

           
        }
            Flash::error('No esta autorizado para eliminar clientes');
            return redirect(route('clientes.index'));
    }

    public function destroyAnexo($id){

        $rol_id = auth()->user()->rol_id;
        $anexos = \DB::table('clientes_anexos')->where('id_anexoCliente', '=',$id)->get();
        $id_user = auth()->user()->id;
        

        foreach($anexos as $anexo){
             $usuario = $anexo->user_id;
             $url = $anexo->url_anexoCliente;
             $idCliente = $anexo->cliente_id;             
        }

        if($id_user == $usuario ||$rol_id ==3 ||$rol_id ==1 ){

            $anexos = \DB::table('clientes_anexos')->where('id_anexoCliente', '=',$id)->delete();
            $enlace = storage_path('clientes'.'/cliente_'.$idCliente.'/anexos'.'/'.$url);
            unlink($enlace);
            return redirect()->back();
            
  
       } else {
            return ('Sólo el propietario puede descargar el archivo');
        }

    }

    public function visualizar($id){

        $rol_id = auth()->user()->rol_id;
        $anexos = \DB::table('clientes_anexos')->where('id_anexoCliente', '=',$id)->get();
        $id = auth()->user()->id;

        foreach($anexos as $anexo){
             $usuario = $anexo->user_id;
             $url = $anexo->url_anexoCliente;
             $idCliente = $anexo->cliente_id;             
        }

        if($id == $usuario ||$rol_id ==3 ||$rol_id ==1 ){

            $enlace = storage_path('clientes'.'/cliente_'.$idCliente.'/anexos'.'/');
            return response()->download($enlace.$url);
  
       } else {
            return ('Sólo el propietario puede descargar el archivo');
        }

    }


    public function buscarCiudad(Request $request)
{
        $data = Ciudad::select('nombre_ciudad','id_ciudad')->where('pais_id',$request->id_pais)->get();

        return response()->json($data);//then sent this data to ajax success*/
}

}
