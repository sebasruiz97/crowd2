<?php
 
namespace App\Http\Controllers;
 
use App\Proyecto;
use Illuminate\Http\Request;
use Redirect,Response;
use Carbon\Carbon;
use Flash;
use App\Mail\InvitacionTester;
use Illuminate\Support\Facades\Mail;
 
class proyectos_testerController extends Controller
{

    public function __construct()
    {        
        $this->middleware([
            'auth','roles:1,3']);    
    }
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
public function index(Request $request)
{
    $id = $request->id;
    $id_proyecto = ($request->idp);

   // dd($request);
    //Print the values
   
    if(request()->ajax()) {
  
        $query = \DB::table('vista_buscar_tester');
        //return datatables()->of(proyecto::select('*'))
        return datatables()->of( $query)
        ->addColumn('action', 'proyectos_tester.action_button')
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);
    }
    return view('proyectos_tester.index', compact('id_proyecto'));
}


/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store($data_sel)
{  


    $data ="Hola" + $data_sel;
    return Response::json($data);
}

 
/**
 * Show the form for editing the specified resource.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{   
    $where = array('user_id' => $id);
    $proyecto = \DB::table('vista_buscar_tester')->where('user_id', '=',  $id)->get();

    return Response::json($proyecto);
}
 
 
/**
 * Remove the specified resource from storage.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
{
   // $proyecto = Proyecto::where('user_id',$id)->delete();
    $proyecto =\DB::table('proyectos_responsables')->where('id_proyecto_responsable', '=', $id)->delete();
 
    return Response::json($proyecto);
}


public function ver(Request $request)
{
    $input = $request->all();
    $id = $request->id;
      //  \Log::info($input);
    dd($id);

    // dd($values);
     //Print the values
    
     if(request()->ajax()) {
    
        $query = \DB::table('vista_buscar_tester')->where('user_id', '=', $id)->get();
  //   $query = \DB::table('vista_buscar_tester');
         //return datatables()->of(proyecto::select('*'))
         return datatables()->of( $query)
         ->addColumn('action', 'proyectos_tester.action_button')
         ->rawColumns(['action'])
         ->addIndexColumn()
         ->make(true);
    
     }
     return view('proyectos_tester.index', compact('id'));
 
}


    public function asignar(Request $request)
    {
        $id_proyecto = ($request->idp);
        if ($request->has('data_sel')) 
        {
            $invitacion_realizada_por = (auth()->user()->id);
            $id = $request->input( 'data_sel' );
            $proyecto= $request->idp;
           
            if ($id!=null)
            {

                $array = explode(',', $id);
                //saco el numero de elementos
                $longitud = count($array);
                //$proyecto = 1;
            
                //Recorro todos los elementos
                for($i=0; $i<$longitud; $i++)
                {
                    //saco el valor de cada elemento
                    $id_asignar = ($array[$i]);
                    echo $id_asignar;
                    $data = array();
                    $data['invitacion_realizada_por'] =  $invitacion_realizada_por;
                    $data['invitacion_proyecto_id'] = $proyecto;
                    $data['invitacion_tester_id'] = $id_asignar;
                    $data['invitacion_fecha_envio'] = date('Y-m-d H:i:s');
                    $data['invitacion_estado'] = 'Sin Enviar';
                    $data['invitacion_fecha_aceptacion'] = NULL;
                    $data['invitacion_fecha_rechazo'] =NULL;
                    $data['invitacion_fecha_no_responde'] = NULL;
                    $data['invitacion_num_horas_cierre'] = 24;
                    

                
                    $invitaciones = \DB::table("proyectos_invitaciones")->where([
                        ["invitacion_tester_id","=", $id_asignar],
                        ["invitacion_proyecto_id", "=", $proyecto]
                        ])->first();
                        
                        

                        if(!$invitaciones)
                        {
                            //insertar nuevo
                            \DB::table('proyectos_invitaciones')->insert($data);
                        }
                        if($invitaciones)
                        {
                            echo "actualizar registro existente";
                            
                            // \DB::table("proyectos_invitaciones")->where([
                            //     ["invitacion_tester_id","=", $id_asignar],
                            //     ["invitacion_proyecto_id", "=", $proyecto]
                            //     ])->where("invitacion_estado","=","Sin Enviar")->orWhere("invitacion_estado","=","Sin Respuesta")->update($data);
                        }
    
                }

                //enviar invitaciones
                $invitaciones = \DB::table('vista_lista_invitados')->where([
                    ['invitacion_estado','=','Sin Enviar'],
                    ['invitacion_proyecto_id', '=', $proyecto],
                    ])->get();
                    
        
                foreach($invitaciones as $invitacion)
                {
                    $emailTester = $invitacion->email;
                    $name = $invitacion->nombre_tester;
                    $proyectoNombre = $invitacion->proyecto_nombre;
                    $proyectoDescripcion = $invitacion->proyecto_descripcion;
                    $proyectoInicio = $invitacion->proyecto_fecha_inicio;
        
                    Mail::to($emailTester)
                        ->queue(new InvitacionTester($name,$proyectoNombre,$proyectoDescripcion,$proyectoInicio));
                }

                
                    Flash::success('Se han enviado las invitaciones a los testers seleccionados');
                     \DB::table("proyectos_invitaciones")->where("invitacion_estado","=","Sin Enviar")->update(['invitacion_estado' =>"Enviada"]);


            }
            else{
                $id="";

            }

        }
        else
        {
            $id="";

        }
        return  $id;

    }


}