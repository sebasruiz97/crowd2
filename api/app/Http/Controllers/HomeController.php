<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DatosBasicos;
use App\User;
use App\PuntosTester;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Arr;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index()
    {

        // Perfil incompleto = Redireccionar a datosBasicos
        if(count(getArrayObligatorios()))
        {
            // return redirect(route('datosBasicos.index'));
        }
        // Esta parte es para cambiar el estado cuando confirma correo

        $usuarios = User::where("id", "=", auth()->user()->id)->get();
        foreach ($usuarios as $usuario) {
            $emailVerificado = $usuario->email_verified_at;
        }

        if ($emailVerificado != '') {

            foreach ($usuarios as $usuario) {
                $data = array();
                $data['estado_id'] = 2;
                $data['updated_at'] = Carbon::now();
                \DB::table('users')->where("id", "=", auth()->user()->id)->update($data);
            }
            // return redirect(route('datosBasicos.index'));
            return view('home.index');
        }

        return view('email/verify');
    }

    public function homeView()
    {
        return view('home');
    }

    public function return()
    {
        return Redirect::back();
    }
}
