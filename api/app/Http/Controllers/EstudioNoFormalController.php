<?php

namespace App\Http\Controllers;

use App\EstudioNoFormal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\GuardarEstudiosNoFormalesRequest;
use App\Http\Controllers\AppBaseController;
use App\User;
use App\DatosBasicos;
use App\PuntosTester;
use\App\Models\TipoEstudio;
use\App\Models\Universidad;
use\App\Models\Plataforma;
use Flash;
use Response;
use Carbon\Carbon;


class EstudioNoFormalController extends Controller
{
    public function __construct()
    {        
        $this->middleware([
            'auth','roles:1,2,3']);    
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = auth()->user()->id;
        $estudiosNoFormales = EstudioNoFormal::where("user_id","=",$user)->paginate(6);;

        foreach($estudiosNoFormales as $estudioNoFormal){
                $tipoEstudio = TipoEstudio::find($estudioNoFormal->tipoEstudio_id);
                $estudioNoFormal["tipoEstudio"] = $tipoEstudio;
                $universidad = Universidad::find($estudioNoFormal->universidad_id);
                $estudioNoFormal["universidad"] = $universidad;
                $plataforma = Plataforma::find($estudioNoFormal->plataforma_id);
                $estudioNoFormal["plataforma"] = $plataforma;
                
        }
        return view('estudios_no_formales.index', compact('estudiosNoFormales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estudios_no_formales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GuardarEstudiosNoFormalesRequest $request, User $user)
    {

        $repetido =  EstudioNoFormal::query()->where([
            ['user_id', '=',  $request->input('user_id')],
            ['tipoEstudio_id', '=', $request->input('tipoEstudio_id')],
            ['nombre_estudioNoFormal', '=', $request->input('nombre_estudioNoFormal')],
            ['fechaFin_estudioNoFormal', '=', $request->input('fechaFin_estudioNoFormal')],
            
        ])->get();

        if($repetido->isEmpty()){
            $id = 'Agregar estudios no formales';
            if($request->input('lugar_estudio')=="En una universidad o instituto"){
                $data = $request->validated();
                $data = array();
                $data['user_id'] = $request->input('user_id');
                $data['tipoEstudio_id'] = $request->input('tipoEstudio_id');
                $data['nombre_estudioNoFormal'] = $request->input('nombre_estudioNoFormal');
                $data['fechaFin_estudioNoFormal'] = $request->input('fechaFin_estudioNoFormal');
                $data['duracion'] = $request->input('duracion');
                $data['lugar_estudio'] = $request->input('lugar_estudio');
                $data['universidad_id'] = $request->input('universidad_id');
                if($request->input('universidad_id')==361){
                $data['otra_universidad'] = $request->input('otra_universidad');
                }else{
                $data['otra_universidad'] = null;
                }
                \DB::table('estudios_no_formales')->insert($data);
                return redirect(route('asignarPuntos',$id));
                }else{
                $data = $request->validated();
                $data = array();
                $data['user_id'] = $request->input('user_id');
                $data['tipoEstudio_id'] = $request->input('tipoEstudio_id');
                $data['nombre_estudioNoFormal'] = $request->input('nombre_estudioNoFormal');
                $data['fechaFin_estudioNoFormal'] = $request->input('fechaFin_estudioNoFormal');
                $data['duracion'] = $request->input('duracion');
                $data['lugar_estudio'] = $request->input('lugar_estudio');
                $data['plataforma_id'] = $request->input('plataforma_id');
                if($request->input('plataforma_id')==6){
                $data['otra_universidad'] = $request->input('otra_universidad');
                }else{
                $data['otra_universidad'] = null;
                }
                \DB::table('estudios_no_formales')->insert($data);
                return redirect(route('asignarPuntos',$id));
                }
        }else{
            Flash::error('Ya tienes registrado un estudio formal, con este tipo, nombre y fecha de finalización');
            return redirect(route('estudiosNoFormales.index'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EstudioNoFormal  $estudioNoFormal
     * @return \Illuminate\Http\Response
     */
    public function show($id_estudioNoFormal)
    {
        $estudiosNoFormales = EstudioNoFormal::where("id_estudioNoFormal","=",$id_estudioNoFormal)->get();
        foreach($estudiosNoFormales as $estudioNoFormal){
            $tipoEstudio = TipoEstudio::find($estudioNoFormal->tipoEstudio_id);
            $estudioNoFormal["tipoEstudio"] = $tipoEstudio;
    }

    if (empty($estudiosNoFormales)) {
        Flash::error('Estudio  no formal no encontrado');
        return redirect(route('estudiosNoFormales.index'));
    }

    return view('estudios_no_formales.show', compact('estudiosNoFormales'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EstudioNoFormal  $estudioNoFormal
     * @return \Illuminate\Http\Response
     */
    public function edit($id_estudioNoFormal)
    {
        $estudiosNoFormales = EstudioNoFormal::where("id_estudioNoFormal","=",$id_estudioNoFormal)->get();
		$tipoEstudio = TipoEstudio::orderBy('nombre_tipoEstudio')->pluck('nombre_tipoEstudio','id_tipoEstudio');
		$universidad = Universidad::orderBy('nombre_universidad')->pluck('nombre_universidad','id_universidad');
		$plataforma = Plataforma::orderBy('nombre_plataforma')->pluck('nombre_plataforma','id_plataforma');
    
        //para dejar seleccionado
        $selTipoEstudio = EstudioNoFormal::where("id_estudioNoFormal","=",$id_estudioNoFormal)->select("tipoEstudio_id")->first();
        $tipoEstudioSeleccionado = $selTipoEstudio->tipoEstudio_id;
        $selUniversidad = EstudioNoFormal::where("id_estudioNoFormal","=",$id_estudioNoFormal)->select("universidad_id")->first();
        $universidadSeleccionada = $selUniversidad->universidad_id;
        $selPlataforma = EstudioNoFormal::where("id_estudioNoFormal","=",$id_estudioNoFormal)->select("plataforma_id")->first();
        $plataformaSeleccionada = $selPlataforma->plataforma_id;
        $selLugar = EstudioNoFormal::where("id_estudioNoFormal","=",$id_estudioNoFormal)->select("lugar_estudio")->first();
        $lugarSeleccionado = $selLugar->lugar_estudio;
        

        if (empty($estudiosNoFormales)) {
            Flash::error('Estudio no formal no encontrado');
            return redirect(route('estudiosNoFormales.index'));
        }

        return view('estudios_no_formales.edit',compact('estudiosNoFormales','universidad','plataforma','universidadSeleccionada','plataformaSeleccionada','tipoEstudio','tipoEstudioSeleccionado','lugarSeleccionado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EstudioNoFormal  $estudioNoFormal
     * @return \Illuminate\Http\Response
     */
    public function update($id_estudioNoFormal, GuardarEstudiosNoFormalesRequest $request)
    {

        if($request->input('lugar_estudio')=="En una universidad o instituto"){
            $data = $request->validated();
            $data = array();
            $data['user_id'] = $request->input('user_id');
            $data['tipoEstudio_id'] = $request->input('tipoEstudio_id');
            $data['nombre_estudioNoFormal'] = $request->input('nombre_estudioNoFormal');
            $data['fechaFin_estudioNoFormal'] = $request->input('fechaFin_estudioNoFormal');
            $data['duracion'] = $request->input('duracion');
            $data['lugar_estudio'] = $request->input('lugar_estudio');
            $data['universidad_id'] = $request->input('universidad_id');
            $data['plataforma_id'] = null;
            \DB::table('estudios_no_formales')->where('id_estudioNoFormal','=',$id_estudioNoFormal)->update($data);
            Flash::success('Estudio no formal actualizado correctamente');
            return redirect(route('estudiosNoFormales.index'));
            }else{
            $data = $request->validated();
            $data = array();
            $data['user_id'] = $request->input('user_id');
            $data['tipoEstudio_id'] = $request->input('tipoEstudio_id');
            $data['nombre_estudioNoFormal'] = $request->input('nombre_estudioNoFormal');
            $data['fechaFin_estudioNoFormal'] = $request->input('fechaFin_estudioNoFormal');
            $data['duracion'] = $request->input('duracion');
            $data['lugar_estudio'] = $request->input('lugar_estudio');
            $data['plataforma_id'] = $request->input('plataforma_id');
            $data['universidad_id'] = null;
            \DB::table('estudios_no_formales')->where('id_estudioNoFormal','=',$id_estudioNoFormal)->update($data);
            Flash::success('Estudio no formal actualizado correctamente');
            return redirect(route('estudiosNoFormales.index'));
            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstudioNoFormal  $estudioNoFormal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_estudioNoFormal)
    {
        $id = 'Eliminar estudios no formales';
        $estudiosNoFormales = EstudioNoFormal::find($id_estudioNoFormal);
        if (empty($estudiosNoFormales)) {
            Flash::error('Estudio no Formal no encontrado');
            return redirect(route('estudiosNoFormales.index'));
        }
        $estudiosNoFormales->delete($id_estudioNoFormal);
        return redirect(route('quitarPuntos',$id));
    }
}
