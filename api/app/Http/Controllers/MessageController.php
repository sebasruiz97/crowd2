<?php

namespace App\Http\Controllers;

use App\Mail\MessageReceived;
use Illuminate\Support\Facades\Mail;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    public function store(Request $request)
    {
      $this->validator($request->all())->validate();

      $msg = request()->validate([
        'nombre'=>'required|max:50|regex:/^[\pL\s\-]+$/u',
        'email'=>'required|email',
        'asunto'=>'required|max:100',
        'mensaje'=>'required|min:4'
      ], [
            'nombre.required' => __('Necesitamos tu nombre'),
            'nombre.max' => __('Tu nombre debe contener máximo 100 caracteres'),
            'nombre.regex' => __('Tu nombre sólo puede contener letras y espacios'),
            'email.required' => __('Necesitamos tu dirección de correo'),
            'email.email' => __('Ingresa una dirección de correo válida'),
            'mensaje.required' => __('Especifica un mensaje'),
            'mensaje.min' => __('El mensaje debe contener mínimo 4 carácteres'),
            'asunto.required' => __('El asunto es requerido'),
            'asunto.max' => __('El asunto no debe contener máximo 100 carácteres'),
      ]);

      Mail::to( env('TEST_EMAIL','info@crowdsqa.co') )->queue(new MessageReceived($msg));
      Flash::success('Hemos recibido tu mensaje, te responderemos en menos de 24 horas');
      return redirect(route('mensaje'));
      //return new MessageReceived($msg);
    }

    protected function validator(array $data)
    {
      return Validator::make($data, [
        'g-recaptcha-response' => 'recaptcha'
      ]);
    }
}