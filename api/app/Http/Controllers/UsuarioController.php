<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GuardarUsuarioRequest;
use App\Http\Requests\EditarUsuarioRequest;
use App\Http\Requests\GuardarClaveUsuarioRequest;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\DatosBasicos;
use App\Models\VistaEstudiosFormales;
use App\Models\VistaEstudiosNoFormales;
use App\Models\VistaDatosLaborales;
use App\Models\VistaDatosFinancieros;
use App\Models\VistaInfoPruebas;
use App\Models\VistaInfoDispositivos;
use App\Models\Anexo;
use Response;
use Flash;
use Carbon\Carbon;


class UsuarioController extends Controller
{

    public function __construct()
    {
        $this->middleware([
            'auth', 'roles:1,4'
        ]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activado = auth()->user()->estado_id;
        $rol = auth()->user()->rol_id;
        $user = auth()->user()->id;

        $usuarios = \DB::table('users')->select('users.id', 'descripcion', 'email', 'users.name', 'experienciaPruebas', 'tipo_documentos.nombre_tipoDocumento', 'users.numeroDocumento', 'nombre_departamento', 'nombre_municipio', 'telefonoCelular', 'nombre_ocupacion', 'users.puntosAcumulados', 'nombre_estado')
            ->leftJoin('vista_datos_basicos', 'users.id', '=', 'vista_datos_basicos.user_id')
            ->leftJoin('tipo_documentos', 'users.tipoDocumento_id', '=', 'tipo_documentos.id_tipoDocumento')
            ->leftJoin('roles', 'users.rol_id', '=', 'roles.id_rol')
            ->leftJoin('estados', 'users.estado_id', '=', 'estados.id_estado')
            ->where('users.deleted_at', '=', null)
            ->get();

        if (request()->ajax()) {
            return datatables()->of($usuarios)
                ->addColumn('action', 'usuarios.action_button')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('usuarios.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    **/
    public function store(GuardarUsuarioRequest $request)
    {

        if (auth()->user()->hasRoles(['1'])) {

            $documento = $request->input('numeroDocumento');
            $email = $request->input('email');
            $user_id = auth()->user()->id;
            $rol =  auth()->user()->rol_id;

            $existeDocumento = \DB::table('users')->where('numeroDocumento', '=', $documento)->get();
            $existeEmail = \DB::table('users')->where('email', '=', $email)->get();
            if ($existeDocumento->isEmpty()) {

                if ($existeEmail->isEmpty()) {

                    $request->validated();

                    $data = array();
                    $data['tipoDocumento_id'] = $request->input('tipoDocumento_id');
                    $data['rol_id'] = $request->input('rol_id');
                    $data['numeroDocumento'] = $request->input('numeroDocumento');
                    $data['name'] = $request->input('name');
                    $data['email'] = $request->input('email');
                    $data['email_verified_at'] =  Carbon::now();
                    $data['password'] =  Hash::make($request->input('password'));
                    $data['terminosCondiciones'] = 'on';
                    $data['estado_id'] = 3;
                    $data['created_at'] = Carbon::now();
                    $insert = \DB::table('users')->insert($data);

                    Flash::success('Usuario creado correctamente');
                    return Response::json($data);
                }

                Flash::error('Ya existe un usuario registrado con este email');
                return Response::json();
            }

            Flash::error('Ya existe un usuario registrado con este número de documento');
            return Response::json();
        }
    }

    public function update(EditarUsuarioRequest $request)
    {

        $user_id = auth()->user()->id;
        $rol =  auth()->user()->rol_id;
        $id_usuario = $request->input('id_usuario');

        if (auth()->user()->hasRoles(['1'])) {

            $documento = $request->input('numeroDocumento_usuario');
            $email = $request->input('email_usuario');
            $user_id = auth()->user()->id;
            $rol =  auth()->user()->rol_id;

            $existeDocumento = \DB::table('users')->where([
                ['numeroDocumento', '=', $documento],
                ['id', '!=', $id_usuario],
            ])->get();

            $existeEmail = \DB::table('users')->where([
                ['email', '=', $email],
                ['id', '!=', $id_usuario],
            ])->get();

            if ($existeDocumento->isEmpty()) {

                if ($existeEmail->isEmpty()) {

                    $request->validated();

                    $data = array();
                    $data['tipoDocumento_id'] = $request->input('tipoDocumento_usuario');
                    $data['rol_id'] = $request->input('rol_usuario');
                    $data['numeroDocumento'] = $request->input('numeroDocumento_usuario');
                    $data['name'] = $request->input('name_usuario');
                    $data['email'] = $request->input('email_usuario');
                    $data['email_verified_at'] =  Carbon::now();
                    $data['updated_at'] = Carbon::now();
                    $insert = \DB::table('users')->where('id', '=', $id_usuario)->update($data);

                    Flash::success('Usuario actualizado correctamente');
                    return Response::json($data);
                }

                Flash::error('Ya existe un usuario registrado con este email');
                return Response::json();
            }

            Flash::error('Ya existe un usuario registrado con este número de documento');
            return Response::json();
        }
    }

    public function cambiarClave(GuardarClaveUsuarioRequest $request)
    {
        if (auth()->user()->hasRoles(['1'])) {

            $datos = $request->input();
            $user_id = auth()->user()->id;
            $rol =  auth()->user()->rol_id;
            $id_usuario = $request->input('usuario_id');

            $request->validated();

            $data = array();
            $data['password'] = Hash::make($request->input('cambio_password'));
            $data['updated_at'] = Carbon::now();
            $insert = \DB::table('users')->where('id', '=', $id_usuario)->update($data);

            Flash::success('Contraseña cambiada correctamente');
            return Response::json($data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Esta parte es para ver si el perfil está lleno

        $puntosDatosBasicos = \DB::table('puntos_tester')->where([
            ['user_id', '=', $id],
            ['motivo', '=', 'Agregar datos basicos'],
        ])->orWhere('motivo', '=', 'Eliminar datos basicos')
            ->sum('puntos');

        $puntosEstudioFormal = \DB::table('puntos_tester')->where([
            ['user_id', '=', $id],
            ['motivo', '=', 'Agregar estudios formales'],
        ])->orWhere('motivo', '=', 'Eliminar estudios formales')
            ->sum('puntos');

        $puntosEstudioNoFormal = \DB::table('puntos_tester')->where([
            ['user_id', '=', $id],
            ['motivo', '=', 'Agregar estudios no formales'],
        ])->orWhere('motivo', '=', 'Eliminar estudios no formales')
            ->sum('puntos');

        $puntosDatosLaborales = \DB::table('puntos_tester')->where([
            ['user_id', '=', $id],
            ['motivo', '=', 'Agregar datos laborales'],
        ])->orWhere('motivo', '=', 'Eliminar datos laborales')
            ->sum('puntos');

        $puntosInfoPruebas = \DB::table('puntos_tester')->where([
            ['user_id', '=', $id],
            ['motivo', '=', 'Agregar conocimiento en pruebas'],
        ])->orWhere('motivo', '=', 'Eliminar conocimiento en pruebas')
            ->sum('puntos');

        $puntosInfoDispositivos = \DB::table('puntos_tester')->where([
            ['user_id', '=', $id],
            ['motivo', '=', 'Agregar dispositivo disponible para probar'],
        ])->orWhere('motivo', '=', 'Eliminar dispositivo disponible para probar')
            ->sum('puntos');

        $puntosDatosFinancieros = \DB::table('puntos_tester')->where([
            ['user_id', '=', $id],
            ['motivo', '=', 'Agregar datos financieros'],
        ])->orWhere('motivo', '=', 'Eliminar datos financieros')
            ->sum('puntos');

        $puntosPerfil = $puntosDatosBasicos +  $puntosEstudioFormal + $puntosEstudioNoFormal + $puntosDatosLaborales +  $puntosInfoPruebas + $puntosInfoDispositivos + $puntosDatosFinancieros;

        $users = \DB::table('users')
            ->leftJoin('vista_datos_basicos', 'users.id', '=', 'vista_datos_basicos.user_id')
            ->where('users.id', '=', $id)
            ->get();

        $estudiosFormales =  VistaEstudiosFormales::query()->where('user_id', $id)->paginate(10);
        $estudiosNoFormales =  VistaEstudiosNoFormales::query()->where('user_id', $id)->paginate(10);
        $datosLaborales =  VistaDatosLaborales::query()->where('user_id', $id)->paginate(10);
        $datosFinancieros =  VistaDatosFinancieros::query()->where('user_id', $id)->paginate(10);
        $conocimientoPruebas =  VistaInfoPruebas::query()->where('user_id', $id)->paginate(10);
        $dispositivos =  VistaInfoDispositivos::query()->where('user_id', $id)->paginate(10);
        $anexos = Anexo::query()->where('user_id', $id)->paginate(10);

        $lugar_estudio = "";
        foreach ($estudiosNoFormales as $estudioNoFormal) {
            $lugar_estudio = $estudioNoFormal->lugar_estudio;
        }

        return view('usuarios.show', compact('lugar_estudio', 'puntosPerfil', 'users', 'estudiosFormales', 'conocimientoPruebas', 'dispositivos', 'estudiosNoFormales', 'datosLaborales', 'datosFinancieros', 'anexos'));
    }

    public function edit($id)
    {
        $where = array('id' => $id);

        $data  = User::where($where)->first();

        $data = $data;
        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasRoles(['1'])) {
            $usuarios = User::where("id", "=", $id)->get();
            foreach ($usuarios as $usuario) {
                $data = array();
                $data['estado_id'] = 4;
                $data['deleted_at'] = Carbon::now();
                \DB::table('users')->where("id", "=", $id)->update($data);

                Flash::success('Usuario desactivado correctamente');
                return redirect(route('usuarios.index'));
            }
        }

        Flash::error('No está autorizado a desactivar usuarios de la plataforma');
        return redirect(route('usuarios.index'));
    }

}
