<?php
 
namespace App\Http\Controllers;
 
use App\PuntosTester;
use Illuminate\Http\Request;
use App\Http\Requests\GuardarAnexoCobroRequest;
use Redirect,Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Mail\PagoTester;
use App\Mail\CambioAnexoCuenta;
use Illuminate\Support\Facades\Mail;
use Flash;

 
class AnexosCobrosController extends Controller
{

    public function __construct()
    {        
        $this->middleware([
            'auth','roles:1,2,3,4']);    
    }
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
public function index($id)
{  
    $id = $id;


        $valoresCuenta = \DB::table('cuentas_cobro')->where('id_cuentaCobro','=',$id)->get();
        foreach($valoresCuenta as $valorCuenta){
            $valor = $valorCuenta->monto_cuentaCobro;
        }

        $salariosMinimo = \DB::table('cuentas_cobro_variables')->where('id_variable_cuentaCobro','=',1)->get('valor_variable_cuentaCobro');
        foreach($salariosMinimo as $salario){
            $minimo = $salario->valor_variable_cuentaCobro;
        }

        $seguridadMinimos = \DB::table('cuentas_cobro_variables')->where('id_variable_cuentaCobro','=',2)->get('valor_variable_cuentaCobro');
        foreach($seguridadMinimos as $ss){
            $minimoSeguridad = $ss->valor_variable_cuentaCobro;
        }

        $seguridadPorcentajes = \DB::table('cuentas_cobro_variables')->where('id_variable_cuentaCobro','=',3)->get('valor_variable_cuentaCobro');
        foreach($seguridadPorcentajes as $ss){
            $minimoPorcentaje = $ss->valor_variable_cuentaCobro;
        }

        if($valor<$minimo){
            $mensaje = 'Para realizar el pago, es obligatorio que subas la cuenta de cobro firmada. No es necesario que cargues la seguridad social';
        }elseif($valor<$minimoSeguridad){
            $mensaje = 'Para realizar el pago, es obligatorio que subas la cuenta de cobro firmada y el soporte de pago de la seguridad social sobre un salario mínimo legal vigente';
        }else{
            $mensaje = 'Para realizar el pago, es obligatorio que subas la cuenta de cobro firmada y el soporte de pago de la seguridad social sobre el '.$minimoPorcentaje.' del monto de la cuenta de cobro. Además, tener en cuenta que ésta cuenta de cobro podría tener retefuente según las normas vigentes colombianas';
        }

    $anexosCobro =  \DB::table('vista_anexos_cobro')->where('cuentaCobro_id','=',$id)->get();

    if(request()->ajax()) {     
      
        return datatables()->of($anexosCobro)
        ->addColumn('action', 'anexos_cobros.action_button')
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);   

        return view('anexos_cobros.index', compact('id','mensaje'));

    }else{

        return view('anexos_cobros.index', compact('id', 'mensaje'));

    }      
   
    
}
 
 
/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */

public function store(GuardarAnexoCobroRequest $request)
{  

    if(auth()->user()->hasRoles(['2','4','1'])){

    $usuario =  auth()->user()->id;   
    $cuentaCobro_id = $request->input('id_cuentaCobro');
    $data = $request->validated();
    $files = $request->file('url_anexo_cuentaCobro');
    $tipoAnexo = $request->input('tipoAnexo_id');

    $cuentasCobro = \DB::table('cuentas_cobro')->where('id_cuentaCobro','=',$cuentaCobro_id)->get();  
    foreach($cuentasCobro as $cuentaCobro){
        $usuario_cuenta = $cuentaCobro->user_id;
        $estado_cuenta = $cuentaCobro->estado_id_cuentaCobro;
        $estado_cuenta = $estado_cuenta;

    }

    if(auth()->user()->hasRoles(['4']) && $tipoAnexo == 10){

        $existe = \DB::table('cuentas_cobro_anexos')
                        ->where([
                            ['tipoAnexo_id','=',10],
                            ['cuentaCobro_id','=',$cuentaCobro_id],
                        ])->get();

        if($existe->isEmpty()){

            foreach($files as $file){                
            $nombre      =   time().'.'.$file->getClientOriginalName();
            $target_path    =   storage_path('cuentas'.'/cuenta_'.$cuentaCobro_id);

            if($file->move($target_path, $nombre)) {
                $data = array();
                $data['cuentaCobro_id'] = $cuentaCobro_id;
                $data['usuario_registra'] = $usuario;
                $data['usuario_cuentaCobro'] = $usuario_cuenta;
                $data['tipoAnexo_id'] = $request->input('tipoAnexo_id');
                $data['url_anexo_cuentaCobro'] = $nombre;
                $data['nota_anexo_cuentaCobro'] = $request->input('nota_anexo_cuentaCobro');
                $data['created_at'] = Carbon::now();
                $data['updated_at'] = Carbon::now();
                \DB::table('cuentas_cobro_anexos')->insert($data);   

            }

            $testers = \DB::table('vista_cuentas_cobro')->where('id_cuentaCobro', $cuentaCobro_id)->get();
            foreach($testers as $tester){
                $user_id = $tester->user_id;
                $nombre = $tester->nombre_titular;
                $fecha = $tester->created_at;
            }

            $correos = \DB::table('users')->where('id', $user_id)->get('email');
            foreach($correos as $correo){
                $emailTester = $correo->email;
            }

            Mail::to($emailTester)
                ->queue(new PagoTester($user_id, $nombre, $fecha, $cuentaCobro_id));
            }

            $data2 = array();   
            $data2['cuentaCobro_id'] = $cuentaCobro_id;
            $data2['user_id'] = auth()->user()->id;
            $data2['cuentaCobro_cambio'] = 'Soporte de pago agregado';
            $data2['nota_cuentaCobro_cambio'] = 'Se ha agregado el soporte de pago de la cuenta de cobro';
            $data2['created_at'] = Carbon::now();
            \DB::table('cuentas_cobro_cambios')->insert($data2);  


            Flash::success('Anexo agregado correctamente');
            return Response::json($data);    
        }
            Flash::info('Ya ha agregado este tipo de anexo, puede eliminarlo y agregarlo nuevamente');

    }elseif(auth()->user()->hasRoles(['2']) && $tipoAnexo != 10){

        if($estado_cuenta ===3 ||$estado_cuenta ===6 ){

            Flash::error('No puede agregar anexos a una cuenta de cobro que ya ha sido aprobada por el contador o pagada');
                return Response::json(); 

        }
            if($tipoAnexo === "8"){
            $existe = \DB::table('cuentas_cobro_anexos')->where([
                ['tipoAnexo_id','=',8],
                ['cuentaCobro_id','=',$cuentaCobro_id],
            ])->get();
            }else{
                $existe = \DB::table('cuentas_cobro_anexos')->where([
                    ['tipoAnexo_id','=',9],
                    ['cuentaCobro_id','=',$cuentaCobro_id],
                ])->get();
            }     

            if($existe->isEmpty()){

                foreach($files as $file){
                
                    $nombre      =   time().'.'.$file->getClientOriginalName();
                    $target_path    =   storage_path('cuentas'.'/cuenta_'.$cuentaCobro_id);

                    if($file->move($target_path, $nombre)) {
                        $data = array();
                        $data['cuentaCobro_id'] = $cuentaCobro_id;
                        $data['usuario_registra'] = $usuario;
                        $data['usuario_cuentaCobro'] = $usuario_cuenta;
                        $data['tipoAnexo_id'] = $request->input('tipoAnexo_id');
                        $data['url_anexo_cuentaCobro'] = $nombre;
                        $data['nota_anexo_cuentaCobro'] = $request->input('nota_anexo_cuentaCobro');
                        $data['created_at'] = Carbon::now();
                        $data['updated_at'] = Carbon::now();
                        \DB::table('cuentas_cobro_anexos')->insert($data);   
                    }

                    if($tipoAnexo == 8){
                        $usuariosCuenta = \DB::table('cuentas_cobro')->where('id_cuentaCobro','=',$cuentaCobro_id)->get('user_id');

                        foreach($usuariosCuenta as $usuario){
                            $user_id = $usuario->user_id;
                        }

                        $data3 = array();   
                        $data3['user_id'] = $user_id;
                        $data3['puntos'] = 1;
                        $data3['motivo'] = 'Se anexa cuenta de cobro firmada requerida para el pago';
                        $data3['created_at'] = Carbon::now();
                        \DB::table('puntos_tester')->insert($data3);  

                        $puntosAcumulados = PuntosTester::where("user_id","=",$user_id)->sum('puntos');
                        $users = \DB::table('users')->where("id","=",$user_id)->get();
                        foreach($users as $user){
                            $data5 = array();
                            $data5['puntosAcumulados'] = $puntosAcumulados;
                            $data5['updated_at'] = Carbon::now();
                            \DB::table('users')->where("id","=",$user_id)->update($data5);
                        }
                    }

                    if($estado_cuenta ===2){

                        $cuentas =  \DB::table('cuentas_cobro_cambios')->where([
                                ['cuentaCobro_id','=',$cuentaCobro_id],
                                ['cuentaCobro_cambio','=','Rechazada por el contador']
                                ])->get('user_id');

                        foreach($cuentas as $cuenta){
                            $contador = $cuenta->user_id;                            
                        }

                        $datosContador = \DB::table('users')->where('id','=',$contador)->get('email');
                        foreach($datosContador as $datoContador){
                            $emailContador = $datoContador->email;
                        }

                        Mail::to($emailContador)
                            ->queue(new CambioAnexoCuenta($cuentaCobro_id));
                    }
                
                }  

                Flash::success('Anexo agregado correctamente');
                return Response::json($data);   

                
            }

            Flash::info('Ya ha agregado este tipo de anexo, puede eliminarlo y agregarlo nuevamente');
            

    }
    }

    return('No está autorizado para agregar anexos a la cuenta de cobro');

}
 
 
/**
 * Show the form for editing the specified resource.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */


public function show($id)
{   

    $rol_id = auth()->user()->rol_id;
    $anexos = \DB::table('cuentas_cobro_anexos')->where('id_anexo_cuentaCobro', '=',$id)->get();
        $id = auth()->user()->id;

        foreach($anexos as $anexo){
             $usuario = $anexo->usuario_cuentaCobro;
             $url = $anexo->url_anexo_cuentaCobro;    
             $cuentaCobro_id =   $anexo->cuentaCobro_id;
        }

        if($id == $usuario ||$rol_id ==3 ||$rol_id ==1 ||$rol_id ==4 ){

          
            $enlace = storage_path('cuentas'.'/cuenta_'.$cuentaCobro_id.'/');          
            return response()->download($enlace.$url);
  
       } else {
            return ('Sólo el propietario puede descargar el archivo');
        }
     
        
}


 
/**
 * Remove the specified resource from storage.
 *
 * @param  \App\Product  $product
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
{
    $user_id = auth()->user()->id;
    $rol_id = auth()->user()->rol_id;

    $anexos = \DB::table('cuentas_cobro_anexos')->where('id_anexo_cuentaCobro', '=',$id)->get();
    foreach($anexos as $anexo){
             $usuario = $anexo->usuario_registra;
             $url = $anexo->url_anexo_cuentaCobro;   
             $cuentaCobro_id = $anexo->cuentaCobro_id;    
             $tipoAnexo = $anexo->tipoAnexo_id;
    }

    $estadosCuentas = \DB::table('cuentas_cobro')->where('id_cuentaCobro', '=',$cuentaCobro_id)->get();
    foreach($estadosCuentas as $estado){
        $estadoCuenta = $estado->estado_id_cuentaCobro;
    }

        if (empty($anexos)) {
            return Response::json($anexos);
        }

        if($user_id == $usuario || $rol_id ==1  ){

            if($estadoCuenta == 1 || $estadoCuenta == 2 ||$estadoCuenta == 4 || $estadoCuenta == 5 || $estadoCuenta == 7 ){
        
                $anexos = \DB::table('cuentas_cobro_anexos')->where('id_anexo_cuentaCobro', '=',$id)->delete();
                $data = storage_path('cuentas'.'/cuenta_'.$cuentaCobro_id.'/'.$url);
                unlink($data);

                if($tipoAnexo == 8){
                $usuariosCuenta = \DB::table('cuentas_cobro')->where('id_cuentaCobro','=',$cuentaCobro_id)->get('user_id');

                foreach($usuariosCuenta as $usuario){
                    $user_id = $usuario->user_id;
                }

                $data3 = array();   
                $data3['user_id'] = $user_id;
                $data3['puntos'] = -1;
                $data3['motivo'] = 'Se elimina anexo de cuenta de cobro firmada requerida para el pago';
                $data3['created_at'] = Carbon::now();
                \DB::table('puntos_tester')->insert($data3);  

                $puntosAcumulados = PuntosTester::where("user_id","=",$user_id)->sum('puntos');
                $users = \DB::table('users')->where("id","=",$user_id)->get();
                foreach($users as $user){
                    $data5 = array();
                    $data5['puntosAcumulados'] = $puntosAcumulados;
                    $data5['updated_at'] = Carbon::now();
                    \DB::table('users')->where("id","=",$user_id)->update($data5);
                
                }
                }

                Flash::error('Anexo eliminado correctamente');
                return Response::json($anexos);
            }

            Flash::error('No puede eliminar los anexos cuando la cuenta de cobro ya ha sido aprobado por el contador o pagada');
            return Response::json();
            
        }       

        Flash::error('Sólo el propietario puede eliminar el archivo');
        return;       

       
    }

}
