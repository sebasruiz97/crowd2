<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Validator,Redirect,Response;
use App\info_dispositivos;
use Auth;
use Yajra\Datatables\Datatables;
use App\Pagina;
use app\User;
use app\DatosBasicos;
use App\listaconocimientopruebas;
use Flash;
use Carbon\Carbon;
use Response;


class formularioinfoDispositivosController extends Controller
{

    public function __construct()
    {
        $this->middleware([
            'auth', 'roles:1,2,3'
        ]);
    }


    // obtener registros
    public  function getDispositivos()
    {

        // obtener ciudades por pais  
        $listado_tipo_dispositivos['data'] = Pagina::getDispositivos();

        return json_encode($listado_tipo_dispositivos);
        exit;
    }



    /* Display todo list */

    public function index(Request $request)
    {
        // $user_id = $request->input('user_id');
        $user = auth()->user()->id;

        if (($user != '')) {
            // $tiposdispositivos = info_dispositivos::where('user_id',$user)->get();
            $tiposdispositivos =  \DB::table('vista_info_dispositivos')->where('user_id', $user)->get();
            $lista_dispositivos  =   \DB::table('indice_dispositivos')->where('user_id', $user)->get();
            // dd($tiposdispositivos); 
        } else {
            // $tiposdispositivos = info_dispositivos::select("*")->get();
            $tiposdispositivos =  \DB::table('vista_info_dispositivos')->get();
            $lista_dispositivos  =   \DB::table('indice_dispositivos')->get();
        }
        // dd($tiposdispositivos);
        $dispositivo_id = $tiposdispositivos->pluck('id_info_dispositivo')->first();
        // listado tipo tipo_dispositivos
        $lista_tipo_dispositivos  =      \DB::table('tipo_dispositivos')->orderBy('nombre_dispositivo', 'asc')->get();
        // $lista_tipo_dispositivos =  pagina::gettipoPruebasDisponiblesxUsuario($user);
        // dd($lista_tipo_dispositivos);
        //listado de tiempo_experiencia


        if (request()->ajax()) {

            return datatables()->of($tiposdispositivos)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    if (($data->id_info_dispositivo != '')) {

                        // $btn = '<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Edit" class="edit btn btn-primary btn-sm" onclick=editTodo("'.$data->id_info_dispositivo.'","'.$data->user_id.'","'.$data->dispositivo_id.'","'.$data->tiempoExperiencia_id.'","'.$data->ejecucion_id.'") >Editar</a>';
                        $btn = "";
                        $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-original-title="Delete" class="btn btn-danger lnr lnr-cross " onclick=deleteTodo("' . $data->id_info_dispositivo . '")></a>';

                        return $btn;
                    }
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('info_dispositivos.formularioinfodispositivos', compact('tiposdispositivos', 'lista_tipo_dispositivos', 'dispositivo_id', 'lista_dispositivos', 'user'));
    }



    /* insert and update todo into mysql database*/
    public function buscar($condiciones)
    {
        $resultados = \DB::table('info_dispositivos')->where($condiciones)->orderByRaw("user_id ASC, 
        dispositivo_id ASC, 
        navegador_id ASC,
        sistemaOperativo_id asc")->get();
        return $resultados;
    }

    public function store(Request $request)
    {

        $user_id = $request->input('user_id');
        $dispositivo_id = $request->input('dispositivo_id');
        $navegador_id = $request->input('navegador_id1');
        $sistemaOperativo_id = $request->input('sistemaOperativo_id1');
        $marca = $request->input('marca_id');
        $modelo = $request->input('modelo_id');

        $data = array(
            'user_id' => $user_id,
            "dispositivo_id" => $dispositivo_id,
            "navegador_id" => $navegador_id,
            "marca_id" => $marca,
            "modelo_id" => $modelo,
            "sistemaOperativo_id" => $sistemaOperativo_id,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        );

        //encontro espacio y acutaliza
        $check = \DB::table('info_dispositivos')->insert($data);

        $tiposdispositivos =  \DB::table('vista_info_dispositivos')->where('user_id', Auth()->user()->id)->get();

        if (count($tiposdispositivos) == 1) {

            //Con esto se agregan los puntos
            $dataPuntos = array(
                'user_id' => auth()->user()->id,
                'puntos' => 1,
                'motivo' => 'Agregar dispositivo disponible para probar',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );
            $puntos = \DB::table('puntos_tester')->insert($dataPuntos);

            //Esta parte, actualiza los puntos acumulados

            $puntosAcumulados = \DB::table('puntos_tester')->where("user_id", "=", auth()->user()->id)->sum('puntos');
            $users = \DB::table('users')->where("id", "=", auth()->user()->id)->get();
            foreach ($users as $user) {
                $data3 = array();
                $data3['puntosAcumulados'] = $puntosAcumulados;
                $data3['updated_at'] = Carbon::now();
                \DB::table('users')->where("id", "=", auth()->user()->id)->update($data3);
            }

            Flash::success('Acción ejecutada correctamente. Si añadiste todos los campos, tienes un punto adicional.');
            return Response::json($check);
        }

        Flash::success('Acción ejecutada correctamente.');
        return Response::json($check);
    }

    /* display edit todo form with data */


    /* delete todo from mysql database */

    public function delete($id)
    {

        $check = \DB::table('info_dispositivos')->where('id_info_dispositivo', '=', $id)->delete();
        $tiposdispositivos =  \DB::table('vista_info_dispositivos')->where('user_id', Auth()->user()->id)->get();

        if (count($tiposdispositivos) == 0) {
            //Con esto se quitan los puntos
            $dataPuntos = array(
                'user_id' => auth()->user()->id,
                'puntos' => -1,
                'motivo' => 'Eliminar dispositivo disponible para probar',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );

            $puntos = \DB::table('puntos_tester')->insert($dataPuntos);

            //Esta parte, actualiza los puntos acumulados

            $puntosAcumulados = \DB::table('puntos_tester')->where("user_id", "=", auth()->user()->id)->sum('puntos');
            // dd($puntosAcumulados);
            $users = \DB::table('users')->where("id", "=", auth()->user()->id)->get();
            foreach ($users as $user) {
                $data3 = array();
                $data3['puntosAcumulados'] = $puntosAcumulados;
                $data3['updated_at'] = Carbon::now();
                \DB::table('users')->where("id", "=", auth()->user()->id)->update($data3);
            }
        }

        Flash::error('Registro eliminado correctamente.');

        /*
        
        */

        // Flash::error('Registro eliminado correctamente. Si lo tenias completamente diligenciado, has perdido un punto por eliminarlo');
    }
}
