<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\crypto;

class RequestDecryptMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('data')){
            try {
                $crypto = new crypto();
                $dataDecryptString = $crypto->validDecrypt($request->get('data'));
                if( isset($dataDecryptString) ){
                    $data = json_decode($dataDecryptString);
                    foreach ($data as $key => $value) {
                        $request->merge([$key => $value]);
                    }
                } else {
                    return response()->json(['error' => 'Invalid data.']);
                }
            } catch (\Throwable $th) {
                return response()->json(['error' => $th->getMessage()]);
            }
        }
        return $next($request);
    }
}
