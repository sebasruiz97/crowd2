<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class info_dispositivos extends Model
{
    use SoftDeletes;

    public $table = 'info_dispositivos';
    protected $primaryKey='id_info_dispositivo';
    protected $guarded = [];
}
