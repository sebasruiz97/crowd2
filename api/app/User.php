<?php

namespace App;

use App\Http\Controllers\Api\Auth\ApiResetPassword;
use App\Http\Controllers\Api\Auth\ApiVerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\MyResetPassword;
use App\Notifications\MyVerifyEmail;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'rol_id','tipoDocumento_id','numeroDocumento','password','terminosCondiciones','estado_id', 'referido_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function findForPassport($identifier) {
    //     return User::orWhere('email', $identifier)->where('estado_id', 1)->first();
    // }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MyResetPassword($token));
    }

    public function sendPasswordApiResetNotification($token)
    {
        $this->notify(new ApiResetPassword($token));
    }

    // public function sendEmailVerificationNotification()
    // {
    //     $this->notify(new MyVerifyEmail);
    // }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new MyVerifyEmail);
    }

    public function sendEmailApiVerificationNotification()
    {
        $this->notify(new ApiVerifyEmail);
    }

    public function hasRoles(array $roles){

        $rol = auth()->user()->rol_id;
        $concat = "";
        $rol = $concat.$rol;

        foreach($roles as $role)
        {
            if($rol === $role)
            {
                return true;
            }
        }
        return false;        
    }
}
