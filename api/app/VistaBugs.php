<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VistaBugs extends Model
{
    use SoftDeletes;

    public $table = 'vista_bugs';

    public function scopeProyectos($query, $proyectos) {
    	if ($proyectos) {
    		return $query->where('proyecto_id','=',$proyectos);
    	}
    }


}
