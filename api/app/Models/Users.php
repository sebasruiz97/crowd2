<?php

namespace App\Models;

use App\Services\CuentaCobro;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Model
{
    use SoftDeletes;

    public $table = 'users';

    public function infoPruebas(){
        return $this->hasMany(InfoPruebas::class, 'user_id');
    }

    public function herramientas(){
        return $this->hasMany(herramientaUser::class, 'user_id');
    }

    public function navegadores(){
        return $this->hasMany(navegadoresUser::class, 'user_id');
    }

    public function alertas(){
        return $this->hasMany(AlertaUsuario::class, 'id_usuario');
    }

    public function cuentascobros(){
        return $this->hasMany(CuentaCobro::class,'id');
    }
}
