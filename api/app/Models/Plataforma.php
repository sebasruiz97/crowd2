<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Plataforma extends Model
{


    protected $primaryKey='id_plataforma';
    public $table = 'plataformas_educativas';
}
