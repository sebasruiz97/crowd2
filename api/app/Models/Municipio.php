<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Municipio extends Model
{
    protected $primaryKey='id_municipio';
    public $table = 'municipios';
}
