<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alerta extends Model
{
    use SoftDeletes;
    protected $primaryKey='id_alerta';
    public $table = 'alertas';
    protected $fillable = ['titulo', 'mensaje','enlace','created_at','update_at'];

    public function alertasUsuario(){
        return $this->hasMany(AlertaUsuario::class, 'id_alerta');
    }

}
