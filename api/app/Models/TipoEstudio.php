<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoEstudio extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_tipoEstudio';
    public $table = 'tipo_estudios';
}
