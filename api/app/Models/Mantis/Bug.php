<?php

namespace App\Models\Mantis;

use Illuminate\Database\Eloquent\Model;

class Bug extends Model
{
    protected $connection = 'mysql_mantis';

    public $table = 'mantis_bug';

    public function users(){
        return $this->belongsTo(UserMantis::class,'reporter_id','user_id','reporter_id');
    }

    public function bug_text(){
        return $this->belongsTo(BugText::class,'bug_text_id','id');
    }

    public function project(){
        return $this->belongsTo(Project::class);
    }

}
