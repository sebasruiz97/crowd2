<?php

namespace App\Models\Mantis;

use Illuminate\Database\Eloquent\Model;

class UserProject extends Model
{
    protected $connection = 'mysql_mantis';

    public $table = 'mantis_project_user_list';

    public function users(){
        return $this->belongsTo(UserMantis::class);
    }

    public function project(){
        return $this->belongsTo(Project::class);
    }

}
