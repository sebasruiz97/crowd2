<?php

namespace App\Models\Mantis;

use Illuminate\Database\Eloquent\Model;

class UserMantis extends Model
{
    protected $connection = 'mysql_mantis';

    public $table = 'mantis_user';

    public function projectsAssign(){
        return $this->hasMany(UserProject::class, 'user_id');
    }

}
