<?php

namespace App\Models\Mantis;

use Illuminate\Database\Eloquent\Model;

class BugText extends Model
{
    protected $connection = 'mysql_mantis';

    public $table = 'mantis_bug_text';
    
}
