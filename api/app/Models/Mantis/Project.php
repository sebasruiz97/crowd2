<?php

namespace App\Models\Mantis;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $connection = 'mysql_mantis';

    public $table = 'mantis_project';

    public function userProject(){
        return  $this->belongsTo(UserProject::class, 'project_id');
    }

    public function bugs(){
        return $this->hasMany(Bug::class)->orderBy('id','desc');
    }

}
