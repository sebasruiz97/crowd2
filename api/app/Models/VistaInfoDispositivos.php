<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VistaInfoDispositivos extends Model
{
    public $table = 'vista_info_dispositivos';
}
