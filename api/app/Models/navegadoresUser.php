<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class navegadoresUser extends Model
{
    //
    use SoftDeletes;
    protected $primaryKey='id_navegador_user';

    public $table = 'navegadores_user';

    protected $fillable = ['navegador_id', 'info_prueba_id','user_id'];

    protected $guarded = ['user_id'];

    public function infoPruebas(){
        return  $this->belongsTo(infoPruebas::class, 'info_prueba_id');
    }

    public function tipoNavegador(){
        return  $this->belongsTo(TipoNavegador::class, 'navegador_id');
    }

    public function users(){
        return  $this->belongsTo(Users::class, 'user_id');
    }
}
