<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ocupacion extends Model
{
    use SoftDeletes;
    
    protected $primaryKey='id_ocupacion';
    public $table = 'ocupaciones';
}
