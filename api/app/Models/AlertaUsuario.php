<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlertaUsuario extends Model
{
    protected $primaryKey='id_alertaUsuario';
    public $table = 'alertas_usuario';
    protected $fillable = ['id_alerta', 'id_usuario','estado','created_at','update_at'];

    public function alerta(){
        return $this->belongsTo(Alerta::class, 'id_alerta');
    }

    public function users(){
        return  $this->belongsTo(Users::class, 'id_usuario');
    }
    
}
