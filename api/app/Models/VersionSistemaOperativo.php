<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VersionSistemaOperativo extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_versionSistemaOperativo';
    public $table = 'version_sistemas_operativos';
}
