<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Secciones extends Model
{
    use SoftDeletes;
    protected $primaryKey='id_seccion';
    public $table = 'secciones';
    protected $fillable = ['nombre_seccion', 'obligatory','created_at','update_at'];
}
