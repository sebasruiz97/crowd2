<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class infoPruebas extends Model
{
    use SoftDeletes;
    
    protected $primaryKey = 'id_info_prueba';

    public $table = 'info_pruebas';

    protected $fillable = ['prueba_id', 'tiempoExperiencia_id','ejecucion_id','herramienta_id','user_id', 'dispositivo_id', 'sistemaOperativo_id', 'navegador_id'];

    protected $guarded = ['user_id'];

    public function users(){
        return  $this->belongsTo(Users::class, 'user_id');
    }

    public function herramientas(){
        return $this->hasMany(HerramientaUser::class, 'info_prueba_id');
    }

    public function navegadores(){
        return $this->hasMany(navegadoresUser::class, 'info_prueba_id');
    }

}
