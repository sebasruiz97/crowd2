<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\DatoFinanciero;

class TipoBanco extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_banco';
    public $table = 'tipo_bancos';
}
