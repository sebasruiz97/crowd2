<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Universidad extends Model
{
    protected $primaryKey='id_universidad';
    public $table = 'universidades';
}
