<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoSistemaOperativo extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_sistemaOperativo';
    public $table = 'tipo_sistemas_operativos';
}
