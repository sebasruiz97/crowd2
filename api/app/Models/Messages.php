<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Messages extends Model
{

    protected $primaryKey='id';
    public $table = 'messages';

    protected $fillable = ['to_id', 'from_id','body'];

    protected $guarded = ['id'];
    
    public function users_to(){
        return  $this->belongsTo(Users::class, 'to_id');
    }

    public function users_from(){
        return  $this->belongsTo(Users::class, 'from_id');
    }

}
