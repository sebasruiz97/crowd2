<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VistaEstudiosFormales extends Model
{
    public $table = 'vista_estudios_formales';
}
