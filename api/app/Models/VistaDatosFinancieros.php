<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VistaDatosFinancieros extends Model
{
    public $table = 'vista_datos_financieros';
}
