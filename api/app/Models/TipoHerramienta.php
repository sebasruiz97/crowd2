<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoHerramienta extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_herramienta';
    public $table = 'tipo_herramientas';

    public function infoPrueba(){
        return $this->hasMany(HerramientaUser::class);
    }

    public function herramientaUser(){
        return $this->hasMany(HerramientaUser::class, 'herramienta_id');
    }

}
