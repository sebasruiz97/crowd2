<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NivelEducacion extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_nivelEducacion';
    public $table = 'niveles_educacion';
}
