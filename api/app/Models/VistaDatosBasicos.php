<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VistaDatosBasicos extends Model
{
    public $table = 'vista_datos_basicos';
}
