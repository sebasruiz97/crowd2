<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreaEstudio extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_areaEstudio';
    public $table = 'area_estudios';
}
