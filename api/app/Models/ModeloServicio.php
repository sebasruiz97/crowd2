<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModeloServicio extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_modeloServicio';
    public $table = 'modelos_servicios';

}
