<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoMetodologia extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_metodologia';
    public $table = 'tipo_metodologias';
}
