<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoDispositivo extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_dispositivo';
    public $table = 'tipo_dispositivos';
}
