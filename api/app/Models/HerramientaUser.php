<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HerramientaUser extends Model
{
    //
    use SoftDeletes;
    protected $primaryKey='id_herramienta_user';

    public $table = 'herramientas_user';

    protected $fillable = ['herramienta_id', 'info_prueba_id','user_id'];

    protected $guarded = ['user_id'];

    public function info_pruebas()
    {
        return $this->hasMany(infoPruebas::class);
    }

    public function tipoHerramienta(){
        return $this->belongsTo(TipoHerramienta::class, 'herramienta_id');
    }

    public function users(){
        return  $this->belongsTo(Users::class, 'user_id');
    }

}
