<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoEjecucion extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_ejecucion';
    public $table = 'tipo_ejecucion';
}
