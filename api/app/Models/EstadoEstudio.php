<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoEstudio extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_estadoEstudio';
    public $table = 'estado_estudios';
}
