<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoNavegador extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_navegador';
    public $table = 'tipo_navegadores';

    public function infoPrueba(){
        return $this->hasMany(navegadoresUser::class);
    }

    public function navegadoresUser(){
        return $this->hasMany(navegadoresUser::class, 'navegador_id');
    }

}
