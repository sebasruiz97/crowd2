<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoPruebas extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_prueba';
    public $table = 'tipo_pruebas';
}
