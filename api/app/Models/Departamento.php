<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Departamento extends Model
{


    protected $primaryKey='id_departamento';
    public $table = 'departamentos';
}
