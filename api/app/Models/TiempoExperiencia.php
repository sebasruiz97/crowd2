<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TiempoExperiencia extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_tiempoExperiencia';
    public $table = 'tiempo_experiencias';
}
