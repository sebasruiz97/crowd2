<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VersionNavegador extends Model
{
    use SoftDeletes;

    protected $primaryKey='id_versionNavegador';
    public $table = 'version_navegadores';
}
