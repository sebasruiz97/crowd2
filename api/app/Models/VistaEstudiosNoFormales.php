<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VistaEstudiosNoFormales extends Model
{
    public $table = 'vista_estudios_no_formales';
}
