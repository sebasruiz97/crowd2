<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bug extends Model
{
    use SoftDeletes;

    public $table = 'vista_bugs';
    protected $primaryKey='id_bug';
    protected $guarded = [];
}
