<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class info_pruebas extends Model
{
    use SoftDeletes;

    public $table = 'info_pruebas';
    protected $primaryKey='id_info_prueba';
    protected $guarded = [];
}
