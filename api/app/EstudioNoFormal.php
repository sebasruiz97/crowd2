<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstudioNoFormal extends Model
{
    use SoftDeletes;

    public $table = 'estudios_no_formales';
    protected $primaryKey='id_estudioNoFormal';
    protected $guarded = [];
}
