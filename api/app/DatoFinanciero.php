<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\TipoBanco;

class DatoFinanciero extends Model
{
    use SoftDeletes;

    public $table = 'datos_financieros';
    protected $primaryKey='id_datoFinanciero';
    protected $guarded = [];
}
