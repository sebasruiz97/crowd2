class ChatSqa {

    constructor(id, name, from, open = true) {
        this.id = id;
        this.name = name;
        this.from = from;
        this.status = open;
    }

    // Metodo para añadir chat flotante
    addChat() {
        const { id, name, from } = this;
        let chatAdd = [{ id, name, from }]
            /**/
    }

}
if (localStorage.getItem('chats')) {
    let chats = JSON.parse(localStorage.getItem('chats'));
    chats.forEach(c => { addChat(c.id, c.name, c.from, false); });
}
$(document).on('click', '.sendChat', sendMsg);
$(document).on('keypress', '.chat_input', function(e) {
    if (e.which == 13) {
        sendMsg(e);
    }
});
$(document).on('click', '.panel-heading', function(e) {
    var $this = $(this).find('span.icon_minim');
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
    scrollChat($(this).siblings().find('#bodyMsg'));
});
$(document).on('click', '.icon_close', function(e) {
    //$(this).parent().parent().parent().parent().remove();
    let id = $(this).parents('#bodyChat').data('ci');
    try {
        let chats = JSON.parse(localStorage.getItem('chats'));
        chats.splice(chats.findIndex(c => c.id == id), 1);
        localStorage.setItem('chats', JSON.stringify(chats));
    } catch (error) {
        localStorage.removeItem('chats');
    }
    $(this).parents('.panel').parent().remove();
});

$(document).on('click', '.chatPointer', function(e) {
    const obj = $(e.target).hasClass('chatPointer') ? $(e.target) : $(e.target).parents('.chatPointer');
    const {
        id,
        name
    } = obj.data();
    const chat = $('#chat_window').find(`[data-ci="${id}"]`);
    const from = $('meta[name="usid"]').attr('content');
    if (!chat[0]) {
        addChat(id, name, from);
        new chat(id, name, from);
    } else {
        if (!chat.find('.glyphicon-minus')[0]) {
            chat.find('.panel-heading').click();
        }
    }
});

function scrollChat(bodyChat) {
    bodyChat.scrollTop(bodyChat[0].scrollHeight);
}

function addChat(id, name, from, open = true) {
    if (localStorage.getItem('chats')) {
        try {
            chatAdd = JSON.parse(localStorage.getItem('chats'));
            if (!chatAdd.find(e => e.id == id)) {
                chatAdd.push({ id, name, from });
            }
        } catch (error) {
            localStorage.removeItem('chats');
            chatAdd = [{ id, name, from }];
        }
    }
    localStorage.setItem('chats', JSON.stringify(chatAdd));
    $.post('/getMsg', { id }, function(data) {
        if (data.mensajes && data.mensajes.length > 0) {
            let chat = $('#bodyChat').clone()
                .attr({
                    'data-ci': id,
                    'data-cf': $('meta[name="usid"]').attr('content')
                })
                .find('#chat_window').attr('data-id', ('chat_window_' + id))
                .parents('#bodyChat')
                .find('.panel-title').append(name)
                .parents('#bodyChat')
                .show()
                .insertBefore($('#chat_window').children());
            data.mensajes.forEach((m, i) => {
                let group = true;
                let msgGroup = false;
                let e = 1;
                // Buscar mensaje padre (Agrupacion por fecha y hora)
                while (group) {
                    const mAnt = data.mensajes[i - e];
                    if (mAnt && mAnt.user_from_id == m.user_from_id) {
                        const dateAnt = mAnt.created_at.slice(0, 10); // Fecha mensaje Anterior
                        const hourAnt = mAnt.created_at.slice(11, 16); // Hora mensaje anterior
                        const date = m.created_at.slice(0, 10); // Fecha mensaje actual
                        const hour = m.created_at.slice(11, 16); // Fecha hora actual
                        if (dateAnt == date && hourAnt == hour) {
                            msgGroup = mAnt.id_mensaje;
                        } else {
                            group = false;
                        }
                    } else {
                        group = false;
                    }
                    e++;
                }
                if (msgGroup) {
                    // Push grupo
                    pushMsg(m, msgGroup);
                } else {
                    // Construir nuevo
                    addMsg(m, id);
                }
            });
            let bodyChat = $('[data-ci="' + id + '"] #bodyMsg');
            bodyChat.scrollTop(bodyChat.height());
            if (open) {
                chat.find('.panel-heading').click();
            }
        }
    });
}

function pushMsg(m, id) {
    const chatMsgTime = $('[data-msgid="' + id + '"] .messages time');
    $('<p>' + m.body + '</p>').insertBefore(chatMsgTime);
}

function addMsg(m, id) {
    let msg = '';
    let status = m.user_from_id == $('meta[name="usid"]').attr('content') ? 'sent' : 'receive';
    msg += '<div data-msgid="' + m.id_mensaje + '" class="row msg_container base_' + status + '" style="margin: 0">';
    let divImage = '<div class="col-md-2 col-xs-2 avatar">';
    divImage += '<img src="/storage/avatars/' + ((status == 'receive' ? m.from_avatar : m.to_avatar) || 'foto_base.png') + '" class="img-responsive">';
    divImage += '</div>';
    if (status == 'receive') {
        msg += divImage;
    }
    msg += '<div class="col-md-10 col-xs-10">';
    msg += '<div class="messages msg_' + status + '">';
    msg += '<p>' + m.body + '</p>';
    msg += '<time datetime="' + m.created_at + '">' + m.created_at.slice(0, 10).replace(/-/g, '/') + ' • ' + m.created_at.slice(11, 16) + '</time>';
    msg += '</div>';
    msg += '</div>';
    if (status == 'sent') {
        msg += divImage;
    }
    msg += '</div>';
    $('#bodyChat[data-ci="' + id + '"]').find('#bodyMsg').append(msg);
}

function sendMsg(e) {
    const group = $(e.target).parents('#bodyChat');
    const body = group.find('input').val();
    const id = group.data('ci');
    const from = group.data('cf');
    if (body.trim() == '') {
        return false;
    }
    $.ajax({
        type: "post",
        url: "/sendMsg",
        data: { body, id },
        success: function(data) { console.log('success:', data); },
        error: function(data) { console.log('Error:', data); }
    });
    group.find('input').val('').focus();
    let d = new Date();
    const data = { id: 'n', user_to_id: id, body, user_from_id: from, created_at: d.toISOString() };
    pushNew(data);
}

function pushNew(m, type = 'sent') {
    const bodyChat = $('#bodyChat[data-ci="' + (type == 'sent' ? m.user_to_id : m.user_from_id) + '"]');
    const lastMsg = bodyChat.find('.msg_container').last().find('.messages');
    if (lastMsg.hasClass('msg_' + type)) {
        const time = lastMsg.find('time').attr('datetime').slice(0, 16);
        if (time == m.created_at.slice(0, 16)) {
            pushMsg(m, lastMsg.parents('.msg_container').data('msgid'));
            scrollChat(bodyChat.find('#bodyMsg'));
            return false;
        }
    }
    addMsg(m, (type == 'sent' ? m.user_to_id : m.user_from_id));
    scrollChat(bodyChat.find('#bodyMsg'));
}