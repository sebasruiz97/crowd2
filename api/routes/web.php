<?php

use Illuminate\Support\Facades\Storage;
use App\Mail\ReporteBugTester;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Log Viewer de produccion
Route::get('verLogCrowd','\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

//listado inicial con resumen de dispositivos
Route::get('info_dispositivos','infoDispositivosController@index')->name('info_dispositivos')->middleware('verified');
Route::get('sinDispositivos','infoDispositivosController@sinDispositivos')->name('sinDispositivos')->middleware('verified');
//Route::resource('infoPruebas', 'infoPruebasController')->middleware('verified');

//listado inicial con resumen de conocimiento en pruebas
Route::get('info_pruebas','infoPruebasController@index')->name('info_pruebas')->middleware('verified');
Route::get('sinPruebas','infoPruebasController@sinPruebas')->name('sinPruebas')->middleware('verified');

//crud de bugs
Route::resource('bugs','BugsController')->middleware('verified');
Route::post('bug/store','BugsController@store')->middleware('verified');
Route::get('bug/delete/{id?}','BugsController@destroy')->middleware('verified');
Route::get('bug/show/{id?}','BugsController@show')->middleware('verified');
Route::get('/buscarVersionSistemaOperativo','BugsController@buscarVersionSistemaOperativo')->middleware('verified');
Route::get('/buscarNavegador','BugsController@buscarNavegador')->middleware('verified');

//crud de Anexos Bugs
Route::resource('anexosBugs','AnexosBugsController')->middleware('verified');
Route::post('anexoBugstore','AnexosBugsController@store')->middleware('verified');
Route::get('anexoBug/{id?}','AnexosBugsController@index')->middleware('verified');
Route::get('anexoBug/delete/{id?}','AnexosBugsController@destroy')->middleware('verified');
Route::get('anexoBug/show/{id?}', 'AnexosBugsController@visualizar');

//crud de cuentas de cobro
Route::resource('cobros','CobrosController')->middleware('verified');
Route::get('cobro/show/{id?}','CobrosController@show')->middleware('verified');
Route::post('cobro/generar','CobrosController@generar')->middleware('verified');
Route::post('cobro/store','CobrosController@store')->middleware('verified');
Route::get('cobro/delete/{id?}','CobrosController@destroy')->middleware('verified');
//Para exportar como pdf
Route::get('imprimir/{id}','CobrosController@imprimir')->middleware('verified')->name('imprimir');
Route::post('usuarioCobro','CobrosController@usuarioCobro')->middleware('verified')->name('usuarioCobro');

Route::get('cobro/enviar','CobrosController@enviar')->middleware('verified')->name('cobros.enviar');
Route::post('cobro/enviar','CobrosController@enviarpdf')->middleware('verified')->name('cobros.enviarpdf');


//crud de Anexos Cuentas de cobro
Route::resource('anexosCobros','AnexosCobrosController')->middleware('verified');
Route::post('anexoCobrostore','AnexosCobrosController@store')->middleware('verified');
Route::get('anexoCobro/{id?}','AnexosCobrosController@index')->middleware('verified');
Route::get('anexoCobro/delete/{id?}','AnexosCobrosController@destroy')->middleware('verified');
Route::get('anexoCobro/show/{id?}', 'AnexosCobrosController@visualizar')->name('descargar');

//crud de dispositivos
Route::get('formulariosinfoDispositivos','formularioinfoDispositivosController@index')->name('formulariosinfoDispositivos')->middleware('verified');
Route::post('formulariosinfoDispositivosstore','formularioinfoDispositivosController@store')->middleware('verified');
Route::get('formulariosinfoDispositivosdelete/{id?}','formularioinfoDispositivosController@delete')->middleware('verified');

//crud de conocimiento en pruebas
Route::get('formulariosinfoPruebas','formularioinfoPruebasController@index')->name('formulariosinfoPruebas')->middleware('verified');
Route::post('formulariosinfoPruebasstore','formularioinfoPruebasController@store')->middleware('verified');
Route::get('formulariosinfoPruebasdelete/{id?}','formularioinfoPruebasController@delete')->middleware('verified');

//funciones para obtener listado de maestros: 
Route::get('/getherramientasxTipo/{id}', 'rutinasInfoPruebasController@getherramientasxTipo')->middleware('verified');
Route::get('/getDispositivosxTipo/{id}', 'rutinasInfoPruebasController@getDispositivosxTipo')->middleware('verified');
Route::get('/getDispositivos', 'rutinasInfoPruebasController@getDispositivos')->middleware('verified');
Route::get('/getMarcas', 'rutinasInfoPruebasController@getMarcas')->middleware('verified');
Route::get('/getModelos', 'rutinasInfoPruebasController@getModelos')->middleware('verified');
Route::get('/getSoxDispxTipo/{id}', 'rutinasInfoPruebasController@getSoxDispxTipo')->middleware('verified');
Route::get('/getNavDispxTipo/{id}', 'rutinasInfoPruebasController@getNavDispxTipo')->middleware('verified');

Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

//Rutas del módulo de Testers
Route::resource('datosBasicos', 'DatosBasicosController')->middleware('verified');
Route::resource('estudiosFormales', 'EstudioFormalController')->middleware('verified');
Route::resource('estudiosNoFormales', 'EstudioNoFormalController')->middleware('verified');
Route::resource('datosLaborales', 'DatoLaboralController')->middleware('verified');
Route::resource('datosFinancieros', 'DatosFinancierosController')->middleware('verified');
Route::resource('anexos','AnexoController')->middleware('auth');
Route::post('anexos/{anexo}','AnexoController@approve')->middleware('auth')->name('anexos.approve');
Route::get('datos','AnexoController@datos')->middleware('verified')->name('datos');
Route::get('terminosCondiciones','AnexoController@terminosCondiciones')->middleware('verified')->name('terminosCondiciones');

//crud de usuarios
Route::resource('usuarios', 'UsuarioController')->middleware('verified');
Route::post('usuario/store','UsuarioController@store')->middleware('verified');
Route::post('usuario/update','UsuarioController@update')->middleware('verified');
Route::post('usuario/cambiarClave','UsuarioController@cambiarclave')->middleware('verified');
// Route::get('usuario/delete/{id?}','UsuarioController@destroy')->middleware('verified');
// Route::get('usuario/show/{id?}','UsuarioController@show')->middleware('verified');

//Rutas de Admin
Route::resource('clientes', 'ClienteController')->middleware('verified');
Route::post('anexoCliente/delete/{id?}','ClienteController@destroyAnexo')->middleware('auth')->name('borrarAnexoCliente');
Route::get('anexoCliente/show/{id?}', 'ClienteController@visualizar')->name('verAnexoCliente');

//Para buscar selects dinámicos
Route::get('/buscarDepartamento','DatosBasicosController@buscarDepartamento')->middleware('verified');
Route::get('/buscarMunicipio','DatosBasicosController@buscarMunicipio')->middleware('verified');
Route::get('/buscarUniversidad','EstudioFormalController@buscarUniversidad')->middleware('verified');

//Para asignar y quitar puntos puntos
Route::get('/asignarPuntos/{id}','PuntosController@asignarPuntos')->name('asignarPuntos')->middleware('verified');
Route::get('/quitarPuntos/{id}','PuntosController@quitarPuntos')->name('quitarPuntos')->middleware('verified');
Route::get('/puntosAcumulados','PuntosController@puntosAcumulados')->name('puntosAcumulados')->middleware('verified');

//Ruta para dashboard NO salen en primera versión
Route::get('inicioTester','InicioTesterController@index')->name('inicioTester')->middleware('verified');
Route::get('inicioLider','InicioLiderController@index')->name('inicioLider')->middleware('verified');
// Route::get('inicioAdmin','InicioAdminController@index')->name('inicioAdmin');
Route::get('puntosTester/{id}','PuntosController@mostrarPuntos')->name('puntosTester')->middleware('verified');

//Rutas para desactivar usuario
Route::get('/desactivar/{id}','DatosBasicosController@desactivar')->name('desactivar')->middleware('verified');

//Ruta para cambiar contraseña
Route::get('/editarContrasena/{id}','DatosBasicosController@editarContrasena')->name('editarContrasena')->middleware('verified');;
Route::put('/actualizarContrasena/{id}','DatosBasicosController@actualizarContrasena')->name('actualizarContrasena')->middleware('verified');;

//Ruta para anexos de tester

/*Route::get('/', function () {
    return view('home');
});*/
Route::get('/', 'HomeController@homeView');

//Ruta para devolver
Route::get('regresar', 'HomeController@return')->name('regresar');

//Rutas de paginas de inicio
Route::view('/about','about')->name('about');
Route::view('/contact','inicio_crowd.contacto')->name('contacto');
Route::post('contact', 'MessageController@store')->name('mensaje');

//crud de Anexos Proyectos
Route::resource('anexosProyectos','AnexosProyectosController')->middleware('verified');
Route::post('anexoProyectostore','AnexosProyectosController@store')->middleware('verified');
Route::get('anexoProyecto/{id?}','AnexosProyectosController@index')->middleware('verified');
Route::get('anexoProyecto/delete/{id?}','AnexosProyectosController@destroy')->middleware('verified');


//Rutas para el modulo de proyectos

Route::resource('misproyectos', 'MisproyectosController')->middleware('verified');
//correos inicio y fin
Route::get('mail/inicio/{id?}', 'InvitacionController@inicio')->middleware('verified');
Route::get('mail/fin/{id?}', 'InvitacionController@fin')->middleware('verified');

Route::resource('proyectos', 'ProyectosController')->middleware('verified');
Route::post('proyecto/store', 'ProyectosController@store')->middleware('verified');
Route::get('proyecto/delete/{id?}', 'ProyectosController@destroy')->middleware('verified');

Route::Resource('invitacion','InvitacionController')->middleware('verified');

Route::resource('invitaciones', 'InvitacionesController')->middleware('verified');
Route::get('invitaciones/edit/{id?}', 'InvitacionesController@edit')->middleware('verified');
Route::get('invitaciones/delete/{id?}', 'InvitacionesController@destroy')->middleware('verified');

Route::resource('proyectos_responsables', 'proyectos_responsablesController')->middleware('verified');
Route::post('proyectos_responsables/store', 'proyectos_responsablesController@store')->middleware('verified');
Route::get('proyectos_responsables/delete/{id?}', 'proyectos_responsablesController@destroy')->middleware('verified');
Route::get('proyectos_responsables/ver/{id?}', 'proyectos_responsablesController@ver')->middleware('verified');
Route::get('proyectos_responsables/edit/{id?}', 'proyectos_responsablesController@edit')->middleware('verified');

Route::get('proyectos_tester', 'proyectos_testerController@index')->middleware('verified');
Route::post('proyectos_tester/store', 'proyectos_testerController@store')->middleware('verified');
Route::get('proyectos_tester/delete/{id?}', 'proyectos_testerController@destroy')->middleware('verified');
Route::get('proyectos_tester/asignar/{id?}', 'proyectos_testerController@asignar')->middleware('verified');

//consultar si existen bugs en estado 1 o 6 para un tester o proyecto
Route::get('bugsxtester/{id?}','ValidarBugsController@tester')->middleware('verified');
Route::get('bugsxproyecto/{id?}','ValidarBugsController@proyecto')->middleware('verified');

Route::resource('alertas', 'AlertasController')->middleware('verified');
Route::post('alertas/asignarUsuarios', 'AlertasController@asignarUsuarios')->middleware('verified')->name('alertas.usuarios');
Route::post('alertas/enviar', 'AlertasController@enviarAlerta')->middleware('verified')->name('alertas.enviar');