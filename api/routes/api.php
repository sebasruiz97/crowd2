<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\Auth\VerificationController;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\DatosBasicosController;
use App\Http\Controllers\Api\Mantis\ProjectController;
use App\Http\Controllers\Api\TipoDocumentosController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\validationsController;
use App\Http\Controllers\Api\EstudioFormalController;
use App\Http\Controllers\Api\EstudioNoFormalController;
use App\Http\Controllers\Api\InfoPruebasController;
use App\Http\Controllers\Api\InfoDispositivos;
use App\Http\Controllers\Api\DatosFormulariosController;
use App\Http\Controllers\Api\DatosFinancierosController;
use App\Http\Controllers\Api\ContactoController;
use App\Http\Controllers\Api\AnexosController;
use App\Http\Controllers\Api\Auth\ResetController;
use App\Http\Controllers\Api\Mantis\BugsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('crypto')->group(function(){

    Route::post('/tipoDocumentos', [TipoDocumentosController::class, 'index'])->name('tipoDocumentos');

    Route::post('/register', [RegisterController::class, 'register'])->name('register');
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/sendContacto', [ContactoController::class, 'store'])->name('contacto');

    Route::post('/forgotPassword', [ResetController::class, 'forgot'])->name('forgotPassword');
    Route::post('/resetPassword', [ResetController::class, 'reset'])->name('resetPassword');

    Route::prefix('user')->group(function () {
        Route::post('/find', [UserController::class, 'find'])->name('findUser');
    });
    
    Route::middleware('auth:api')->group(function () {
        Route::get('/userinfo', [AuthController::class, 'getUserInfo'])->name('userinfo');
        Route::post('/verifyEmail/resend', [VerificationController::class, 'resend'])->name('resendEmail');
        Route::post('/verifyEmail/{id}/{hash}', [VerificationController::class, 'verify'])->name('verifyEmail');
        Route::post('/homeData', [HomeController::class, 'HomeData'])->name('homeData');
        Route::post('/uploadProfileImg', [DatosBasicosController::class, 'savePicture'])->name('savePicture');

        // datos
        Route::post('/registro/datosBasicos', [DatosBasicosController::class, 'saveData'])->name('registroDatosBasicos');
        Route::post('/datosForm/basicos', [DatosFormulariosController::class, 'datosBasicos'])->name('datosBasicosForm');
        Route::post('/datosBasicos', [DatosBasicosController::class, 'getData'])->name('dataDatosBasicos');

        Route::delete('/registro/estudioFormal/{id}', [EstudioFormalController::class, 'delete'])->name('deleteEstudioFormal');
        Route::post('/registro/estudioFormal', [EstudioFormalController::class, 'crud'])->name('registroEstudioFormal');
        Route::post('/datosForm/estudioFormal', [DatosFormulariosController::class, 'estudioFormal'])->name('estudioFormalForm');

        Route::delete('/registro/estudioNoFormal/{id}', [EstudioNoFormalController::class, 'delete'])->name('deleteEstudioNoFormal');
        Route::post('/registro/estudioNoFormal', [EstudioNoFormalController::class, 'crud'])->name('registroNoEstudioFormal');
        Route::post('/datosForm/estudioNoFormal', [DatosFormulariosController::class, 'estudioNoFormal'])->name('estudioNoFormalForm');

        Route::post('/infoPruebas', [InfoPruebasController::class, 'index'])->name('getConocimientoPruebas');
        Route::delete('/registro/infoPruebas/{id}', [InfoPruebasController::class, 'delete'])->name('deleteConocimientoPruebas');
        Route::post('/registro/infoPruebas', [InfoPruebasController::class, 'crud'])->name('registroConocimientoPruebas');
        Route::post('/datosForm/infoPruebas', [DatosFormulariosController::class, 'conocimientoPruebas'])->name('conocimientoPruebas');

        Route::post('/infoDispositivos', [InfoDispositivos::class, 'index'])->name('getInfoDispositivos');
        Route::delete('/registro/infoDispositivos/{id}', [InfoDispositivos::class, 'delete'])->name('deleteInfoDispositivos');
        Route::post('/registro/infoDispositivos', [InfoDispositivos::class, 'crud'])->name('registroInfoDispositivos');
        Route::post('/datosForm/infoDispositivos', [DatosFormulariosController::class, 'infoDispositivos'])->name('infoDispositivos');
    
        Route::post('/datosFinancieros', [DatosFinancierosController::class, 'index'])->name('getDatosFinancieros');
        Route::delete('/registro/datosFinancieros/{id}', [DatosFinancierosController::class, 'delete'])->name('deleteDatosFinancieros');
        Route::post('/registro/datosFinancieros', [DatosFinancierosController::class, 'crud'])->name('registroDatosFinancieros');
        Route::post('/datosForm/datosFinancieros', [DatosFormulariosController::class, 'datosFinancieros'])->name('datosFinancieros');

        Route::post('/anexos', [AnexosController::class, 'index'])->name('anexos');
        Route::delete('/registro/anexos/{id}', [AnexosController::class, 'delete'])->name('deleteAnexos');
        Route::post('/registro/anexos', [AnexosController::class, 'insert'])->name('insertAnexos');

        Route::post('/validReto', [validationsController::class, 'validReto'])->name('validReto');
        Route::post('/reportBug', [BugsController::class, 'reportBug'])->name('reportBug');
    
        Route::post('/project', [ProjectController::class, 'data'])->name('reto');
        
        Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
    });
});



