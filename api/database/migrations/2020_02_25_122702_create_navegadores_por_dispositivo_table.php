<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavegadoresPorDispositivoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('navegadores_por_dispositivo', function (Blueprint $table) {
            $table->bigIncrements('id_navegadorPorDispositivo');
            $table->bigInteger('navegador_id')->unsigned();
            $table->bigInteger('dispositivo_id')->unsigned();
            $table->foreign('navegador_id')->references('id_navegador')->on('tipo_navegadores');
            $table->foreign('dispositivo_id')->references('id_dispositivo')->on('tipo_dispositivos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('navegadores_por_dispositivo');
    }
}
