<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePruebasPorUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pruebas_por_usuario', function (Blueprint $table) {
            $table->bigIncrements('id_pruebaUsuario');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('prueba_id')->unsigned();
            $table->bigInteger('tiempoExperiencia_id')->unsigned();
            $table->bigInteger('ejecucion_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('prueba_id')->references('id_prueba')->on('tipo_pruebas');
            $table->foreign('tiempoExperiencia_id')->references('id_tiempoExperiencia')->on('tiempo_experiencias');
            $table->foreign('ejecucion_id')->references('id_ejecucion')->on('tipo_ejecucion');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pruebas_por_usuario');
    }
}
