<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id_cliente');
            $table->string('nombre_cliente');
            $table->bigInteger('sector_id')->unsigned();
            $table->string('nit_cliente')->unique();
            $table->bigInteger('modeloServicio_id')->unsigned();
            $table->string('descripcion');
            $table->string('tarifa_cliente');
            $table->bigInteger('pais_id')->unsigned();
            $table->bigInteger('ciudad_id')->unsigned();
            $table->string('direccion_cliente');
            $table->string('nombre_contactoPruebas');
            $table->string('telefono_contactoPruebas');
            $table->string('correo_contactoPruebas');
            $table->string('nombre_contactoFacturacion');
            $table->string('telefono_contactoFacturacion');
            $table->string('correo_contactoFacturacion');
            $table->string('plazo_pagoFactura');
            $table->string('fecha_recibidoFactura');
            $table->string('correo_envioFactura');
            $table->string('acuerdos_adicionales')->nullable();
            $table->foreign('sector_id')->references('id_sector')->on('sectores');
            $table->foreign('modeloServicio_id')->references('id_modeloServicio')->on('modelos_servicios');
            $table->foreign('pais_id')->references('id_pais')->on('paises');
            $table->foreign('ciudad_id')->references('id_ciudad')->on('ciudades');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
