<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavegPorDispPorPruebasPorUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('naveg_por_disp_por_pruebas_por_usuario', function (Blueprint $table) {
            $table->bigIncrements('id_naveDispPrueUsua');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('prueba_id')->unsigned();
            $table->bigInteger('dispositivo_id')->unsigned();
            $table->bigInteger('navegador_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('prueba_id')->references('id_prueba')->on('tipo_pruebas');
            $table->foreign('navegador_id')->references('id_navegador')->on('tipo_navegadores');
            $table->foreign('dispositivo_id')->references('id_dispositivo')->on('tipo_dispositivos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('naveg_por_disp_por_pruebas_por_usuario');
    }
}
