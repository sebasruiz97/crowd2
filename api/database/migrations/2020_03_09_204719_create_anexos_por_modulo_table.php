<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnexosPorModuloTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anexos_por_modulo', function (Blueprint $table) {
            $table->bigIncrements('id_anexoPorModulo');
            $table->bigInteger('tipoAnexo_id')->unsigned();
            $table->bigInteger('modulo_id')->unsigned();
            $table->foreign('tipoAnexo_id')->references('id_tipoAnexo')->on('tipos_anexos');
            $table->foreign('modulo_id')->references('id_modulo')->on('modulos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anexos_por_modulo');
    }
}
