<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoPorDispPorPruebasPorUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('so_por_disp_por_pruebas_por_usuario', function (Blueprint $table) {
            $table->bigIncrements('id_soDispPrueUsua');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('prueba_id')->unsigned();
            $table->bigInteger('dispositivo_id')->unsigned();
            $table->bigInteger('sistemaOperativo_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('prueba_id')->references('id_prueba')->on('tipo_pruebas');
            $table->foreign('sistemaOperativo_id')->references('id_sistemaOperativo')->on('tipo_sistemas_operativos');
            $table->foreign('dispositivo_id')->references('id_dispositivo')->on('tipo_dispositivos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('so_por_disp_por_pruebas_por_usuario');
    }
}
