<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatosFinancierosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_financieros', function (Blueprint $table) {
            $table->bigIncrements('id_datoFinanciero');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('banco_id')->unsigned();
            $table->bigInteger('cuentaBancaria_id')->unsigned();
            $table->string('numeroCuenta');
            $table->string('nombreTitular');
            $table->string('sucursal');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('banco_id')->references('id_banco')->on('tipo_bancos');
            $table->foreign('cuentaBancaria_id')->references('id_cuentaBancaria')->on('tipo_cuentas_bancarias');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_financieros');
    }
}
