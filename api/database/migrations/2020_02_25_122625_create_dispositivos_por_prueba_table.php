<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDispositivosPorPruebaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispositivos_por_prueba', function (Blueprint $table) {
            $table->bigIncrements('id_dispositivoPorPrueba');
            $table->bigInteger('dispositivo_id')->unsigned();
            $table->bigInteger('prueba_id')->unsigned();
            $table->foreign('dispositivo_id')->references('id_dispositivo')->on('tipo_dispositivos');
            $table->foreign('prueba_id')->references('id_prueba')->on('tipo_pruebas');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispositivos_por_prueba');
    }
}
