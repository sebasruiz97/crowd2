<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatosLaboralesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_laborales', function (Blueprint $table) {
            $table->bigIncrements('id_datoLaboral');
            $table->bigInteger('user_id')->unsigned();
            $table->string('empresa');
            $table->bigInteger('sector_id')->unsigned();
            $table->bigInteger('cargo_id')->unsigned();
            $table->date('fechaInicio_datoLaboral');
            $table->date('fechaFin_datoLaboral');
            $table->string('responsabilidades');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('sector_id')->references('id_sector')->on('sectores');
            $table->foreign('cargo_id')->references('id_cargo')->on('cargos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_laborales');
    }
}
