<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavegadoresDispositivosPorUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('navegadores_dispositivos_por_usuario', function (Blueprint $table) {
            $table->bigIncrements('id_navegadorDispositivoUsuario');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('dispositivo_id')->unsigned();
            $table->bigInteger('navegador_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('dispositivo_id')->references('id_dispositivo')->on('tipo_dispositivos');
            $table->foreign('navegador_id')->references('id_navegador')->on('tipo_navegadores');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('navegadores_dispositivos_por_usuario');
    }
}
