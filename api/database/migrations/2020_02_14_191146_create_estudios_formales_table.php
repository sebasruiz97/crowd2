<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudiosFormalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudios_formales', function (Blueprint $table) {
            $table->bigIncrements('id_estudioFormal');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('nivelEducacion_id')->unsigned();
            $table->bigInteger('areaEstudio_id')->unsigned();
            $table->bigInteger('estadoEstudio_id')->unsigned();
            $table->date('fechaInicio_estudioFormal');
            $table->date('fechaFin_estudioFormal');
            $table->string('universidad');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('nivelEducacion_id')->references('id_nivelEducacion')->on('niveles_educacion');
            $table->foreign('areaEstudio_id')->references('id_areaEstudio')->on('area_estudios');
            $table->foreign('estadoEstudio_id')->references('id_estadoEstudio')->on('estado_estudios');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudios_formales');
    }
}
