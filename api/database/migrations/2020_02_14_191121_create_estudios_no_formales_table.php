<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudiosNoFormalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudios_no_formales', function (Blueprint $table) {
            $table->bigIncrements('id_estudioNoFormal');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('tipoEstudio_id')->unsigned();
            $table->string('nombre_estudioNoFormal');
            $table->date('fechaFin_estudioNoFormal');
            $table->string('duracion');
            $table->string('lugar');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('tipoEstudio_id')->references('id_tipoEstudio')->on('tipo_estudios');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudios_no_formales');
    }
}
