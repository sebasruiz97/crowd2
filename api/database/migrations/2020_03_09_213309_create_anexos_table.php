<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnexosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anexos', function (Blueprint $table) {
            $table->bigIncrements('id_anexo');
            $table->bigInteger('tipoAnexo_id')->unsigned();
            $table->bigInteger('modulo_id')->unsigned();
            $table->string('url');
            $table->string('comentarios')->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->bigInteger('cliente_id')->unsigned()->nullable();
            $table->bigInteger('proyecto_id')->unsigned()->nullable();
            $table->foreign('tipoAnexo_id')->references('id_tipoAnexo')->on('tipos_anexos');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('modulo_id')->references('id_modulo')->on('modulos');
            $table->foreign('cliente_id')->references('id_cliente')->on('clientes');
            $table->foreign('proyecto_id')->references('id_proyecto')->on('proyectos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anexos');
    }
}
