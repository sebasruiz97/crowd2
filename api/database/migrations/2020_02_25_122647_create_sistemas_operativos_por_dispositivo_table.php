<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSistemasOperativosPorDispositivoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistemas_operativos_por_dispositivo', function (Blueprint $table) {
            $table->bigIncrements('id_sistemaOperativoPorPrueba');
            $table->bigInteger('sistemaOperativo_id')->unsigned();
            $table->bigInteger('dispositivo_id')->unsigned();
            $table->foreign('sistemaOperativo_id')->references('id_sistemaOperativo')->on('tipo_sistemas_operativos');
            $table->foreign('dispositivo_id')->references('id_dispositivo')->on('tipo_dispositivos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sistemas_operativos_por_dispositivo');
    }
}
