<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHerramientasPorPruebasPorUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('herramientas_por_pruebas_por_usuario', function (Blueprint $table) {
            $table->bigIncrements('id_herrPrueUsua');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('prueba_id')->unsigned();
            $table->bigInteger('herramienta_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('prueba_id')->references('id_prueba')->on('tipo_pruebas');
            $table->foreign('herramienta_id')->references('id_herramienta')->on('tipo_herramientas');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('herramientas_por_pruebas_por_usuario');
    }
}
