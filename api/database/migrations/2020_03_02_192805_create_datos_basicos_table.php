<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatosBasicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_basicos', function (Blueprint $table) {
            $table->bigIncrements('id_datoBasico');
            $table->bigInteger('user_id')->unsigned();
            $table->string('experienciaPruebas');
            $table->string('genero');
            $table->bigInteger('pais_id')->unsigned();
            $table->bigInteger('ciudad_id')->unsigned();
            $table->string('direcion');
            $table->string('telefonoFijo')->nullable();
            $table->string('telefonoCelular')->nullable();
            $table->bigInteger('ocupacion_id')->unsigned();
            $table->date('fechaNacimiento');
            $table->string('edad')->nullable();
            $table->string('puntosAcumulados')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('pais_id')->references('id_pais')->on('paises');
            $table->foreign('ciudad_id')->references('id_ciudad')->on('ciudades');
            $table->foreign('ocupacion_id')->references('id_ocupacion')->on('ocupaciones');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_basicos');
    }
}
