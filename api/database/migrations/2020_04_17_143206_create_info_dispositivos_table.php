<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfoDispositivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_dispositivos', function (Blueprint $table) {
            $table->bigIncrements('id_info_dispositivo');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('dispositivo_id')->unsigned();
            $table->bigInteger('navegador_id')->unsigned();
            $table->bigInteger('sistemaOperativo_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('navegador_id')->references('id_navegador')->on('tipo_navegadores');
            $table->foreign('dispositivo_id')->references('id_dispositivo')->on('tipo_dispositivos');
            $table->foreign('sistemaOperativo_id')->references('id_sistemaOperativo')->on('tipo_sistemas_operativos');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_dispositivos');
    }
}
