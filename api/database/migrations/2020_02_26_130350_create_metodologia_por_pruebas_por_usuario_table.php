<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetodologiaPorPruebasPorUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metodologia_por_pruebas_por_usuario', function (Blueprint $table) {
            $table->bigIncrements('id_metoPrueUsua');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('prueba_id')->unsigned();
            $table->bigInteger('metodologia_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('prueba_id')->references('id_prueba')->on('tipo_pruebas');
            $table->foreign('metodologia_id')->references('id_metodologia')->on('tipo_metodologias');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metodologia_por_pruebas_por_usuario');
    }
}
