<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tipoDocumento_id')->unsigned();
            $table->bigInteger('rol_id')->unsigned();
            $table->string('numeroDocumento')->unique();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('terminosCondiciones');
            $table->bigInteger('estado_id')->unsigned();
            $table->string('avatar')->default("foto_base.png");
            $table->string('puntosAcumulados')->nullable();
            $table->foreign('estado_id')->references('id_estado')->on('estados');
            $table->foreign('tipoDocumento_id')->references('id_tipoDocumento')->on('tipo_documentos');
            $table->foreign('rol_id')->references('id_rol')->on('roles');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
