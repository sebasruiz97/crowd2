<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDispositivosPorUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispositivos_por_usuario', function (Blueprint $table) {
            $table->bigIncrements('id_dispositivoUsuario');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('dispositivo_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('dispositivo_id')->references('id_dispositivo')->on('tipo_dispositivos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispositivos_por_usuario');
    }
}
