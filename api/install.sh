# instala dependencias (vendor)
composer update
# instala modulos (laravel mix)
npm i
# compilar laravel mix
npm run dev

# Limpiar cache
php artisan cache:clear
php artisan config:clear
php artisan view:clear
php artisan config:cache

composer dump-autoload